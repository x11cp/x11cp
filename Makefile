xabacus:
	(cd src/xabacus && xmkmf -a && make)

xabacus-clean:
	(cd src/xabacus && make clean)

xcolors:
	(cd src/xcolors && xmkmf -a && make)

xcolors-clean:
	(cd src/xcolors && make clean)

xdtm:
	(cd src/xdtm && xmkmf -a && make)

xdtm-clean:
	(cd src/xdtm && make clean)

xinvest:
	(cd src/xinvest && xmkmf -a && make)

xinvest-clean:
	(cd src/xinvest && make clean)

xmabacus:
	(cd src/xabacus && ./configure && make)

xmabacus-clean:
	(cd src/xabacus && make clean)

xmcolor:
	(cd src/xmcolor && xmkmf -a && make)

xmcolor-clean:
	(cd src/xmcolor && make clean)

xmoontool:
	(cd src/xmoontool && make)

xmoontool-clean:
	(cd src/xmoontool && make clean)

xgas:
	(cd src/xgas && xmkmf -a && make)

xgas-clean:
	(cd src/xgas && make clean)

xsol:
	(cd src/xsol && make)

xsol-clean:
	(cd src/xsol && rm ./xsol 2>/dev/null)

xtartan:
	(cd src/xtartan && xmkmf -a && make)

xtartan-clean:
	(cd src/xtartan && make clean)

xzoom:
	(cd src/xzoom && xmkmf -a && make)

xzoom-clean:
	(cd src/xzoom && make clean)

mgdiff:
	(cd src/mgdiff && xmkmf -a && make)

mgdiff-clean:
	(cd src/mgdiff && make clean)

xsclock:
	(cd src/xsclock && xmkmf -a && make)

xsclock-clean:
	(cd src/xsclock && make clean)

xdotclock:
	(cd src/xdotclock && ./configure && make)

xdotclock-clean:
	(cd src/xdotclock && make clean)

xarclock:
	(cd src/xarclock && xmkmf -a && make)

xarclock-clean:
	(cd src/xarclock && make clean)

bclock:
	(cd src/bclock && xmkmf -a && make)

bclock-clean:
	(cd src/bclock && make clean)

plotmtv:
	(cd src/plotmtv && xmkmf -a && make)

plotmtv-clean:
	(cd src/plotmtv && make clean)

vgp:
	(cd src/vgp && ./configure && make)

vgp-clean:
	(cd src/vgp && make clean)

xmtoolbar:
	(cd src/xmtoolbar && make -f makefile.simple)

xmtoolbar-clean:
	(cd src/xmtoolbar && make -f makefile.simple clean)

xwpick:
	(cd src/xwpick && xmkmf -a && make)

xwpick-clean:
	(cd src/xwpick && make clean)

icao:
	(cd src/icao && xmkmf -a && make)

icao-clean:
	(cd src/icao && make clean)

xrolodex:
	(cd src/xrolodex && xmkmf -a && make)

xrolodex-clean:
	(cd src/xrolodex && make clean)

xtt:
	(cd src/xtt && xmkmf -a && make)

xtt-clean:
	(cd src/xtt && make clean)

xmeltdown:
	(cd src/xmeltdown && make)

xmeltdown-clean:
	(cd src/xmeltdown && make clean)
