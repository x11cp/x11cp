.TH XTARTAN 1 "11 February 1991" "X Version 11"
.\"Version 2.3

.SH NAME
xtartan \- draw various tartans as window backgrounds

.SH SYNOPSIS
.B xtartan
[-draw] [-list] [-print] [-version]
[-r] [-s scale] [-t tartanname] [-w linewidth]
[-np namepattern] [-sp setpattern] [-dark] [-modern]
[-fade] [-fv fadevalue] [-min] [-minimum]

.SH DESCRIPTION
.I Xtartan
generates a pixmap containing a Scottish tartan and sets it as the background
of a new window (or the root window if -r is used).
Tartan sett information and color information are specified using X11
resources, allowing you to easily add your own tartan if it is not already
included, or to modify the pattern or individual colors of existing tartans.
.LP
The standard applications default file for Xtartan includes 
descriptions for over 240 setts.

.SH OPTIONS
.TP
.B -draw
Draw the tartan as a window background (the default action).
.TP
.B -list
Print out a list of the names of all of the available tartans.
.TP
.B -print
Print out the sett information for the specified tartan.
.TP
.B -version
Print out the version number of the xtartan program.
.TP
.B -r
Display on the root window instead of creating a new window.
.TP
.B -s
Scale up the width of the stripes in the tartan by the specified integer
(default 2).
.TP
.B -w
Scale up with width of the lines drawn by the specified integer.
.TP
.B -t
Use the specified tartan.
If not specified, the MacBeth tartan is used.
.TP
.B -np
Use the specified name pattern to filter tartans for -list and when cycling
through the tartans.
Only tartans which contain the specified string as a substring of the name
will be used.
.TP
.B -sp
Use the specified sett pattern to filter tartans for -list and when cycling
through the tartans.
Only tartans which contain the specified string as a substring of the sett
will be used.
.TP
.B -dark -modern
Using either of these will display the tartan in its darker 
modern colors as opposed to the traditional ancient colors.
.TP
.B -fade -fv
Using -fade will fade the colormap to create a tartan that is
usable as a background for WWW pages.  The fadeValue (-fv)
resource controls how bright.  Algorithm details are later in
this document.
.TP
.B -min -minimum
Either of these switches forces the displayed window (in "draw"
mode) to be the exact size of the tartan after taking scaling
into account.  Details are below, in the showMinimum resource.
.TP
.B "Xt switches"
In addition to the above switches, any of the standard top-level switches
for the Xt toolkit can be specified (such as -display).

.SH RESOURCES
.LP
In addition to standard Xt toolkit resources, xtartan looks for the resources
described below. All resources are application resources (xtartan.<resource>).
.TP
.B action
Type String, class Action.
Specifies the action to take; one of "draw", "print", "list", or "version".
Setting the action to one of these is equivalent to specifying the command
line option of the same name.
.TP
.B colorCode.<codename>
Type String, class ColorCode.
Specifies the color name translation for the indicated color code.
Color codes are used in the sett descriptions.
For example, if the sett used "BK4" and the resource "*.colorCode.BK: Black"
was specified, the BK4 would be drawn as a black stripe of width four.
By specifying a resource such as "XTartan.colorCode.BK: Grey" in your
personal resources (or with the -xrm command line option),
you can override the translation of the BK color code and
make it come out as Grey instead of Black.
.TP
.B firstTartan
Type String, class firstTartan (sic - no capitalization).
Specifies the name of the first tartan in the list of tartans.
All tartan sett information is specified in resources.
See the descriptions of the sett and nextTartan resources.
.TP
.B lineWidth
Type Int, class LineWidth.
Specifies the line width factor.
Equivalent to the "-w" command line option.
.TP
.B <tartanname>.nextTartan
Type String, class nextTartan (sic - no capitalization).
Specifies the name of the next tartan in the list of tartans.
The chain of tartans starts with the tartan specified with the firstTartan
resource and goes through each nextTartan resource.
When Xtartan starts up, it reads the chain into memory so that it can go
forwards or backwards in the list.
You can add your own tartan into the middle of the chain by overriding the
nextTartan resource of the tartan preceding your tartan (to refer to your
tartan) and by specifying the nextTartan resource on your new tartan to refer
to the tartan formerly referred to by the preceding tartan.
For example, if you wanted to add the tartan Foobar between Fletcher and
Forbes, you could do this by specifying the following resources in your
personal resources:
.sp
.nf
.nj
XTartan.Fletcher.nextTartan: Foobar
XTartan.Foobar.sett: B4 R4 (a simple sett)
XTartan.Foobar.nextTartan: Forbes
.fi
.ju
.sp
You should run xtartan with the -list option after adding a tartan to make
sure you have not broken the chain.
.TP
.B scale
Type Int, class Scale.
Specifies the scale factor.
Equivalent to the "-s" command line option.
.TP
.B <tartanname>.sett
Type String, class sett (sic - no capitalization).
Specifies the sett information for the tartan.
Each color stripe is represented by a color code immediately followed by
a thread count, e.g. BK4. The color code is translated by the colorCode.<code>
resource.
The sett string can contain comments in parentheses (one level only), and some
special codes, which are preceded by a percent character (%).
The special codes modify the interpretation of the sett string as follows:
.sp
.nf
.nj
%a - the sett is asymmetric (no pivots)
%b - all thread counts are even; they are divided
     by two when read in
%p - the last two colors are a double pivot
%v - vertical pattern (which follows) is different from
     horizontal pattern (which precedes)
.fi
.ju
.sp
The sett pattern is drawn from left to right and from bottom to top.
.TP
.B tartan
Type String, class Tartan.
Specifies the name of the tartan to use.
Equivalent to the "-t" command line option.
.TP
.B useRoot
Type Boolean, class UseRoot.
If true, set the background of the root window instead of
opening a new window.
Equivalent to the "-r" command line option.
.TP
.B dark
Type Boolean, class Dark.
If true, use the darker, modern colors instead of the
brighter "ancient" colors.  This can be toggled interactively.
.TP
.B fade
Type Boolean, class Fade
If true, fade the image to create a backdrop tartan.
This can be toggled interactively.
.TP
.B fadeValue
Type int, class FadeValue
Controls the brightness of the faded backdrop.  Defaults
to 2.  The algorithm scales all color values to use the top 
"fv" fraction of the available range; e.g., if the fadeValue 
is 5, then the top 1/5th of the color range is used.  Out of
a 65534 color range, the minimum value is 49149 instead of 0.
.TP
.B showMinimum
Type Boolean, class ShowMinimum
If true, the window will always be the minimum size to hold
the current tartan, taking scaling into account.  This means
the window dimensions will change when the scale is changed
or when the tartan itself is changed.  Default is False,
meaning the window will stay at its current size until
adjusted by either turning showMinimum on (see below), or
resizing using window-manager handles.

.SH INTERACTIVE OPTIONS
.LP
While xtartan is running, you can change tartans, scale, linewidth 
and dark mode by
entering keyboard command characters (except when -r is used).
The actions defined in xtartan are listed below, followed by the
default translation bindings.
.TP
.B "exit()"
Causes xtartan to exit.
.TP
.B "help()"
Prints out a list of what the standard translation binding do.
.TP
.B "info()"
Prints out the current tartan name, scale, and line width.
.TP
.B "lineWidth(new)"
Changes the line width multiplier.
The argument is a numeric string optionally preceded by a "+" or
"-" character.
If there is no sign character, then the new number is used directly as
the new line width.
If there is a leading sign, then the new number is either added or
subtracted from the current value, and the result is used as the new
line width.
.TP
.B "name()"
Prints out the name of the current tartan.
.TP
.B "scale(new)"
Changes the scale multiplier.
The argument is a numeric string and is interpreted in the same
was as for the lineWidth action.
.TP
.B "setTartan(name)"
Switches the current tartan to the specified tartan.
If the name is "+", the next tartan in the list is selected.
If the name is "-", the previous tartan in the list is selected.
If the name is "0", the first tartan in the list is selected.
For these specials, the name and sett patterns are used to filter out
tartans which do not match those patterns.
.TP
.B "sett()"
Prints out the name and sett of the current tartan, and translations
for the color codes used in that sett.
.TP
.B "dark()"
Toggles between dark and bright colors (modern vs. ancient).
.TP
.B "xpm()"
Dumps the current tartan to an xpm file in the current directory.
The file will be named after the tartan's name, with "_dark",
"_fade", and ".xpm" added appropriately.
.TP
.B "gif()"
Dumps the current tartan to a gif file in the current directory.
The file will be named after the tartan's name, with "_dark",
"_fade", and ".gif" added appropriately.
.TP
.B "fade()"
Toggles between normal and faded colors.
.TP
.B "minimum()"
Toggles between displaying minimum window and constant window.
.TP
.B "root()"
Will install the current tartan to the root window and exit.  As the program
exits, it will print the command line to regenerate the tartan it just
installed.

.LP
The standard translation bindings (in the default app-defaults file) are:
.sp
.DS
Ctrl<Key>C : exit()\\n\\
   :<Key>? : help()\\n\\
   :<Key>h : help()\\n\\
   :<Key>f : setTartan(0)\\n\\
   :<Key>n : setTartan(+)\\n\\
   :<Key>N : name()\\n\\
   :<Key>p : setTartan(-)\\n\\
   :<Key>P : sett()\\n\\
   :<Key>q : exit()\\n\\
   :<Key>t : info()\\n\\
   :<Key>* : scale(+1)\\n\\
   :<Key>/ : scale(-1)\\n\\
   :<Key>+ : lineWidth(+1)\\n\\
   :<Key>- : lineWidth(-1)\\n\\
   :<Key>= : scale() lineWidth()\\n\\
   :<Key>d : dark()\\n\\
   :<Key>x : xpm()\\n\\
   :<Key>g : gif()\\n\\
   :<Key>a : fade()\\n\\
   :<Key>r : root()\\n\\
   :<Key>m : minimum()
.DE

.SH BUGS AND WARNINGS
.LP
When using line widths greater than one, there are often alignment problems
when abutting the tartan tiles, and you can see a line delimiting the
edge of the background pixmap where the tartan lines do not match properly.
.sp
Strange patterns can be caused by using line widths which are large
compared to the width of the color stripes in the tartan.  The algorithm
has been changed and you can now only go up to a scale factor of 11.
.sp
Not all of the "tartans" are actual tartans; some are random patterns to
test out the various drawing capabilities of the program.  The tartan
labeled "Mine" is not really a tartan, but is a plaid pattern in some cotton
flanel I use for a great kilt.
.sp
Not all tartans look good/accurate in dark mode.  The dark mode algorithm
was optimized to keep reds, yellows, and whites relatively bright while
darkening the blues and greens.  This works very well for clans like MacLeod
and Sutherland.  Odd colors like maroon and pink (Melville), or tartans
with heavy blue-red blends (MacGilllivray) may become darker, richer, or
more yellowish than they should be.

.SH SOURCES
.LP
The sett descriptions came from a number of different sources.
The source for each sett in the app-defaults file is indicated in a comment.
The following sources were used:
.RS
.LP
"The Tartan Weaver's Guide" by James D. Scarlett, first published 1985
by Shepheard-Walwyn (Publishers) Ltd., 26 Charing Cross Road,
London WC2H 0DH.
This book contains color pictures of 142 tartans, and thread counts for
228 tartans, plus a bit of history about each tartan (but not about the
people associated with them).
It contains the notice, "Copyright (c) Illustrations Shepheard-Walwyn
(Publishers) Ltd. 1977."
I presume this means that the illustrations in this book are copyrighted,
but the sett information is not.
The setts listed in this source have been taken from a number of other
sources.
The source for each sett is well documented, making this a good starting
point if you are interested in tracing the history of a particular tartan.
Setts from this source are indicated with (W) in the app-defaults file.
.LP
"The Official Tartan map of tartans approved by clan chiefs,
the standing council of Scottish chiefs, or the Lord Lyon King of Arms"
by Dunbar and Pottinger, published by Elm Tree Books in 1976.
(Entered by J. Dean Brock (brock@cs.unc.edu))
Setts from this source are indicated with (D) in the app-defaults file.
.LP
"The Clans and Tartans of Scotland" by Robert Bain, published by Collins,
London and Glasgow,
in 1950 (first published 1938). I find no copyright notice in "The Clans..."
nor any text restricting distribution of information nor reserving rights
in any manner.
This book does not contains thread counts; thread colors and counts for these
setts have been estimated from the photographs, so there is a good chance
that they do not exactly match any official sett.
Each tartan faces a one page description of the history of the people
associated with the tartan.
Setts from this source are indicated with (C) in the app-defaults file.
.RE
.LP
Because of the difficulty of establishing the authenticity of tartans, there
are a great many tartans for which more than one sett is listed.
These are listed with various suffixes which generally indicated what
authority was used for that version of the tartan (e.g. _VS for tartans
which came from the Vestiarium Scoticum).
.LP
In many cases, photographic plates show a sett which is clearly not the same
as the thread counts given.
Usually the difference is in the size (thread count) for particular stripes,
or in the color of a stripe (e.g. white vs. yellow).
When the difference was more than this, I have included additional setts
to reflect the photograph as well as the printed sett.
.LP
Please remember that the sett information here has been manually transcribed
from printed material, and transcription errors may have occurred.
In some cases the sources have contained errors, some in the printed thread
counts, some weaving errors in the photographic examples; there
may well be additional errors that I am not aware of.
I have done my best to ensure the correctness of the information provided,
but there are bound to be some errors remaining.
.LP
If you are serious about getting the correct definition for a sett, please
do a little research and find some authoritative references.
The list above is a good start, or you can go to your local library and
look up "tartans" in their subject index.

.SH COPYRIGHT
.LP
This software is not copyrighted.
.LP
Xtartan and the sett definitions that are included with it
have not been sanctioned by any Scottish authority;
no guarantee is placed on the accuracy of the tartans it produces.

.SH ORIGINAL AUTHOR
Jim McBeath
.br
Globetrotter Software, Saratoga, California
.br
jimmc@hisoft.uucp (Highland Software, Palo Alto)

.SH CURRENT MAINTAINENCE BY
Joe Shelby
.br
jshelby@autometric.com
