/* actions.c - translation table stuff and action routines for xtartan
 * by Jim McBeath (jimmc@hisoft.uucp)
 *
 * 25.Oct.89  jimmc  Initial definition
 *  9.Jan.91  jimmc  v2.0: Use XTartan resource file
 */

#include <X11/Intrinsic.h>
#include <stdio.h>
#ifdef HAS_XPM
#include <xpm.h>
#endif
#include "tartan.h"

extern char *GetStringResource();

extern Widget TTopWidget;
extern Display *TDisplay;
extern Colormap TColormap;
extern Drawable TDrawable;
extern char *TartanName;
extern char *TartanSett;
extern TartanInfo *curtartan;
extern Window TWindow, TRootWindow;
extern Screen *TScreen;

static void Root(w,xev,argv,argcp)
Widget w;
XEvent *xev;
String *argv;
int *argcp;
{
	TWindow = TRootWindow;
	redraw();
	setrootbg(TDisplay,TScreen,TRootWindow,TDrawable);
	printf("Setting Root Window.  To repeat this tartan, enter\n");
	printf("     xtartan -r -t %s%s-s %d -w %d%s-fv %d\n",
	       TartanName, 
	       (Rdata.dark ? " -dark " : " "),
	       getScale(),
	       getWidth(),
	       (Rdata.fade ? " -fade " : " "),
	       Rdata.fadeValue);
	printf("Exiting.\n");
	exit(0);
}

static void
Help(w,xev,argv,argcp)
{
	char *s;

	s = GetStringResource(TTopWidget,"helpString");
	if (s) {
		(void)printf("%s",s);
		XtFree(s);
	} else {
		(void)printf("No help available\n");
	}
}

static void
SetTart(w,xev,argv,argcp)
Widget w;
XEvent *xev;
String *argv;
int *argcp;
{
	if (*argcp>0)
		(void)setTartan(argv[0]);
}

static void
PrintName(w,xev,argv,argcp)
Widget w;
XEvent *xev;
String *argv;
int *argcp;
{
	(void)printf("%s\n", TartanName);
}

static void
PrintSettAction(w,xev,argv,argcp)
Widget w;
XEvent *xev;
String *argv;
int *argcp;
{
	PrintSett();
}

static void
DoExit(w,xev,argv,argcp)
Widget w;
XEvent *xev;
String *argv;
int *argcp;
{
	exit(0);
}

static void
Info(w,xev,argv,argcp)
Widget w;
XEvent *xev;
String *argv;
int *argcp;
{
	(void)printf("%s scale=%d linewidth=%d\n",
		TartanName,getScale(),getWidth());
}

static void
Scale(w,xev,argv,argcp)
Widget w;
XEvent *xev;
String *argv;
int *argcp;
{
	if (*argcp>0)
		changeScale(argv[0]);
	else
		changeScale("2");		/* default */
}

static void
LineWidth(w,xev,argv,argcp)
Widget w;
XEvent *xev;
String *argv;
int *argcp;
{
	if (*argcp>0)
		changeWidth(argv[0]);
	else
		changeWidth("1");	/* default */
}

static void StringPrintStripeColors();
static void StringPrintSett(char *string)
{
	char tmpstr[2048];
	string[0] = 0;
	if (!curtartan) {
		(void)printf("No tartan selected\n");
		return;
	}
	sprintf(tmpstr,"%s: %s --", curtartan->name, curtartan->sett);
	strcat(string,tmpstr);
	StringPrintStripeColors(string,&(curtartan->hstripes),0);
	if (!(curtartan->vstripes.flags & HVSAME))
	    StringPrintStripeColors(string,&(curtartan->vstripes),&(curtartan->hstripes));
}

static void StringPrintStripeColors(string,sinfo, refsinfo)
char *string;
Sinfo *sinfo;
Sinfo *refsinfo;
{
	char tmpstr[2048];
	int i;
	S1info *s1i;
	char *color;
	char *colorLongname();

	for (i=0; sinfo->slist[i].color; i++) {
		s1i = sinfo->slist+i;
		if (IsInStripeList(sinfo,i,sinfo->slist+i))
			continue;
		if (IsInStripeList(refsinfo,-1,sinfo->slist+i))
			continue;
		color = s1i->color;
		sprintf(tmpstr,"%s:%s -",color,colorLongname(color));
		strcat(string,tmpstr);
	}
}

static void
CreateXpmFile(w,xev,argv,argcp)
Widget w;
XEvent *xev;
String *argv;
int *argcp;
{
#ifdef HAS_XPM
	Pixmap pixmap = (Pixmap) TDrawable; /* i know they're all XID's */
	XpmInfo info;
	XpmImage image;
	char colorcmts[2048];

	char filename[1024];

	XpmCreateXpmImageFromPixmap(TDisplay, pixmap, None, &image, NULL);

	/* store the color info in color comments */
	StringPrintSett(colorcmts);
	info.valuemask = XpmComments;
	info.hints_cmt = XtNewString("hint info here");
	info.colors_cmt = colorcmts;
	info.pixels_cmt = XtNewString("pixel info here");

	/* tag a "_dark" if in dark mode and "_fade" if faded */
	sprintf(filename, "%s%s%s.xpm", curtartan->name, 
		(Rdata.dark ? "_dark" : ""),
		(Rdata.fade ? "_fade" : ""));
	XpmWriteFileFromXpmImage(filename, &image, &info);
	printf("Xpm file %s created\n", filename);
	XtFree(info.hints_cmt);
	XtFree(info.pixels_cmt);
#else
	printf("Xpm not configured in this installation of XTartan\n");
#endif
}

static void
CreateGifFile(wig,xev,argv,argcp)
Widget wig;
XEvent *xev;
String *argv;
int *argcp;
{
#ifdef HAS_GD
	int w,h;
	XImage *ximage;
	Pixmap pixmap = (Pixmap) TDrawable; /* i know they're all XID's */
	char filename[1024];
	/* tag a "_dark" if in dark mode and "_fade" if faded */
	sprintf(filename, "%s%s%s.gif", curtartan->name, 
		(Rdata.dark ? "_dark" : ""),
		(Rdata.fade ? "_fade" : ""));
	w = h = 0;
	CreateImageFromPixmap(TDisplay,pixmap,&ximage,&w,&h);
	CreateGifFromXImage(filename,TDisplay,TColormap,ximage);
	printf("Gif file %s created\n", filename);
#else
	printf("Gif not configured in this installation of XTartan\n");
#endif
}

static void
Dark(w,xev,argv,argcp)
Widget w;
XEvent *xev;
String *argv;
int *argcp;
{
	Rdata.dark = !Rdata.dark;
	redraw();
}

static void
Fade(w,xev,argv,argcp)
Widget w;
XEvent *xev;
String *argv;
int *argcp;
{
	Rdata.fade = !Rdata.fade;
	redraw();
}

static void
Minimum(w,xev,argv,argcp)
Widget w;
XEvent *xev;
String *argv;
int *argcp;
{
	Rdata.minimum = !Rdata.minimum;
	redraw();
}

static void
Dump(w,xev,argv,argcp)
Widget w;
XEvent *xev;
String *argv;
int *argcp;
{
	(void)printf("Tartan setts:\n");
	DumpTartans();
	(void)printf("Color codes in cache:\n");
	dumpColors();
}

XtActionsRec actions[] = {
	{"dump",(XtActionProc)	Dump },
	{"exit",(XtActionProc)	DoExit },
	{"help",(XtActionProc)	Help },
	{"info",(XtActionProc)	Info },
	{"lineWidth",(XtActionProc)	LineWidth },
	{"name",(XtActionProc)	PrintName },
	{"scale",(XtActionProc)	Scale },
	{"setTartan",(XtActionProc)	SetTart },
	{"sett",(XtActionProc)	PrintSettAction },
	{"xpm",(XtActionProc)	CreateXpmFile},
	{"gif",(XtActionProc)   CreateGifFile},
	{"dark",(XtActionProc)	Dark},
	{"fade",(XtActionProc)  Fade},
	{"minimum",(XtActionProc) Minimum},
	{"root",(XtActionProc) Root},
};

void initActions(appctx)
XtAppContext appctx;
{
	XtAppAddActions(appctx,actions,XtNumber(actions));
}

/* end */
