/* misc.c - miscellaneous functions for xtartan
 * by Jim McBeath (jimmc@hisoft.uucp)
 *
 *  7.Jan.88  jimmc  Initial definition (X10)
 * 24.Oct.89  jimmc  Convert to X11, Xt; general restructuring
 *  9.Jan.91  jimmc  v2.0: Split out tartan data stuff to tartan.c
 */

#include <X11/Intrinsic.h>
#include <X11/StringDefs.h>
#include <stdio.h>
#include <stdarg.h>

extern char *Progname;
extern Display *TDisplay;

void Bell()
{
	XBell(TDisplay,0);
}

/* VARARGS1 - like printf */
void Warn(const char *fmt, ...)     /* print message */
{
	va_list pvar;

	fmt = va_arg(pvar,char *);
	(void)fprintf(stderr,"%s: ",Progname);
	(void)vfprintf(stderr,fmt,pvar);
	(void)fprintf(stderr,"\n");
	va_end(pvar);
}

/* VARARGS1 - like printf */
void Fatal(const char *fmt)     /* print message and exit */
{
	va_list pvar;

	fmt = va_arg(pvar,char *);
	(void)fprintf(stderr,"%s: ",Progname);
	(void)vfprintf(stderr,fmt,pvar);
	(void)fprintf(stderr,"\n");
	va_end(pvar);
	exit(1);
}

char *
GetSubResource(widget,sub,name)
Widget widget;
char *sub, *name;
{
	String string;
	static XtResource resources[] = {
		{ "", "", XtRString, sizeof(String),
			0, XtRString, NULL},
	};
	XtResource *tresource;

	tresource = (XtResource *)XtMalloc(sizeof(resources));
	tresource[0] = resources[0];
	tresource->resource_name = name;
	tresource->resource_class = name;
	XtGetSubresources(widget, &string, sub, sub,
	    tresource, (Cardinal)1, (ArgList)NULL, (Cardinal)0 );
	XtFree((char *)tresource);
	return string;
}

String
GetStringResource(widget, resourcename)
Widget widget;
char *resourcename;	/* what to look up in the file */
{
	String string;
	static XtResource resources[] = {
		{ "", "", XtRString, sizeof(String),
			0, XtRString, (caddr_t)NULL}
	};    
	XtResource *tresource;

	tresource = (XtResource *)XtMalloc(sizeof(resources));
	tresource[0] = resources[0];
	tresource->resource_name = resourcename;
	tresource->resource_class = resourcename;
	XtGetApplicationResources( widget, &string,
	    tresource, (Cardinal)1, (ArgList)NULL, (Cardinal)0 );
	XtFree((char *)tresource);
	return string;
}

/* end */
