/* tartan.c - tartan data functions
 * by Jim McBeath (jimmc@hisoft.uucp)
 *
 *  9.Jan.91  jimmc  v2.0: Split out from misc.c
 */

#include <X11/Intrinsic.h>
#include <stdio.h>
#include <ctype.h>
#include "tartan.h"

extern char *GetStringResource();
extern char *GetSubResource();

extern char *Progname;
extern Display *TDisplay;
extern Window TWindow;
extern Widget TTopWidget;
extern char *NamePattern;
extern char *SettPattern;

TartanInfo *firsttartan, *lasttartan;
TartanInfo *curtartan;
char *TartanName;
char *TartanSett;
S1info zaps1info = {0};

static
char *
findsubstr(sub,main)
char *sub;
char *main;
{
	int sl, ml;
	char *m;

	sl = strlen(sub);
	ml = strlen(main);
	m = main;
	while (sl<=ml) {
		if (strncmp(m,sub,sl)==0)
			return m;
		m++; ml--;
	}
	return (char *)0;
}

static
int
tartanMatches(p)
TartanInfo *p;
{
	if (NamePattern && *NamePattern) {
		if (!findsubstr(NamePattern,p->name))
			return 0;	/* name does not match */
	}
	if (SettPattern && *SettPattern) {
		if (!findsubstr(SettPattern,p->sett))
			return 0;	/* sett does not match */
	}
	return 1;		/* matches */
}

int
anyTartanMatches()
{
	TartanInfo *p;

	for (p=firsttartan; p; p=p->next) {
		if (tartanMatches(p))
			return 1;
	}
	return 0;	/* none of the tartans match the patterns */
}

static TartanInfo *
findTartan(name)
char *name;
{
	TartanInfo *p;

	for (p=firsttartan; p; p=p->next) {
		if (strcmp(p->name,name)==0)
			return p;
	}
	return (TartanInfo *)0;
}

static char *tbuf;
static int tbufsize;

int		/* 1 if we found the tartan, 0 if can't do it */
setTartan(name)
char *name;
{
	int l;
	TartanInfo *p;

	if (!curtartan) {
		if (!firsttartan) {
			Bell();
			return 0;
		}
		curtartan = firsttartan;
	}
	if (strcmp(name,"")==0) {
		/* no change to current tartan - just update */
	}
	else if (strcmp(name,"+")==0) {
		/* move to next tartan */
		curtartan = curtartan->next;
		if (!curtartan)
			curtartan = firsttartan;
		while (!tartanMatches(curtartan)) {
			curtartan = curtartan->next;
			if (!curtartan)
				curtartan = firsttartan;
		}
	}
	else if (strcmp(name,"-")==0) {
		/* move to previous tartan */
		curtartan = curtartan->prev;
		if (!curtartan)
			curtartan = lasttartan;
		while (!tartanMatches(curtartan)) {
			curtartan = curtartan->prev;
			if (!curtartan)
				curtartan = lasttartan;
		}
	}
	else if (strcmp(name,"0")==0) {
		/* move to first tartan in list */
		curtartan = firsttartan;
		while (!tartanMatches(curtartan)) {
			curtartan = curtartan->next;
			if (!curtartan)
				curtartan = firsttartan;
		}
	}
	else {
		/* move to named tartan */
		p = findTartan(name);
		if (!p) {
			Bell();
			return 0;
		}
		curtartan = p;
	}
	TartanName = curtartan->name;
	TartanSett = curtartan->sett;
	parseTartan();

	if (!TWindow) return 1;

	l = strlen(Progname)+strlen(TartanName)+2;
	if (l>tbufsize) {
		tbufsize = l;
		if (tbuf) free(tbuf);
		tbuf = XtMalloc(tbufsize);
	}
	(void)sprintf(tbuf,"%s %s",Progname,TartanName);
	redraw();
	XStoreName(TDisplay,TWindow,tbuf);
	return 1;
}

void ListTartans()
{
	TartanInfo *p;
	int l,d;

	printf("Known tartans are:\n");
	l = 0;
	for (p=firsttartan; p; p=p->next) {
		if (!tartanMatches(p)) continue;
		d = strlen(p->name)+1;
		if (l+d>=80) {
			printf("\n");
			l = 0;
		}
		printf(" %s",p->name);
		l += d;
	}
	printf("\n");
}

void PrintTartan()
{
	if (!curtartan) {
		printf("No tartan selected\n");
		return;
	}
	printf("Name: %s\n", curtartan->name);
	printf("Sett: %s\n", curtartan->sett);
}

void PrintSett()
{
	if (!curtartan) {
		printf("No tartan selected\n");
		return;
	}
	printf("%s: %s\n", curtartan->name, curtartan->sett);
	printf("Color translations:\n");
	PrintStripeColors(&(curtartan->hstripes),(Sinfo *)0);
	if (!(curtartan->vstripes.flags & HVSAME))
	    PrintStripeColors(&(curtartan->vstripes),&(curtartan->hstripes));
}

int
IsInStripeList(sinfo,n,s1i)
Sinfo *sinfo;
int n;
S1info *s1i;
{
	int i;

	if (!sinfo) return 0;
	for (i=0; sinfo->slist[i].color && (i<n || n<0); i++) {
		if (strcmp(sinfo->slist[i].color,s1i->color)==0)
			return 1;
	}
	return 0;
}

void PrintStripeColors(sinfo, refsinfo)
Sinfo *sinfo;
Sinfo *refsinfo;
{
	int i;
	S1info *s1i;
	char *color;
	char *colorLongname();

	for (i=0; sinfo->slist[i].color; i++) {
		s1i = sinfo->slist+i;
		if (IsInStripeList(sinfo,i,sinfo->slist+i))
			continue;
		if (IsInStripeList(refsinfo,-1,sinfo->slist+i))
			continue;
		color = s1i->color;
		printf(" %s: %s\n",color,colorLongname(color));
	}
}

void DumpTartans()
{
	TartanInfo *p;

	for (p=firsttartan; p; p=p->next) {
		printf("%s: %s\n",p->name, p->sett);
	}
}

void addTartan(name,sett)
char *name;
char *sett;
{
	TartanInfo *p;

	p = (TartanInfo *)XtCalloc(1,sizeof(TartanInfo));
	p->name = name;
	p->sett = sett;
	p->next = (TartanInfo *)0;
	if (firsttartan) {
		lasttartan->next = p;
		p->prev = lasttartan;
		lasttartan = p;
	} else {
		firsttartan = lasttartan = p;
		p->prev = (TartanInfo *)0;
	}
}

void readTartans()
{
	char *name;
	char *sett;

	name = GetStringResource(TTopWidget,"firstTartan");
	if (!name) {
		Warn("No firstTartan resource specified - no tartans loaded");
		return;
	}
	while (name) {
		sett = GetSubResource(TTopWidget,name,"sett");
		if (sett) {
			addTartan(name,sett);
		} else {
			Warn("No sett resource for tartan %s",name);
		}
		name = GetSubResource(TTopWidget,name,"nextTartan");
	}
}

static S1info *sbuf;
static int sbufcount, sbufalloc;

/* parses the sett string in the current tartan */
void parseTartan()
{
	char *s;
	char ccbuf[100];	/* color code */
	char *p;
	int n;
	int size;
	int bflag=0;
	Sinfo *stripes;

	if (!curtartan) return;
	if (curtartan->isparsed) return;
	/* set up defaults */
	curtartan->hstripes.flags = SYM;
	curtartan->vstripes.flags = HVSAME;
	stripes = &(curtartan->hstripes);
	s = curtartan->sett;
	sbufcount = 0;
	while (*s) {
		while (isspace(*s)) s++;
		if (!*s) break;
		if (*s=='(') {	/* look for comments */
			while (*s && *s!=')') s++;	/* skip to end */
			if (*s==')') s++;
			/* can't use close paren in a comment */
			continue;
		}
		if (*s=='%') {	/* look for specials */
			s++;
			switch (*s) {
			case 'a':	/* asymmetric */
				stripes->flags &= ~SYM;
				break;
			case 'b':	/* halve all thread counts */
				bflag = 1;
				break;
			case 'p':	/* double-pivot */
				stripes->flags |= PIVOT2;
				break;
			case 'v':	/* done with horiz, now comes vert */
				size = (sbufcount+1)*sizeof(S1info);
				stripes->slist = (S1info *)XtMalloc(size);
				memmove((void *)(stripes->slist),(void *)sbuf,size);
				stripes->slist[sbufcount].color = (char *)0;
				stripes = &(curtartan->vstripes);
				stripes->flags = SYM;
				sbufcount = 0;
				break;
			default:
				Warn("Unknown % code %c in sett for %s",
					*s, curtartan->name);
				break;
			}
			s++;	/* skip over flag */
			continue;	/* back to start of while loop */
		}
		if (!isalpha(*s)) {
			Warn("Bad character %c in sett string for %s",
				*s, curtartan->name);
			s++;
			continue;
		}
		p = ccbuf;
		while (isalpha(*s)) *p++ = *s++;
		*p = 0;
		while (isspace(*s)) s++;
		if (!*s) break;
		n = 0;
		while (isdigit(*s)) n = n*10 + (*s++ - '0');
		if (bflag) n = n/2;
		if (sbufcount>=sbufalloc) {
			if (sbufalloc) sbufalloc *= 2;
			else sbufalloc = 15;
			size = sbufalloc * sizeof(S1info);
			if (sbuf) sbuf=(S1info *)XtRealloc((char *)sbuf,size);
			else sbuf=(S1info *)XtMalloc(size);
		}
		sbuf[sbufcount] = zaps1info;
		sbuf[sbufcount].color = XtMalloc(strlen(ccbuf)+1);
		(void)strcpy(sbuf[sbufcount].color,ccbuf);
		sbuf[sbufcount].width = n;
		sbufcount++;
	}
	size = (sbufcount+1)*sizeof(S1info);
	stripes->slist = (S1info *)XtMalloc(size);
	memmove( (void *)(stripes->slist),(void *)sbuf,size );
	stripes->slist[sbufcount].color = (char *)0;
	curtartan->isparsed = 1;
}

/* end */
