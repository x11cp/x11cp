/* tartan.h (formerly tartaninfo.h)
 * Jim McBeath (jimmc@hisoft.uucp)
 *
 * 11.Jan.88  jimmc  Extracted from xtartan.c
 * 13.Jan.88  Remove numstripes field in Sinfo, use null color name instead
 *  9.Jan.99  jimmc  v2.0: Resource-based version
 */

/* fill styles */
#define VSTRIPE (1<<10)
#define HSTRIPE (1<<11)
#define SOLID 1		/* solid rectangle */
#define RLINES 2	/* lines in same direction as stripes */
#define HLINES 3	/* horizontal lines */
#define VLINES 4	/* vertical lines */
#define NOP 5		/* draw nothing */
#define ALINES 6	/* angled lines (lower left to upper right) */

/* stripe group flags */
#define HVSAME (1<<0)	/* vertical stripes are same as horizontal */
#define SYM    (1<<1)	/* symmetrical stripe pattern */
#define PIVOT2 (1<<2)	/* last two colors are double-pivot */

typedef struct _s1info {	/* info about one stripe */
	char *color;		/* name of color code of the stripe */
				/* (null indicates end of a list of stripes) */
	int width;		/* width of the stripe */
	int style;		/* style of the stripe */
} S1info;

typedef struct _sinfo {		/* info about a set of stripes */
	int flags;		/* flags about the stripe group */
	S1info *slist;		/* info about each stripe */
		/* end of the list is specified by a stripe with a null
		 * color name. */
} Sinfo;

typedef struct _tartaninfo {
	struct _tartaninfo *next, *prev;
	char *name;	/* name of this tartan */
	char *sett;	/* the sett data */
/* the remainder of the structure is filled in only when the tartan sett
 * string is parsed (on demand) */
	int isparsed;		/* true after it has been parsed */
	Sinfo hstripes;		/* info about horizontal stripes */
				/* stripes listed from bottom to top */
	Sinfo vstripes;		/* info about vertical stripes */
				/* stripes listed from left to right */
} TartanInfo;

typedef struct rinfo {
	Bool useroot;
	int scale;
	int lwidth;
	char *tartanname;
	char *action;
	char *namepattern;
	char *settpattern;
	Bool dark;
	Bool fade;
	int fadeValue;
	Bool minimum;
} RInfo, *RInfoPtr;
extern RInfo Rdata;

/* function prototypes */
void redraw();
void FillStripes();
void FillBox();
void Usage();
void initDisplay();
void parseTartan();
void PrintStripeColors();

/* end */
