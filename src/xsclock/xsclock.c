#include <stdio.h>
#include <strings.h>
#include <stdlib.h>
#include <unistd.h>
#include <math.h>
#include <time.h>
#include <X11/Xlib.h>
#include <X11/Xatom.h>

#define setfg(x,y) XSetForeground(dpy,x,GetColor(y))
#define SIZE 56

struct tm *tmval;           
char *color_bg="rgb:90/90/a0"; /* background color*/
char *color_hour="blue2";      /* hours hands color*/
char *color_min="green";       /* minutes hand color */
char *color_sec="red";         /* seconds hand color */
char *color_num="ivory";       /* mark color */
char *color_num_s="gray20";    /* mark shadow color */
XArc Numbers[12];              /* mark position */
XArc Numbers_s[12];            /* mark shadow position */
Font dayFont;
char the_today[7];
int flag_day = False;

Display *dpy;            
Window root, win;
int scrn;
GC gc, gc_sec, gc_hour, gc_min, gc_num, gc_num_s;
char *window_name={"xsclock"}; 


unsigned long GetColor(char *color)
{  
  Colormap cmap;
  XColor c0, c1;
  cmap = DefaultColormap(dpy, 0);
  XAllocNamedColor(dpy, cmap, color, &c1, &c0);
  return(c1.pixel);
} 

void drawClock()
{
  int deg_sec, deg_min, deg_hour;
  time_t current_time;
  
  current_time = time(NULL);
  tmval = localtime(&current_time);

  sprintf(the_today, "%02d / %02d", tmval->tm_mon+1, tmval->tm_mday);
  deg_sec = tmval->tm_sec*6.0;
  deg_min = tmval->tm_min*6.0;
  deg_hour = tmval->tm_hour*30.0 + deg_min/12.0;

  XFillRectangle(dpy, win, gc, 0, 0, SIZE, SIZE);
  XFillArcs(dpy, win, gc_num, Numbers, 12);
  XFillArcs(dpy, win, gc_num_s, Numbers_s, 12);
  XDrawLine(dpy, win, gc_hour, SIZE/2, SIZE/2, 
            SIZE/2+18*sin(M_PI*deg_hour/180.0), 
            SIZE/2-18*cos(M_PI*deg_hour/180.0));
  XDrawLine(dpy, win, gc_min, SIZE/2, SIZE/2, 
            SIZE/2+23*sin(M_PI*deg_min/180.0), 
            SIZE/2-23*cos(M_PI*deg_min/180.0));
  if (flag_day ){
    XDrawString(dpy, win, gc_num_s, 15, 44, the_today, 7); 
    XDrawString(dpy, win, gc_num, 14, 43, the_today, 7); 
  }
  XDrawLine(dpy, win, gc_sec, 
            SIZE/2-10*sin(M_PI*deg_sec/180.0), 
            SIZE/2+10*cos(M_PI*deg_sec/180.0), 
            SIZE/2+27*sin(M_PI*deg_sec/180.0),  
            SIZE/2-27*cos(M_PI*deg_sec/180.0)); 
  XFillArc(dpy, win, gc_sec, SIZE/2-2, SIZE/2-2, 4, 4, 0, 64*360);
}  

void main(int argc, char **argv)
{
  int i, cx;
  char *vx;
  for (i = 0; i < 12; i++) {
    Numbers[i].x = SIZE/2+25*cos(M_PI*i/6.0)-2;
    Numbers_s[i].x = Numbers[i].x + 1;
    Numbers[i].y = SIZE/2+25*sin(M_PI*i/6.0)-2;
    Numbers_s[i].y = Numbers[i].y + 1;
    Numbers_s[i].width = Numbers[i].width = 4;         
    Numbers_s[i].height = Numbers[i].height = 4;
    Numbers_s[i].angle1 = Numbers[i].angle1 = 0;
    Numbers_s[i].angle2 = Numbers[i].angle2 = 64*360;
  }

  for (cx=1; cx < argc; cx++) {
    vx = argv[cx];
      if (strcmp(vx,"-day") == 0 || strcmp(vx,"--day") == 0){
       flag_day = True;
      } else if (strcmp(vx,"-h") == 0 || strcmp(vx,"--help") == 0) {
       printf("usage: xsclock [--day]\n");
       exit(0);
      }
  }
  dpy = XOpenDisplay(NULL);
  scrn=DefaultScreen(dpy);
  root=RootWindow(dpy,scrn);

  win = XCreateSimpleWindow(dpy,root,0,0,
                              SIZE,SIZE,0,0,0);
  XStoreName(dpy,win,window_name);

  gc=XCreateGC(dpy,win,0,NULL);
  gc_sec=XCreateGC(dpy,win,0,NULL);
  gc_min=XCreateGC(dpy,win,0,NULL);
  gc_hour=XCreateGC(dpy,win,0,NULL);
  gc_num=XCreateGC(dpy,win,0,NULL);
  gc_num_s=XCreateGC(dpy,win,0,NULL);
  setfg(gc,color_bg);
  setfg(gc_sec,color_sec);
  setfg(gc_min,color_min);
  setfg(gc_hour,color_hour);
  setfg(gc_num,color_num);
  setfg(gc_num_s,color_num_s);
  dayFont=XLoadFont(dpy,
       "-adobe-helvetica-medium-r-normal--8-*-*-*-*-*-iso8859-1");
  XSetFont(dpy, gc_num_s, dayFont);
  XSetFont(dpy, gc_num, dayFont);

  XSetLineAttributes(dpy,gc_hour,2,LineSolid, 
                            CapRound,JoinMiter);
  XMapWindow(dpy,win);

  while(1) {    /* Never ends(^^; */
   drawClock();
   XFlush(dpy);   
   usleep(300000L);     /* please adjust (^^;; */
  }
}
