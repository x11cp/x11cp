7
0 0 0 4
Lesson 3: Subtraction
This Lesson demonstrates subtracting 7 from 11

Press Space-bar to Begin
0 1 1 4
Lesson 3: Subtraction (11-7)
First Represent 11 as 10...
(pending operation)
Press Space-bar to Continue
0 0 1 4
Lesson 3: Subtraction (11-7)
...and 1.
(This is 11)
Press Space-bar to Continue
0 0 0 4
Lesson 3: Subtraction (11-7)
We Represent -7 as -10+3, so...

Press Space-bar to Continue
0 1 -1 4
Lesson 3: Subtraction (11-7)
-10 ...
(pending operation +3)
Press Space-bar to Continue
0 0 3 4
Lesson 3: Subtraction (11-7)
...+3 Completes The Operation
The Answer is +4.
Press Space-bar
0 0 0 4
Conclusion: (End)
To perform addition, move beads towards the 
mid-beam; to subtract, move beads away from it.
Press Space-bar
