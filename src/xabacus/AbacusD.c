/*-
# X-BASED ABACUS
#
#  AbacusD.c
#
###
#
#  Copyright (c) 1999	David Albert Bagley, bagleyd@tux.org
#
#  Abacus demo and neat pointers from
#  Copyright (c) 1991 - 98  Luis Fernandes, elf@ee.ryerson.ca
#
#                   All Rights Reserved
#
#  Permission to use, copy, modify, and distribute this software and
#  its documentation for any purpose and without fee is hereby granted,
#  provided that the above copyright notice appear in all copies and
#  that both that copyright notice and this permission notice appear in
#  supporting documentation, and that the name of the author not be
#  used in advertising or publicity pertaining to distribution of the
#  software without specific, written prior permission.
#
#  This program is distributed in the hope that it will be "useful",
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
#
*/

/* Methods demo file for Abacus */

#include <stdlib.h>
#include <stdio.h>
#ifdef VMS
#include <unixlib.h>
#else
#if HAVE_UNISTD_H
#include <unistd.h>
#endif
#endif
#include <X11/IntrinsicP.h>
#include <X11/Intrinsic.h>
#include <X11/StringDefs.h>
#include <X11/CoreP.h>
#include "AbacusP.h"

static void InitializeAbacusDemo(Widget request, Widget renew);
static void ExposeAbacusDemo(Widget renew, XEvent * event, Region region);

#if 0
static void ResizeAbacusDemo(AbacusWidget w);

#endif
static void DestroyAbacusDemo(Widget old);
static Boolean SetValuesAbacusDemo(Widget current, Widget request, Widget renew);

static void QuitAbacusDemo(AbacusWidget w, XEvent * event, char **args, int nArgs);
static void SelectAbacusDemo(AbacusWidget w, XEvent * event, char **args, int nArgs);
static void NextAbacusDemo(AbacusWidget w, XEvent * event, char **args, int nArgs);
static void RepeatAbacusDemo(AbacusWidget w, XEvent * event, char **args, int nArgs);
static void MoreAbacusDemo(AbacusWidget w, XEvent * event, char **args, int nArgs);

static void SetAllColors(AbacusWidget w, Boolean init);
static void ClearDemo(AbacusWidget w);
static void QueryDemo(AbacusWidget w, Boolean advance);
static void IncDemo(AbacusWidget w);

static char defaultTranslationsAbacusDemo[] =
"<KeyPress>q: quit()\n\
   Ctrl<KeyPress>C: quit()\n\
   <Btn1Down>: select()\n\
   <KeyPress>c: select()\n\
   <KeyPress>n: next()\n\
   <KeyPress>r: repeat()\n\
   <KeyPress>0x20: more()\n\
   <KeyPress>0x40: more()\n\
   <KeyPress>KP_Space: more()\n\
   <KeyPress>Return: more()";

/* KP_Space does not work here
   0x20 is SP (' ') in ASCII  (DP in EBCDIC)
   0x40 is SP (' ') in EBCDIC ('@' in ASCII) 
 */


static XtActionsRec actionsListAbacusDemo[] =
{
	{"quit", (XtActionProc) QuitAbacusDemo},
	{"select", (XtActionProc) SelectAbacusDemo},
	{"next", (XtActionProc) NextAbacusDemo},
	{"repeat", (XtActionProc) RepeatAbacusDemo},
	{"more", (XtActionProc) MoreAbacusDemo}
};

static XtResource resourcesAbacusDemo[] =
{
	{XtNwidth, XtCWidth, XtRDimension, sizeof (Dimension),
	 XtOffset(AbacusWidget, core.width), XtRString, "436"},
	{XtNheight, XtCHeight, XtRDimension, sizeof (Dimension),
	 XtOffset(AbacusWidget, core.height), XtRString, "98"},
	{XtNmono, XtCMono, XtRBoolean, sizeof (Boolean),
	 XtOffset(AbacusWidget, abacus.mono), XtRString, "FALSE"},
	{XtNreverse, XtCReverse, XtRBoolean, sizeof (Boolean),
	 XtOffset(AbacusWidget, abacus.reverse), XtRString, "FALSE"},
	{XtNdemoPath, XtCDemoPath, XtRString, sizeof (String),
	 XtOffset(AbacusWidget, abacusDemo.path), XtRString, DEMOPATH},
	{XtNdemoFont, XtCDemoFont, XtRString, sizeof (String),
	 XtOffset(AbacusWidget, abacusDemo.font), XtRString, "-*-times-*-r-*-*-*-180-*-*-*-*"},
	{XtNdemoForeground, XtCForeground, XtRPixel, sizeof (Pixel),
	 XtOffset(AbacusWidget, abacusDemo.foreground), XtRString, XtDefaultForeground},
	{XtNdemoBackground, XtCBackground, XtRPixel, sizeof (Pixel),
	 XtOffset(AbacusWidget, abacusDemo.background), XtRString, XtDefaultBackground},
	{XtNdeck, XtCDeck, XtRInt, sizeof (int),
	 XtOffset(AbacusWidget, abacus.deck), XtRString, "-1"},
	{XtNrail, XtCRail, XtRInt, sizeof (int),
	 XtOffset(AbacusWidget, abacus.rail), XtRString, "0"},
	{XtNnumber, XtCNumber, XtRInt, sizeof (int),
	 XtOffset(AbacusWidget, abacus.number), XtRString, "0"},
	{XtNframed, XtCFramed, XtRBoolean, sizeof (Boolean),
	 XtOffset(AbacusWidget, abacusDemo.framed), XtRString, "FALSE"},
	{XtNselectCallback, XtCCallback, XtRCallback, sizeof (caddr_t),
	 XtOffset(AbacusWidget, abacus.select), XtRCallback, NULL}
};

AbacusClassRec abacusDemoClassRec =
{
	{
		(WidgetClass) & widgetClassRec,		/* superclass */
		"AbacusDemo",	/* class name */
		sizeof (AbacusRec),	/* widget size */
		NULL,		/* class initialize */
		NULL,		/* class part initialize */
		FALSE,		/* class inited */
		(XtInitProc) InitializeAbacusDemo,	/* initialize */
		NULL,		/* initialize hook */
		XtInheritRealize,	/* realize */
		actionsListAbacusDemo,	/* actions */
		XtNumber(actionsListAbacusDemo),	/* num actions */
		resourcesAbacusDemo,	/* resources */
		XtNumber(resourcesAbacusDemo),	/* num resources */
		NULLQUARK,	/* xrm class */
		TRUE,		/* compress motion */
		TRUE,		/* compress exposure */
		TRUE,		/* compress enterleave */
		TRUE,		/* visible interest */
		(XtWidgetProc) DestroyAbacusDemo,	/* destroy */
#if 0
		(XtWidgetProc) ResizeAbacusDemo,	/* resize */
#else
		NULL,		/* resize */
#endif
		(XtExposeProc) ExposeAbacusDemo,	/* expose */
		(XtSetValuesFunc) SetValuesAbacusDemo,	/* set values */
		NULL,		/* set values hook */
		XtInheritSetValuesAlmost,	/* set values almost */
		NULL,		/* get values hook */
		NULL,		/* accept focus */
		XtVersion,	/* version */
		NULL,		/* callback private */
		defaultTranslationsAbacusDemo,	/* tm table */
		NULL,		/* query geometry */
		NULL,		/* display accelerator */
		NULL		/* extension */
	},
	{
		0		/* ignore */
	}
};

WidgetClass abacusDemoWidgetClass = (WidgetClass) & abacusDemoClassRec;

#define LINES 4
#define CHARS 64

typedef enum {
	INTRO, QUERY, DISPLAY, CONCL
} texttypes;

static char *introframelesstext[] =
{
	"Place this window below the abacus, then click & leave",
	"the pointer in the abacus window to begin the demo.",
	"During the demo, use the Space-bar to step.",
	"Type `q' to quit the demo.",
};

static char *introframedtext[] =
{
	"Click & leave the pointer in the abacus window",
	"to begin the demo.",
	"During the demo, use the Space-bar to step.",
	"Type `q' to quit the demo.",
};

static char *querytext[] =
{
	"Type:",
	"  `n' for the Next lesson",
	"  `r' to Repeat previous lesson",
	"  `q' to Quit the demo",
};

static char *concltext[] =
{
	"Here Endth The Lesson",
	"",
	"",
	"",
};

/* This array represents 4 lines of text that are to be displayed in the
 * demo window; the module drawDemoWindow() will be passed a pointer to this
 * array */
static char displaytext[LINES][CHARS];

static void
loadFont(AbacusWidget w)
{
	/*char *fontname = "-*-times-*-r-*-*-*-180-*"; */
	char       *altfontname = "8x13";

	/* Access font */
	if ((w->abacusDemo.fontInfo = XLoadQueryFont(XtDisplay(w),
					      w->abacusDemo.font)) == NULL) {
		(void) fprintf(stderr, "Cannot open %s font\n",
			       w->abacusDemo.font);
		(void) fprintf(stderr, "Attempting %s font as alternate\n",
			       altfontname);

		if ((w->abacusDemo.fontInfo = XLoadQueryFont(XtDisplay(w),
						     altfontname)) == NULL) {
			(void) fprintf(stderr,
				       "Cannot open %s alternate font\n",
				       altfontname);
			(void) fprintf(stderr,
				       "use the -demofont option to specify a font to use\n");
			exit(-1);
		}
	}
	w->abacusDemo.fontHeight = w->abacusDemo.fontInfo->max_bounds.ascent +
		w->abacusDemo.fontInfo->max_bounds.descent;
}

static void
drawText(AbacusWidget w, GC gc, texttypes textt)
{
	int         line;
	static Bool firstTime = True;

	if (!firstTime)
		XFillRectangle(XtDisplay(w), XtWindow(w), w->abacusDemo.inverseGC,
			       0, 0, w->core.width, w->core.height);
	else
		firstTime = False;

	for (line = 0; line < LINES; line++) {
		char       *temp;

		switch (textt) {
			case INTRO:
				if (w->abacusDemo.framed)
					temp = introframedtext[line];
				else
					temp = introframelesstext[line];
#ifdef DEBUG
				(void) fprintf(stderr, " INTRO\n");
#endif
				break;
			case CONCL:
				w->abacusDemo.started = False;
				temp = concltext[line];
#ifdef DEBUG
				(void) fprintf(stderr, " CONCL\n");
#endif
				break;
			case QUERY:
				w->abacusDemo.query = True;
				temp = querytext[line];
#ifdef DEBUG
				(void) fprintf(stderr, " QUERY\n");
#endif
				break;
			default:
				temp = displaytext[line];
#ifdef DEBUG
				(void) fprintf(stderr, " DISPLAY\n");
#endif
				break;
		}
#ifdef DEBUG
		(void) printf("%d, %d %s\n", w->abacusDemo.fontHeight, line, temp);
#endif
		XDrawString(XtDisplay(w), XtWindow(w), gc,
			    1, w->abacusDemo.fontHeight + (w->abacusDemo.fontHeight * line),
			    temp, strlen(temp));
		XFlush(XtDisplay(w));
	}
}

/* Find the lessons... this way its easy to add more */
static void
setLessons(AbacusWidget w)
{
	static char fname[512];

	w->abacusDemo.lessons = 0;
	for (;;) {
		w->abacusDemo.lessons++;
		(void) sprintf(fname, "%s/Lesson%d.cmd",
			  (w->abacusDemo.path[0]) ? w->abacusDemo.path : ".",
			       w->abacusDemo.lessons);
		if ((w->abacusDemo.fp = fopen(fname, "r")) == NULL) {
			w->abacusDemo.lessons--;
#ifdef DEBUG
			(void) printf("lessons = %d\n", w->abacusDemo.lessons);
#endif
			return;
		}
		(void) fclose(w->abacusDemo.fp);
	}
}

static void
startLesson(AbacusWidget w)
{
	static char fname[512];

	(void) sprintf(fname, "%s/Lesson%d.cmd",
		       (w->abacusDemo.path[0]) ? w->abacusDemo.path : ".",
		       w->abacusDemo.lessonCount + 1);

#ifdef DEBUG
	printf("Lesson name = %s\n", fname);
#endif
	if ((w->abacusDemo.fp = fopen(fname, "r")) == NULL) {
		(void) fprintf(stderr,
		    "Error, could not open lesson script-file: %s\n", fname);
		exit(1);
	}
	(void) fscanf(w->abacusDemo.fp, "%d\n", &(w->abacusDemo.lessonLength));
#ifdef DEBUG
	(void) printf("lessonLength = %d\n", w->abacusDemo.lessonLength);
#endif
}

static void
doLesson(AbacusWidget w, GC gc)
{
	int         line;

	/* load first instance */
	(void) fscanf(w->abacusDemo.fp, "%d %d %d %d\n",
		      &(w->abacusDemo.deck), &(w->abacusDemo.rail),
		      &(w->abacusDemo.number), &(w->abacusDemo.lines));
#ifdef DEBUG
	(void) printf("deck = %d, rail = %d, number = %d, lines = %d\n",
		      w->abacusDemo.deck, w->abacusDemo.rail,
		      w->abacusDemo.number, w->abacusDemo.lines);
#endif

	for (line = 0; line < w->abacusDemo.lines; line++) {
		(void) fgets(displaytext[line], 64, w->abacusDemo.fp);
		displaytext[line][strlen(displaytext[line]) - 1] = '\0';
	}

	/* A '0' in demo number signifies that only the text is to be
	 * displayed (i.e. nothing is to be added or subtracted).
	 */

	if (w->abacusDemo.lessonLength && w->abacusDemo.number != 0) {
		abacusCallbackStruct cb;

		cb.reason = ABACUS_MOVE;
		cb.deck = w->abacusDemo.deck;
		cb.rail = w->abacusDemo.rail;
		cb.number = w->abacusDemo.number;
		XtCallCallbacks((Widget) w, XtNselectCallback, &cb);
	}
	drawText(w, gc, DISPLAY);

	if (--w->abacusDemo.lessonLength < 0) {
		(void) fclose(w->abacusDemo.fp);

		drawText(w, gc, CONCL);
		(void) sleep(1);
		drawText(w, gc, QUERY);
	}
}

static void
InitializeAbacusDemo(Widget request, Widget renew)
{
	AbacusWidget w = (AbacusWidget) renew;

	w->abacus.depth = DefaultDepthOfScreen(XtScreen(w));
	SetAllColors(w, True);
	w->abacusDemo.started = False;
	w->abacusDemo.query = False;
	w->abacusDemo.lessonCount = 0;

#if 0
	ResizeAbacusDemo(w);
#endif
	setLessons(w);
	loadFont(w);
}

static void
DestroyAbacusDemo(Widget old)
{
	AbacusWidget w = (AbacusWidget) old;

	XtReleaseGC(old, w->abacusDemo.foregroundGC);
	XtReleaseGC(old, w->abacusDemo.inverseGC);
	XtRemoveCallbacks(old, XtNselectCallback, w->abacus.select);
}

#if 0
static void
ResizeAbacusDemo(AbacusWidget w)
{
}

#endif

static void
ExposeAbacusDemo(Widget renew, XEvent * event, Region region)
{
	AbacusWidget w = (AbacusWidget) renew;

	if (w->core.visible) {
		XFillRectangle(XtDisplay(w), XtWindow(w),
			       w->abacusDemo.inverseGC, 0, 0, w->core.width, w->core.height);
		if (w->abacusDemo.started)
			drawText(w, w->abacusDemo.foregroundGC, DISPLAY);
		else if (w->abacusDemo.query)
			drawText(w, w->abacusDemo.foregroundGC, QUERY);
		else
			drawText(w, w->abacusDemo.foregroundGC, INTRO);
	}
}

static      Boolean
SetValuesAbacusDemo(Widget current, Widget request, Widget renew)
{
	AbacusWidget c = (AbacusWidget) current, w = (AbacusWidget) renew;
	Boolean     redraw = False;

#if 0
	Boolean     redrawText = False;

#endif

	if (w->core.background_pixel != c->core.background_pixel ||
	    w->abacus.foreground != c->abacus.foreground ||
	    w->abacusDemo.background != c->abacusDemo.background ||
	    w->abacusDemo.foreground != c->abacusDemo.foreground ||
	    w->abacus.reverse != c->abacus.reverse ||
	    w->abacus.mono != c->abacus.mono) {
		SetAllColors(w, False);
		redraw = True;
#if 0
		redrawText = True;
#endif
	}
#if 0
	if (!redraw && XtIsRealized(renew) && renew->core.visible) {
		/* Redraw text */
	}
#endif
	if (w->abacus.deck == ABACUS_CLEAR) {
		w->abacus.deck = ABACUS_IGNORE;
		ClearDemo(w);
	}
	if (w->abacus.deck == ABACUS_NEXT) {
		w->abacus.deck = ABACUS_IGNORE;
		QueryDemo(w, True);
	}
	if (w->abacus.deck == ABACUS_REPEAT) {
		w->abacus.deck = ABACUS_IGNORE;
		QueryDemo(w, False);
	}
	if (w->abacus.deck == ABACUS_MORE) {
		w->abacus.deck = ABACUS_IGNORE;
		IncDemo(w);
	}
	return (redraw);
}

static void
QuitAbacusDemo(AbacusWidget w, XEvent * event, char **args, int nArgs)
{
	XtCloseDisplay(XtDisplay(w));
	exit(0);
}

static void
SelectAbacusDemo(AbacusWidget w, XEvent * event, char **args, int nArgs)
{
	ClearDemo(w);
}

static void
NextAbacusDemo(AbacusWidget w, XEvent * event, char **args, int nArgs)
{
	QueryDemo(w, True);
}

static void
RepeatAbacusDemo(AbacusWidget w, XEvent * event, char **args, int nArgs)
{
	QueryDemo(w, False);
}

static void
MoreAbacusDemo(AbacusWidget w, XEvent * event, char **args, int nArgs)
{
	IncDemo(w);
}

static void
SetAllColors(AbacusWidget w, Boolean init)
{
	XGCValues   values;
	XtGCMask    valueMask;

	valueMask = GCForeground | GCBackground;
	if (w->abacus.depth < 2 || w->abacus.mono) {
		if (w->abacus.reverse) {
			values.background = w->abacus.foreground;
			values.foreground = w->core.background_pixel;
		} else {
			values.foreground = w->abacus.foreground;
			values.background = w->core.background_pixel;
		}
	} else {
		if (w->abacus.reverse) {
			values.foreground = w->abacusDemo.background;
			values.background = w->abacusDemo.foreground;
		} else {
			values.background = w->abacusDemo.background;
			values.foreground = w->abacusDemo.foreground;
		}
	}
	if (!init)
		XtReleaseGC((Widget) w, w->abacusDemo.foregroundGC);
	w->abacusDemo.foregroundGC = XtGetGC((Widget) w, valueMask, &values);

	if (w->abacus.depth < 2 || w->abacus.mono) {
		if (w->abacus.reverse) {
			values.foreground = w->abacus.foreground;
			values.background = w->core.background_pixel;
		} else {
			values.background = w->abacus.foreground;
			values.foreground = w->core.background_pixel;
		}
	} else {
		if (w->abacus.reverse) {
			values.background = w->abacusDemo.background;
			values.foreground = w->abacusDemo.foreground;
		} else {
			values.foreground = w->abacusDemo.background;
			values.background = w->abacusDemo.foreground;
		}
	}
	if (!init)
		XtReleaseGC((Widget) w, w->abacusDemo.inverseGC);
	w->abacusDemo.inverseGC = XtGetGC((Widget) w, valueMask, &values);
}

static void
ClearDemo(AbacusWidget w)
{
	abacusCallbackStruct cb;

	w->abacusDemo.started = True;
	w->abacusDemo.query = False;
	cb.reason = ABACUS_CLEAR;
	XtCallCallbacks((Widget) w, XtNselectCallback, &cb);
	startLesson(w);
	doLesson(w, w->abacusDemo.foregroundGC);
}

static void
QueryDemo(AbacusWidget w, Boolean advance)
{
	if (w->abacusDemo.query) {
		abacusCallbackStruct cb;

		w->abacusDemo.started = True;
		w->abacusDemo.query = False;
		if (w->abacusDemo.lessons && advance)
			w->abacusDemo.lessonCount =
				(w->abacusDemo.lessonCount + 1) % w->abacusDemo.lessons;
		cb.reason = ABACUS_CLEAR;
		XtCallCallbacks((Widget) w, XtNselectCallback, &cb);
		startLesson(w);
		doLesson(w, w->abacusDemo.foregroundGC);
	}
}

static void
IncDemo(AbacusWidget w)
{
	if (w->abacusDemo.started && !w->abacusDemo.query) {
		w->abacusDemo.query = False;
		doLesson(w, w->abacusDemo.foregroundGC);
	}
}
