/*
 *   xmcolor - Motif(tm) Color Mixer utility
 *
 *   Copyright (C) 1994  Ti Kan
 *   E-mail: ti@amb.org
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */
#ifndef LINT
static char *_xmcolor_c_ident_ = "@(#)xmcolor.c	1.11 94/12/05";
#endif

#include <stdio.h>
#include <ctype.h>
#include <sys/types.h>
#include <errno.h>
#include <stdlib.h>
#include <X11/keysym.h>
#include <Xm/Xm.h>
#include <Xm/Form.h>
#include <Xm/Frame.h>
#include <Xm/Label.h>
#include <Xm/PushB.h>
#include <Xm/List.h>
#include <Xm/Text.h>
#include <Xm/Separator.h>
#include <Xm/DrawingA.h>
#include <Xm/Scale.h>
#include <Xm/MessageB.h>
#include <X11/Xatom.h>
#include "patchlevel.h"
#include "xmcolor.xbm"


#define PROGNAME	"xmcolor"	/* Program name */
#define NPRI		3		/* Number of primary colors */

#define RED		0		/* Defines used as array index */
#define GREEN		1
#define BLUE		2
#define MIX		3

#define SCALE_MAX	100		/* Slider maximum value */

#define XcolormixNrgbFilePath	"rgbFilePath"
#define XcolormixCRgbFilePath	"RgbFilePath"

#define CHSET1		"chset1"
#define CHSET2		"chset2"
#define CHSET3		"chset3"


typedef struct colorent {
	int		index;		/* Index number */
	unsigned int	red;		/* Red value */
	unsigned int	green;		/* Green value */
	unsigned int	blue;		/* Blue value */
	struct colorent	*next;		/* Next list element */
} colorent_t;

typedef struct appdata {
	char		*rgbfile;	/* Path name to rgb.txt file */
} appdata_t;


XtResource		resources[] = {
	{
		XcolormixNrgbFilePath, XcolormixCRgbFilePath,
		XmRString, sizeof(String),
		XtOffsetOf(appdata_t, rgbfile), XmRImmediate,
		(XtPointer) "/home/n6tadam/Downloads/xmcolor-1.0/rgb.txt",
	},
};

XrmOptionDescRec	options[] = {
	{ "-rgbfile",	"*rgbFilePath",	XrmoptionSepArg, NULL },
};

String			fallbacks[] = {
	"*rgbFilePath: /home/n6tadam/Downloads/xmcolor-1.0/rgb.txt",
	"*slider0*foreground: Red",
	"*slider1*foreground: Green",
	"*slider2*foreground: Blue",
	"*logoLabel*foreground: CadetBlue4",
	"*logoLabel*background: White",
	"*foreground: Black",
	"*background: Grey",
	"*aboutDialog*fontList:	-*-times-bold-i-*--24-240-*=chset1, -*-times-bold-i-*--12-120-*=chset2, -*-helvetica-medium-r-*--10-100-*=chset3, fixed",
	"*fontList: -*-helvetica-medium-r-*--10-100-*",
	"*highlightThickness: 2",
	"*highlightColor: DarkOrchid",
	"*borderWidth: 0",
	NULL,
};

Widget  	toplevel,		/* Widget ids */
		mainform,
		frame,
		mixbox,
		text,
		exitbtn,
		aboutbtn,
		predefbtn,
		label[NPRI],
		slider[NPRI],
		predefform,
		list,
		sep,
		okbtn,
		aboutbox,
		logolbl;

appdata_t	app_data;		/* Application resources */

unsigned long	pixels[NPRI + 1];	/* Pixel values */
XColor		colors[NPRI + 1];	/* X color structures */
GC		mixbox_gc;		/* Graphics context for drawing */
Display		*display;		/* X display pointer */
int		screen;			/* Screen number */

colorent_t	*chead;			/* Pre-defined color list head */
int		cfactor[NPRI];		/* Pixel value conversion factor */
char		strbuf[128];		/* General purpose string buffer */
char		errmsg[128];		/* Error message string buffer */


/*
 * do_exit
 *	Callback routine for the exit button
 */
/*ARGSUSED*/
void
do_exit(Widget w, XtPointer client_data, XtPointer call_data)
{
	exit(0);
}


/*
 * chg_color
 *	Callback routine for the slider widgets.
 */
/*ARGSUSED*/
void
chg_color(Widget w, XtPointer client_data, XtPointer call_data)
{
	int			n = (int) client_data;
	XmScaleCallbackStruct	*p =
		(XmScaleCallbackStruct *)(void *) call_data;

	XmListDeselectAllItems(list);

	/* Change colorcell pixel values in response to slider movement */
	switch (n) {
	case RED:
		colors[MIX].red = colors[RED].red =
			p->value * 0xffff / SCALE_MAX;
		break;
	case GREEN:
		colors[MIX].green = colors[GREEN].green =
			p->value * 0xffff / SCALE_MAX;
		break;
	case BLUE:
		colors[MIX].blue = colors[BLUE].blue =
			p->value * 0xffff / SCALE_MAX;
		break;
	}
	XStoreColor(display, DefaultColormap(display, screen), &colors[n]);
	XStoreColor(display, DefaultColormap(display, screen), &colors[MIX]);

	/* Update color name display */
	sprintf(strbuf, "#%04X%04X%04X",
		colors[MIX].red, colors[MIX].green, colors[MIX].blue);
	XmTextSetString(text, strbuf);
	XmTextSetInsertionPosition(text, strlen(strbuf));
}


/*
 * redraw_mixbox
 *	Event handler routine for the mixbox widget.
 */
/*ARGSUSED*/
void
redraw_mixbox(Widget w, XtPointer client_data, XEvent *event)
{
	Window			mixboxwin = XtWindow(mixbox);
	unsigned int		dummy,
				mixbox_width,
				mixbox_height;
	static Boolean		first = True;
	static unsigned int	color_x,
				color_y,
				color_width,
				color_height;

	/* First time we get any event, initialize the color region location */
	if (first) {
		first = False;
		XGetGeometry(display, mixboxwin, (Window *) &dummy,
			     (int *) &dummy, (int *) &dummy,
			     &mixbox_width, &mixbox_height, &dummy, &dummy);
		
		color_width = mixbox_width * 80 / 100;
		color_height = mixbox_height * 80 / 100;
		color_x = (mixbox_width - color_width) / 2;
		color_y = (mixbox_height - color_height) / 2;
	}

	/* Handle specified events */
	switch (event->type) {
	case Expose:
		/* Redraw color region */
		if (event->xexpose.window == mixboxwin &&
		    event->xexpose.count == 0) {
			XClearWindow(display, mixboxwin);
			XFillRectangle(display, mixboxwin, mixbox_gc,
				       color_x, color_y,
				       color_width, color_height);
		}
		break;
	case ConfigureNotify:
		XGetGeometry(display, mixboxwin, (Window *) &dummy,
			     (int *) &dummy, (int *) &dummy,
			     &mixbox_width, &mixbox_height, &dummy, &dummy);

		color_width = mixbox_width * 80 / 100;
		color_height = mixbox_height * 80 / 100;
		color_x = (mixbox_width - color_width) / 2;
		color_y = (mixbox_height - color_height) / 2;
		XClearWindow(display, mixboxwin);
		XFillRectangle(display, mixboxwin, mixbox_gc,
			       color_x, color_y, color_width, color_height);
		break;
	default:
		/* "Don't care" events */
		break;
	}
}


/*
 * text_vfy
 *	Callback routine for text widget to verify user input
 */
/*ARGSUSED*/
void
text_vfy(Widget w, XtPointer client_data, XtPointer call_data)
{
	XmTextVerifyCallbackStruct	*p =
		(XmTextVerifyCallbackStruct *)(void *) call_data;

	if (p->reason != XmCR_MODIFYING_TEXT_VALUE)
		return;

	p->doit = True;

	if (p->startPos != p->endPos)
		return;

	switch (p->text->format) {
	case FMT8BIT:
		if (p->text->length > 1)
			return;

		if (p->currInsert > 13) {
			p->doit = False;
			return;
		}

		if (p->currInsert == 0 && p->text->ptr[0] != '#') {
			p->doit = False;
			return;
		}

		if (p->currInsert > 0 && !isxdigit(p->text->ptr[0])) {
			p->doit = False;
			return;
		}

		if (islower(p->text->ptr[0]))
			p->text->ptr[0] = toupper(p->text->ptr[0]);

		break;

	case FMT16BIT:
	default:
		/* Don't know how to handle 16-bit character set yet */
		p->doit = False;
		return;
	}
}


/*
 * text_chg
 *	Callback routine for the text widget to handle user input
 */
/*ARGSUSED*/
void
text_chg(Widget w, XtPointer client_data, XtPointer call_data)
{
	XmScaleCallbackStruct	cb;
	XmAnyCallbackStruct	*p =
		(XmAnyCallbackStruct *)(void *) call_data;
	char			*cp;
	unsigned int		rval,
				gval,
				bval;

	if (p->reason != XmCR_ACTIVATE)
		return;

	XmListDeselectAllItems(list);

	if ((cp = XmTextGetString(w)) == NULL) {
		XBell(display, 50);
		return;
	}

	if (cp[0] != '#') {
		XBell(display, 50);
		return;
	}

	cp++;
	switch (strlen(cp)) {
	case 12:
		if (sscanf(cp, "%04X%04X%04X\n", &rval, &gval, &bval) != 3) {
			XBell(display, 50);
			return;
		}
		break;
	case 9:
		if (sscanf(cp, "%03X%03X%03X\n", &rval, &gval, &bval) != 3) {
			XBell(display, 50);
			return;
		}
		rval <<= 4;
		gval <<= 4;
		bval <<= 4;
		break;
	case 6:
		if (sscanf(cp, "%02X%02X%02X\n", &rval, &gval, &bval) != 3) {
			XBell(display, 50);
			return;
		}
		rval <<= 8;
		gval <<= 8;
		bval <<= 8;
		break;
	case 3:
		if (sscanf(cp, "%01X%01X%01X\n", &rval, &gval, &bval) != 3) {
			XBell(display, 50);
			return;
		}
		rval <<= 12;
		gval <<= 12;
		bval <<= 12;
		break;
	default:
		XBell(display, 50);
		return;
	}

	cb.reason = XmCR_VALUE_CHANGED;

	cb.value = rval * SCALE_MAX / 0xffff;
	XmScaleSetValue(slider[RED], cb.value);
	colors[MIX].red = colors[RED].red = rval;
	XStoreColor(display, DefaultColormap(display, screen), &colors[RED]);

	cb.value = gval * SCALE_MAX / 0xffff;
	XmScaleSetValue(slider[GREEN], cb.value);
	colors[MIX].green = colors[GREEN].green = gval;
	XStoreColor(display, DefaultColormap(display, screen), &colors[GREEN]);

	cb.value = bval * SCALE_MAX / 0xffff;
	XmScaleSetValue(slider[BLUE], cb.value);
	colors[MIX].blue = colors[BLUE].blue = bval;
	XStoreColor(display, DefaultColormap(display, screen), &colors[BLUE]);

	XStoreColor(display, DefaultColormap(display, screen), &colors[MIX]);
}


/*
 * do_popup
 *	Callback routine to pop-up a subwindow
 */
void
do_popup(Widget w, XtPointer client_data, XtPointer call_data)
{
	Widget	target = (Widget)(void *) client_data;

	if (XtIsManaged(target))
		XtUnmanageChild(target);
	else
		XtManageChild(target);
}


/*
 * do_popdown
 *	Callback routine to pop-down a subwindow
 */
/*ARGSUSED*/
void
do_popdown(Widget w, XtPointer client_data, XtPointer call_data)
{
	Widget	target = (Widget)(void *) client_data;

	if (XtIsManaged(target))
		XtUnmanageChild(target);

	if (target == predefform)
		XmListDeselectAllItems(list);
}


/*
 * prefdef_sel
 *	Callback routine to handle list selection
 */
/*ARGSUSED*/
void
predef_sel(Widget w, XtPointer client_data, XtPointer call_data)
{
	XmScaleCallbackStruct	cb;
	XmListCallbackStruct	*p =
		(XmListCallbackStruct *)(void *) call_data;
	colorent_t		*q;
	unsigned int		rval,
				gval,
				bval;

	if (p->reason != XmCR_BROWSE_SELECT)
		return;

	cb.reason = XmCR_VALUE_CHANGED;

 	for (q = chead; q != NULL; q = q->next) {
		if (q->index != p->item_position)
			continue;

		rval = q->red * cfactor[RED];
		cb.value = rval * SCALE_MAX / 0xffff;
		XmScaleSetValue(slider[RED], cb.value);
		colors[MIX].red = colors[RED].red = rval;
		XStoreColor(display, DefaultColormap(display, screen),
			    &colors[RED]);

		gval = q->green * cfactor[GREEN];
		cb.value = gval * SCALE_MAX / 0xffff;
		XmScaleSetValue(slider[GREEN], cb.value);
		colors[MIX].green = colors[GREEN].green = gval;
		XStoreColor(display, DefaultColormap(display, screen),
			    &colors[GREEN]);

		bval = q->blue * cfactor[BLUE];
		cb.value = bval * SCALE_MAX / 0xffff;
		XmScaleSetValue(slider[BLUE], cb.value);
		colors[MIX].blue = colors[BLUE].blue = bval;
		XStoreColor(display, DefaultColormap(display, screen),
			    &colors[BLUE]);

		XStoreColor(display, DefaultColormap(display, screen),
			    &colors[MIX]);

		/* Update color name display */
		sprintf(strbuf, "#%04X%04X%04X",
			colors[MIX].red, colors[MIX].green, colors[MIX].blue);
		XmTextSetString(text, strbuf);
		XmTextSetInsertionPosition(text, strlen(strbuf));

		break;
	}
}


/*
 * bm_to_px
 *	Convert a bitmap into a pixmap
 */
Pixmap
bm_to_px(Widget w, void *bits, int width, int height, int depth)
{
	Window		window	 = XtWindow(w);
	GC		pixmap_gc;
	XGCValues	val;
	Pixmap		tmp_bitmap;
	static Pixmap	ret_pixmap;

	tmp_bitmap = XCreateBitmapFromData(
		display, window, (char *) bits, width, height
	);
	if (tmp_bitmap == (Pixmap) NULL)
		return ((Pixmap) NULL);

	if (depth == 1) {
		ret_pixmap = tmp_bitmap;
		return (ret_pixmap);
	}

	/* Create pixmap with depth */
	ret_pixmap = XCreatePixmap(display, window, width, height, depth);
	if (ret_pixmap == (Pixmap) NULL)
		return ((Pixmap) NULL);

	/* Allocate colors for pixmap if on color screen */
	/* Get pixmap color configuration */
	XtVaGetValues(w, XmNforeground, &val.foreground,
		      XmNbackground, &val.background, NULL);

	/* Create GC for pixmap */
	pixmap_gc = XCreateGC(display, window,
			      GCForeground | GCBackground, &val);
		
	/* Copy bitmap into pixmap */
	XCopyPlane(display, tmp_bitmap, ret_pixmap, pixmap_gc,
		   0, 0, width, height, 0, 0, 1);

	/* No need for the bitmap any more, so free it */
	XFreePixmap(display, tmp_bitmap);

	return (ret_pixmap);
}


/*
 * predef_setup
 *	Set up pre-defined colors list
 */
Boolean
predef_setup(void)
{
	FILE		*fp;
	colorent_t	*cur = NULL,
			*p;
	int		index = 0;
	unsigned int	rval,
			gval,
			bval;
	char		buf[128],
			name[64];
	XmString	xs;

	if (app_data.rgbfile == NULL)
		return False;

	sprintf(errmsg, "%s: Cannot open %s", PROGNAME, app_data.rgbfile);
	if ((fp = fopen(app_data.rgbfile, "r")) == NULL) {
		perror(errmsg);
		return False;
	}

	while (fgets(buf, sizeof(buf), fp) != NULL) {
		if (buf[0] == '!' || buf[0] == '*' || buf[0] == '#' ||
		    buf[0] == '\n' || buf[0] == '\0')
			/* Comment or blank line */
			continue;

		if (sscanf(buf, "%u %u %u %[^\n]\n",
			   &rval, &gval, &bval, name) != 4)
			continue;

		if (cur != NULL && rval == cur->red &&
		    gval == cur->green && bval == cur->blue)
			continue;

		p = (colorent_t *) XtMalloc(sizeof(colorent_t));

		if (chead == NULL)
			chead = p;
		else
			cur->next = p;

		cur = p;
		p->index = ++index;
		p->red = rval;
		p->green = gval;
		p->blue = bval;
		p->next = NULL;

		xs = XmStringCreateSimple(name);

		XmListAddItemUnselected(list, xs, index);

		XmStringFree(xs);

		if (cfactor[RED] == 0 && strcmp(name, "red") == 0) {
			if (rval != 0)
				cfactor[RED] = 0xffff / rval;
			else
				cfactor[RED] = 0xffff / gval;
		}
		else if (cfactor[GREEN] == 0 && strcmp(name, "green") == 0) {
			if (gval != 0)
				cfactor[GREEN] = 0xffff / gval;
			else
				cfactor[GREEN] = 0xffff / rval;
		}
		else if (cfactor[BLUE] == 0 && strcmp(name, "blue") == 0) {
			if (bval != 0)
				cfactor[BLUE] = 0xffff / bval;
			else
				cfactor[BLUE] = 0xffff / rval;
		}
	}

	/* Just in case the rgb.txt file is messed up... */
	if (cfactor[RED] == 0)
		cfactor[RED] = 1;
	if (cfactor[GREEN] == 0)
		cfactor[GREEN] = 1;
	if (cfactor[BLUE] == 0)
		cfactor[BLUE] = 1;

	fclose(fp);
	return True;
}


/*
 * about_setup
 *	Set up the about popup message box
 */
void
about_setup(void)
{
	XmString	xs_progname,
			xs_desc,
			xs_info,
			xs_tmp,
			xs;
	Pixmap		px;

	xs_progname = XmStringCreateLtoR(PROGNAME, CHSET1);

	sprintf(strbuf, "   v%s%s PL%d\n%s\n%s\n%s\n\n",
		VERSION,
		VERSION_EXT,
		PATCHLEVEL,
		"Motif(tm) Color Mixer",
		"Copyright (C) 1994  Ti Kan",
		"E-mail: ti@amb.org");

	xs_desc = XmStringCreateLtoR(strbuf, CHSET2);

	sprintf(strbuf, "%s\n%s\n",
		"This is free software and comes with no warranty.",
		"See the GNU General Public License for details.");

	xs_info = XmStringCreateLtoR(strbuf, CHSET3);

	/* Set the dialog box message */
	xs_tmp = XmStringConcat(xs_progname, xs_desc);
	xs = XmStringConcat(xs_tmp, xs_info);

	XtVaSetValues(aboutbox, XmNmessageString, xs, NULL);
	XmStringFree(xs_progname);
	XmStringFree(xs_desc);
	XmStringFree(xs_info);
	XmStringFree(xs_tmp);
	XmStringFree(xs);

	/* Set up logo pixmap */
	px = bm_to_px(logolbl, xmcolor_bits, xmcolor_width, xmcolor_height, 1);
	XtVaSetValues(toplevel, XmNiconPixmap, px, NULL);

	px = bm_to_px(logolbl, xmcolor_bits, xmcolor_width, xmcolor_height,
		      DefaultDepth(display, DefaultScreen(display)));
	XtVaSetValues(logolbl, XmNlabelPixmap, px, NULL);
	XtVaSetValues(aboutbox, XmNsymbolPixmap, px, NULL);
}


/*
 * create_widgets
 *	Create all program widgets and do basic widget setup
 */
void
create_widgets(void)
{
	int		i,
			n,
			top,
			bot;
	XmString	xs;
	Arg		args[20];

        /* Create form widget as a holder of other widgets */
	i = 0;
	XtSetArg(args[i], XmNwidth, 400); i++;
	XtSetArg(args[i], XmNheight, 160); i++;
        mainform = XmCreateForm(toplevel, "mainForm", args, i);
	XtManageChild(mainform);

	/* Create pre-defined colors popup form dialog */
	xs = XmStringCreateSimple("Color Selection");
	i = 0;
	XtSetArg(args[i], XmNwidth, 250); i++;
	XtSetArg(args[i], XmNheight, 440); i++;
	XtSetArg(args[i], XmNautoUnmanage, False); i++;
	XtSetArg(args[i], XmNdialogStyle, XmDIALOG_MODELESS); i++;
	XtSetArg(args[i], XmNdialogTitle, xs); i++;
	predefform = XmCreateFormDialog(toplevel, "popupForm", args, i);
	XmStringFree(xs);

	/* Create about pop-up message box */
	xs = XmStringCreateSimple("About");
	i = 0;
	XtSetArg(args[i], XmNautoUnmanage, True); i++;
	XtSetArg(args[i], XmNdialogStyle, XmDIALOG_MODELESS); i++;
	XtSetArg(args[i], XmNdialogTitle, xs); i++;
	aboutbox = XmCreateInformationDialog(toplevel, "aboutDialog", args, i);
	XtUnmanageChild(
		XmMessageBoxGetChild(aboutbox, XmDIALOG_HELP_BUTTON)
	);
	XtUnmanageChild(
		XmMessageBoxGetChild(aboutbox, XmDIALOG_CANCEL_BUTTON)
	);
	XmStringFree(xs);

        /* Create colormix output frame */
	i = 0;
	XtSetArg(args[i], XmNleftAttachment, XmATTACH_POSITION); i++;
	XtSetArg(args[i], XmNrightAttachment, XmATTACH_POSITION); i++;
	XtSetArg(args[i], XmNtopAttachment, XmATTACH_POSITION); i++;
	XtSetArg(args[i], XmNbottomAttachment, XmATTACH_POSITION); i++;
	XtSetArg(args[i], XmNleftPosition, 65); i++;
	XtSetArg(args[i], XmNrightPosition, 95); i++;
	XtSetArg(args[i], XmNtopPosition, 5); i++;
	XtSetArg(args[i], XmNbottomPosition, 69); i++;
	XtSetArg(args[i], XmNshadowType, XmSHADOW_ETCHED_IN); i++;
	frame = XmCreateFrame(mainform, "mixBoxFrame", args, i);
	XtManageChild(frame);

        /* Create colormix output drawing area */
        mixbox = XmCreateDrawingArea(frame, "mixBox", NULL, 0);
	XtManageChild(mixbox);

	/* Add event handler for the mixbox */
        XtAddEventHandler(mixbox, ExposureMask | StructureNotifyMask,
			  False, (XtEventHandler) redraw_mixbox, NULL);

        /* Create text widget for text output */
	i = 0;
	XtSetArg(args[i], XmNleftAttachment, XmATTACH_POSITION); i++;
	XtSetArg(args[i], XmNrightAttachment, XmATTACH_POSITION); i++;
	XtSetArg(args[i], XmNtopAttachment, XmATTACH_POSITION); i++;
	XtSetArg(args[i], XmNbottomAttachment, XmATTACH_POSITION); i++;
	XtSetArg(args[i], XmNleftPosition, 65); i++;
	XtSetArg(args[i], XmNrightPosition, 95); i++;
	XtSetArg(args[i], XmNtopPosition, 75); i++;
	XtSetArg(args[i], XmNbottomPosition, 95); i++;
        XtSetArg(args[i], XmNeditable, True); i++;
        XtSetArg(args[i], XmNeditMode, XmSINGLE_LINE_EDIT); i++;
        XtSetArg(args[i], XmNcursorPositionVisible, True); i++;
        XtSetArg(args[i], XmNcursorPosition, 0); i++;
        text = XmCreateText(mainform, "textBox", args, i);
	XtManageChild(text);
        XtAddCallback(text, XmNmodifyVerifyCallback,
		      (XtCallbackProc) text_vfy, NULL);
        XtAddCallback(text, XmNactivateCallback,
		      (XtCallbackProc) text_chg, NULL);

        /* Create slider widgets */
	for (n = 0; n < NPRI; n++) {
		switch (n) {
		case RED:
			top = 5;
			bot = 25;
			break;
		case GREEN:
			top = 27;
			bot = 47;
			break;
		case BLUE:
			top = 49;
			bot = 69;
			break;
		}

		sprintf(strbuf, "slider%d", n);
		i = 0;
		XtSetArg(args[i], XmNleftAttachment, XmATTACH_POSITION); i++;
		XtSetArg(args[i], XmNrightAttachment, XmATTACH_POSITION); i++;
		XtSetArg(args[i], XmNtopAttachment, XmATTACH_POSITION); i++;
		XtSetArg(args[i], XmNbottomAttachment, XmATTACH_POSITION); i++;
		XtSetArg(args[i], XmNleftPosition, 5); i++;
		XtSetArg(args[i], XmNrightPosition, 50); i++;
		XtSetArg(args[i], XmNtopPosition, top); i++;
		XtSetArg(args[i], XmNbottomPosition, bot); i++;
		XtSetArg(args[i], XmNshowValue, True); i++;
		XtSetArg(args[i], XmNorientation, XmHORIZONTAL); i++;
		XtSetArg(args[i], XmNprocessingDirection, XmMAX_ON_RIGHT); i++;
		XtSetArg(args[i], XmNminimum, 0); i++;
		XtSetArg(args[i], XmNmaximum, SCALE_MAX); i++;
		slider[n] = XmCreateScale(mainform, strbuf, args, i);
		XtManageChild(slider[n]);

        	/* Register callbacks */
        	XtAddCallback(slider[n], XmNvalueChangedCallback,
			      (XtCallbackProc) chg_color, (XtPointer) n);
        	XtAddCallback(slider[n], XmNdragCallback,
			      (XtCallbackProc) chg_color, (XtPointer) n);

		/* Create color label */
		sprintf(strbuf, "label%d", n);
		i = 0;
		XtSetArg(args[i], XmNleftAttachment, XmATTACH_POSITION); i++;
		XtSetArg(args[i], XmNrightAttachment, XmATTACH_POSITION); i++;
		XtSetArg(args[i], XmNtopAttachment, XmATTACH_POSITION); i++;
		XtSetArg(args[i], XmNbottomAttachment, XmATTACH_POSITION); i++;
		XtSetArg(args[i], XmNleftPosition, 52); i++;
		XtSetArg(args[i], XmNrightPosition, 62); i++;
		XtSetArg(args[i], XmNtopPosition, top); i++;
		XtSetArg(args[i], XmNbottomPosition, bot); i++;
		XtSetArg(args[i], XmNlabelType, XmPIXMAP); i++;
		XtSetArg(args[i], XmNbackground, colors[n].pixel); i++;
		label[n] = XmCreateLabel(mainform, strbuf, args, i);
		XtManageChild(label[n]);
	}

	/* Exit pushbutton */
	xs = XmStringCreateSimple("Exit");
	i = 0;
	XtSetArg(args[i], XmNleftAttachment, XmATTACH_POSITION); i++;
	XtSetArg(args[i], XmNrightAttachment, XmATTACH_POSITION); i++;
	XtSetArg(args[i], XmNtopAttachment, XmATTACH_POSITION); i++;
	XtSetArg(args[i], XmNbottomAttachment, XmATTACH_POSITION); i++;
	XtSetArg(args[i], XmNleftPosition, 5); i++;
	XtSetArg(args[i], XmNrightPosition, 16); i++;
	XtSetArg(args[i], XmNtopPosition, 80); i++;
	XtSetArg(args[i], XmNbottomPosition, 95); i++;
	XtSetArg(args[i], XmNlabelString, xs); i++;
	exitbtn = XmCreatePushButton(mainform, "exitButton", args, i);
	XtManageChild(exitbtn);
	XmStringFree(xs);
        XtAddCallback(exitbtn, XmNactivateCallback,
		      (XtCallbackProc) do_exit, NULL);

	/* About popup pushbutton */
	xs = XmStringCreateSimple("About");
	i = 0;
	XtSetArg(args[i], XmNleftAttachment, XmATTACH_POSITION); i++;
	XtSetArg(args[i], XmNrightAttachment, XmATTACH_POSITION); i++;
	XtSetArg(args[i], XmNtopAttachment, XmATTACH_POSITION); i++;
	XtSetArg(args[i], XmNbottomAttachment, XmATTACH_POSITION); i++;
	XtSetArg(args[i], XmNleftPosition, 18); i++;
	XtSetArg(args[i], XmNrightPosition, 29); i++;
	XtSetArg(args[i], XmNtopPosition, 80); i++;
	XtSetArg(args[i], XmNbottomPosition, 95); i++;
	XtSetArg(args[i], XmNlabelString, xs); i++;
	aboutbtn = XmCreatePushButton(mainform, "aboutButton", args, i);
	XtManageChild(aboutbtn);
	XmStringFree(xs);
        XtAddCallback(aboutbtn, XmNactivateCallback,
		      (XtCallbackProc) do_popup, (XtPointer) aboutbox);

	/* Predefined popup pushbutton */
	xs = XmStringCreateSimple("Pre-defined");
	i = 0;
	XtSetArg(args[i], XmNleftAttachment, XmATTACH_POSITION); i++;
	XtSetArg(args[i], XmNrightAttachment, XmATTACH_POSITION); i++;
	XtSetArg(args[i], XmNtopAttachment, XmATTACH_POSITION); i++;
	XtSetArg(args[i], XmNbottomAttachment, XmATTACH_POSITION); i++;
	XtSetArg(args[i], XmNleftPosition, 42); i++;
	XtSetArg(args[i], XmNrightPosition, 62); i++;
	XtSetArg(args[i], XmNtopPosition, 80); i++;
	XtSetArg(args[i], XmNbottomPosition, 95); i++;
	XtSetArg(args[i], XmNlabelString, xs); i++;
	predefbtn = XmCreatePushButton(mainform, "preDefinedButton", args, i);
	XtManageChild(predefbtn);
	XmStringFree(xs);
        XtAddCallback(predefbtn, XmNactivateCallback,
		      (XtCallbackProc) do_popup, (XtPointer) predefform);

	/* Create scrolled list */
	i = 0;
	XtSetArg(args[i], XmNleftAttachment, XmATTACH_POSITION); i++;
	XtSetArg(args[i], XmNrightAttachment, XmATTACH_POSITION); i++;
	XtSetArg(args[i], XmNtopAttachment, XmATTACH_POSITION); i++;
	XtSetArg(args[i], XmNbottomAttachment, XmATTACH_POSITION); i++;
	XtSetArg(args[i], XmNleftPosition, 4); i++;
	XtSetArg(args[i], XmNrightPosition, 96); i++;
	XtSetArg(args[i], XmNtopPosition, 3); i++;
	XtSetArg(args[i], XmNbottomPosition, 90); i++;
	XtSetArg(args[i], XmNautomaticSelection, False); i++;
	XtSetArg(args[i], XmNselectionPolicy, XmBROWSE_SELECT); i++;
	XtSetArg(args[i], XmNlistSizePolicy, XmCONSTANT); i++;
	XtSetArg(args[i], XmNscrollBarDisplayPolicy, XmSTATIC); i++;
	XtSetArg(args[i], XmNscrolledWindowMarginWidth, 2); i++;
	list = XmCreateScrolledList(predefform, "colorList", args, i);
	XtManageChild(list);
        XtAddCallback(list, XmNbrowseSelectionCallback,
		      (XtCallbackProc) predef_sel, NULL);

	/* Create separator */
	i = 0;
	XtSetArg(args[i], XmNleftAttachment, XmATTACH_FORM); i++;
	XtSetArg(args[i], XmNrightAttachment, XmATTACH_FORM); i++;
	XtSetArg(args[i], XmNtopAttachment, XmATTACH_POSITION); i++;
	XtSetArg(args[i], XmNbottomAttachment, XmATTACH_NONE); i++;
	XtSetArg(args[i], XmNtopPosition, 91); i++;
	XtSetArg(args[i], XmNleftOffset, 1); i++;
	XtSetArg(args[i], XmNrightOffset, 1); i++;
	sep = XmCreateSeparator(predefform, "separator", args, i);
	XtManageChild(sep);

	/* Create OK button */
	xs = XmStringCreateSimple("OK");
	i = 0;
	XtSetArg(args[i], XmNleftAttachment, XmATTACH_POSITION); i++;
	XtSetArg(args[i], XmNrightAttachment, XmATTACH_POSITION); i++;
	XtSetArg(args[i], XmNtopAttachment, XmATTACH_POSITION); i++;
	XtSetArg(args[i], XmNbottomAttachment, XmATTACH_POSITION); i++;
	XtSetArg(args[i], XmNleftPosition, 40); i++;
	XtSetArg(args[i], XmNrightPosition, 60); i++;
	XtSetArg(args[i], XmNtopPosition, 93); i++;
	XtSetArg(args[i], XmNbottomPosition, 98); i++;
	XtSetArg(args[i], XmNlabelString, xs); i++;
	okbtn = XmCreatePushButton(predefform, "okButton", args, i);
	XtManageChild(okbtn);
	XmStringFree(xs);
        XtAddCallback(okbtn, XmNactivateCallback,
		      (XtCallbackProc) do_popdown, (XtPointer) predefform);

	/* Create logo label */
	i = 0;
	XtSetArg(args[n], XmNlabelType, XmPIXMAP); i++;
	XtSetArg(args[n], XmNmarginHeight, 0); i++;
	XtSetArg(args[n], XmNmarginWidth, 0); i++;
	XtSetArg(args[n], XmNhighlightThickness, 0); i++;
	XtSetArg(args[n], XmNwidth, xmcolor_width); i++;
	XtSetArg(args[n], XmNheight, xmcolor_height); i++;
	logolbl = XmCreateLabel(mainform, "logoLabel", args, i);
}


/*
 * usage
 *	Print command line syntax
 */
void
usage(void)
{
	fprintf(stderr, "Usage: %s [-rgbfile path]\n%s\n", PROGNAME,
		"Standard Xt Intrinsics and Motif options are supported.");
}


/*
 * main
 *	The main routine
 */
void
main(int argc, char **argv)
{
	int		n;
	XGCValues	val;
	XtAppContext	app;

        /* Initialize X toolkit */
        toplevel = XtVaAppInitialize(
		&app,
		"XMcolor",
		options, XtNumber(options),
		&argc, argv,
		fallbacks,
		NULL
	);

	/* Get application resources */
	XtVaGetApplicationResources(
		toplevel,
		(XtPointer) &app_data,
		resources,
		XtNumber(resources),
		NULL
	);

	/* Check command line for unknown arguments */
	if (argc > 1) {
		usage();
		exit(1);
	}

	/* Set display and screen */
	display = XtDisplay(toplevel);
	screen = DefaultScreen(display);

	/* Check color capability of server display */
	if (DisplayCells(display, screen) <= 2) {
		fprintf(stderr, "%s: Cannot run on non-color display.\n",
			PROGNAME);
		exit(1);
	}

	/* Allocate color cells */
	int err;
	XAllocColorCells(display, DefaultColormap(display, screen),
			      False, NULL, 0, pixels, NPRI + 1);
	if (err == BadMatch) {
		fprintf(stderr, "%s: Cannot allocate needed colors: %s\n",
			PROGNAME, strerror(errno));
		exit(2);
	}

	/* Initialize colorcells */
	for (n = 0; n < NPRI + 1; n++) {
		colors[n].pixel = pixels[n];
		colors[n].red = colors[n].green = colors[n].blue = 0;
		colors[n].flags = DoRed | DoGreen | DoBlue;
		XStoreColor(display, DefaultColormap(display, screen),
			    &colors[n]);
	}
	colors[RED].flags = DoRed;
	colors[GREEN].flags = DoGreen;
	colors[BLUE].flags = DoBlue;

	/* Create all widgets */
	create_widgets();

        /* Realize widgets */
        XtRealizeWidget(toplevel);

	/* Set up pre-defined color selections */
	if (!predef_setup())
		XtSetSensitive(predefbtn, False);

	/* Set up the about message box */
	about_setup();

	/* Set window title */
	strcpy(strbuf, "Motif Color Mixer");
	XStoreName(display, XtWindow(toplevel), strbuf);

	/* Create GC for drawing on the mixbox */
	val.foreground = colors[MIX].pixel;
	mixbox_gc = XCreateGC(display, XtWindow(mixbox), GCForeground, &val);

	/* Color name text */
	strcpy(strbuf, "#000000000000");
	XmTextSetString(text, strbuf);
	XmTextSetInsertionPosition(text, 13);

        /* Enter event handling loop in toolkit */
        XtAppMainLoop(app);

        exit(0);
}
