.\"
.\" @(#)xmcolor.man	1.1 94/12/05
.\"
.\"   xmcolor - Motif(tm) Color Mixer
.\"
.\"   Copyright (C) 1994  Ti Kan
.\"   E-mail: ti@amb.org
.\"
.\"   This program is free software; you can redistribute it and/or modify
.\"   it under the terms of the GNU General Public License as published by
.\"   the Free Software Foundation; either version 2 of the License, or
.\"   (at your option) any later version.
.\"
.\"   This program is distributed in the hope that it will be useful,
.\"   but WITHOUT ANY WARRANTY; without even the implied warranty of
.\"   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.\"   GNU General Public License for more details.
.\"
.\"   You should have received a copy of the GNU General Public License
.\"   along with this program; if not, write to the Free Software
.\"   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
.\"
.TH XMCOLOR 1 "5 December 1994" "v1.0"

.SH "NAME"
xmcolor \- Color mixer utility for X11/Motif

.SH "SYNOPSIS"
.B xmcolor
.nh
[\fItoolkitoption ...] [-\fIrgbfile\fP path]

.SH "DESCRIPTION"
.I Xmcolor
is a color mixer utility that is useful as an interactive tool to
set up the color resources of other X applications.  You can browse
the color database of pre-defined colors, or mix a custom color
using the red, green and blue sliders.  The color name (in RGB format)
is displayed in a text field and can be cut-and-paste'ed into
a text editor (such as when editing the app-defaults file of another
X application).
.P
You can also edit the RGB format color name string directly in the
text field and see the resultant color.  See the X(1) manual page
for a description of the RGB format color name convention.
.P
The resultant color is shown in a large square color box, and the
strengths of the individual red, green and blue components
are also displayed in separate smaller color boxes.
.P
While the use of a mouse is natural with
.I xmcolor,
all functionality can also be operated via the keyboard.
This is in conformance to the guidelines published in the
.I OSF/Motif Style Guide
from the Open Software Foundation.

.SH "OPTIONS"
All standard \fIXt Intrinsics\fR toolkit options are supported.
In addition,
.I xmcolor
supports the following options:
.TP
.I \-rgbfile\ path
.br
Specifies the path name to the color database file.  If this option is
not used, the default device used is specified in the
.I xmcolor
XMcolor*rgbFilePath X resource.  This is typically
.I /usr/lib/X11/rgb.txt.

.SH "X RESOURCES"
.I Xmcolor
has several adjustable X resources to customize its look and feel.
Notably, the colors of virtually every feature on
\fIxmcolor\fP's windows can be changed, as well as the text fonts.
.PP
The resource names and their defaults can be found in the
\fBLIBDIR\fI/app-defaults/XMcolor\fR
file (where LIBDIR is typically \fI/usr/lib/X11\fP).
It is not recommended
that you change values in the \fBLIBDIR\fI/app-defaults/XMcolor\fR file,
unless you want the changes to be forced upon all users of
.I xmcolor
on the system.  Instead, make a copy of this file, change the copy as you
see fit, then place it in your home directory.  Your custom resource
settings will then override the defaults when
.I xmcolor
is subsequently started.  Alternatively, you may also place specific
resources you wish to override in the
.I .Xdefaults
file in your home directory.

.SH "SEE ALSO"
X(1).

.SH "AUTHOR"
Ti Kan (\fIti@amb.org\fR)
.br
AMB Research Laboratories, Sunnyvale, CA, U.S.A.
.PP
Comments, suggestions, and bug reports are always welcome.
