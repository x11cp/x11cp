.TH xwpick 1 "10 September 1994"
.IX xwpick
.SH NAME
\fIxwpick\fP \- pick images from an X11-screen and store in files
.SH SYNOPSIS
\fBxwpick\fP [\fB\-local\fP] [\fB\-window id\fP] [\fB\-gray\fP] [\fB\-reverse\fP] [\fB\-pause\fP] [\fB\-format frmt\fP] [<\fBfile\fP>]
.SH DESCRIPTION
\fIXwpick\fP lets you pick an image from an arbitrary window or rectangular
area of an X11-server and write it to a file in a variety of formats. 
.PP
The output format is defined whether by the \fB\-format\fP option or
by the extension in the file name. Possible formats/extentions are:
.PP
.TP 8
.B "ps"
An Encapsulated PostScript file with a compressed image. The image is
centered, rotated and scaled to fill the maximum space on a page. It
is displayed in color on viewers and printers that support color Postscript,
otherwise it is displayed as grayscale. This format is convenient for
transparency preparation.
.PP
.TP 8
.B "eps"
Also an Encapsulated PostScript file with a compressed image, but the image
is only centered, not rotated and scaled. It is intended for insertion
into a document.
.PP
.TP 8
.B "epsi"
The same as \fBeps\fP, but contains a black and white preview.  
.PP
.TP 8
.B "gif"
Graphics Interchange Format (GIF). Use this format when you want to keep files
or transfer them to other computers. It is also convenient for visualisations,
for example, with \fIxv\fP by John Bradley. 
.PP
.TP 8
.B "pcx"
PCX format for IBM PC.
.PP
.TP 8
.B "pict"
PICT format for Macintosh. It is intended for image transfer on Macintosh,
but GIF format is also convenient for this purpose
(see Macintosh \fIGIFConverter\fP by Kevin A. Mitchell). 
.PP
.TP 8
.B "ppm"
PPM format from the PBM Plus library by Jef Poskanzer. Use it and a routine
from the PBM Plus library if you wish to have the image in a format not
mentioned above.
.PP
The main feature of \fIxwpick\fP is that it uses the Lempel-Ziv
Welch (LZW) compression scheme for image encoding in Postscript, thus
producing very compact files (4-5 times less than files produced
with Run-Length encoding (RLE) and 10-20 times less than files produced
without compression)
.SH HOW IT WORKS
.PP
When \fIxwpick\fP is invoked, the user sees a blinking rectangle
surrounding the contents of the window in which the mouse pointer is
currently placed. When the mouse is moved to a different window,
the perimeter automatically changes to the size of the new window.
If the mouse pointer is placed on the Window Manager border of a window,
then the blinking rectangle will surround the window together with
the Window Manager border. To select the image inside the blinking
rectangle it is sufficient to click the left mouse button.
.PP
If a user-defined perimeter is required, then hold down the left mouse
button to choose the first corner of the perimeter and then drag the
mouse to define the opposite corner. The blinking rectangle will expand
with the movement of the mouse.
.PP
If it is required to produce some changes inside the selected area just
before outputing to a file, for example, to change the palette or display
a pop-up menu, then the \fB\-pause\fP option can be used.
.SH OPTIONS
.PP
.TP 14
.B "\-local"
When \fB\-local\fP is in the parameter list, \fIxwpick\fP picks an image from
the window under the mouse pointer. This option is intended to pick images
from pop-up menus, which are on the screen only when a mouse button is
pressed and disappear immediately after the button is released.
.TP 14
.B "\-window id"
Pick an image from the window with integer identifier equal to \fBid\fP.
The identifier for a window can be obtained with the X Window program
\fIxwininfo\fP. To pick the entire screen (root window) the user may use
the word \fBroot\fP as an identifier.
.TP 14
.B "\-gray"
Transfer the image to grayscale. This option can be used to optimize
output on level 1 gray scale PostScript printers.
.TP 14
.B "\-reverse"
Transfer the image to reverse colors. This option can help you
to save the toner on your printer in case when the image is too dark.
.TP 14
.B "\-pause"
Do not output the image till the <SPACE> bar will be pressed. This
option allows to use the mouse to produce some changes inside the
selected image just before outputing to a file.
.TP 14
.B "\-format frmt"
Set output format. The format is defined by \fBfrmt\fP string. If this
option is omited, then the output format is defined by the extension in
the file name. Possible \fBfrmt\fP strings/extensions are:
\fBps\fP, \fBeps\fP, \fBepsi\fP, \fBgif\fP, \fBpcx\fP, \fBpict\fP, \fBppm\fP.
In case when \fB\-format\fP is in the parameter list and a file name is
omited the output is directed to the standard output.  
.SH EXAMPLES
.TP 8
\(bu
xwpick
.PP
This is the simplest form of use. You will be prompted to input
a file name.
.TP 8
\(bu
xwpick image.ppm
.br
ppmtoxpm image  >image.xpm
.PP
This is an example of how to get an image in X11 pixmap format.
First you pick the image from the screen into the file \fIimage.ppm\fP
and then convert it using the \fIppmtoxpm\fP routine from the PBM PLUS
library.
.TP 8
\(bu
xwpick \-local menu.epsi
.PP
This is an example of how to pick the image of a pop-up menu
window and store it in a file as encapsulated Postscript with
preview.
.TP 8
\(bu
xwininfo
.br
xwpick \-w 0x8000c1 \-g clock.ps
.PP
This is an example of how to pick an image from the window
by the window's identifier. First you find the identifier using
the \fIxwininfo\fP command and then pick an image from the window 
and store it in a Postscript file as a grayscale image.
.TP 8
\(bu
xwpick \-window root \-format ps | lpr
.PP
This is an example of how to send an image of the root window directly
to the printer.
.SH AUTHOR
Evgeni Chernyaev           chernaev@mx.ihep.su
.SH SEE ALSO
xwd(1), xv(1), xgrabsc(1), XtoPS(1), xwininfo(1), ppm(5)
.SH COPYRIGHT
Copyright (C) 1993, 1994 by Evgeni Chernyaev.
.\"
.\" Permission to use, copy, modify, and distribute this software and its
.\" documentation for non-commercial purpose is hereby granted without fee,
.\" provided that the above copyright notice appear in all copies and that
.\" both the copyright notice and this permission notice appear in supporting
.\" documentation.  This software is provided "as is" without express or
.\" implied warranty.
.\"
