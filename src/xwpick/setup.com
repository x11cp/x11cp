$!**********************************************************************
$!                                                                     *
$! Name: SETUP.COM                                                     *
$!                                                                     *
$! Function: Make symbol definitions for XWPICK 2.20 under OpenVMS.    *
$!                                                                     *
$! Keep this file in the same directory where the binaries are kept.   *
$!                                                                     *
$!**********************************************************************
$ WRITE SYS$OUTPUT "SETting UP XWPICK (v 2.20)..."
$ THIS_PATH = F$ELEMENT (0, "]", F$ENVIRONMENT ("PROCEDURE")) + "]"
$ XWPICK :== $'THIS_PATH'XWPICK.EXE
$!
$!  Put the help library into the next available
$!  help library slot: HLP$LIBRARY_...
$!
$ LIB = "HLP$LIBRARY"
$ X = F$TRNLNM (LIB, "LNM$PROCESS")
$ IF (X .eqs. "") THEN GOTO INSERT
$ If (X .eqs. "''THIS_PATH'XWPICK.HLB") THEN GOTO EXIT
$ BASE = LIB + "_"
$ N = 1
$NEXTLIB:
$   LIB := 'BASE''N'
$   X = F$TRNLNM (LIB, "LNM$PROCESS")
$   IF (X .eqs. "") THEN GOTO INSERT
$   IF (X .eqs. "''THIS_PATH'XWPICK.HLB") THEN GOTO EXIT
$   N = N + 1
$   GOTO NEXTLIB
$INSERT:
$   DEFINE 'LIB' 'THIS_PATH'XWPICK.HLB
$EXIT:
$   EXIT
