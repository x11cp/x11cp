#### xrolodex.res ####   directory of resource names for `xrolodex'

#### Most users will not need, or care about, the contents of
#### this file!

                   Name                           Class
                   ----                           -----
xrolodex
--------
Application
resources:         viewportMenuBar                ViewportMenuBar
                   viewportRows                   ViewportRows
                   viewportColumns                ViewportColumns
                   indexRows                      IndexRows
                   indexColumns                   IndexColumns
                   entryDelimiter                 EntryDelimiter
                   directoryMask                  DirectoryMask


                   mainWindow                     Form
                   databaseLabel                  Label
                   mainMenuBar                    RowColumn
                   menuFileButton                 CascadeButton
                   menuEditButton                 CascadeButton
                   menuFindButton                 CascadeButton
                   menuOpenButton                 PushButton
                   menuSaveButton                 PushButton
                   menuSaveAsButton               PushButton
                   menuQuitButton                 PushButton
                   menuExitButton                 PushButton
                   menuNewButton                  PushButton
                   menuCopyButton                 PushButton
                   menuDeleteButton               PushButton
                   menuUndeleteButton             PushButton
                   menuFindEntryButton            PushButton
                   menuIndexButton                PushButton
                   fileSelectionDialog            MessageBox
                   findShell                      topLevelShell
                   findBox                        Form
                   findLabel                      Label
                   findText                       Text
                   findRadioBox                   RowColumn
                   findInsensitiveButton          ToggleButton
                   findSensitiveButton            ToggleButton
                   findButtonBox                  RowColumn
                   findForwardButton              PushButton
                   findReverseButton              PushButton
                   findDismissButton              PushButton
                   helpShell                      topLevelShell
                   helpOuterBox                   PanedWindow
                   helpWindow                     ScrolledWindow
                   helpText                       Text
                   helpButtonbox                  RowColumn
                   helpDismissButton              PushButton


editor/viewport
---------------
                   entryEditWindow (Frame)        Editor (Frame)
                   editBox                        Form
depending on
the value of
viewportMenuBar,
either             menuBar                        RowColumn
or                 popupMenu                      RowColumn

                   editWindow                     ScrolledWindow
                   edit                           Text
                   
                   replaceShell                   topLevelShell
                   entryEditWindow (Frame)        Editor (Frame)
                   replacePane                    PanedWindow
                   replaceBox                     Form
                   replaceLabel                   Label
                   replaceFindBox                 Form
                   replaceReplaceBox              Form
                   replaceFindLabel               Label
                   replaceReplaceLabel            Label
                   replaceFindText                Text
                   replaceReplaceText             Text
                   replaceButtonBox               RowColumn
                   replaceFindButton              PushButton
                   replaceReplaceButton           PushButton
                   replaceDismissButton           PushButton


popup index
-----------
                   listShellShell                 topLevelShell
                   entryIndex (Frame)             ListShell (Frame)
                   pane                           PanedWindow
                   port                           ScrolledWindow
                   list                           List
                   box                            RowColumn
                   dismiss                        PushButton


control panel
-------------
                   entryControlPanel (Frame)      CtrlPanel (Frame)
                   ctrlBox                        RowColumn
                   separator0                     Separator
                   separator1                     Separator
                   ...
                   First                          PushButton
                   Last                           PushButton
                   ...
                   Index                          PushButton


dialog boxes
------------
                   openUnsavedDialog (Frame)      Dialog (Frame)
                   quitUnsavedDialog (Frame)      Dialog (Frame)
                   overwriteDialog (Frame)        Dialog (Frame)
                   messageDialog (Frame)          Dialog (Frame)
