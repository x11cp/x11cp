/**** listshell.c ****/

/**********************************************************************
*  Copyright (c) 1991, 1992 Iris Computing Laboratories.
*
*  This software is provided for demonstration purposes only.  As
*  freely-distributed, modifiable source code, this software carries
*  absolutely no warranty.  Iris Computing Laboratories disclaims
*  all warranties for this software, including any implied warranties
*  of merchantability and fitness, and shall not be liable for
*  damages of any type resulting from its use.
*  Permission to use, copy, modify, and distribute this source code
*  for any purpose and without fee is hereby granted, provided that
*  the above copyright and this permission notice appear in all copies
*  and supporting documentation, and provided that Iris Computing
*  Laboratories not be named in advertising or publicity pertaining
*  to the redistribution of this software without specific, written
*  prior permission.
**********************************************************************/


/**************************************************************************
A `ListShell' object is a Motif XmList widget in a shell.  The
application can specify whether the pop-up shell should disappear
or stay active when the user selects an item from the list.
Because it's in a pop-up shell, the application must call the
list-shell realize function.

Sample usage:

#include "listshell.h"
...
...
void main(argc, argv)
int argc;
char *argv[];
{
	void ListSelection(), ...;
	Widget topLevel, ...;
	ListShell list1, list2;
	Arg args[10];
	int i;
	static char *seasons[] = {"Winter", "Spring", "Summer", "Fall", NULL};
	XtAppContext app;

	topLevel = XtAppInitialize(&app, "ApplicationClass",
		(XrmOptionDescList) NULL, 0,
		&argc, argv, (String *) NULL, (ArgList) NULL, 0);
	list1 = listShell_create(topLevel, "ApplicationClass", "listShell1",
		seasons, XmSTRING_DEFAULT_CHARSET, 0, 4, listShell_STAY_UP,
		listShell_WIN_MGR_DECOR);
	listShell_add_callback(list1, ListSelection, 1);
	list2 = listShell_create(topLevel, "ApplicationClass", "listShell2",
		seasons, XmSTRING_DEFAULT_CHARSET, 0, 2, listShell_NO_STAY_UP,
		listShell_MIN_WIN_MGR_DECOR);
	listShell_add_callback(list2, ListSelection, 2);
	...
	...
	XtRealizeWidget(topLevel);
	listShell_realize(list1);
	listShell_realize(list2);
	...
	...
**************************************************************************/


#include "listshell.h"


/*
Private callback functions:
*/

static void ListShellDismiss();


/*
Private support functions:
*/

static int listShell_allocate_list();


/*
Public functions:
*/

/*
listShell_create() creates a `ListShell' object.  The
application can specify the type of window manager
behavior, among other parameters.
*/

ListShell listShell_create(ref_wid, app_class, instance_name,
	items, char_set, num_cols, num_rows, stay_up_behavior,
	win_mgr_behavior)
Widget ref_wid;
char *app_class;
char *instance_name;
char *items[];
XmStringCharSet char_set;
int num_cols, num_rows;
int stay_up_behavior, win_mgr_behavior;
{
	Arg args[10];
	int i;
	Dimension height, margin_height;
	ListShell lObject;

	if (!(lObject = (ListShell) XtMalloc((Cardinal) sizeof(_ListShell))))
		return NULL;
	lObject->self = lObject;
	i = 0;
	XtSetArg(args[i], XmNallowShellResize, (XtArgVal) True); i++;
	XtSetArg(args[i], XmNmappedWhenManaged, (XtArgVal) False); i++;
	if (win_mgr_behavior == listShell_WIN_MGR_DECOR)
		lObject->shell = XtAppCreateShell(NULL, app_class,
			topLevelShellWidgetClass, XtDisplay(ref_wid), args, i);
	else
		lObject->shell = XtAppCreateShell(NULL, app_class,
			transientShellWidgetClass, XtDisplay(ref_wid), args, i);
	/*
	instances have precedence over classes:
	*/
	lObject->instance = XtCreateManagedWidget(instance_name,
		xmFrameWidgetClass, lObject->shell, NULL, 0);
	lObject->class = XtCreateManagedWidget("ListShell",
		xmFrameWidgetClass, lObject->instance, NULL, 0);
	i = 0;
	XtSetArg(args[i], XmNsashWidth, (XtArgVal) 1); i++;
	XtSetArg(args[i], XmNsashHeight, (XtArgVal) 1); i++;
	lObject->pane = XtCreateManagedWidget("pane",
		xmPanedWindowWidgetClass, lObject->class, args, i);
	i = 0;
/*	XtSetArg(args[i], XmNvisualPolicy, (XtArgVal) XmVARIABLE); i++;*/
	lObject->port = XtCreateManagedWidget("port",
		xmScrolledWindowWidgetClass, lObject->pane, args, i);
	lObject->num_items = 0;
	lObject->list_items = NULL;
	i = 0;
	if (items) {
		if (listShell_allocate_list(lObject, items, char_set)) {
			XtSetArg(args[i], XmNitems, (XtArgVal) lObject->list_items); i++;
			XtSetArg(args[i], XmNitemCount, (XtArgVal) lObject->num_items); i++;
		}
	}
	if (num_rows > 0) {
		XtSetArg(args[i], XmNvisibleItemCount, (XtArgVal) num_rows); i++;
	}
/*	XtSetArg(args[i], XmNlistSizePolicy, (XtArgVal) XmRESIZE_IF_POSSIBLE); i++;*/
/*	XtSetArg(args[i], XmNselectionPolicy, (XtArgVal) XmBROWSE_SELECT); i++;*/
	lObject->list = XtCreateManagedWidget("list",
		xmListWidgetClass, lObject->port, args, i);
	if (stay_up_behavior != listShell_STAY_UP)
		XtAddCallback(lObject->list, XmNbrowseSelectionCallback,
			ListShellDismiss, (XtPointer) lObject);
	lObject->stay_up_behavior = stay_up_behavior;

	i = 0;
	XtSetArg(args[i], XmNadjustLast, (XtArgVal) False); i++;
	XtSetArg(args[i], XmNborderWidth, (XtArgVal) 0); i++;
	XtSetArg(args[i], XmNorientation, (XtArgVal) XmHORIZONTAL); i++;
	lObject->commandBox = XtCreateManagedWidget("box",
		xmRowColumnWidgetClass, lObject->pane, args, i);
	i = 0;
	XtSetArg(args[i], XmNlabelString,
		XmStringCreateLtoR("Dismiss", char_set)); i++;
	lObject->commandDismiss = XtCreateManagedWidget("dismiss",
		xmPushButtonWidgetClass, lObject->commandBox, args, i);
	XtAddCallback(lObject->commandDismiss,
		XmNactivateCallback, ListShellDismiss, (XtPointer) lObject);
	if (num_cols > 0) {
		XmFontList fontlist;
		XmFontContext context;

		i = 0;
		XtSetArg(args[i], XmNfontList, &fontlist); i++;
		XtGetValues(lObject->list, args, i);
		if (XmFontListInitFontContext(&context, fontlist)) {
			XFontStruct *font;
			XmStringCharSet charset;

			if (XmFontListGetNextFont(context, &charset, &font)) {
				Dimension temp_width, width;

				width = (num_cols + 1) * font->max_bounds.width;	/* hack */
				i = 0;
				XtSetArg(args[i], XmNhighlightThickness, &temp_width); i++;
				XtGetValues(lObject->list, args, i);
				width += 2 * temp_width;
				i = 0;
				XtSetArg(args[i], XmNlistMarginWidth, &temp_width); i++;
				XtGetValues(lObject->list, args, i);
				width += 2 * temp_width;
				i = 0;
				XtSetArg(args[i], XmNscrolledWindowMarginWidth, &temp_width); i++;
				XtGetValues(lObject->port, args, i);
				width += 2 * temp_width;
				i = 0;
				XtSetArg(args[i], XmNwidth, (XtArgVal) width); i++;
				XtSetValues(lObject->list, args, i);
			}
			XmFontListFreeFontContext(context);
		}
	}
	i = 0;
	XtSetArg(args[i], XmNmarginHeight, &margin_height); i++;
	XtGetValues(lObject->commandBox, args, i);
	i = 0;
	XtSetArg(args[i], XmNheight, &height); i++;
	XtGetValues(lObject->commandDismiss, args, i);
	i = 0;
	XtSetArg(args[i], XmNpaneMinimum,
		(XtArgVal) (height + (margin_height * 2))); i++;
	XtSetArg(args[i], XmNpaneMaximum,
		(XtArgVal) (height + (margin_height * 2))); i++;
	XtSetValues(lObject->commandBox, args, i);
	remove_sash_traversal(lObject->pane);
	return lObject;
}	/* listShell_create */


/*
listShell_destroy() frees the storage for a `ListShell' object.
*/

void listShell_destroy(lObject)
ListShell lObject;
{
	XtDestroyWidget(lObject->shell);
	listShell_free_list(lObject);
	XtFree(lObject->self);
}	/* listShell_destroy */


/*
listShell_make_list() builds a list from a null-terminated
array of strings and initializes the list widget's item resources.
*/

void listShell_make_list(lObject, items, char_set)
ListShell lObject;
char *items[];
XmStringCharSet char_set;
{
	if (listShell_allocate_list(lObject, items, char_set)) {
		Arg args[5];
		int i;

		i = 0;
		XtSetArg(args[i], XmNitems, (XtArgVal) lObject->list_items); i++;
		XtSetArg(args[i], XmNitemCount, (XtArgVal) lObject->num_items); i++;
		XtSetValues(lObject->list, args, i);
	}
}	/* listShell_make_list */


/*
listShell_free_list() frees the storage for the list's
array of strings.
*/

void listShell_free_list(lObject)
ListShell lObject;
{
	if (lObject->list_items) {
		int i;

		for (i = 0; i < lObject->num_items; i++)
			XmStringFree(lObject->list_items[i]);
		XtFree(lObject->list_items);
		lObject->list_items = NULL;
	}
}	/* listShell_free_list */


/*
listShell_realize() realizes the top-level shell window
that houses the list selection widget.
*/

void listShell_realize(lObject)
ListShell lObject;
{
	XtRealizeWidget(lObject->shell);
}	/* listShell_realize */


/*
listShell_activate() maps the `ListShell' window.
*/

void listShell_activate(lObject)
ListShell lObject;
{
	if (!XtIsRealized(lObject->shell))
		return;
	XtMapWidget(lObject->shell);
}	/* listShell_activate */


/*
listShell_deactivate() removes the `ListShell' window.
*/

void listShell_deactivate(lObject)
ListShell lObject;
{
	XmListDeselectAllItems(lObject->list);
	XtUnmapWidget(lObject->shell);
}	/* listShell_deactivate */


/*
listShell_add_callback() registers a callback for the
list widget.
*/

void listShell_add_callback(lObject, cb_function, client_data)
ListShell lObject;
void (*cb_function)();
XtPointer client_data;
{
	XtAddCallback(lObject->list,
		XmNbrowseSelectionCallback, cb_function, client_data);
}	/* listShell_add_callback */


/*
For the following functions listShell_<widget_name>() returns
the widget IDs of the internal widgets.
*/

Widget listShell_shell(lObject)
ListShell lObject;
{
	return lObject->shell;
}	/* listShell_shell */


Widget listShell_instance(lObject)
ListShell lObject;
{
	return lObject->instance;
}	/* listShell_instance */


Widget listShell_class(lObject)
ListShell lObject;
{
	return lObject->class;
}	/* listShell_class */


Widget listShell_pane(lObject)
ListShell lObject;
{
	return lObject->pane;
}	/* listShell_pane */


Widget listShell_commandBox(lObject)
ListShell lObject;
{
	return lObject->commandBox;
}	/* listShell_commandBox */


Widget listShell_commandDismiss(lObject)
ListShell lObject;
{
	return lObject->commandDismiss;
}	/* listShell_commandDismiss */


Widget listShell_port(lObject)
ListShell lObject;
{
	return lObject->port;
}	/* listShell_port */


Widget listShell_list(lObject)
ListShell lObject;
{
	return lObject->list;
}	/* listShell_list */


/*
Private support functions:
*/

/*
listShell_allocate_list() builds a dynamic list from a
null-terminated array of strings.
*/

static int listShell_allocate_list(lObject, items, char_set)
ListShell lObject;
char *items[];
XmStringCharSet char_set;
{
	int i;

	listShell_free_list(lObject);
	if (!items)
		return FALSE;
	for (lObject->num_items = 0; items[lObject->num_items];
			lObject->num_items++)
		/* count the number of items */;
	lObject->list_items = (XmString *)
		XtMalloc((Cardinal) (sizeof(XmString) * lObject->num_items));
	if (lObject->list_items) {
		for (i = 0; i < lObject->num_items; i++)
			lObject->list_items[i] = XmStringCreateLtoR(items[i], char_set);
		return TRUE;
	}
	else
		return FALSE;
}	/* listShell_allocate_list */


/*
Private callback functions:
*/

/*
ListShellDismiss() is a private callback that removes
the `ListShell' window.
*/
/*ARGSUSED*/
static void ListShellDismiss(w, client_data, call_data)
Widget w;
XtPointer client_data;
XtPointer call_data;
{
	ListShell lObject = (ListShell) client_data;

	XtUnmapWidget(lObject->shell);
}	/* ListShellDismiss */

