/**** xrolo.help.t ****/

/*
This file contains the text for `xrolodex's help screens.
*/

	static int xrolo_help_rows = 18;
	static int xrolo_help_columns = 70;

	static char *xrolo_help_data[] = {
"Help (General):\n\
\n\
`xrolodex' is a Rolodex(R)-style application for maintaining a set of\n\
\"virtual business cards.\"  `xrolodex's top-level window includes four\n\
principal components:\n\
 - an application-level menu bar,\n\
 - a label area for displaying the name of the current rolodex file,\n\
 - a control panel for entry-level rolodex manipulation, and\n\
 - an edit window (with local menu bar or pop-up menu) for editing the\n\
   current rolodex entry, also providing a basic viewport facility.\n\
\n\
`xrolodex' has the following command-line option:\n\
\n\
{your prompt} xrolodex [<filename>] [-iconName <icon name>]\n\
\n\
The filename is optional.  That is, you can specify a file via the\n\
file selection box that's activated by the \"Open\" entry in the \"File\"\n\
menu.\n\
\n\
To create a new rolodex file, first use \"Open\" to associate the\n\
rolodex contents with a filename, and then simply type in each entry.\n\
\n\
`xrolodex' has the following client-level resources:\n\
\n\
Resource            Default Value    Range of Values       Type\n\
--------            -------------    ---------------       ----\n\
viewportMenuBar     False            {True, False}         Boolean\n\
viewportRows        12               @ 12 or greater       integer\n\
viewportColumns     40               @ 6 or greater        integer\n\
indexRows           15               positive integer      integer\n\
indexColumns        30               positive integer      integer\n\
centerDialogs       True             {True, False}         Boolean\n\
entryDelimiter      \"####\"           any char. string      string\n\
directoryMask       $HOME/*.xrolo    any char. string      string\n\
\n\
The default viewport menu is a pop-up, background menu; see the\n\
\"Editor\" help menu.  There are approximate lower limits on the\n\
size of the viewport window because Motif will force the viewport's\n\
row-column dimensions to be large enough to fill the top-level window,\n\
which must be large enough to accommodate the top-level menu bar and\n\
the control panel.\n\
\n\
The default card delimiter sequence is \"####\" on a separate line,\n\
that is, \"####\\n\".  There is no reason for the typical `xrolodex'\n\
user to specify the delimiter option; it is transparent during normal\n\
`xrolodex' use.\n\
\n\
Unless otherwise specified using the `directoryMask' resource,\n\
`xrolodex' uses your home directory as the reference point for the\n\
file selection box.\n\
",


"Help (Files):\n\
\n\
With `xrolodex' the rolodex database is completely free-form.  There\n\
are no limitations on the number of entries, and no limitations on\n\
the character dimensions of an individual entry.  Individual\n\
entries are separated in the rolodex file by preceding each entry\n\
with a special character sequence, which is transparent to users.\n\
\n\
Since `xrolodex's rolodex file is a simple stream of bytes,\n\
experienced UNIX users can use other text processing tools to\n\
manipulate the rolodex file(s).  In particular, you can use your\n\
own/preferred text editor, if you need to perform a considerable\n\
amount of editing on a particular file.  With `xrolodex' you can\n\
maintain as many distinct rolodex files as you'd like.  The \"Open...\"\n\
menu option allows you to load any rolodex file via a file selection\n\
box.  The file type is \"*.xrolo\" in the user's home directory.\n\
\n\
During normal operations, `xrolodex' maintains a rolodex file in\n\
memory.  That is, when you \"Open\" a file, its contents are read\n\
into RAM, and all rolodex operations are applied to this \"memory\n\
file.\"  When you select \"Save\" from the \"File\" menu, the rolodex\n\
file as it exists in memory is saved to disk, replacing the\n\
original rolodex file.  The first time that a file is saved after\n\
it's been loaded with \"Open...\" a back-up of the original file is\n\
created with \".bak\" appended to the filename.  For subsequent save\n\
operations (without an intervening use of \"Open...\"), the back-up\n\
file remains unchanged.\n\
\n\
The \"Quit\" menu option terminates the application without saving\n\
pending modifications to the rolodex file.  If there are unsaved\n\
changes to the file, you are prompted and given the opportunity to\n\
save changes to disk before the program terminates.  The \"Exit\"\n\
menu option terminates the application after saving any pending\n\
modifications to the rolodex file; the save process is automatic,\n\
i.e., there is no user prompt.\n\
\n\
There is no \"New\" menu entry.  To create a new rolodex file, first\n\
use \"Open\" to associate the rolodex contents with a filename, and\n\
then simply type in each entry.\n\
",


"Help (Edit Entries):\n\
\n\
With `xrolodex' the rolodex database is completely free-form.  There\n\
are no limitations on the number of entries, and no limitations on\n\
the character dimensions of an individual entry.  Individual\n\
entries are separated in the rolodex file by preceding each entry\n\
with a special character sequence, which is transparent to users.\n\
\n\
When viewing an individual rolodex entry you can change it simply\n\
by moving the mouse pointer into the rolodex entry viewport and\n\
using normal Motif editing commands--the viewport is implemented\n\
with a standard Motif text widget.  If the number of lines exceeds\n\
the height of the window, you can scroll the edit window.\n\
\n\
\"New/Ins\" clears the edit window and allows you to enter a new rolodex\n\
entry.  `xrolodex' *automatically* saves the contents of the current\n\
rolodex entry in the file (if modifications have been made) any time\n\
that you preform an operation that switches to another rolodex entry,\n\
e.g., \"Next\", \"Index\", etc.\n\
\n\
You can select \"Copy\" to enter a duplicate of any entry into the\n\
rolodex file.  This provide a convenient mechanism for creating a new\n\
entry by copying an existing rolodex entry, and then making changes\n\
to the new entry.  As an alternative, you could use \"Copy\" (from the\n\
viewport \"Edit\" menu) to copy an entry's text to the clipboard,\n\
select \"New/Ins\" to create an empty entry, and then \"Paste\" the text\n\
from the clipboard--pressing \"Copy\" is considerably faster.\n\
\n\
\"Delete\" deletes the current rolodex entry.  `xrolodex' provides a\n\
single level of undo capability for \"Delete\"ions with the \"Undelete\"\n\
button.  \"Undelete\" recovers the deleted entry at the current\n\
position in the rolodex file.\n\
\n\
\"New/Add\" is similar to \"New/Ins\", except that it inserts/adds the\n\
entry *after* the current entry.  The insert/add operations are con-\n\
sistent with text editors, such as `vi', where \"insert\" means before\n\
the current character and \"add\" means after the current character.\n\
Also, \"New/Add\" makes it possible to add an entry to the very end\n\
of the rolodex database.\n\
",


"Help (Find Entries):\n\
\n\
\"Find Entry...\" activates a top-level window in which you can enter\n\
a string.  `xrolodex' will search forward or backward, beginning with\n\
the next (previous) rolodex entry, for the first entry that contains\n\
the specified text string, presenting that entry in the viewport.\n\
By default, text searches across entries are case insensitive.  You\n\
can enable case sensitive searching with a toggle button from the\n\
radio box.\n\
\n\
\"Index...\" activates a top-level window that displays the first line\n\
of every rolodex entry.  With this convention in mind, you should make\n\
sure that the beginning of the first line of each entry contains\n\
meaningful information, e.g., a person's name.  If you select one of\n\
the entries displayed in the index window, that rolodex entry will be\n\
displayed in the edit window/viewport.\n\
",


"Help (Sort Entries):\n\
\n\
The \"Sort\" menu contains two entries:  \"Ascending\" and \"Descending\".\n\
The rolodex entries are sorted in memory and there is no provision for\n\
undoing the sort, so you may want to consider saving the rolodex file\n\
before performing a sort.\n\
\n\
Each rolodex entry is free-form--there are no sort fields.  You can,\n\
in effect, establish a single sort field by placing sort information\n\
at the beginning of each entry, e.g., on the first line.  Since\n\
sorting is applied to the entire stream of characters that makes up\n\
the entry, line breaks have no real effect on the sorting algorithm.\n\
In the simplest case, you can just enter a person's name on the first\n\
line, last name first, e.g.:\n\
\n\
<last name>, <first name>, <middle initial>\n\
<remainder of rolodex entry>\n\
",


"Help (Control Panel):\n\
\n\
For convenience, several of the menu buttons are present in the\n\
control panel as well.  The control panel is to the left of the\n\
viewport and is used for moving *among* rolodex entries.  It contains\n\
buttons for the most commonly used operations so that the user doesn't\n\
have to go to a pull-down menu.  The \"First\", \"Last\", \"Next\", and\n\
\"Previous\" buttons are self-explanatory.  Several other command\n\
buttons require explanation.\n\
\n\
\"New/Ins\" clears the edit window and allows you to enter a new rolodex\n\
entry.  `xrolodex' *automatically* saves the contents of the current\n\
rolodex entry in the file (if modifications have been made) any time\n\
that you preform an operation that switches to another rolodex entry,\n\
e.g., \"Next\", \"Index\", etc.\n\
\n\
You can select \"Copy\" to enter a duplicate of any entry into the\n\
rolodex file.  This provide a convenient mechanism for creating a new\n\
entry by copying an existing rolodex entry, and then making changes\n\
to the new entry.  As an alternative, you could use \"Copy\" (from the\n\
viewport \"Edit\" menu) to copy an entry's text to the clipboard,\n\
select \"New\" to create an empty entry, and then \"Paste\" the text\n\
from the clipboard--pressing \"Copy\" is considerably faster.\n\
\n\
\"Index...\" activates a top-level window that displays the first line\n\
of every rolodex entry.  With this convention in mind, you should make\n\
sure that the beginning of the first line of each entry contains\n\
meaningful information, e.g., a person's name.  If you select one of\n\
the entries displayed in the index window, that rolodex entry will be\n\
displayed in the edit window/viewport.\n\
\n\
The buttons in the control panel are organized such that those buttons\n\
providing duplicate services, i.e., operations equivalent to a button\n\
in a pull-down menu, are at the bottom of the control panel.  Thus, if\n\
you need to reduce the height of `xrolodex', these nonessential\n\
buttons will be clipped (made invisible) before the essential buttons.\n\
In this case, you can use the pull-down menus to perform these\n\
operations until you free up enough screen space to expand `xrolodex'\n\
such that the entire control panel is once again visible.\n\
",


"Help (Editor):\n\
\n\
`xrolodex' can be configured with either a local menu bar or a pop-up\n\
menu system for the editor/viewport facility.  If `xrolodex' doesn't\n\
display a local menu bar at the top of the viewport, you should use\n\
mouse button 3 to activate the pop-up menu system.  To request a\n\
local menu bar for the viewport instead of the pop-up menu system,\n\
you should set the resource `viewportMenuBar' to \"True\", e.g.,\n\
\n\
xrolodex*viewportMenuBar:  True\n\
\n\
The editor facility provides menus for copy, cut, and paste\n\
operations and for search-and-replace operations.  The editor\n\
window allows the user to edit the text within the currently\n\
displayed rolodex entry, not the entire rolodex file--the scrollbar\n\
will not navigate across entries.  Entry-level rolodex operations\n\
are performed elsewhere.  Since the editor maintains its internal\n\
state from one entry to the next, you can copy and paste text\n\
across entries.\n\
\n\
Within the edit window you can copy and cut the primary selection\n\
to the clipboard, paste text from the clipboard, and clear the\n\
clipboard.  Copy, cut, paste, and clear operations apply to the Motif\n\
clipboard, not the X clipboard.  You can use the \"Find Selection\"\n\
menu option to find the next occurence (forward) of the currently\n\
selected text.  The \"Search and Replace...\" menu option activates a\n\
top-level window that you can use to enter search text manually.  It\n\
also contains a \"Replace\" button so that you can \"Find\" a text\n\
segment and then \"Replace\" it.  Coordinated operations with \"Find\"\n\
and \"Replace\" will allow you to make changes to the text throughout\n\
the current rolodex entry.\n\
\n\
With `xrolodex' the rolodex database is completely free-form.  There\n\
are no limitations on the number of entries, and no limitations on\n\
the character dimensions of an individual entry.  Individual\n\
entries are separated in the rolodex file by preceding each entry\n\
with a special character sequence, which is transparent to users.\n\
",


"Help (Info):\n\
\n\
`xrolodex' is a copyrighted, freeware software product of:\n\
\n\
Iris Computing Laboratories\n\
The Spectro Group, Inc.\n\
1219 Luisa Street\n\
Suite 9\n\
Santa Fe, New Mexico 87501  USA\n\
\n\
Phone:  +1-505-988-2670  (USA, Mountain Time Zone)\n\
Fax:    +1-505-988-2489\n\
Email:  support@spectro.com\n\
\n\
\"Copyrighted freeware\" means that you are free to use it\n\
and pass it on to others, but you shouldn't sell it, etc.\n\
\n\
We welcome your comments and suggestions for `xrolodex' at the\n\
above addresses and phone numbers.\n\
",
};

