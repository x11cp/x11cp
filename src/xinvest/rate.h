/*
  Xinvest is copyright 1996 by Mark Buser.

  Permission is hereby granted to copy
  and freely distribute copies of this
  program for non-commercial purposes
  without fee, provided that this notice
  appears in all copies.
  
  All redistributions must be in their
  entirety as originally distributed.
  Feel free to modify Xinvest, but
  modified versions may not be distributed
  without prior consent of the author.
  
  This software is provided 'as-is'
  without any express or implied warranty.
  In no event will the author be held
  liable for any damages resulting from
  the use of this software.

  $Revision: 2.8 $ $Date: 1997/08/06 23:24:37 $
*/
#ifndef RATE_H
#define RATE_H

#include "nav.h"
#include "transP.h"

typedef struct {
  double div;
  double value;
  double yield;    /* yield over period */
  double annYield; /* yield annualized */
} YIELD;

#define CURRENT_DATE -1
#define FIRST_DATE    0
#define LAST_DATE     1

void rateAddTrans( TRANS *, int);    /* Add one transaction to deposits */
void rateClrTrans();                 /* Clear all deposits */
void rateAddAccount( int, NAV *);    /* Add trans from account until NAV date */
void rateClrAccount();               /* Clear all deposits */
double rate_IRR ( double );          /* IRR of deposits to reach parameter */

double rate_fee( int, long, long);   /* Fees from 1st long date to second */
double rate_value( int, long, long);
double rate_cost( int, long, long);
double rate_withdrawal( int, long, long);
double rate_distrib( int, long, long);
double rate_shares( int, long, long);
double rate_yield( int, long, long, YIELD *);
void   rate_add_account( int, long, long);

char *daytostr ( long, char *);          /* Change (long)date to (char *)date */
long strtoday ( char * );                /* Change (char *)date to (long)date */

/* OBSOLETE, delete when found */
double account_fee( int, NAV *);
double account_value( int, NAV *);
double account_cost( int, NAV *);
double account_withdrawal( int, NAV *);
double account_distrib( int, NAV *);     /* Amount of dividends taken */
double account_shares( int, NAV *);

#endif
