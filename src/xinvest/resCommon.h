"Xinvest.title: 				Xinvest 2.5.1",
"Xinvest.fontList: -*-helvetica-bold-r-*-*-*-100-*-*-*-*-*-*=small,\
-*-helvetica-bold-r-*-*-*-180-*-*-*-*-*-*=large",
"*Butform.fractionBase:			7",
"*Quit.topPosition:			0",
"*About.topPosition:			1",
"*Edit.topPosition:			2",
"*Gain.topPosition:			3",
"*Graphbut.topPosition:			4",
"*Portfolio.topPosition:			5",
"*Calculate.topPosition:			6",
"*Quit.labelString:			Exit",
"*About.labelString:			About Xinvest",
"*Edit.labelString:			Transaction Editor",
"*Gain.labelString:			Return Report",
"*Graphbut.labelString:			Show Plot",
"*Portfolio.labelString:			Asset Allocation",
"*Calculate.labelString:			Financial Calculator",
"*Savebut.labelString:			Save",
"*Newbut.labelString:			New",
"*Openbut.labelString:			Open",
"*Nextbut.labelString:			Next",
"*Prevbut.labelString:			Previous",
"*Acctform.button_0.labelString:		This Account",
"*Acctform.button_1.labelString:		Open Accounts",
"*Acctform.button_2.labelString:		Portfolio",
"*Screenbut.labelString:			Next/Prev Screen",
"*Timeform.button_0.labelString:		1 Day",
"*Timeform.button_1.labelString:		7 Day",
"*Timeform.button_2.labelString:		30 Day ",
"*Timeform.button_3.labelString:		1 Quarter",
"*Timeform.button_4.labelString:		Year to Date",
"*Timeform.button_5.labelString:		1 Year",
"*Timeform.button_6.labelString:		Since Inception",
"*Timeform.button_7.labelString:		Custom",
"*prevTrans.labelString:			Previous Transaction",
"*nextTrans.labelString:			Next Transaction",
"*firstTrans.labelString:		First Transaction",
"*lastTrans.labelString:			Last Transaction",
"*Gaincalclast.labelString:		Last NAV",
"*Gaincalcmode.labelString:		Account/Portfolio",
"*Gaincalcrow.zero.labelString:		0",
"*Gaincalcrow.one.labelString:		1",
"*Gaincalcrow.two.labelString:		2",
"*Gaincalcrow.three.labelString:		3",
"*Gaincalcrow.four.labelString:		4",
"*Gaincalcrow.five.labelString:		5",
"*Gaincalcrow.six.labelString:		6",
"*Gaincalcrow.seven.labelString:		7",
"*Gaincalcrow.eight.labelString:		8",
"*Gaincalcrow.nine.labelString:		9",
"*Gaincalcrow.clr.labelString:		C",
"*Gaincalcrow.pnt.labelString:		.",
"*Gainvalrow.cmp.labelString:		=",
"*menubar*fontList:	-*-helvetica-bold-r-*-*-*-120-*-*-*-*-iso8859-*",
"*menubar.button_0.labelString:		File",
"*menubar.button_0.mnemonic:		F",
"*menubar.button_1.labelString:		Edit",
"*menubar.button_1.mnemonic:		E",
"*menubar.button_2.labelString:		Tools",
"*menubar.button_2.mnemonic:		T",
"*menubar.button_3.labelString:		Options",
"*menubar.button_3.mnemonic:		O",
"*menubar.button_4.labelString:		Account",
"*menubar.button_4.mnemonic:		A",
"*menubar.button_5.labelString:		View",
"*menubar.button_5.mnemonic:		V",
"*menubar.button_6.labelString:		Help",
"*menubar.button_6.mnemonic:		H",
"*Filemenu.tearOffModel:			XmTEAR_OFF_ENABLED",
"*Filemenu*fontList:	-*-helvetica-bold-r-*-*-*-120-*-*-*-*-iso8859-*",
"*Filemenu.button_0.labelString:		New",
"*Filemenu.button_0.mnemonic:		N",
"*Filemenu.button_1.labelString:		Open...",
"*Filemenu.button_1.mnemonic:		O",
"*Filemenu.button_2.labelString:		Close",
"*Filemenu.button_2.mnemonic:		C",
"*Filemenu.button_3.labelString:		Save",
"*Filemenu.button_3.mnemonic:		S",
"*Filemenu.button_4.labelString:		Save As...",
"*Filemenu.button_4.mnemonic:		A",
"*Filemenu.button_5.labelString:		Quit",
"*Filemenu.button_5.mnemonic:		Q",
"*Toolmenu.tearOffModel:			XmTEAR_OFF_ENABLED",
"*Toolmenu*fontList:	-*-helvetica-bold-r-*-*-*-120-*-*-*-*-iso8859-*",
"*Toolmenu.button_0.labelString:		About",
"*Toolmenu.button_0.mnemonic:		A",
"*Toolmenu.button_1.labelString:		Edit Transaction",
"*Toolmenu.button_1.mnemonic:		E",
"*Toolmenu.button_2.labelString:		Return Report",
"*Toolmenu.button_2.mnemonic:		R",
"*Toolmenu.button_3.labelString:		Plot",
"*Toolmenu.button_3.mnemonic:		P",
"*Toolmenu.button_4.labelString:		Asset Allocation",
"*Toolmenu.button_4.mnemonic:		s",
"*Toolmenu.button_5.labelString:		Financial Calculator",
"*Toolmenu.button_5.mnemonic:		F",
"*Editmenu*fontList:	-*-helvetica-bold-r-*-*-*-120-*-*-*-*-iso8859-*",
"*Editmenu.button_0.labelString:		Copy",
"*Editmenu.button_0.mnemonic:		C",
"*Editmenu.button_0.accelerator:		Ctrl<Key>C",
"*Editmenu.button_0.acceleratorText:	Ctrl+C",
"*Editmenu.button_1.labelString:		Cut",
"*Editmenu.button_1.mnemonic:		t",
"*Editmenu.button_1.accelerator:		Ctrl<Key>X",
"*Editmenu.button_1.acceleratorText:	Ctrl+X",
"*Editmenu.button_2.labelString:		Paste",
"*Editmenu.button_2.mnemonic:		P",
"*Editmenu.button_2.accelerator:		Ctrl<Key>V",
"*Editmenu.button_2.acceleratorText:	Ctrl+V",
"*Accountmenu*fontList:	-*-helvetica-bold-r-*-*-*-120-*-*-*-*-iso8859-*",
"*Accountmenu.button_0.labelString:	Previous",
"*Accountmenu.button_0.mnemonic:		P",
"*Accountmenu.button_0.accelerator:	Shift Alt<Key>Tab",
"*Accountmenu.button_0.acceleratorText:	Shift+Alt+Tab",
"*Accountmenu.button_1.labelString:	Next",
"*Accountmenu.button_1.mnemonic:		N",
"*Accountmenu.button_1.accelerator:	Alt<Key>Tab",
"*Accountmenu.button_1.acceleratorText:	Alt+Tab",
"*Accountmenu.button_2.labelString:	Apply Changes",
"*Accountmenu.button_2.mnemonic:		A",
"*Accountmenu.button_2.accelerator:	Ctrl<Key>A",
"*Accountmenu.button_2.acceleratorText:	Ctrl+A",
"*Accountmenu.more.labelString:		More...",
"*Accountmenu.more.mnemonic:		M",
"*Viewmenu.button_0.labelString:		Message Log...",
"*Viewmenu.button_0.mnemonic:		M",
"*Optionmenu.tearOffModel:		XmTEAR_OFF_ENABLED",
"*Optionmenu.button_0.labelString:	Zoom",
"*Optionmenu.button_0.mnemonic:		Z",
"*Optionmenu.button_0.accelerator:	Ctrl<Key>Z",
"*Optionmenu.button_0.acceleratorText:	Ctrl+Z",
"*Optionmenu.button_1.labelString:	Show Points",
"*Optionmenu.button_1.mnemonic:		S",
"*Optionmenu.button_1.accelerator:	Ctrl<Key>P",
"*Optionmenu.button_1.acceleratorText:	Ctrl+P",
"*Optionmenu.button_2.labelString:	Show Grid",
"*Optionmenu.button_2.mnemonic:		G",
"*Optionmenu.button_2.accelerator:	Ctrl<Key>G",
"*Optionmenu.button_2.acceleratorText:	Ctrl+G",
"*Helpmenu.button_0.labelString:         Help...",
"*Helpmenu.button_0.mnemonic:            H",
"*VPane.paneMinimum:			450",
"*Translabel.labelString:		No Open Accounts",
"*Translabel.fontList:	-*-helvetica-bold-r-*-*-*-120-*-*-*-*-iso8859-*",
"*Transtext.rows:			14",
"*Graphframe.shadowType:			SHADOW_ETCHED_IN",
"*Graphframe.paneMinimum:		175",
"*Graph.height:				175",
"*Graph.width:				450",
"*Fileframe.shadowType:			SHADOW_ETCHED_IN",
"*Listfile.visibleItemCount:		5",
"*Listfile.scrollBarDisplayPolicy:	static",
"*ListfileSW.rightOffset: 		3",
"*ListfileSW.bottomOffset: 		3",
"*Midform.bottomOffset:			3",
"*Toolframe.shadowType:			SHADOW_ETCHED_IN",
"*Infoframe.shadowType:			SHADOW_ETCHED_IN",
"*calframe.shadowType:			SHADOW_ETCHED_IN",
"*calrow.orientation:			VERTICAL",
"*calrow*spacing:			0",
"*daterow.orientation:			HORIZONTAL",
"*daterow.spacing:			0",
"*daterow*marginWidth:			0",
"*yearUparrow.width:			25",
"*yearUparrow.shadowThickness:		0",
"*yearDnarrow.width:			25",
"*yearDnarrow.shadowThickness:		0",
"*yeartext.columns:			4",
"*monthoptmenu.button_0.labelString:	January",
"*monthoptmenu.button_0.mnemonic:	J",
"*monthoptmenu.button_1.labelString:	February",
"*monthoptmenu.button_1.mnemonic:	F",
"*monthoptmenu.button_2.labelString:	March",
"*monthoptmenu.button_2.mnemonic:	M",
"*monthoptmenu.button_3.labelString:	April",
"*monthoptmenu.button_3.mnemonic:	A",
"*monthoptmenu.button_4.labelString:	May",
"*monthoptmenu.button_4.mnemonic:	a",
"*monthoptmenu.button_5.labelString:	June",
"*monthoptmenu.button_5.mnemonic:	n",
"*monthoptmenu.button_6.labelString:	July",
"*monthoptmenu.button_6.mnemonic:	l",
"*monthoptmenu.button_7.labelString:	August",
"*monthoptmenu.button_7.mnemonic:	g",
"*monthoptmenu.button_8.labelString:	September",
"*monthoptmenu.button_8.mnemonic:	S",
"*monthoptmenu.button_9.labelString:	October",
"*monthoptmenu.button_9.mnemonic:	O",
"*monthoptmenu.button_10.labelString:	November",
"*monthoptmenu.button_10.mnemonic:	N",
"*monthoptmenu.button_11.labelString:	December",
"*monthoptmenu.button_11.mnemonic:	D",
"*Timeframe.shadowType:			SHADOW_ETCHED_IN",
"*Timeframe*fontList:		-*-helvetica-bold-r-*-*-*-120-*-*-*-*-iso8859-*",
"*Timeframe.leftOffset:			0",
"*TimeLabel.labelString:			Report Period",
"*TimeLabel.childVerticalAlignment:	ALIGNMENT_WIDGET_TOP",
"*Timeform.fractionBase:			8",
"*Timeform.*.shadowThickness:		1",
"*Timeform.*.highlightThickness:		1",
"*Timeform*indicatorOn:			False",
"*Timeform*fillOnSelect:			True",
"*Timeform.button_0.leftPosition:	0",
"*Timeform.button_1.leftPosition:	1",
"*Timeform.button_2.leftPosition:	2",
"*Timeform.button_3.leftPosition:	3",
"*Timeform.button_4.leftPosition:	4",
"*Timeform.button_5.leftPosition:	5",
"*Timeform.button_6.leftPosition:	6",
"*Timeform.button_7.leftPosition:	7",
"*Acctframe.shadowType:			SHADOW_ETCHED_IN",
"*Acctframe*fontList:		-*-helvetica-bold-r-*-*-*-120-*-*-*-*-iso8859-*",
"*Acctframe.topOffset:			25",
"*Acctframe.leftOffset:			10",
"*AcctLabel.labelString:			Accounts",
"*AcctLabel.childVerticalAlignment:	ALIGNMENT_WIDGET_TOP",
"*Acctform.fractionBase:			3",
"*Acctform.*.shadowThickness:		1",
"*Acctform.*.highlightThickness:		1",
"*Acctform*indicatorOn:			False",
"*Acctform*fillOnSelect:			True",
"*Acctform.button_0.leftPosition:	0",
"*Acctform.button_1.leftPosition:	1",
"*Acctform.button_2.leftPosition:	2",
"*Valframe.shadowType:			SHADOW_ETCHED_IN",
"*Valframe*fontList:		-*-helvetica-bold-r-*-*-*-120-*-*-*-*-iso8859-*",
"*Valframe.topOffset:			5",
"*Valframe.leftOffset:			20",
"*ValLabel.labelString:			Options",
"*ValLabel.childVerticalAlignment:	ALIGNMENT_WIDGET_TOP",
"*Valrow.spacing:			0",
"*Valrow.marginHeight:			0",
"*Valrow.orientation:			VERTICAL",
"*Valrow.numColumns:			1",
"*Valrow.button_0.labelString:		Details",
"*Valrow.button_1.labelString:		Annualize",
"*Valrow.button_2.labelString:		Live Window",
"*Screenbut.indicatorOn:			False",
"*Screenbut.fillOnSelect:		True",
"*Screenbut.shadowThickness:		1",
"*Screenbut.highlightThickness:		1",
"*ReportInnerform.fractionBase:		5",
"*Reportbut.showAsDefault:		1",
"*Reportbut.topOffset:			10",
"*Reportbut.leftPosition:		2",
"*Reportbut.rightPosition:		3",
"*reportoptmenu.button_0.labelString:	Start",
"*reportoptmenu.button_0.mnemonic:	S",
"*reportoptmenu.button_1.labelString:	End",
"*reportoptmenu.button_1.mnemonic:	E",
"*Graphpframe.shadowType:		SHADOW_ETCHED_IN",
"*Graphpframe*fontList:		-*-helvetica-bold-r-*-*-*-120-*-*-*-*-iso8859-*",
"*GraphMode.labelString:			Graph Mode",
"*GraphMode.childVerticalAlignment:	ALIGNMENT_WIDGET_TOP",
"*Graphprow.marginHeight:		0",
"*Graphprow.marginWidth:			10",
"*Graphprow.orientation:			HORIZONTAL",
"*Graphprow.XmToggleButtonGadget.indicatorOn:	False",
"*Graphprow.XmToggleButtonGadget.fillOnSelect:	True",
"*Graphprow.XmToggleButtonGadget.shadowThickness:2",
"*Graphprow.XmToggleButtonGadget.marginTop:	0",
"*Graphprow.XmToggleButtonGadget.marginBottom:	0",
"*Graphprow.XmToggleButtonGadget.marginLeft:	0",
"*Graphprow.XmToggleButtonGadget.marginRight:	0",
"*Graphhframe.shadowType:		SHADOW_ETCHED_IN",
"*Graphhframe*fontList:		-*-helvetica-bold-r-*-*-*-120-*-*-*-*-iso8859-*",
"*Graphhframe.leftOffset:		15",
"*GraphType.labelString:			Graph Type",
"*GraphType.childVerticalAlignment:	ALIGNMENT_WIDGET_TOP",
"*Graphhrow.marginHeight:		0",
"*Graphhrow.marginWidth:			10",
"*Graphhrow.orientation:			HORIZONTAL",
"*Graphhrow.XmToggleButtonGadget.indicatorOn:	False",
"*Graphhrow.XmToggleButtonGadget.fillOnSelect:	True",
"*Graphhrow.XmToggleButtonGadget.shadowThickness:2",
"*Graphhrow.XmToggleButtonGadget.marginTop:	0",
"*Graphhrow.XmToggleButtonGadget.marginBottom:	0",
"*Graphhrow.XmToggleButtonGadget.marginLeft:	0",
"*Graphhrow.XmToggleButtonGadget.marginRight:	0",
"*Graphvframe.shadowType:		SHADOW_ETCHED_IN",
"*Graphvframe.topOffset:			10",
"*GraphVar.fontList:		-*-helvetica-bold-r-*-*-*-120-*-*-*-*-iso8859-*",
"*GraphVar.labelString:			Graph Variables",
"*GraphVar.childVerticalAlignment:	ALIGNMENT_WIDGET_TOP",
"*Graphvscroll.height:			105",
"*Graphvscroll.width:			175",
"*Graphvrow*marginTop:			0",
"*Graphvrow*marginBottom:		0",
"*Graphvrow*marginHeight:		0",
"*Graphvrow*XmToggleButton.spacing:	0",
"*Graphvrow*XmToggleButton.indicatorSize:	15",
"*Graphvrow*label_0.labelString:		Price",
"*Graphvrow*label_1.labelString:		Shares",
"*Graphvrow*label_2.labelString:		Cost",
"*Graphvrow*label_3.labelString:		Value",
"*Graphvrow*label_4.labelString:		Distribution",
"*Graphvrow*label_5.labelString:		IRR",
"*Graphvrow*label_6.labelString:		TR",
"*Graphvrow*label_7.labelString:		Constant",
"*Graphvrow*label_8.labelString:		Shares (ACC)",
"*Graphvrow*label_9.labelString:		Cost (ACC)",
"*Graphvrow*label_10.labelString:	Value (ACC)",
"*Graphvrow*label_11.labelString:	Distrib (ACC)",
"*Graphvrow*label_12.labelString:	Price (MAVG)",
"*Graphvrow*label_13.labelString:	Shares (MAVG)",
"*Graphvrow*label_14.labelString:	Cost (MAVG)",
"*Graphvrow*label_15.labelString:	Distrib (MAVG)",
"*Graphslabel.alignment:			ALIGNMENT_END",
"*Graphscale.topOffset:			10",
"*Graphscale.leftOffset:			10",
"*Graphscale.orientation:		VERTICAL",
"*Graphscale.maximum:			52",
"*Graphscale.minimum:			1",
"*Graphscale.value:			13",
"*Graphscale.editable:			True",
"*Graphscale.showValue:			True",
"*Graphscale.showArrows:			EACH_SIDE",
"*Graphscale.slidingMode:		SLIDER",
"*Graphscale.sliderMark:			THUMB_MARK",
"*Graphslabel.labelString:		Trans",
"*Graphcform.topOffset:			10",
"*Graphvlabel.labelString:		Constant",
"*Graphvstart.columns:			7",
"*Graphvstart.value:			1000",
"*Graphvstart.leftOffset:		10",
"*Graphvrate.columns:			4",
"*Graphvrate.value:			10.0",
"*Portform*fontList:		-*-helvetica-bold-r-*-*-*-120-*-*-*-*-iso8859-*",
"*PortAccount.labelString:		This Account",
"*PortAccount.childVerticalAlignment:	ALIGNMENT_WIDGET_TOP",
"*PortAccounts.labelString:		All Accounts",
"*PortAccounts.childVerticalAlignment:	ALIGNMENT_WIDGET_TOP",
"*Portselrow.orientation:		HORIZONTAL",
"*Portselrow.packing:			PACK_COLUMN",
"*Portselrow.marginWidth:		20",
"*Portselrow.spacing:			10",
"*Portbutform.fractionBase:		4",
"*Portbutform.Add.labelString:		Add Account",
"*Portbutform.Add.leftPosition:		0",
"*Portbutform.Add.rightPosition:		2",
"*Portbutform.Remove.labelString:	Remove Account",
"*Portbutform.Remove.leftPosition:	2",
"*Portbutform.Remove.rightPosition:	4",
"*Portbutform.AddAll.labelString:	Add All Accounts",
"*Portbutform.AddAll.leftPosition:	0",
"*Portbutform.AddAll.rightPosition:	2",
"*Portbutform.Clear.labelString:		Remove All Accounts",
"*Portbutform.Clear.leftPosition:	2",
"*Portbutform.Clear.rightPosition:	4",
"*Portbutform*recomputeSize:		False",
"*Portpercentlabel.alignment:		ALIGNMENT_CENTER",
"*Portpercentlabel.labelString:		Category    Percent   Value",
"*Portform*Portpercentlabel.fontList: -*-fixed-bold-r-*-*-*-120-*-*-*-*-iso8859-*",
"*Portform*XmList.fontList:  	-*-fixed-bold-r-*-*-*-120-*-*-*-*-iso8859-*",
"*Portpercentlist*visibleItemCount:	5",
"*Portpercentlist*scrollBarDisplayPolicy: static",
"*Portvaluelabel.alignment:		ALIGNMENT_BEGINNING",
"*Calcform*fontList:		-*-helvetica-bold-r-*-*-*-120-*-*-*-*-iso8859-*",
"*Calcframe.topOffset:			10",
"*Calclabel.marginHeight:                0",
"*Calclabel.marginWidth:                 0",
"*Calcrow.topOffset:			10",
"*Calcrow.orientation:			HORIZONTAL",
"*Calcrow.packing:			PACK_COLUMN",
"*Calcrow.numColumns:			4",
"*Calcrow.isAligned:			True",
"*Calcrow.entryAlignment:		ALIGNMENT_END",
"*Calcrow*marginWidth:                   0",
"*Calcrow*marginHeight:                  2",
"*Calcrow*recomputeSize:                 False",
"*Calcrow*spacing:                       0",
"*Calcrow.XmLabelGadget.marginRight:     5",
"*Calcrow*XmTextField.columns:		8",
"*Calcrow.XmTextField.marginWidth:       5",
"*Butform.topOffset:			3",
"*Butform.leftOffset:			3",
"*Butform.rightOffset:			3",
"*Help*topicframe.shadowType:		SHADOW_ETCHED_IN",
"*Help*topiclist.visibleItemCount:	15",
"*Help*textframe.shadowType:		SHADOW_ETCHED_IN",
"*Help*text.columns:			40",
"*Help*text.fontList:	-*-helvetica-medium-r-*-*-*-120-*-*-*-*-iso8859-*",
"*Help*fontList:		-*-helvetica-bold-r-*-*-*-120-*-*-*-*-iso8859-*",
"*Status.title:				Message Log",
"*Status*text.rows:			4",
"*Status*text.columns:			40",
"*Status*fontList:	-*-helvetica-bold-r-*-*-*-120-*-*-*-*-iso8859-*",
"*Filesel.dialogTitle:			File Select",
"*Filesel*fontList:	-*-helvetica-bold-r-*-*-*-120-*-*-*-*-iso8859-*",
"*Filesel*pattern:			*.inv",
"*alert.dialogTitle:			Alert",
"*alert*fontList:	-*-helvetica-bold-r-*-*-*-120-*-*-*-*-iso8859-*",
"*Accountsel.dialogTitle:		Account Select",
"*Accountsel.listLabelString:		Open Accounts",
"*Accountsel.cancelLabelString:          Cancel",
"*Accountsel.okLabelString:              Select",
"*Accountsel*fontList:	-*-helvetica-bold-r-*-*-*-120-*-*-*-*-iso8859-*",
"*Report.dialogTitle:			Report",
"*ReportScroll.width:			600",
"*ReportScroll.height:			200",
"TipShell*foreground:			Black",
"TipShell*background:			Yellow",
"Xinvest.color0:				Red",
"Xinvest.color1:				Blue",
"Xinvest.color2:				Green",
"Xinvest.color3:				Yellow",
"Xinvest.color4:				Purple",
"Xinvest.color5:				Brown",
"Xinvest.color6:				Orange",
"Xinvest.color7:				Grey100",
