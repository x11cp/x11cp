/*
  Xinvest is Copyright 1995-97 Mark Buser,
  All Rights Reserved.

  Permission is hereby granted to copy
  and freely distribute copies of this
  program for non-commercial purposes
  without fee, provided that this notice
  appears in all copies.
  
  All redistributions must be in their
  entirety as originally distributed.
  Feel free to modify Xinvest, but
  modified versions may not be distributed
  without prior consent of the author.
  
  This software is provided 'as-is'
  without any express or implied warranty.
  In no event will the author be held
  liable for any damages resulting from
  the use of this software.

  $Revision: 2.31 $ $Date: 1997/08/19 11:43:46 $
*/

#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <Xm/PushB.h>
#include <Xm/ToggleB.h>
#include <Xm/DrawingA.h>
#include <Xm/ScrolledW.h>
#include <Xm/Text.h>
#include <Xm/Scale.h>

#include "account.h"
#include "color.h"
#include "drawing.h"
#include "graph.h"
#include "graphP.h"
#include "nav.h"
#include "rate.h"
#include "status.h"
#include "trans.h"
#include "xutil.h"

#include "pixmaps/cumstipple.xbm"
#include "pixmaps/movstipple.xbm"


static int multiAccount = 0;
static int v_active = 0;

/* Remember where all the buttons are. We rely on an implicit initialization
   to zero.  */
static Widget Graphvbuttons[NUM_PLOTS];

/* Remember what's selected in single account mode */
static PLOT_VAR *accountSingle = (PLOT_VAR *)NULL;

static int by_trans = True;
static unsigned int zoom = False;
static unsigned int show_point = False;
static unsigned int grid = False;

static int   x_border;  /* How much space for vertical gradations */
static int   y_border;  /* How much space for horizontal gradations */
static int   t_border;  /* How much space for title */

/* Set back to a "normal" drawing format */
static void ResetDrawAttrib ( Widget Drawing, GC gc, Pixel color )
{
  /* Reset the foreground color */
  XSetForeground ( XtDisplay(Drawing), gc, color);
  
  /* Reset the line style - plot by line */
  XSetLineAttributes( XtDisplay(Drawing), gc,
                      2, LineSolid, CapNotLast, JoinBevel );

  /* Reset the fill style - plot by bar */
  XSetFillStyle (  XtDisplay (Drawing), gc, FillSolid );

}


/* Set drawing format as per attrib encoding */
static void SetDrawAttrib ( Widget Drawing, GC gc, int attrib )
{

#define COLOR (attrib & 0x07)
#define STYLE ((attrib & 0x18) >> 3)
#define ATTRIB_MAX 0x17

  char   dash_dash[] = { 8, 8, 0 };
  char   dash_dot[] =  { 4, 1, 4, 0 };
  int    line_style[] = { LineSolid, LineOnOffDash, LineOnOffDash };
  int    fill_style[] = { FillSolid, FillOpaqueStippled, FillOpaqueStippled };

  static Pixmap mov_stipple = (Pixmap) NULL;
  static Pixmap cum_stipple = (Pixmap) NULL;

  /* Get some stipples to make bars for alterate plots show up differently */
  if ( mov_stipple == (Pixmap) NULL ) {

    Pixel foreground, background;
 
    foreground = GetColor( BLACK );
    background = GetColor( WHITE );
 
    mov_stipple = XCreatePixmapFromBitmapData( XtDisplay(Drawing),
                                      RootWindowOfScreen(XtScreen( Drawing)),
                                      (char *)movstipple_bits,
                                      movstipple_width, movstipple_height,
                                      foreground, background, 1 );
 
    cum_stipple = XCreatePixmapFromBitmapData( XtDisplay(Drawing),
                                      RootWindowOfScreen(XtScreen( Drawing)),
                                      (char *)cumstipple_bits,
                                      cumstipple_width, cumstipple_height,
                                      foreground, background, 1 );
  }

  /* Set the foreground color */
  XSetForeground ( XtDisplay(Drawing), gc, GetColor ( COLOR ));
  
  /* Set the line style - plot by line */
  XSetLineAttributes( XtDisplay(Drawing), gc,
                      2, line_style[ STYLE ], CapNotLast, JoinBevel );

  /* If we're LineOnOffDash set the next dash pattern */
  if ( STYLE == 1)
     XSetDashes ( XtDisplay (Drawing), gc, 0, dash_dash, strlen(dash_dash) );
  if ( STYLE == 2)
     XSetDashes ( XtDisplay (Drawing), gc, 0, dash_dot, strlen(dash_dot) );

  /* Set the fill style - plot by bar */
  XSetFillStyle (  XtDisplay (Drawing), gc, fill_style[ STYLE ]);

  /* If we're stipple set the next stipple pattern */
  if ( STYLE == 1)
     XSetStipple ( XtDisplay (Drawing), gc, cum_stipple );
  if ( STYLE == 2)
     XSetStipple ( XtDisplay (Drawing), gc, mov_stipple );

#undef COLOR
#undef STYLE

}

/* 
** Given: a toggle button with its Pixmaps already created. Fill
** them with a scale model of what the plot will look like for the 
** variable represented by this button.   If background exists use this
** for the pixmap background otherwise use the button background.
*/
void makeLegend (Widget button, int attrib, Pixel background)
{
  static GC legendgc = (GC)NULL;
  static Pixel saveback = (Pixel)-1;

  Pixmap legendon, legendoff;

  if (legendgc == (GC)NULL) 
    legendgc = XCreateGC ( XtDisplay(button), 
                           RootWindowOfScreen( XtScreen(button) ), 
                           0, NULL );

  /* Use a specified bg or the button bg */
  if ( background != (Pixel)-1 )
    saveback = background;
  if ( saveback == (Pixel)-1 )
    XtVaGetValues ( button, XmNbackground, &saveback, NULL);
  XSetBackground (  XtDisplay(button), legendgc, saveback );

  XtVaGetValues ( button, XmNlabelPixmap, &legendoff, 
                          XmNselectPixmap, &legendon, 
                  NULL);

  /* We're clearing a legend */
  if (attrib == LEGEND_INIT) {

    /* fill (initialize) on and off pixmaps with bg color */
    ResetDrawAttrib ( button, legendgc, saveback );
    XFillRectangle ( XtDisplay(button), legendoff, legendgc,
                     0, 0, LEGEND_WIDTH, LEGEND_HEIGHT );
    XFillRectangle ( XtDisplay(button), legendon, legendgc,
                     0, 0, LEGEND_WIDTH, LEGEND_HEIGHT );

    XtVaSetValues ( button, 
                    XmNlabelPixmap, legendoff, 
                    XmNselectPixmap, legendon, 
                    NULL);
    return;  
  }


  /* Fill the pixmap with background color */
  ResetDrawAttrib ( button, legendgc, saveback );
  XFillRectangle ( XtDisplay(button), legendon, legendgc,
                     0, 0, LEGEND_WIDTH, LEGEND_HEIGHT );

  SetDrawAttrib ( button, legendgc, attrib );
  /* Constant is always a line, never a bar */
  if (by_trans == True && button != Graphvbuttons[CONST-1]) 
    XFillRectangle ( XtDisplay(button), legendon, legendgc,
                     0, 0, LEGEND_WIDTH, LEGEND_HEIGHT );
  else {
    XDrawLine ( XtDisplay(button), legendon, legendgc,
                0, LEGEND_HEIGHT/2, LEGEND_WIDTH, LEGEND_HEIGHT/2 );
  }

  /* Update pixmap */
  XtVaSetValues ( button, XmNselectPixmap, legendon, NULL);

}


static void constCheckPoint( int set, int attrib )
{
  static int constant = -1;
  int account;
  PLOT_VAR *next;

  if (multiAccount) {
    if (set)
      constant = attrib;

    if (getVActive() == 0)
      constant = -1;

    for (account = 1; account <= numAccounts(); account++) {
      getAccount ( account, ACCOUNT_PLOT, &next);
      next->attrib[CONST-1] = constant;
      if (constant == -1)
        next->valid &= ~(CONSTS);
      else
        next->valid |= CONSTS;
    }
  }
}


PLOT_VAR *makeGraphVar()
{
  PLOT_VAR *new = (PLOT_VAR *) XtCalloc (1, sizeof(PLOT_VAR));
  int i;

  for (i = 0; i < NUM_PLOTS; i++)
    new->attrib[i] = -1;
  new->valid = 0;

  return (new);
}


/* 
** For routines that don't care if we're single or multi, but need the
** right attributes.  This routine knows what to do in either case. 
*/
static PLOT_VAR *graph_attrib( int account ) 
{
  PLOT_VAR *multi;

  if (multiAccount) {
    getAccount ( account, ACCOUNT_PLOT, &multi );
    return (multi);
  } 

  return (accountSingle);
}
    


static int graph_set_var ( int which, int state )
{
  static int single_attrib = 0;
  static int multi_attrib = 0;

  PLOT_VAR *accountGraph;   /* Account color/drawing attributes */

  /* 
  ** Pick which source of what's on: in single account mode, accountSingle,
  ** in multi-mode get from the account info. 
  */
  accountGraph = graph_attrib ( activeAccount() );

  if (state == XmSET) {
     v_active |= (1 << (int) which);
 
     /* Draw in the legend for this plot */

     /* If no attribute yet, get next one. */
     if ( accountGraph->attrib[which] == -1 ) { 
       if (multiAccount)
         accountGraph->attrib[which] = multi_attrib++ % ATTRIB_MAX;
       else
         accountGraph->attrib[which] = single_attrib++ % ATTRIB_MAX;
     }
     makeLegend ( Graphvbuttons[which], accountGraph->attrib[which], -1);

  } else {
     accountGraph->attrib[which] = -1;
     v_active &= ~(1 << (int) which);
  }

  accountGraph->valid = v_active;

  /* If this account is empty, reset single_attrib */
  if (!multiAccount && v_active == 0)
    single_attrib = 0;
  
  /* If all accounts empty, reset multi_attrib */
  if ( multiAccount && v_active == 0 && getVActive() == 0 )
    multi_attrib = 0;

  /* Force an expose to update displayed pixmap */
  if ( Graphvbuttons[which] )
    XClearArea ( XtDisplay( Graphvbuttons[which] ), 
                 XtWindow( Graphvbuttons[which] ), 0, 0, 0, 0, True );

  return ( accountGraph->attrib[which] );
}


static void Graph_vsense()
{
  extern Widget Graphscale;
  extern Widget Graphcform;
  extern Widget GraphconstToggle;

  if ( (v_active & MAVGS) == 0) /* Moving Avg. */
    XtSetSensitive( Graphscale, False );
  else
    XtSetSensitive( Graphscale, True );


  /* Need something else with constant */
  if ( getVActive() )
    XtSetSensitive( GraphconstToggle, True );
  else {
    XtSetSensitive( GraphconstToggle, False );

    /* Set button display to off, set account attrib
    ** to off, checkpoint all accounts */
    XmToggleButtonSetState( GraphconstToggle, False, False );
    graph_set_var ( CONST-1, XmUNSET );
    constCheckPoint ( True, -1 );
  }


  if ( v_active & CONSTS)   /* Constant rate, enable field entry */
    XtSetSensitive( Graphcform, True );
  else
    XtSetSensitive( Graphcform, False );


  drawDrawingArea( ); 

}

/* 
** Switch from/to single/multi account mode.  Display attributes for the
** current account's selected vars.  Switch next attrib from/to single/multi.
** drawDrawingArea is necessary to update the graph.
*/

/* ARGSUSED */
void graph_pselect ( Widget w, XtPointer client_data, XtPointer call_data)
{
  int which = (int) client_data;
  XmToggleButtonCallbackStruct *state = 
         (XmToggleButtonCallbackStruct *) call_data;

  extern Widget Graphhline, Graphhbar;
  PLOT_VAR *restore;

  int i, enable;
  int save_p = multiAccount; 

  /* Only change state variable on toggle set, not toggle unset */
  if ( state->set && which == 0 )
    multiAccount = False;
  else if ( state->set && which == 1 )
    multiAccount = True;

  if ( multiAccount != save_p ) {      /* Switch account modes */

    constCheckPoint ( False, (int)NULL );

    if (multiAccount) {
      /* In this mode, bar graphs are verboten */
      enable = False;
      getAccount ( activeAccount(), ACCOUNT_PLOT, &restore );
  
    } else {
      /* enable plot by_transaction */
      enable = True;
      restore = accountSingle;
    }

    /* This will set by_trans to False, which we depend on when the
    ** Graphvbuttons callback gets invoked.
    */
    if (!enable)
      XmToggleButtonSetState ( Graphhline, XmSET, True );
    XtSetSensitive ( Graphhbar, enable);

    /* This avoids the unsightly effect of v_active and graphVar->active not
    ** being in sync in graph_vselect. */
    v_active = 0;

    /* Change the legends */
    for (i = 0; i < NUM_PLOTS; i++) {

      if ( Graphvbuttons[i] != NULL ) {
        if ( restore->attrib[i] != -1 ) {
          XmToggleButtonSetState ( Graphvbuttons[i], True, False );
          graph_set_var ( i, XmSET );
        } else {
          XmToggleButtonSetState ( Graphvbuttons[i], False, False );
          graph_set_var ( i, XmUNSET );
        }
      }

    } /* for */
    
    Graph_vsense();

  }
}

void displayGraph( int toAccount )
{
  PLOT_VAR *to;
  int i;

  if (multiAccount) {

    constCheckPoint ( False, (int)NULL );

    getAccount ( toAccount, ACCOUNT_PLOT, &to );

    /* This avoids the unsightly effect of v_active and graphVar->active not
    ** being in sync in graph_vselect. */
    v_active = 0;

    /* Change the legends */
    for (i = 0; i < NUM_PLOTS; i++) {

      /* Never change CONST from here */
      if ( Graphvbuttons[i] != NULL ) {
        if ( to->attrib[i] != -1 ) {
          XmToggleButtonSetState ( Graphvbuttons[i], True, False );
          graph_set_var ( i, XmSET );
        } else {
          XmToggleButtonSetState ( Graphvbuttons[i], False, False );
          graph_set_var ( i, XmUNSET );
        }
      }

    } /* for */

    Graph_vsense();  /* Redisplay plot window */
  }

}


/* Any plots on in current mode? Ignore CONSTANT plot */
int getVActive()
{
  int i;
  int on = 0;
  PLOT_VAR *multi;
  
  if (multiAccount) {
    for (i = 1; i <= numAccounts(); i++) {
      getAccount ( i, ACCOUNT_PLOT, &multi );
      on |= multi->valid;
    }
    return (on & ~CONSTS);
  } 

  if (accountSingle == (PLOT_VAR *)NULL )
    accountSingle = makeGraphVar();
  return ( accountSingle->valid & ~CONSTS);
}


void graph_vselect ( Widget w, XtPointer client_data, XtPointer call_data)
{

  XmToggleButtonCallbackStruct *state = 
        (XmToggleButtonCallbackStruct *) call_data;
  int which = (int)client_data;
  int attrib;

  /* Remember where this button is so we can set/get legends for it */
  Graphvbuttons[which] = w;

  if (state->set) 
    attrib = graph_set_var ( which, XmSET );
  else
    attrib = graph_set_var ( which, XmUNSET );

  if ( which == CONST-1 )
    constCheckPoint ( True, attrib );

  Graph_vsense();
}


void graphSense( int graphing)
{
  extern Widget Optionmenu;
  Widget zoom, spnt, grid;

  zoom = XtNameToWidget ( Optionmenu, "button_0" );
  spnt = XtNameToWidget ( Optionmenu, "button_1" );
  grid = XtNameToWidget ( Optionmenu, "button_2" );

  if (graphing) {
     XtSetSensitive ( zoom, True);
     if (by_trans) 
       XtSetSensitive ( spnt, False);
     else
       XtSetSensitive ( spnt, True);
     XtSetSensitive ( grid, True);
  } else {
    XtSetSensitive ( zoom, False);
    XtSetSensitive ( spnt, False);
    XtSetSensitive ( grid, False);
  }
}


void graph_hselect ( Widget w, XtPointer client_data, XtPointer call_data)
{
  
  int which = (int) client_data;
  XmToggleButtonCallbackStruct *state = 
         (XmToggleButtonCallbackStruct *) call_data;

  int save_t = by_trans;

  Widget scaleLabel;
  XmString label_str;

  PLOT_VAR *accountGraph;
  int i;

  if (state->set && which == 0)
    by_trans = True;
  else if (state->set && which == 1) 
    by_trans = False;

  /* Set menu sensitive; since we're here, graphing is on (True) */
  graphSense( True );
     
  /* Something changed, redraw the graph. No select/no select states have 
  ** changed. */
  if ( by_trans != save_t) {

    constCheckPoint ( False, (int)NULL );

    /* Change the MAVG label */
    XtVaGetValues ( XtParent(w), XmNuserData, &scaleLabel, NULL);
    if (by_trans)
      label_str = XmStringCreateLocalized ( "Trans" );
    else
      label_str = XmStringCreateLocalized ( "Weeks" );
    if (scaleLabel)
      XtVaSetValues ( scaleLabel, XmNlabelString, label_str, NULL);
    XmStringFree ( label_str );

    /* Get the attributes for this set of legends */
    accountGraph = graph_attrib ( activeAccount() );

    /* Change the legends */
    for (i = 0; i < NUM_PLOTS; i++) {
      if ( Graphvbuttons[i] != NULL ) {
        if ( accountGraph->attrib[i] != -1 ) 
          graph_set_var ( i, XmSET );
        else
          graph_set_var ( i, XmUNSET );
      }
    }

    drawDrawingArea( ); 
  }
}

/* Just replot the graph on moving average value change */
/* ARGSUSED */
void graph_slide ( Widget w, XtPointer client_data, XtPointer call_data)
{
  drawDrawingArea( ); 
}

/* ARGSUSED */
void graph_option (Widget w, XtPointer client_data, 
                   XmToggleButtonCallbackStruct *call_data)
{
  int option = (int) client_data;

  switch (option) {

    case 0: zoom = (call_data->set == XmSET);
            break;
    case 1: show_point = (call_data->set == XmSET);
            break;
    case 2: grid = (call_data->set == XmSET);
            break;
    default:
            return;
  }
  drawDrawingArea( ); 
}


/* ARGSUSED */
void const_change ( Widget w, XtPointer client_data, XtPointer call_data)
{
  
  char  *val, *check;
  char  errmsg[80];

  val = XmTextGetString ( w );
  strtod ( val, &check);
  if ( check >= val && check < (val + strlen(val)) ) {
     sprintf ( errmsg, "Not a valid number: \'%s\'", val);
     write_status ( errmsg, ERR);
     XtFree ( val );

     /* Select the text, so user knows to change it */
     XmTextSetSelection ( w, 0, XmTextGetLastPosition(w), CurrentTime );
     return;
  }
  XtFree ( val );
  drawDrawingArea( ); 
}

/* Make next obscured list item visible.  It's traversed to regardless */
/* ARGSUSED */
void graphTraverse( Widget w, XtPointer client_data, XtPointer call_data)
{
  XmTraverseObscuredCallbackStruct *cbs = 
     (XmTraverseObscuredCallbackStruct *) call_data;

  int next, prev;
  int position;
  int numChildren = 0;

  /* Get which position we're headed to */
  XtVaGetValues ( cbs->traversal_destination, 
                  XmNuserData, &position, 
                  NULL);

  /* Get number of positions from row column */
  XtVaGetValues ( XtParent(XtParent(cbs->traversal_destination)), 
                  XmNnumChildren, &numChildren, 
                  NULL);

  /* All the ways we could go to next or previous widget */
  next = cbs->direction == XmTRAVERSE_DOWN || cbs->direction == XmTRAVERSE_NEXT
         || cbs->direction == XmTRAVERSE_RIGHT;
  prev = cbs->direction == XmTRAVERSE_UP || cbs->direction == XmTRAVERSE_PREV
         || cbs->direction == XmTRAVERSE_LEFT;

  /* Don't wrap at the boundaries, reset focus to previous list item */
  if (position == 0 && next) 
    XmProcessTraversal ( w, XmTRAVERSE_PREV );
  else if (position == numChildren-1 && prev )
    XmProcessTraversal ( w, XmTRAVERSE_NEXT );
  else 
    XmScrollVisible ( w, cbs->traversal_destination, 0, 0);
}


static void calc_xy ( XPoint *pnt, long xvalue, double yvalue, 
                      struct attrib *plot )
{
  double intermediate;

  intermediate = yvalue * (double)plot->scale - (double)plot->min;
  pnt->y = plot->height -  (int) ( intermediate / (double)plot->range * 
                                   (double)(plot->height-t_border));
  pnt->x = (int) ((xvalue - plot->daymin) * plot->width ) 
           / plot->dayrange;
  pnt->x += x_border;
}

static void plot_xy ( XPoint *pnts, struct attrib *plot, int attrib,
                      Widget Drawing, GC gc, Pixmap Drawpixmap )
{
  Pixel  savefore;

  XtVaGetValues ( Drawing, XmNforeground, &savefore, NULL );

  SetDrawAttrib( Drawing, gc, attrib ); 

  if (plot->num_pnts == 1)
    XFillRectangle ( XtDisplay(Drawing), Drawpixmap, gc,
                     pnts[0].x - 2, pnts[0].y -2, 4, 4 );
  else
    XDrawLines ( XtDisplay(Drawing), Drawpixmap, gc, pnts, plot->num_pnts,  
                 CoordModeOrigin);

  if (show_point) {
    XPoint daypnts[2];
    int i, daywidth;

    /* How wide is a day in pixels */
    if (plot->dayrange >= 2) {
      calc_xy ( &(daypnts[0]), plot->daymin, (double)0.0, plot);
      calc_xy ( &(daypnts[1]), plot->daymin+1, (double)0.0, plot);
      daywidth = daypnts[1].x - daypnts[0].x +2;
      if (daywidth < 6)
        daywidth = 6;
    } else
     daywidth = 6;

    XSetLineAttributes( XtDisplay(Drawing), gc, 0,
                        LineSolid, CapNotLast, JoinBevel ); 

    for (i = 0; i < plot->num_pnts; i++)
      XFillRectangle ( XtDisplay(Drawing), Drawpixmap, gc,
                       pnts[i].x - daywidth/2, pnts[i].y -3, 
                       daywidth, 6 );
  }

  ResetDrawAttrib ( Drawing, gc, savefore );
}

static int reset = 0;
static void calc_plot_reset ()
{
  reset = 0;
}

static void calc_plot_bar ( double value, int plot_type, 
                            struct attrib *plot, 
                            Widget Drawing, GC gc, Pixmap Drawpixmap )
{
  PLOT_VAR *accountGraph;

  int  width, height;
  int  offset;
  int  i;

  double intermediate;

  static int prev_x[ NUM_PLOTS +1];
  static int prev_py[ NUM_PLOTS +1];
  static int prev_ny[ NUM_PLOTS +1];

  Pixel foreground, background;

  /* Save foreground and background */
  XtVaGetValues ( Drawing, 
                  XmNbackground, &background, 
                  XmNforeground, &foreground, 
                  NULL );

  /* Get plot attribute for this account and variable */
  accountGraph = graph_attrib ( activeAccount() );
  SetDrawAttrib( Drawing, gc, accountGraph->attrib[plot_type-1]); 

  /* Reset on first point; first bar needs starting point */
  if ( reset == 0 ) {
    for (i= PRICE; i <= NUM_PLOTS; i++) {
      prev_x[i] = x_border; 
      prev_py[i] = prev_ny[i] = 0; 
    }
    reset = 1;         /* only reset once */
  }

  /* Do we need to calculate full screen projections? */
  if ( plot->min > 0 ) 
    intermediate = value*(double)plot->scale - (double)plot->min;
  /* Need to calculate size from zero line */
  else 
    intermediate = abs ( value*(double)plot->scale );

  height = (intermediate/(double)plot->range) * (double)(plot->height-t_border);
  width = ((int)plot->width / plot->num_pnts);
                          
  /* Detect if this bar would blot out previous, smaller bar */
  offset = 0;
  for (i = PRICE; i < plot_type; i++) {
    if ( value >= 0.0 ) {
      if ( height > prev_py[i] && prev_py[i] > offset)
        offset = prev_py[i];
    } else  {
      if ( height > prev_ny[i] && prev_ny[i] > offset)
        offset = prev_ny[i];
    }
  }
                       
  /* Draw rectangle */
  if ( plot->min < 0 )
    XFillRectangle ( XtDisplay(Drawing), Drawpixmap, gc,
                     prev_x[plot_type], 
                     (value >= 0.0)? plot->zero-height : plot->zero + offset,
                     width, 
                     height-offset );
  else
    XFillRectangle ( XtDisplay(Drawing), Drawpixmap, gc,
                     prev_x[plot_type], plot->height-height,
                     width, height-offset );
    
  XSetLineAttributes( XtDisplay(Drawing), gc,
                      0, LineSolid, CapNotLast, JoinBevel );
  XSetFillStyle ( XtDisplay (Drawing), gc, FillSolid );
  XSetForeground( XtDisplay(Drawing), gc, GetColor( BLACK ) );

  /* Draw outline */
  if ( plot->min < 0 )
    XDrawRectangle ( XtDisplay(Drawing), Drawpixmap, gc,
                     prev_x[plot_type], 
                     (value >= 0.0)? plot->zero-height : plot->zero +offset,
                     width, 
                     height-offset );
  else
    XDrawRectangle ( XtDisplay(Drawing), Drawpixmap, gc,
                     prev_x[plot_type], plot->height-height,
                     width, height-offset );
   
  prev_x[plot_type] += width; 

  if (value >= 0.0) {
    prev_py[plot_type] = height;
    prev_ny[plot_type] = 0;
  } else {
    prev_py[plot_type] = 0;
    prev_ny[plot_type] = height;
  }

  /* Restore colors */
  ResetDrawAttrib ( Drawing, gc, foreground );
}


/* 
** This routine knows how to calculate each possible plot value for a
** given transaction.  Returns structure holding these values and a 
** status word defining which are valid.
*/
static struct trans_value *calc_trans_values( int account, int transno, 
                                              struct attrib *plot) 
{
 PLOT_VAR *graphVar;
 void   *transactions;
 long   trans_date;
 int    trans_type;
 int    trans_reinvest;
 int    trans_major, trans_minor;
 double trans_shares;
 double trans_nav;
 double trans_load;

 NAV *navp;
 float nav_value;
  
 static struct trans_value trans;

 graphVar = graph_attrib ( account );
 getAccount ( account, ACCOUNT_TRANS, &transactions );

 trans_date = GetTransLDate(transactions, transno);
 trans_type = GetTransType(transactions, transno);
 trans_shares = GetTransShares(transactions, transno);
 trans_nav = GetTransNav(transactions, transno);
 trans_load = GetTransLoad(transactions, transno);
 trans_reinvest = GetTransFlag(transactions, transno, TRANS_REINVEST);
 trans_major = GetTransSplit(transactions, transno, TRANS_MAJOR);
 trans_minor = GetTransSplit(transactions, transno, TRANS_MINOR);

 getAccount ( account, ACCOUNT_NAV, &navp );
 getNav ( navp, NAV_VALUE, &nav_value );


 /* Price */
 trans.value[PRICE-1] = trans_nav;

 /* Shares, if not reinvesting dividends don't count */
 if ( trans_type != T_DIST || trans_reinvest )
   trans.value[SHARE-1] = trans_shares;
 else
   trans.value[SHARE-1] = 0.0;
 /* Possible load on sale, we now have less shares */
 if ( trans_type == T_SELL )
   trans.value[SHARE-1] -= (trans_load / trans_nav);

 /* Cost */
 if ( trans_type & (T_BUY | T_SELL) ) 
   trans.value[COST-1] = trans_nav * trans_shares + trans_load;
 else if ( trans_type == T_DIST && !trans_reinvest )
   trans.value[COST-1] = trans_nav * -trans_shares + trans_load;
 else
   trans.value[COST-1] = 0.0;

 /* Value */
 trans.value[VALUE-1] = nav_value * trans.value[SHARE-1];

 /* Distribution */
 if ( trans_type == T_DIST )
   trans.value[DISTRIB-1] = trans_nav * trans_shares;
 else
   trans.value[DISTRIB-1] = 0.0;

 /* Initialize all cumulative sums */
 if (transno == 1) {
   trans.value[SHARE_CUM-1] = 0.0;
   trans.value[COST_CUM-1] = 0.0;
   trans.value[VALUE_CUM-1] = 0.0;
   trans.value[DISTRIB_CUM-1] = 0.0;
 }

 /* Shares cumulative */
 trans.value[SHARE_CUM-1] += trans.value[SHARE-1];
 if (trans_type == T_SPLIT)
   trans.value[SHARE_CUM-1] = (trans.value[SHARE_CUM-1] * trans_major) 
	                      / trans_minor;

 /* Cost cumulative */
 trans.value[COST_CUM-1] += trans.value[COST-1];

 /* Value cumulative */
 trans.value[VALUE_CUM-1] += trans.value[VALUE-1];

 /* Distribution cumulative */
 trans.value[DISTRIB_CUM-1] += trans.value[DISTRIB-1];

 /* IRR */
 if ( PLOT_ACTIVE(IRRRATE) ) {

   /* Return is zero by def'n and IRR has trouble converging, so
   ** keeps warning from being posted. */
   if (transno == 1) {
     trans.value[IRRRATE-1] = 0.0; 
     rateClrTrans();
     rateAddTrans ( transactions, transno);
   } else {
     rateAddTrans ( transactions, transno );
     if ( trans_reinvest )
       trans.value[IRRRATE-1] = rate_IRR( trans.value[SHARE_CUM-1] * trans_nav);
     else
       trans.value[IRRRATE-1] = rate_IRR( trans.value[SHARE_CUM-1] * trans_nav +
		                          trans.value[DISTRIB_CUM-1] ); 
   }
 }

 /* TR */
 if ( PLOT_ACTIVE(TRRATE) ) {
   NAV this;
   double cost;

   setNav (&this, NAV_VALUE, &trans_nav);
   setNav (&this, NAV_DATE, GetTransDate( transactions, transno));
   cost = account_cost ( account, &this );

   if ( cost == (double)0.0 )
     trans.value[TRRATE-1] = 0.0;
   else
     trans.value[TRRATE-1] = 
        100.0 * ( (account_value ( account, &this ) + 
                   account_withdrawal( account, &this ) - cost) / cost );
 }

 /* Moving averages */
 if ( PLOT_ACTIVE(PRICE_MAVG) || PLOT_ACTIVE(SHARE_MAVG) || 
      PLOT_ACTIVE(COST_MAVG) || PLOT_ACTIVE(DISTRIB_MAVG) ) {
   int j;

   trans.value[PRICE_MAVG-1] = 0.0;
   trans.value[SHARE_MAVG-1] = 0.0;
   trans.value[COST_MAVG-1] = 0.0;
   trans.value[DISTRIB_MAVG-1] = 0.0;

   if (plot->by_trans) {
     if (transno >= plot->mov_window) {
       for (j = transno-plot->mov_window+1; j <= transno; j++) {
	 int major, minor;

         trans.value[PRICE_MAVG-1] += GetTransNav(transactions, j);

         if ( GetTransType(transactions,j) != T_DIST || 
	      GetTransFlag(transactions,j,TRANS_REINVEST) )
           trans.value[SHARE_MAVG-1] += GetTransShares(transactions, j);
         if ( GetTransType(transactions,j) == T_SELL)
           trans.value[SHARE_MAVG-1] -= 
                GetTransLoad(transactions,j)/GetTransNav(transactions,j);
	 if ( GetTransType(transactions,j) == T_SPLIT) {
	   major = GetTransSplit (transactions,j,TRANS_MAJOR);
	   minor = GetTransSplit (transactions,j,TRANS_MINOR);
           trans.value[SHARE_MAVG-1] = (trans.value[SHARE_MAVG-1] * major)
		                       / minor;
	 }

         if ( GetTransType(transactions, j) & (T_BUY | T_SELL) )
           trans.value[COST_MAVG-1] += GetTransNav(transactions, j) *
                                       GetTransShares(transactions, j)
                                       + GetTransLoad(transactions, j);
         if ( GetTransType(transactions,j) == T_DIST && 
	     !GetTransFlag(transactions,j,TRANS_REINVEST) )
           trans.value[COST_MAVG-1] += GetTransNav(transactions, j) *
                                       -GetTransShares(transactions, j)
                                       + GetTransLoad(transactions, j);

         if ( GetTransType(transactions, j) & T_DIST )
           trans.value[DISTRIB_MAVG-1] += GetTransNav(transactions, j) * 
                                          GetTransShares(transactions, j);
       }
       trans.value[PRICE_MAVG-1] /= (float)plot->mov_window;
       trans.value[SHARE_MAVG-1] /= (float)plot->mov_window;
       trans.value[COST_MAVG-1] /= (float)plot->mov_window;
       trans.value[DISTRIB_MAVG-1] /= (float)plot->mov_window;
     }

   } else { /* by date */
     if ( trans_date - plot->daymin > plot->mov_window*7 ) {
       j = transno;
       while ( j > 0 && (trans_date - GetTransLDate(transactions, j)) 
                         <= plot->mov_window*7 ) {
	 int major, minor;

         trans.value[PRICE_MAVG-1] += GetTransNav(transactions, j);

         if ( GetTransType(transactions,j) != T_DIST || 
	      GetTransFlag(transactions,j,TRANS_REINVEST) )
           trans.value[SHARE_MAVG-1] += GetTransShares(transactions, j);
         if ( GetTransType(transactions,j) == T_SELL)
           trans.value[SHARE_MAVG-1] -= 
                GetTransLoad(transactions,j)/GetTransNav(transactions,j);
	 if ( GetTransType(transactions,j) == T_SPLIT) {
	   major = GetTransSplit (transactions,j,TRANS_MAJOR);
	   minor = GetTransSplit (transactions,j,TRANS_MINOR);
           trans.value[SHARE_MAVG-1] = (trans.value[SHARE_MAVG-1] * major)
		                       / minor;
	 }

         if ( GetTransType(transactions, j) & (T_BUY | T_SELL) )
           trans.value[COST_MAVG-1] += GetTransNav(transactions, j) * 
                                       GetTransShares(transactions, j)
                                       + GetTransLoad(transactions, j);
         if ( GetTransType(transactions,j) == T_DIST && 
	     !GetTransFlag(transactions,j,TRANS_REINVEST) )
           trans.value[COST_MAVG-1] += GetTransNav(transactions, j) * 
                                       -GetTransShares(transactions, j)
                                       + GetTransLoad(transactions, j);

         if ( GetTransType(transactions, j) == T_DIST)
           trans.value[DISTRIB_MAVG-1] += GetTransNav(transactions, j) * 
                                          GetTransShares(transactions, j);
         j--;
       }
       trans.value[PRICE_MAVG-1] /= (float)(transno-j);
       trans.value[SHARE_MAVG-1] /= (float)(transno-j);
       trans.value[COST_MAVG-1] /= (float)(transno-j);
       trans.value[DISTRIB_MAVG-1] /= (float)(transno-j);
     }
   }
 }

 return (&trans);

}


/* 
** Run through all transactions in an account, picking out the largest and
** smallest plot values that are currently active.
*/
static void max_or_min ( int account, struct attrib *plot ) 
{
  PLOT_VAR *graphVar;
  struct trans_value *trans;
  int numTrans;
  int transno, plot_type;
  int err_on = suppressed();

  graphVar = graph_attrib ( account );
  getAccount( account, ACCOUNT_NUM_TRANS, &numTrans );

  /* No warning messages about calculations when only determining range. 
  ** They're enabled when the drawing routine recalculates them. */

  suppress (True);

  for (transno = 1; transno <= numTrans; transno++) {
    trans = calc_trans_values ( account, transno, plot );

    for (plot_type = PRICE; plot_type <= NUM_PLOTS; plot_type++)
      if ( PLOT_ACTIVE(plot_type) && plot_type != CONST ) {
        MAX_OR_MIN ( trans->value[plot_type-1], plot->scale )
      }
  }
 
  /* only off if not previously on */
  if (!err_on)
    suppress (False);

}


void plot ( Widget Drawing, Pixmap Drawpixmap, GC gc ) 
{
  extern Widget Graphcform;
  extern Widget Graphscale;

  int    i, j;
  int    x, y;

  int    div, perdiv;

  struct attrib plot_attrib;

  XPoint *pnts = NULL;
  Pixel  savefore, saveback;
  extern XFontStruct *small;

  int account = activeAccount();
  PLOT_VAR *graphVar;
  void *trans;
  int numTrans;
  long trans_date;
  NAV *navp;

  /* 
  ** Determine size of various graph areas: title, plot, scale areas.
  */

  XtVaGetValues (Drawing, XmNforeground, &savefore, 
                          XmNbackground, &saveback,
                          XmNheight,     &plot_attrib.height,
                          XmNwidth,      &plot_attrib.width,
                 NULL );

  x_border = XTextWidth (small, "9999999", strlen("9999999")) +5;
  y_border = small->ascent + small->descent +5;
  t_border = small->ascent + small->descent;

  /* Don't plot on horizontal ticks */
  plot_attrib.height -= y_border;  
  /* Don't plot on vertical ticks */
  plot_attrib.width  -= x_border;  

  /* Suck up any extra pixels into title area */
  t_border += ( (plot_attrib.height-t_border) % PIXELS_PER_VDIV);


  /* 
  ** Get range of graph, both horizontal and vertical
  */

  /* Get size of moving window */
  XmScaleGetValue ( Graphscale, &(plot_attrib.mov_window) );

  /* This is a better starting min (y) than zero */
  plot_attrib.min = INT_MAX;
  plot_attrib.max = 0;

  /* Needed for max/min calculation */
  plot_attrib.by_trans = by_trans;
  plot_attrib.scale = 100; 

  if (multiAccount) {
    long daymax = 0L;
    long daymin = 0L;

    /* Loop though all accounts.  If any plot vars are on get the 1st and
    ** last transaction dates.  Compare against current min and max.  Save if
    ** new endpoint.  Note that min and max may be in different accounts. 
    ** Check for max/min graph values similarly.  Graph by transaction is not 
    ** supported. Ignore this account if only CONST is on. */
   
    for ( account = 1; account <= numAccounts(); account++ ) {
      getAccount ( account, ACCOUNT_PLOT, &graphVar );
      if ( graphVar->valid & ~(CONSTS) ) {
        getAccount( account, ACCOUNT_NUM_TRANS, &numTrans );
        getAccount( account, ACCOUNT_TRANS, &trans );
        if ( daymin == 0L || GetTransLDate( trans, 1 ) < daymin )
          daymin = GetTransLDate( trans, 1 );
        if ( GetTransLDate(trans, numTrans) > daymax )
          daymax = GetTransLDate( trans, numTrans );
        max_or_min ( account, &plot_attrib );
      }
    }
    plot_attrib.daymin = daymin -1;
    plot_attrib.dayrange = daymax - plot_attrib.daymin +1;

  } else {
    /* Just like above, but only one account. Graph by transaction is 
    ** supported. */
    getAccount( account, ACCOUNT_NUM_TRANS, &numTrans );
    getAccount( account, ACCOUNT_TRANS, &trans );
    getAccount( account, ACCOUNT_NAV, &navp );
    if ( by_trans == True ) {
      plot_attrib.daymin   = 1;
      plot_attrib.dayrange = numTrans;
    } else {
      /* Never have a range of zero, pad each end of plot by one day */
      plot_attrib.daymin =   GetTransLDate(trans, 1) -1;
      plot_attrib.dayrange = GetTransLDate(trans, numTrans) - 
                               plot_attrib.daymin +1;
    }
    max_or_min ( account, &plot_attrib );
  }

  /*
  ** Calculate various properties relating to the graph scale.
  */

  /* If not zooming, start at negative value or zero */
  if ( !zoom  && plot_attrib.min > 0)
     plot_attrib.min = 0;
  plot_attrib.range = plot_attrib.max - plot_attrib.min;


  /* Maximize the scale if we can */
  plot_attrib.scale = 100;             /* Scale to the penny */

  /* Can we boost precision to tenth of a penny? */
  if ( plot_attrib.range <= INT_MAX/10 &&
       plot_attrib.max   <= INT_MAX/10 &&
       plot_attrib.min   <= INT_MAX/10 ) {
    plot_attrib.scale *= 10;     /* looks like we can */
    plot_attrib.max *= 10;
    plot_attrib.min *= 10;
    plot_attrib.range = plot_attrib.max - plot_attrib.min;
  }


  /* Make the range a multiple of the per division value.  */

  /* How many divisions and the value per division at "real" range. */
  div =  (plot_attrib.height - t_border)/ PIXELS_PER_VDIV;

  if (plot_attrib.range == 0)        /* Bad thing #1 range = 0 */

    if (plot_attrib.max == 0)        /* Bad thing #2 max == 0, range will = 0 */
                                     /* Probably have a plot of all zeros */
      perdiv =  1*plot_attrib.scale; /* No max, make it a buck */
    else
      perdiv =  plot_attrib.max;     /* No range, make max one division */

  else                               /* Normal case divide up range */
    perdiv =  plot_attrib.range / div;


  /* Make min and max halfway into upper or lower grid */

  if (plot_attrib.min != 0)
    plot_attrib.min -= perdiv/2;
  plot_attrib.max += perdiv/2;
  plot_attrib.range = plot_attrib.max - plot_attrib.min;

  /* Final parameters of the graph */

  /* Value per tick */
  perdiv = (int)(plot_attrib.range / div);

  /* Real range after truncated math */
  plot_attrib.range = perdiv * div;

  plot_attrib.num_pnts  = numTrans;

  /* Where is the zero line */
  plot_attrib.zero =  plot_attrib.height - 
                          ( (int)( (plot_attrib.height-t_border) * 
                                   -plot_attrib.min) / plot_attrib.range );
    

  /*
  ** Loop through each source of info generating the coordinates 
  **
  */

  /* Plot x,y lines */
  if ( !by_trans ) {

    if (multiAccount)
      account = 1;

    do {
      graphVar = graph_attrib ( account );
      getAccount ( account, ACCOUNT_NUM_TRANS, &numTrans );
      getAccount ( account, ACCOUNT_TRANS, &trans );

      if (graphVar->valid) {   /* Anybody on */

        if (numTrans == 1 && !suppressed()) 
          write_status ("Only one point in this line.", WARN);

        /* Allocate points for XDrawLines */
        pnts = (XPoint *) XtRealloc ( (char *)pnts, 
                                      sizeof(XPoint)*(numTrans + 1) );
        if (!pnts) {
          write_status ("Couldn't allocate memory for plot points.", ERR);
          return;
        }

        plot_attrib.num_pnts  = numTrans;

        /* Loop through each line, generate all lines, place into pnts */
        for (i = 1; i <= NUM_PLOTS; i++) {

          if ( PLOT_ACTIVE(i) && i != CONST) {

            for (j = 1; j <= numTrans; j++) {
              struct trans_value *t;

              trans_date = GetTransLDate ( trans, j );
              t = calc_trans_values ( account, j, &plot_attrib );
              calc_xy ( &pnts[j], trans_date, t->value[i-1], &plot_attrib );
            }

            /* ok, draw lines onto pixmap */
            plot_xy ( &pnts[1], &plot_attrib, graphVar->attrib[i-1],
                      Drawing, gc, Drawpixmap );

          } /* if plot active */
        }
      }

      if (multiAccount)  /* next account */
        account++;
        
    } while ( multiAccount && account <= numAccounts() );

  } else {  /* plot bar graph */

    /* This time loop through each transaction and generate bar for
    ** each transaction.  This is to allow overlap detection. */

    graphVar = graph_attrib ( account );
    calc_plot_reset();

    for (i = 1; i <= numTrans; i++) {
      struct trans_value *t;

      t = calc_trans_values ( account, i, &plot_attrib );

      for (j = PRICE; j <= NUM_PLOTS; j++) {
        /* 
        ** THIS ORDER IS IMPORTANT BECAUSE calc_plot_bar ONLY CHECKS
        ** THE "LESSER" PLOT TYPES FOR OVERLAP
        */
        if ( PLOT_ACTIVE (j) )
          calc_plot_bar ( t->value[j-1], j, &plot_attrib,
                          Drawing, gc, Drawpixmap );
      }
    } /* end for */

  } /* end if by bars*/

         
  /* This doesn't match the other plot types so is handled separately */ 
  if ( PLOT_ACTIVE( CONST ) ) {   /* constant at fixed rate */

    static Widget wstart = (Widget) NULL; 
    static Widget wrate = (Widget) NULL;
    static GC const_gc = NULL;
    XRectangle plot_clip;
    PLOT_VAR *accountGraph;

    char  *str;
    float start, end;
    float rate;
    int   start_y;

    /* need range by date even if plotting by transaction */
    long graphdays = GetTransLDate(trans, numTrans) - GetTransLDate(trans, 1);

    /* Find our widget names from the parent */
    if (wstart == NULL || wrate == NULL) {
      wstart = XtNameToWidget ( Graphcform, "Graphvstart");
      wrate = XtNameToWidget ( Graphcform, "Graphvrate");
    }

    /* Get user selection. Checked in text callback. */
    str = XmTextGetString ( wstart );
    start = strtod ( str, NULL ) * plot_attrib.scale;
    XtFree ( str );

    str = XmTextGetString ( wrate );
    rate = strtod ( str, NULL );           /* Checked in text callback */
    XtFree ( str );

    /* Y is calculated as an APR per year, w/o compounding */
    start_y = (float)(plot_attrib.height-t_border) * 
              (start - (float)plot_attrib.min) / (float)plot_attrib.range;   
    x = plot_attrib.width + x_border;

    /* Start plus interest */
    end = start + start*(rate/100.0 * graphdays/365.0);
    y = (float)(plot_attrib.height-t_border) * 
        (end - (float)plot_attrib.min) / (float)plot_attrib.range;
  
    /* Make a GC for setting the constant line clip area */
    if (const_gc == NULL ) 
       const_gc = XCreateGC ( XtDisplay(Drawing), 
                              RootWindowOfScreen( XtScreen(Drawing)), 0, 0);
    /* Get the plot drawing attribute. */
    accountGraph = graph_attrib ( activeAccount() );
    SetDrawAttrib ( Drawing, const_gc, accountGraph->attrib[CONST-1] );

    /* Do these values cross the plot range? */
    if (!( (start > plot_attrib.max && end > plot_attrib.max) ||
           (start < plot_attrib.min && end < plot_attrib.min)
         ) ) {

      /* Clip the line so that it only appears within the plot area */
      plot_clip.x = (short) x_border;
      plot_clip.y = (short) t_border;
      plot_clip.width = (short) plot_attrib.width;
      plot_clip.height = (short) plot_attrib.height - t_border;
      XSetClipRectangles ( XtDisplay(Drawing), const_gc, 
                           0, 0, &plot_clip, 1, Unsorted );

      XDrawLine ( XtDisplay(Drawing), Drawpixmap, const_gc, 
                  x_border, plot_attrib.height - start_y, 
                  x, plot_attrib.height - y );
    }
  }

  /* 
  ** Draw in scales, grid, etc.
  */ 
  {
    int    next; 
    int    str_width, str_height;

    /* user title from transaction file */
    char   *title = NULL; 
    char   status[80];
    char   tick[20];

    /* If we have errors, but had them before, don't inform user */
    int    errors = 0;

    Pixel bot_shadow;

    if ( isColorVisual( XtDisplay(Drawing) ) )
      XmGetColors ( XtScreen (Drawing), GetColormap(), saveback, 
                    NULL, NULL, &bot_shadow, NULL );
    else
      bot_shadow = saveback;

    title = createAccountName ( activeAccount() );
    getAccount( activeAccount(), ACCOUNT_NAV, &navp );

    /* 
    ** Draw display type and info about NAV change.
    */
    sprintf( status, 
             "PLOT=> %-11.11s   NAV=> %-7.7s   %30.30s", 
             (by_trans)?"Transaction":(multiAccount)?"Date Multi":"Date",
             (isNavChanged(activeAccount(),navp))?"Changed":"Last",
             (title)?title:NULL ); 
    XtFree (title);

    XSetFont ( XtDisplay (Drawing), gc, small->fid );
    str_height = small->ascent +1;
    str_width = XTextWidth ( small, status, strlen(status) );

    if ( bound_ok ( str_width +5, plot_attrib.width, &errors, 0) ) {
       XSetForeground( XtDisplay(Drawing), gc, bot_shadow );
       XDrawString( XtDisplay(Drawing), Drawpixmap, gc, 
                    (int)x_border +5 +1, str_height +1, status, strlen(status));
       XSetForeground( XtDisplay(Drawing), gc, savefore );
       XDrawString( XtDisplay(Drawing), Drawpixmap, gc, 
                    (int)x_border +5, str_height, status, strlen(status) );
    }

    /* Add tick marks and values */
    XSetLineAttributes( XtDisplay(Drawing), gc, 0, 
                        LineSolid, CapProjecting, JoinRound );
 
    /* Vertical ticks */
    next = plot_attrib.min;

    y = plot_attrib.height;
    do {

      XSetForeground( XtDisplay(Drawing), gc, savefore);
      /* Not encroaching on title area, draw the line */
      if ( y >= t_border)
        XDrawLine( XtDisplay(Drawing), Drawpixmap, gc,
                   x_border, y,
                   (grid == True)?plot_attrib.width+x_border:x_border+5, y);

      /* Adjust resolution */
      if ( (float)plot_attrib.range / (float)plot_attrib.scale > 99.0 ||
           (float)plot_attrib.min   / (float)plot_attrib.scale > 99.0 )
        sprintf( tick, "%5d", next/plot_attrib.scale);
      else
        sprintf( tick, "%5.2f", (float)next/(float)plot_attrib.scale);

      /* Center text around the tick mark */
      XSetForeground( XtDisplay(Drawing), gc, bot_shadow );
      XDrawString( XtDisplay(Drawing), Drawpixmap, gc,
                   x_border - XTextWidth( small, tick, strlen(tick)) -5 +1,
                   y +1,
                   tick, strlen(tick) );
      XSetForeground( XtDisplay(Drawing), gc, savefore );
      XDrawString( XtDisplay(Drawing), Drawpixmap, gc,
                   x_border - XTextWidth( small, tick, strlen(tick)) -5,
                   y,
                   tick, strlen(tick) );

      next += perdiv;
      y -= PIXELS_PER_VDIV;

    } while ( div-- > 0 );

    /* Perpendicular to vertical ticks */
    XSetForeground( XtDisplay(Drawing), gc, bot_shadow );
    XDrawLine( XtDisplay(Drawing), Drawpixmap, gc, 
               x_border+1, 0, x_border+1, plot_attrib.height);
    XSetForeground( XtDisplay(Drawing), gc, savefore);
    XDrawLine( XtDisplay(Drawing), Drawpixmap, gc, 
               x_border, 0, x_border, plot_attrib.height);

    /* Horizontal ticks */
    div = plot_attrib.width / PIXELS_PER_HDIV; 

    if (by_trans) {
      /* if there are less transactions than ticks, reduce ticks */
      if ( numTrans < div) 
        div = numTrans;
      /* start around middle of bar */
      x = x_border + (int)plot_attrib.width/div/2;
    } else {
      if ( plot_attrib.dayrange < div)
        div = plot_attrib.dayrange;
      x = x_border;
    }

    /*
    ** CAREFUL: I use the last "y" from above to tell me where the top
    ** of the graph is when grids are on.  Increment because loop went one
    ** too far.
    */
    y += PIXELS_PER_VDIV;

    for ( i = 0; i < div; i++,x += (int)plot_attrib.width/div) {

      if (by_trans)
        sprintf( tick, "%ld", (long) ( (x-x_border) / 
                 ((int)plot_attrib.width/numTrans)) + plot_attrib.daymin );
      else {
        /* To gain precision, convert to doubles.  Add 0.5 so values in the
        ** range of x.5 to x.99 round up when truncated by long cast. */
        long today = (long)(
                            (double)plot_attrib.daymin + 
                            (double)i*(double)plot_attrib.dayrange/(double)div +
                            0.5
                           );
        strcpy ( tick,  daytostr ( today, GetTransDate( trans, 1)) ); 
      }

      XSetForeground( XtDisplay(Drawing), gc, bot_shadow );
      XDrawString( XtDisplay(Drawing), Drawpixmap, gc, 
                   x +1, plot_attrib.height +15 +1, 
                   tick, strlen(tick) );
      XSetForeground( XtDisplay(Drawing), gc, savefore );
      XDrawString( XtDisplay(Drawing), Drawpixmap, gc, 
                   x, plot_attrib.height +15, 
                   tick, strlen(tick) );

      XSetForeground( XtDisplay(Drawing), gc, savefore);
      XDrawLine( XtDisplay(Drawing), Drawpixmap, gc,
                 x, (grid == True)?y:plot_attrib.height-5,
                 x, plot_attrib.height );

    }

    /* Perpendicular to horizontal ticks */
    XSetForeground( XtDisplay(Drawing), gc, bot_shadow );
    XDrawLine( XtDisplay(Drawing), Drawpixmap, gc, 
               x_border, plot_attrib.height +1, 
               plot_attrib.width + x_border, plot_attrib.height +1);
    XSetForeground( XtDisplay(Drawing), gc, savefore );
    XDrawLine( XtDisplay(Drawing), Drawpixmap, gc, 
               x_border, plot_attrib.height, 
               plot_attrib.width + x_border, plot_attrib.height);

    /* Highlight zero line if the graph goes negative */
    if (plot_attrib.min < 0 ) {
      XSetLineAttributes ( XtDisplay(Drawing), gc, 3, 
                           LineSolid, CapProjecting, JoinRound );
      XDrawLine( XtDisplay(Drawing), Drawpixmap, gc, 
                 x_border, plot_attrib.zero,
                 plot_attrib.width + x_border, plot_attrib.zero);
      XSetLineAttributes ( XtDisplay(Drawing), gc, 0, 
                           LineSolid, CapProjecting, JoinRound );
    }

    plot_attrib.width  += x_border;  /* put back scale area */
    plot_attrib.height += y_border;  /* put back height */

    XSetForeground( XtDisplay(Drawing), gc, savefore);
    XSetBackground( XtDisplay(Drawing), gc, saveback);

    /* If anything went awry, pop-up a message */
    /* We could get very specific, since we know where it went wrong */
    /* but don't right now. */
    if ( errors && !suppressed() )
      write_status ("Title too large, increase graph size or\n"
                    "reduce font sizes. See \"help Fonts\".", WARN ); 
  }

} /* end plot */
