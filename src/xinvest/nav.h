/*
  Xinvest is copyright 1995 by Mark Buser.

  Permission is hereby granted to copy
  and freely distribute copies of this
  program for non-commercial purposes
  without fee, provided that this notice
  appears in all copies.
  
  All redistributions must be in their
  entirety as originally distributed.
  Feel free to modify Xinvest, but
  modified versions may not be distributed
  without prior consent of the author.
  
  This software is provided 'as-is'
  without any express or implied warranty.
  In no event will the author be held
  liable for any damages resulting from
  the use of this software.

  $Revision: 2.3 $ $Date: 1996/11/08 22:42:40 $
*/
#ifndef NAV_H
#define NAV_H

typedef struct _NAV_info {
  int  month;
  int  date;
  int  year;
  float value;
} NAV;

#define NAV_VALUE 1
#define NAV_MONTH 2
#define NAV_DAY   3
#define NAV_YEAR  4
#define NAV_DATE  5

void getNav (NAV *, int, void *); /* Get a nav field */
void setNav (NAV *, int, void *); /* Change a nav field */
int  isNavChanged(int, NAV *);    /* NAV info same as last transaction? */
long navToLDate (NAV *);          /* Return a NAV date in long format */
#endif
