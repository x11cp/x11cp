/*
  Xinvest is copyright 1995, 1996 by Mark Buser.

  Permission is hereby granted to copy
  and freely distribute copies of this
  program for non-commercial purposes
  without fee, provided that this notice
  appears in all copies.
  
  All redistributions must be in their
  entirety as originally distributed.
  Feel free to modify Xinvest, but
  modified versions may not be distributed
  without prior consent of the author.
  
  This software is provided 'as-is'
  without any express or implied warranty.
  In no event will the author be held
  liable for any damages resulting from
  the use of this software.

  $Revision: 2.6 $ $Date: 1997/08/18 23:43:43 $
*/

#include <stdlib.h>

#include <Xm/DialogS.h>
#include <Xm/Form.h>
#include <Xm/Frame.h>
#include <Xm/LabelG.h>
#include <Xm/MessageB.h>
#include <Xm/PanedW.h>
#include <Xm/PushB.h>
#include <Xm/Text.h>

#include "xutil.h"

extern Widget Transtext;

/* Globals */
static Widget status_dialog = NULL;

static Widget text_w;
static char *text =  NULL;

/* Forward declarations */
void statusCB ( Widget, XtPointer, XtPointer );

static void TellUser ( Widget parent,
               int    type,
               char   *what )
{
  Widget dialog;
  XmString x_text;

  x_text = XmStringCreateLtoR ( what, XmFONTLIST_DEFAULT_TAG );

  dialog = XmCreateMessageDialog (parent, "alert", NULL, 0);
  XtVaSetValues ( dialog, 
                  XmNdialogType,    type,
                  XmNmessageString, x_text,
                  NULL );

  XtSetSensitive ( XmMessageBoxGetChild ( dialog, XmDIALOG_HELP_BUTTON),
                   False );
  XtUnmanageChild ( XmMessageBoxGetChild ( dialog, XmDIALOG_CANCEL_BUTTON) );

  XmStringFree (x_text);

  XtManageChild ( dialog );
  XtPopup ( XtParent (dialog), XtGrabNone );

}


static void createStatus()
{

  Widget pane, form, button;

  int n;
  Arg args[10];

  Dimension height, h; 
  XmTextPosition pos;

  if (status_dialog == NULL) {

    n = 0;
    XtSetArg(args[n], XmNdeleteResponse, XmUNMAP); n++;
    status_dialog = XmCreateDialogShell ( GetTopShell(Transtext),"Status", 
                                          args, n );

    pane = XtVaCreateWidget ( "pane", xmPanedWindowWidgetClass, status_dialog,
                               XmNsashWidth, 1,
                               XmNsashHeight, 1,
                               NULL );

    /* Scrolled text widget actual log text */
    n = 0;
    XtSetArg(args[n], XmNscrollVertical,        True); n++;
    XtSetArg(args[n], XmNscrollHorizontal,      False); n++;
    XtSetArg(args[n], XmNeditMode,              XmMULTI_LINE_EDIT); n++;
    XtSetArg(args[n], XmNeditable,              False); n++;
    XtSetArg(args[n], XmNcursorPositionVisible, False); n++;
    XtSetArg(args[n], XmNautoShowCursorPosition,  False); n++;
    XtSetArg(args[n], XmNwordWrap,  True); n++;
    if (text != NULL) {
      XtSetArg(args[n], XmNvalue, text); n++;
    }
    text_w = XmCreateScrolledText( pane, "text", args, n);

    XtManageChild( text_w );

    form = XtVaCreateWidget ( "form", xmFormWidgetClass, pane,
                              XmNfractionBase, 5,
                              NULL );
    button = XtVaCreateManagedWidget ( "OK", xmPushButtonWidgetClass, form,
                                       XmNtopAttachment,    XmATTACH_FORM,
                                       XmNbottomAttachment, XmATTACH_FORM,
                                       XmNleftAttachment,   XmATTACH_POSITION,
                                       XmNleftPosition,     2,
                                       XmNrightAttachment,  XmATTACH_POSITION,
                                       XmNrightPosition,    3,
                                       XmNshowAsDefault,    True,
                                       XmNdefaultButtonShadowThickness, 1,
                                       NULL );
    XtAddCallback ( button, XmNactivateCallback,
                           (XtCallbackProc) statusCB, (XtPointer)1 ); 

    XtManageChild ( form );
    XtManageChild ( pane );

    XtVaGetValues ( form,   XmNmarginHeight, &h, NULL);
    XtVaGetValues ( button, XmNheight, &height, NULL);
    
    height += 2 * h;

    XtVaSetValues ( form, XmNpaneMaximum, height, 
                          XmNpaneMinimum, height,
                    NULL );

    pos  = XmTextGetLastPosition ( text_w );
    XmTextShowPosition ( text_w, pos );

  }

}

/* ARGSUSED */
void statusCB ( Widget w, XtPointer client_data, XtPointer call_data)
{
  if (status_dialog == NULL)
    createStatus();

  if ( (int)client_data != 0) {
     XtUnmanageChild ( status_dialog);
     XtPopdown (status_dialog);
  } else { 
     XtManageChild ( status_dialog);
     XtPopup ( status_dialog, XtGrabNone );
  }
}


void write_status ( char *msg, int level )
{
  static lineno = 1;
  char message[512];
  char slevel[10];

  XmTextPosition pos;

  switch (level) {
    case XmDIALOG_INFORMATION:
         strcpy ( slevel, "INFO:");
         break;
    case XmDIALOG_WARNING:
         strcpy ( slevel, "WARNING:");
         break;
    case XmDIALOG_ERROR:
         strcpy ( slevel, "ERROR:");
         break;
    case XmDIALOG_MESSAGE:
         strcpy ( slevel, "LOG:");
         break;
  }

  sprintf ( message, "== %d == %.7s ========\n%s\n", lineno++, slevel, msg);
 
  if (text == NULL) {
    text = XtCalloc ( strlen(message)+1, sizeof(char) );
    strcpy ( text, message );
  } else {
    text = XtRealloc (text, strlen(text) + strlen(message)+1 );
    strcat ( text, message );
  }

  if (status_dialog) {
    pos  = XmTextGetLastPosition ( text_w );
    XmTextReplace ( text_w, pos, pos, message );   
    pos  = XmTextGetLastPosition ( text_w );
    XmTextShowPosition ( text_w, pos );
  }
  
  /* Messages just go to log area.  They are to make more clear where an
  ** error message came from.
  */
  if ( level != XmDIALOG_MESSAGE)
    TellUser (GetTopShell(Transtext), level, msg);
}

/* Bounds check.  If first param is less than or equal to second param
** all is well; return True.  Save this info in a bit mapped int.  This
** way we can remember what error occurred by its position in the int.
*/
int bound_ok ( value, bound, status, flag )
int value;
int bound;
int *status;
int flag;
{
   if ( value <= bound ) {
      *status &= ~(1 << flag);
      return True;
   } else {
      *status |= (1 << flag);
      return False;
   }
}

static int _suppress = False;

/* These routines allow one to disable any given error by querying this
** variable.  I don't want graph errors on expose redrawings.
*/

/* Suppress an error */
void suppress(int now)
{
   if ( now == True)
     _suppress = True;
   else
     _suppress = False;
}

/* Are errors suppressed */
int suppressed()
{
  return (_suppress == True);
}
