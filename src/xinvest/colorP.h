/*
  Xinvest Copyright 1997 by Mark Buser,
  All Rights Reserved.

  Permission is hereby granted to copy
  and freely distribute copies of this
  program for non-commercial purposes
  without fee, provided that this notice
  appears in all copies.
  
  All redistributions must be in their
  entirety as originally distributed.
  Feel free to modify Xinvest, but
  modified versions may not be distributed
  without prior consent of the author.
  
  This software is provided 'as-is'
  without any express or implied warranty.
  In no event will the author be held
  liable for any damages resulting from
  the use of this software.

  $Revision: 1.1 $ $Date: 1997/07/20 18:41:39 $
*/
#ifndef COLORP_H
#define COLORP_H

/* This must match order of enum Colors in color.h */
static char *colors[] = { 
NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,   /* user app colors */
"Red", "Forest Green", "Grey60", "White", "Black" /* local named colors */
};

#endif
