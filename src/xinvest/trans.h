/*
  Xinvest Copyright 1995-97 Mark Buser,
  All Rights Reserved.

  Permission is hereby granted to copy
  and freely distribute copies of this
  program for non-commercial purposes
  without fee, provided that this notice
  appears in all copies.
  
  All redistributions must be in their
  entirety as originally distributed.
  Feel free to modify Xinvest, but
  modified versions may not be distributed
  without prior consent of the author.
  
  This software is provided 'as-is'
  without any express or implied warranty.
  In no event will the author be held
  liable for any damages resulting from
  the use of this software.

  $Revision: 2.6 $ $Date: 1997/05/22 23:30:13 $
*/

#define T_BUY 	   1      /* For access routines, types of transactions */
#define T_SELL     2
#define T_DIST     4
#define T_PRICE    8
#define T_SPLIT   16

#define T_NEW      1      /* For rd_trans, start new transaction history */
#define T_SAME     0      /* For rd_trans, add to existing history */

/* Access routines */
int AreTrans();           /* Are there any transactions, returns how many */

/* Parse transaction line, new file if int is non-zero */
char *rd_trans( char *, int);

/* Return pointer to current transactions, NULL on none */
void *GetTrans();    

char *GetTransTitle();    /* Return title, NULL on none */
char *GetTransTicker();   /* Return ticker symbol, NULL on none */ 

/* The first param of the following is pointer to a TRANS structure.
** The second, the tranaction number of interest.
*/

/* Return date of transaction */
char *GetTransDate( void *, int );   /* as char */ 
long GetTransLDate( void *, int );   /* as long */ 

/* Return type of transaction (T_BUY, etc) */
int  GetTransType( void *, int );   

/* Return NAV of transaction */
double GetTransNav( void *, int );    

/* Return number of shares of transaction */
double GetTransShares( void *, int ); 

/* Return load of transaction */
double GetTransLoad( void *, int ); 

#define TRANS_REINVEST 1
/* Return value of requested flag */
int GetTransFlag ( void *, int, int);

#define TRANS_MAJOR 1
#define TRANS_MINOR 2
/* Return value of requested split */
int GetTransSplit ( void *, int, int);
