/*
  Xinvest is copyright 1995 by Mark Buser.

  Permission is hereby granted to copy
  and freely distribute copies of this
  program for non-commercial purposes
  without fee, provided that this notice
  appears in all copies.
  
  All redistributions must be in their
  entirety as originally distributed.
  Feel free to modify Xinvest, but
  modified versions may not be distributed
  without prior consent of the author.
  
  This software is provided 'as-is'
  without any express or implied warranty.
  In no event will the author be held
  liable for any damages resulting from
  the use of this software.

  $Revision: 1.3 $ $Date: 1997/01/13 22:44:03 $
*/

typedef struct {
  char *label; 
  char *question;
  char *yes;
  char *no;
  int  dflt;
  char *cmd;
} QandA;

#define YES 1
#define NO  2

int AskUser ( Widget parent,
              char   *question, 
              char   *ans1,
              char   *ans2,
              int    default_ans );
