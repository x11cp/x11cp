/*
  Xinvest Copyright 1995-97 Mark Buser,
  All Rights Reserved.

  Permission is hereby granted to copy
  and freely distribute copies of this
  program for non-commercial purposes
  without fee, provided that this notice
  appears in all copies.
  
  All redistributions must be in their
  entirety as originally distributed.
  Feel free to modify Xinvest, but
  modified versions may not be distributed
  without prior consent of the author.
  
  This software is provided 'as-is'
  without any express or implied warranty.
  In no event will the author be held
  liable for any damages resulting from
  the use of this software.

  $Revision: 2.8 $ $Date: 1997/08/09 19:41:00 $
*/

int activeAccount();              /* Return index number of current account */
int numAccounts();                 /* Return number of open accounts */



/* Return True if at least one account has 'flag' set */
#define ACCOUNT_DIRTY         1   /* The account has changed */
#define ACCOUNT_UPDATE        2   /* Changes need to be processed */
#define ACCOUNT_IN_PORTFOLIO  4   /* Account has been added  */
#define ACCOUNT_WRITABLE      8   /* Account file is writable  */

int accountStatus(int which, unsigned flag ); /* Is 'flag' set in 'which' */
int accountsStatus(int flag );    /* Is flag set anywhere */

/* Set account dirty flag to unmodified */
void accountClean( int );

#define ALL_ACCOUNTS -1           /* flag to add or delete all open accounts */
/* Set/clear account in portfolio flag */
void accountAddPortfolio( int );
void accountDelPortfolio( int );



/* Returns index number of account if passed filename matches an already open
** account. */
int accountIsOpen (char *);

/* Display the next account in line */
void cycleAccount();



/* Valid setAccount fields */
#define ACCOUNT_FILENAME  1
#define ACCOUNT_NUM_TRANS 2
#define ACCOUNT_TRANS     3
#define ACCOUNT_NUMTRANS  4
#define ACCOUNT_TITLE     5
#define ACCOUNT_TICKER    6
#define ACCOUNT_NAV       7
#define ACCOUNT_TRANSTEXT 8
#define ACCOUNT_PLOT      9

/* Set/get account fields */
void getAccount ( int, int, void *);
void setAccount ( int, int, void *);



/* Open an account.  1st param is filename, second pointer to text holding
** transactions, third file writable.  Returns index of new account */
int newAccount ( char *, char *, int);

/* Close account with index int */
void closeAccount ( int );

/* Process account transactions for account */
void processAccount ( int );

/* Update display with an account */
void displayAccount ( int );

/* Save account info into file */
void saveAccount ( FILE *, int);

/* Creation routine for account menu */
void makeAccountmenu();

/* Return a good name for an open account */
char *createAccountName (int);

/* Find account matching ticker name, update it's NAV */
int accountMatchTicker (char *, int, int, int, double);

/* Change background of Widget if passed date matches one in transaction */
void accountHighlightDay (char *, int, Widget);
