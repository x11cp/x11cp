/*
  Xinvest Copyright 1997 Mark Buser,
  All Rights Reserved.

  Permission is hereby granted to copy
  and freely distribute copies of this
  program for non-commercial purposes
  without fee, provided that this notice
  appears in all copies.
  
  All redistributions must be in their
  entirety as originally distributed.
  Feel free to modify Xinvest, but
  modified versions may not be distributed
  without prior consent of the author.
  
  This software is provided 'as-is'
  without any express or implied warranty.
  In no event will the author be held
  liable for any damages resulting from
  the use of this software.

  $Revision: 1.2 $ $Date: 1997/08/06 23:21:25 $
*/
#ifndef PIXMAPP_H
#define PIXMAPP_H

/* The pixmaps */
#include "pixmaps/stop.xpm"       /* Button box pixmaps*/
#include "pixmaps/about.xpm"
#include "pixmaps/change.xpm"
#include "pixmaps/calculator.xpm"
#include "pixmaps/graph.xpm"
#include "pixmaps/portfolio.xpm"
#include "pixmaps/edit.xpm"

#include "pixmaps/save.xpm"       /* file manager buttons */
#include "pixmaps/new.xpm"
#include "pixmaps/open.xpm"
#include "pixmaps/prev.xpm"
#include "pixmaps/next.xpm"

#include "pixmaps/DtCMday.s.pm"   /* total return report buttons */
#include "pixmaps/DtCMwk.s.pm"
#include "pixmaps/DtCMmth.s.pm"
#include "pixmaps/quarter.xpm"
#include "pixmaps/DtCMyr.s.pm"
#include "pixmaps/ytd.xpm"
#include "pixmaps/first.xpm"
#include "pixmaps/pick.xpm"
#include "pixmaps/folder.xpm"

#include "pixmaps/check.xpm"      /* portfolio buttons */
#include "pixmaps/x.xpm"

#include "pixmaps/acct.xpm"       /* graph mode buttons */
#include "pixmaps/accts.xpm"

#include "pixmaps/bargraph.xpm"   /* graph buttons */
#include "pixmaps/linegraph.xpm"

#include "pixmaps/last.xpm"       /* calculator buttons */
#include "pixmaps/nexttrans.xpm"
#include "pixmaps/prevtrans.xpm"

#include "pixmaps/info.xpm"       /* The "about" screen */
#include "pixmaps/tile.xpm"       /* Drawing stipple pattern */

#include "pixmaps/fv.xpm"         /* Function calculator equation pixmaps */
#include "pixmaps/sfv.xpm"
#include "pixmaps/pp.xpm"
#include "pixmaps/pv.xpm"
#include "pixmaps/rate.xpm"

#include "pixmaps/Xinvest.l.pm"   /* The icon */

/* This must be initialized in the same order as Pixmaps in pixname.h */
static char **pixname[] = {
             stop_xpm, about_xpm, change_xpm, calculator_xpm,
             graph_xpm, port_xpm, edit_xpm,
             save_xpm, new_xpm, open_xpm, prev_xpm, next_xpm,
             DtCMday_s_pm, DtCMwk_s_pm, DtCMmth_s_pm, quarter_xpm, 
             DtCMyr_s_pm, ytd_xpm, first_xpm, pick_xpm,
             check_xpm, x_xpm, 
             acct_xpm, accts_xpm, folder_xpm,
             bargraph_xpm, linegraph_xpm,
             last_xpm, nexttrans_xpm, prevtrans_xpm,
             info_xpm, tile_xpm,
             fv_xpm, sfv_xpm, pp_xpm, pv_xpm, rate_xpm,
             Xinvest_l_pm
            };
#endif
