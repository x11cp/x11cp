/*
  Xinvest is Copyright 1995-97 Mark Buser,
  All Rights Reserved.

  Permission is hereby granted to copy
  and freely distribute copies of this
  program for non-commercial purposes
  without fee, provided that this notice
  appears in all copies.
  
  All redistributions must be in their
  entirety as originally distributed.
  Feel free to modify Xinvest, but
  modified versions may not be distributed
  without prior consent of the author.
  
  This software is provided 'as-is'
  without any express or implied warranty.
  In no event will the author be held
  liable for any damages resulting from
  the use of this software.

  $Revision: 2.15 $ $Date: 1997/08/16 20:53:31 $
*/

#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#ifdef STRFMON
#include <monetary.h>
#endif

#ifndef NeXT
#include <values.h>
#endif

#include <Xm/List.h>
#include <Xm/Text.h>
#include <Xm/RowColumn.h>

#include "account.h"
#include "color.h"
#include "drawing.h"
#include "portfolio.h"
#include "nav.h"
#include "rate.h"
#include "status.h"
#include "util.h"
#include "xutil.h"

static NODE root  = { "Root", -1, False, NULL, NULL };
static NODE funds = { "Accounts", -1, False, NULL, NULL };
static NODE all   = { "Flatten", -1, False, NULL, NULL };
static double total_value = 0.0;

/* forward declarations */
static double what_value ( NODE * );
static double extract_percent ( char * );
static char  *mknodes ( NODE *, char *, float, int);
static char  *delnodes ( NODE *, char *, float, int );
static int   exists ( NODE *, char *);
static NODE  *create ( NODE *, char *);
static void  delete ( NODE *, NODE *);

/* Set portfolio buttons, option menu to sensitive or not */
void portSense()
{
  extern Widget Portbut;
  extern Widget Portpbut;
  extern Widget Portmbut;
  extern Widget Portabut;
  extern Widget Portcbut;
  extern Widget Portoptmenu;
  Widget w;

  int    account = activeAccount();
  int    numTrans = 0;
  int    enable;
  int    accountIn, accountsIn;

  if (account)
    getAccount( account, ACCOUNT_NUM_TRANS, &numTrans );  

  accountIn = accountStatus (account, ACCOUNT_IN_PORTFOLIO);
  accountsIn = accountsStatus (ACCOUNT_IN_PORTFOLIO);

  /* ACCOUNT ADD */
  /* If there are trans and not already in portfolio, enable add */
  enable = (numTrans > 0) && (accountIn == False);
  XtSetSensitive ( Portpbut, (enable)?True:False );

  /* ACCOUNT MINUS */
  /* If already in portfolio, enable minus */
  enable = accountIn;
  XtSetSensitive ( Portmbut, (enable)?True:False );

  /* ACCOUNT CLEAR */
  /* If something in portfolio, enable */
  enable = accountsIn;
  XtSetSensitive ( Portcbut, (enable)?True:False );

  /* ACCOUNT ADD_ALL */
  /* If everyone is in, no need to enable */
  enable = (accountsIn != numAccounts());
  XtSetSensitive ( Portabut, (enable)?True:False );

  /* PORTFOLIO BUTTON AND MENU */
  /* If there are transactions or there are nodes, enable the main button */
  enable = (numTrans > 0) || (accountsIn == True);
  XtSetSensitive ( Portbut, (enable)?True:False );
  XtVaGetValues ( Portbut, XmNuserData, &w, NULL);
  XtSetSensitive( w, (enable)?True:False );

  /* OPTION MENU */
  /* If we have stuff in portfolio, enable focus change */
  enable = accountsIn;
  XtSetSensitive ( Portoptmenu, (enable)?True:False );
}

/************************************************************************/
/* Given a node name, make it the current root and display its children */
/************************************************************************/
void portTraverse ( Widget w, XtPointer client_data, XtPointer call_data)
{
  extern Widget Portpercentlist;

  NODE   *node;
  LINK   *current;
  char   *next;   
  char   name[40];
  int    item = (int) client_data;

  /* Get new parent node name */

  XmListCallbackStruct *cbl = (XmListCallbackStruct *) call_data;
  if ( XmIsList ( w ) ) {
    XmStringGetLtoR ( cbl->item, XmFONTLIST_DEFAULT_TAG, &next );

    /* Get name, remove value, percentage */
    sscanf ( next, "%s %*s %*s", name );
    XtFree ( next );

    /* Find the node from the name */
    XtVaGetValues ( Portpercentlist, XmNuserData, &node, NULL );

    if ( strcmp ( "..", name) == 0) {
      /* Up to parent node */
      current = node->parent;

      /* New current parent */
      if ( current && current->child->children )
        XtVaSetValues ( Portpercentlist, XmNuserData, current->child, NULL );
      else
        write_status ("Reached top of hierarchy.", INFO);     

    } else {

      /* Find the node from the name */
      XtVaGetValues ( Portpercentlist, XmNuserData, &node, NULL );
      current = node->children;

      while ( current ) {
        if ( strcmp ( current->child->name, name ) == 0 )
          break;
	current = current->next;
      }

      /* New current parent */
      if ( current && current->child->children )
        XtVaSetValues ( Portpercentlist, XmNuserData, current->child, NULL );
      else
        write_status ("Reached bottom of hierarchy.", INFO);     
    }

  }

  else {
    if ( item == 0 ) { /* root */ 
       XtVaSetValues ( Portpercentlist, XmNuserData, &root, NULL );
    }
    else if ( item == 1 ) { /* funds */
       XtVaSetValues ( Portpercentlist, XmNuserData, &funds, NULL );
    }
    else if ( item == 2 ) { /* all */
       XtVaSetValues ( Portpercentlist, XmNuserData, &all, NULL );
    }
  }

  portUpdateDisplay();

}

/* Given an account, add/sub it to/from the tree, increment/decrement the *
** total value.                                                           */
void portAccount( int account, int what )
{
  char   *title;        /* The user supplied title for an account */
  char   *filename;
  char   *text, *end;
  char   *front, *rear;
  char   dummy_str[80];

  float    value;

  double   all_percents = 0.0;

  char     errmsg[512];
  char     *node_error;

  getAccount ( account, ACCOUNT_FILENAME, &filename);

  if ( filename == NULL ) {
    write_status ("Account must have a filename\n"
                  "before modifying portfolio.", 
                  ERR );
    return;
  }

  if (what == PORT_ADD) {
    if ( accountStatus ( account, ACCOUNT_IN_PORTFOLIO ) ) {
      sprintf( errmsg, "File %s\nis already in the portfolio.", filename );
      write_status ( errmsg, WARN );
      return;
    } 
    accountAddPortfolio ( account );
  } else {
    if ( accountStatus ( account, ACCOUNT_IN_PORTFOLIO ) == False) {
      sprintf( errmsg, "File %s\nis not in the portfolio.", filename );
      write_status ( errmsg, ERR );
      return;
    }
    accountDelPortfolio ( account );
  }


  /* Look for percents to feed to mknodes */
  title = createAccountName (account);
  getAccount ( account, ACCOUNT_TRANSTEXT, &text);
  if ( text ) {
    /* This is destructive to text, so copy it */
    text = strdup (text);
 
    value = account_value ( account, NULL );

    front = text;
    end = text + strlen (text);

    while ( front >= text && front < end) {
      /* This places \0 at end of each line */
      rear = strchr ( front, '\n' );
      if (rear)
        *rear = '\0';

      /* 
      ** Update root node
      */
      if ( strncasecmp ( "percent", front, 7 ) == 0 ) {

        all_percents += extract_percent ( front );

        /* make dummy percents line */
        strcpy ( dummy_str, front );

        /* true, report errors */
        if (what == PORT_ADD) 
          node_error = mknodes ( &root, front, value, True );
        else 
          node_error = delnodes ( &root, front, value, True );
       
        if (node_error) {
          sprintf ( errmsg, "%s:\n%s", title, node_error);
          write_status (errmsg, ERR);
        }

        /*
        ** Update all node 
        */
        front = dummy_str;

        /* skip to percent digits */
        while ( !isdigit (*front) )  
          front++;
        /* skip over percent digits */
        while ( isdigit (*front) || ispunct(*front) )  
          front++;

        /* remove '.' from elements only */
        while ( *front != '\0' ) { 
          if ( *front == '.' )
            *front = '_';
          front++;
        }

        /* False, skip errors caught above */
        if (what == PORT_ADD)
          mknodes ( &all, dummy_str, value, False );
        else
          delnodes ( &all, dummy_str, value, False ); 
      }
      front = rear +1;
    }

    if ( what == PORT_ADD) {
      if ( all_percents < (double)  99.99999 || 
           all_percents > (double) 100.00000 ) {
        sprintf( errmsg, "%s:\nTotal percents (%f) < 100%%.", 
                 title, all_percents );
        write_status ( errmsg, INFO );
      }
    }

    /* 
    ** Update funds node 
    */

    /* Create dummy percents line */
    if (title)      /* Use user title if there is one */
      rear = title;
    else {
      rear = strrchr ( filename, '/' );
      if ( rear == NULL )
        rear = filename;
      else
        rear++;     /* skip the / */
    }
    sprintf ( dummy_str, "Percent 100 %s", rear );

    /* 
    ** Fix title so it doesn't have any . (heirarchy delimiter) or spaces
    ** (param delimiter).
    */
    front = strstr ( dummy_str, rear);
    while ( *front != '\0' ) { 
      if ( *front == '.' || *front == ' ' )
        *front = '_';
      front++;
    }

    if ( what == PORT_ADD )
      mknodes ( &funds, dummy_str, value, False );
    else
      delnodes ( &funds, dummy_str, value, False );

    XtFree (text);
    XtFree (title);

    /* Update total value label */
    if ( what == PORT_ADD )
      total_value += value;
    else
      total_value -= value;

  } /* if text */

}


/* Callback for all portfolio buttons */
/* ARGSUSED */
void portAddSubList ( Widget w, XtPointer client_data, XtPointer call_data)
{

  extern Widget Portpbut;
  extern Widget Portmbut;
  extern Widget Portcbut;
  extern Widget Portabut;
  extern Widget Portoptmenu;
  extern Widget Portpercentlist;

  int    account, i;
  int    function = (int) client_data;

  /* Disable other plus or minus till done */
  XtSetSensitive ( Portpbut, False );
  XtSetSensitive ( Portmbut, False );
  XtSetSensitive ( Portcbut, False );
  XtSetSensitive ( Portabut, False );

  switch (function) {

    case 0:
           /* Only add active account */
           account = activeAccount();
           portAccount (account, PORT_ADD);
           break;

    case 1:
           /* Only remove active account */
           account = activeAccount();
           portAccount (account, PORT_DELETE);
           break;

    case 2:
           /* Add all accounts */
           for (i=1; i <= numAccounts(); i++) 
             portAccount (i, PORT_ADD);
           break;

    case 3: 
           {  /* Remove all, the faster way */
           NODE *parent;
           NODE *child;
           NODE *node;
           static NODE *parents[] = { &root, &funds, &all };

           for (i=0; i < XtNumber(parents); i++ )  {

             node = parents[i];
             while ( node->children ) {
               parent = node;
               child = node->children->child;
               while ( child->children ) {
                 parent = child;
                 child = child->children->child; 
               }
               delete ( parent, child );
             }

           }

           /* Clear file names in portfolio */
           accountDelPortfolio ( ALL_ACCOUNTS );

           /* Update total value */
           total_value = 0.0;
           }
           break;
  }

  /* Set option menu back to root */
  {
    Widget menu;
    WidgetList buttons;
    Widget cascade;
    XmString x_root;

    XtVaGetValues ( Portoptmenu, XmNsubMenuId, &menu, NULL );
    XtVaGetValues ( menu, XmNchildren, &buttons, NULL );
    XtVaSetValues ( Portoptmenu,
                    XmNmenuHistory, buttons[0],
                    NULL);

    x_root = XmStringCreateLocalized ( "Root" );
    cascade = XmOptionButtonGadget ( Portoptmenu );
    XtVaSetValues ( cascade,
                    XmNlabelString, x_root,
                    NULL );
    XmStringFree ( x_root );
  }

  /* Update percentage tree and display top level to list */
  XtVaSetValues ( Portpercentlist, XmNuserData, &root, NULL );
  portUpdateDisplay();
  portSense();
}


/* Get current level node, display its children and percentages to list window
** and graph.  Update total value display.
*/
void portUpdateDisplay ( )
{
  extern Widget Portpercentlist;
  extern Widget Portvaluelabel;

  NODE   *node;
  LINK   *current;
  double value;
  double local_total;
  char   percent[80];

  char   total_str[40];
  XmString value_str;
  XmString x_percent;

  /* Update percentage tree and display top level to list */
  XtVaGetValues ( Portpercentlist, XmNuserData, &node, NULL );
  if (node == NULL) {
    XtVaSetValues ( Portpercentlist, XmNuserData, &root, NULL );
    node = &root;
  }
 
  current = node->children;

  XmListDeleteAllItems( Portpercentlist );

  if ( node->parent ) {
    /* The ".." node */
    x_percent = XmStringCreateLocalized("..");
    XmListAddItemUnselected( Portpercentlist, x_percent, 0 );
    XmStringFree (x_percent);
  }
 
  local_total = what_value ( node );

  while ( current) {

    value = what_value ( current->child );
    sprintf ( percent, "%-15.15s %6.2f%% %9.2f", 
              current->child->name, 100.0*value/local_total, value );

    x_percent = XmStringCreateLocalized(percent);
    XmListAddItemUnselected( Portpercentlist, x_percent, 0 );
    XmStringFree (x_percent);

    current = current->next;
  }

#ifdef STRFMON
  strfmon ( total_str, 80, "Total Portfolio Value: %n", total_value );
#else
  sprintf( total_str, "Total Portfolio Value: $%2.2f", total_value );
#endif
  value_str = XmStringCreateLocalized( total_str ); 
  XtVaSetValues ( Portvaluelabel, XmNlabelString, value_str, NULL );
  XmStringFree ( value_str );

  drawDrawingArea ( );
}


static int fill ( int slots, int tofill, int lowpos, int highpos, int *cellLoc)
{
  int filling = tofill;

  do {
    if ( (slots-2) > 0 ) {
       if (filling > 2 || (filling == slots-1) )
         filling -= fill ( slots-2, filling-2, lowpos+1, highpos-1, cellLoc); 
       else
         filling -= fill ( slots-2, filling, lowpos+1, highpos-1, cellLoc); 
       slots -= (slots-2);
    } else {

      switch (slots) {
      
       case 1:  cellLoc[lowpos] = (filling)?1:0; 
                break;
       case 2:  cellLoc[lowpos] = (filling)?1:0;
                cellLoc[highpos] = (filling == 2)?1:0;
                break;
      }
      filling -= filling;
      slots -= slots;
    }
  } while (filling > 0 && slots > 0);

  return (tofill);

}

/*
** Draw a pie chart of percentages, called from redrawDrawing (Drawing
** area widget callback).
*/
int pie ( Widget Drawing, Pixmap Drawpixmap, GC gc )  
{
    extern Widget Portpercentlist;
    /* To get some colors allocated from resource file */

    int count, i;

    XmStringTable list;
    char *item;

    float value, total = 0.0;
    float percent;
    char  wedge[40];
    char  blurb[40];

    int errors = 0;

    Dimension height, width;
    Pixel foreground, background;
    Pixel color;
    Pixel bot_shadow;

    int *angle1, *angle2;
    int x, y;
    int major_radius;
    int minor_radius;

    int linex, liney;
 
    extern XFontStruct *small, *large;
    char title[] = "Asset Allocation";
    int textWidth;
    int textHeight;


    /* Draw a pie chart for each item in the percent list window */
    /* Draw in name, percent, and value in the wedge */


    XtVaSetValues ( Drawing, XmNunitType, XmPIXELS, NULL);
    XtVaGetValues ( Drawing, XmNheight, &height,
                             XmNwidth,  &width,
                             XmNforeground, &foreground,
                             XmNbackground, &background,
                    NULL);

   if ( isColorVisual ( XtDisplay(Drawing) ) )
     XmGetColors ( XtScreen (Drawing), GetColormap(), background,
                   NULL, NULL, &bot_shadow, NULL );
   else
     bot_shadow = background;

    XSetForeground ( XtDisplay(Drawing), gc, foreground );
    XSetArcMode ( XtDisplay(Drawing), gc, ArcPieSlice);
    XSetLineAttributes( XtDisplay(Drawing), gc,
                        0, LineSolid, CapNotLast, JoinBevel );

    /* How many items to put on chart */
    XtVaGetValues ( Portpercentlist, XmNitemCount, &count, 
                                     XmNitems,     &list,
                    NULL );

    if (count == 0)
       return(count);

    XmStringGetLtoR ( list[0], XmFONTLIST_DEFAULT_TAG, &item ); 
    if ( strcmp ( item, "..") == 0 ) {
      /* Skip ".." element */
      count--;
      list = &(list[1]);
    }
    XtFree ( item );

    /* Generate all the angles in one fell swoop */

    angle1 = (int *)XtCalloc ( count, sizeof(int) );
    angle2 = (int *)XtCalloc ( count, sizeof(int) );
    if ( angle1 == NULL || angle2 == NULL) {
      write_status( "Couldn't allocate memory for pie chart angles.", ERR );
      return(0);
    }
 
    for ( i=0; i < count; i++ ) {
      XmStringGetLtoR ( list[i], XmFONTLIST_DEFAULT_TAG, &item ); 
      sscanf ( item, "%s %f%% %*f", &wedge[0], &percent );
      XtFree ( item );

      /* Move starting angle to 12 o'clock (+90).  This makes picking which
      ** cell to print in _much_ more tractable.
      */
      if ( i == 0 )
        angle1[i] = 90;                         /* This is +90 */
      else
        angle1[i] = angle1[i-1] + angle2[i-1];

      /* Insure no round errors on last wedge */
      if ( i == count-1 )
        angle2[i] = (360+90) - angle1[i];       /* This is +90 */
      else
        angle2[i] =  360 * percent / 100.0;

    }


    /* Draw Title */
    XSetFont ( XtDisplay (Drawing), gc, large->fid );
    textWidth = XTextWidth ( large, title, strlen ( title ) );
    textHeight = large->ascent + large->descent;

    x = (int)(width - textWidth)/2;
    if (bound_ok ( x+textWidth, width, &errors, count ) ) {
      XSetForeground ( XtDisplay(Drawing), gc, bot_shadow );
      XDrawString ( XtDisplay (Drawing), Drawpixmap, gc,
                    x +1, textHeight +1, title, strlen ( title ) );
      XSetForeground ( XtDisplay(Drawing), gc, foreground );
      XDrawString ( XtDisplay (Drawing), Drawpixmap, gc,
                    x, textHeight, title, strlen ( title ) );
    }

    /* Calculate size and position of ellipses */
    major_radius = (int)(0.6 * (int)width /2);
    minor_radius = (int)(0.7 * (int)(height-textHeight) /2);

    x = (int)(width - 2* major_radius) / 2;
    y = (int)(height - 2* minor_radius) / 2;


    /* Draw shadow ellipse and extenders to "real" ellipse */
    XSetForeground ( XtDisplay(Drawing), gc, foreground );
    XDrawArc ( XtDisplay (Drawing), Drawpixmap, gc, 
               x, y + (int)(0.2*minor_radius), 
               2*major_radius, 2*minor_radius, 0*64, 360*64 );

    /* Draw vertical at each side */

    /* Left */
    XDrawLine ( XtDisplay (Drawing), Drawpixmap, gc, 
                x, y + minor_radius, 
                x, y + minor_radius + (int)(0.2*minor_radius) );

    /* Right */
    XSetForeground ( XtDisplay(Drawing), gc, foreground );
    XDrawLine ( XtDisplay (Drawing), Drawpixmap, gc, 
                x + 2*major_radius, y + minor_radius, 
                x + 2*major_radius, y + minor_radius + (int)(0.2*minor_radius));


    /* Draw "projected" pie a slice at a time */
    for ( i = 0; i < count; i++ ) {

      Pixel first;

      color = GetColor (i % COLOR_NUM);
      if (i == 0)
        first = color;

      /* If we end up with the same color last as first, do a switch to
      ** prevent it */
      if ( count > 1 && i == count-1 && first == color)
        color = GetColor ( (i+1) % COLOR_NUM);

      XSetForeground ( XtDisplay(Drawing), gc, color );

      XFillArc ( XtDisplay (Drawing), Drawpixmap, gc, 
                 x, y + (int)(0.2*minor_radius), 
                 2*major_radius, 2*minor_radius, angle1[i]*64, angle2[i]*64 );
    }

    /* Fill possible glitch in projected slices. 
    ** This draws a box bounded by top/bottom wedges with sides located at the
    ** circumference and center.  This covers any open areas or miscolored
    ** areas from previous draws.
    */
    if ( count > 1 ) {  /* no glitches if one slice */

      for ( i = 0; i < count; i++ ) {
        XPoint point[4];
        Pixel first;

        color = GetColor (i % COLOR_NUM);
        if (i == 0)
          first = color;

        /* If we end up with the same color last as first, do a switch to
        ** prevent it */
        if ( count > 1 && i == count-1 && first == color)
          color = GetColor ( (i+1) % COLOR_NUM);

        XSetForeground ( XtDisplay(Drawing), gc, color );

        /* Common computes */

        /* upper center */
        point[0].x = x + major_radius;
        point[0].y = y + minor_radius;

        /* lower center */
        point[1].x = point[0].x;
        point[1].y = point[0].y + (int)(0.2 * minor_radius) +5;


        /* Left side of wedge */
        if ( angle1[i] < 270 ) {

          /* lower circumference */ 
          point[2].x = major_radius * 
                       cos( (double) (2*M_PI * angle1[i]/360.0) );
          point[2].x += x + major_radius;

          point[2].y = minor_radius * 
                       sin( (double) (2*M_PI * angle1[i]/360.0) );
          point[2].y = y + minor_radius - point[2].y +2;

          /* upper circumference */
          point[3].x = point[2].x; 
          point[3].y = point[2].y + (int)(0.2 * minor_radius);

          XFillPolygon ( XtDisplay(Drawing), Drawpixmap, gc, 
                         point, 4, Convex, CoordModeOrigin );
        }
        /* Right side of wedge */
        if ( (angle1[i] + angle2[i]) > 270 ) {

          /* lower circumference */ 
          point[2].x = major_radius * 
                       cos( (double) (2*M_PI * (angle1[i]+angle2[i])/360.0) );
          point[2].x += x + major_radius;

          point[2].y = minor_radius * 
                       sin( (double) (2*M_PI * (angle1[i]+angle2[i])/360.0) );
          point[2].y = y + minor_radius - point[2].y +2;

          /* upper circumference */
          point[3].x = point[2].x; 
          point[3].y = point[2].y + (int)(0.2 * minor_radius);

          XFillPolygon ( XtDisplay(Drawing), Drawpixmap, gc, 
                         point, 4, Convex, CoordModeOrigin );
        }
      }

    }

    /* Draw "real" pie chart a slice at a time */
    for ( i=0; i < count; i++ ) {

      Pixel first;

      color = GetColor (i % COLOR_NUM);
      if (i == 0)
        first = color;

      /* If we end up with the same color last as first, do a switch to
      ** prevent it */
      if ( count > 1 && i == count-1 && first == color)
        color = GetColor ( (i+1) % COLOR_NUM);

      XSetForeground ( XtDisplay(Drawing), gc, color );

      /* Draw pie slice */
      XFillArc ( XtDisplay (Drawing), Drawpixmap, gc, 
                 x, y, 2*major_radius, 2*minor_radius, 
                 angle1[i]*64, angle2[i]*64 );

      /* Outline circumference in background */
      XSetForeground ( XtDisplay(Drawing), gc, foreground );
      XDrawArc ( XtDisplay (Drawing), Drawpixmap, gc, 
                 x, y, 2*major_radius, 2*minor_radius, 
                 angle1[i]*64, angle2[i]*64 );

      if ( count > 1 ) {

        /* Draw pie wedge lines */

        linex = major_radius * cos( (double) (2*M_PI * angle1[i]/360.0) );
        /* skew coord back to X */
        linex += x + major_radius;

        liney = minor_radius * sin( (double) (2*M_PI * angle1[i]/360.0) );
        /* skew coord back to X */
        liney = y + minor_radius - liney;

        /* Draw pie line from center to circumference */
        XSetForeground ( XtDisplay(Drawing), gc, foreground );
        XDrawLine ( XtDisplay (Drawing), Drawpixmap, gc,
                    linex, liney,
                    x + major_radius, y + minor_radius
                  );
        /* Drop projection to shadow if not hidden. */
        if ( angle1[i] > 180 && angle1[i] < 360) {
          XSetForeground ( XtDisplay(Drawing), gc, foreground );
          XDrawLine ( XtDisplay (Drawing), Drawpixmap, gc,
                      linex, liney,
                      linex, liney + (int)(0.2*minor_radius)
                    );
        }
      }
    }

    /* Draw in some text */

    /* See how many text fields we can fit.  Any labels not fitting are
    ** just ommitted.  Draw to side of circle, advancing up one side and 
    ** down the other. */

    {
      int cells;
      int cellheight;
      int onleft = 0;

      float radius = 0.75;

      struct textTable {
                         unsigned left_x;
                         unsigned right_x;
                         unsigned y;
                       } *textLoc;

      struct pieTable {  unsigned onleft;
                         unsigned x;
                         unsigned y;
                         int slot;
                      } *sliceLoc;

      /* Where does the text go? Calculate positions of maximum number of
      ** cells of text that can fit.
      */
      cellheight = small->ascent+small->descent + large->ascent+large->descent;
      cells = (height - (large->ascent + large->descent)) / cellheight;

      textLoc = (struct textTable *) XtCalloc (cells, sizeof(struct textTable));
      for ( i=0; i < cells; i++ ) {
        textLoc[i].y = (i+1) * cellheight;
        textLoc[i].left_x  = 5;
        textLoc[i].right_x = x + 2*major_radius +15;
      }

      /* Where does each line end */
      sliceLoc = (struct pieTable *) XtCalloc (count, sizeof(struct pieTable));
      for ( i=0; i < count; i++ ) {

        /* Draw to angle that bisects each wedge at .75 radius.
        ** Use polar to cartesian formula x = r * cos(@), y = r * sin(@)
        */
        if ( count > 1 ) {
          sliceLoc[i].x = radius * major_radius *
                  cos( (double) (2*M_PI * (angle1[i]+angle2[i]/2)/360.0) );
          /* skew coord back to X */
          sliceLoc[i].x += x + major_radius;

          sliceLoc[i].y = radius * minor_radius *
                  sin( (double) (2*M_PI * (angle1[i]+angle2[i]/2)/360.0) );
          /* skew coord back to X */
          sliceLoc[i].y = y + minor_radius - sliceLoc[i].y;

        } else {
          sliceLoc[i].x = x + major_radius;
          sliceLoc[i].y = y + minor_radius;
        }

        if ( (angle1[i] + angle2[i]/2) >= 90 &&
             (angle1[i] + angle2[i]/2) < 270 ) {
          sliceLoc[i].onleft = True;
          onleft++;
        } else
          sliceLoc[i].onleft = False;

        sliceLoc[i].slot = -1;
      }

      /* Now the tricky part, which text gets which cell */
      /* This is not optimal, just spaces them equidistant. Does not
      ** move from one side to the other if there is an imbalance.
      */
      {
        int left, right;
        int *leftSlots = (int *) XtCalloc ( cells, sizeof(int) );
        int *rightSlots = (int *) XtCalloc ( cells, sizeof(int) );

        /* Fill sets the vector passed with ones in the positions
        ** where there should be text displayed.
        */
        if (onleft)
          fill ( cells, onleft, 0, cells-1, leftSlots );
        if (count-onleft)
          fill ( cells, count - onleft, 0, cells-1, rightSlots );

        /* Ok lay'em out */
        left = 0; right = cells -1;
        for (i = 0; i < count; i++) {
          if (sliceLoc[i].onleft) 
            /* skip any 0's in the vector, no text there */
            do {
              if (leftSlots[left] == 1) {
                sliceLoc[i].slot = left++;
                break;
              }
            } while (++left < cells);
          else
            do {
              if (rightSlots[right] == 1) {
                sliceLoc[i].slot = right--;
                break;
              }
            } while (--right >= 0);
        }

        XtFree ((char *)leftSlots);
        XtFree ((char *)rightSlots);
      }


      for ( i=0; i < count; i++) {
        /* Draw lines from text to end of line in two segments.  First is a
        ** short horizontal line from text, the second dives to the end point */

        XPoint point[3];   /* three points for two segments */

        if (sliceLoc[i].slot != -1) {

          /* A short horizontal line */
          if (sliceLoc[i].onleft) {
            point[0].x = x -10 +1;
            point[1].x = point[0].x +5;
          } else {
            point[0].x = x + 2*major_radius +10 +1;
            point[1].x = point[0].x -5;
          }

          point[0].y = textLoc[sliceLoc[i].slot].y +1;
          point[1].y = point[0].y;
 
          /* then a dip to the pie */
          point[2].x = sliceLoc[i].x +1;
          point[2].y = sliceLoc[i].y +1;
 
          XSetForeground ( XtDisplay (Drawing), gc, bot_shadow);
          XDrawLines ( XtDisplay (Drawing), Drawpixmap, gc,
                       point, XtNumber(point), CoordModeOrigin);

          point[0].x--; point[0].y--;
          point[1].x--; point[1].y--;
          point[2].x--; point[2].y--;
          XSetForeground ( XtDisplay (Drawing), gc, foreground);
          XDrawLines ( XtDisplay (Drawing), Drawpixmap, gc,
                       point, XtNumber(point), CoordModeOrigin);
       }

      }

      for (i=0; i < count; i++) {

        int numchars;
        int textx;        /* just a convenience to figure left or right x */
        int texty;

        /*
        ** Get the text to display
        */
        XmStringGetLtoR ( list[i], XmFONTLIST_DEFAULT_TAG, &item );
        /* big assumption, scanf will always find a '%' */
        sscanf ( item, "%s %f%% %f", &wedge[0], &percent, &value );
        sprintf ( blurb, "%2.2f %%", percent );

        /* Increment value at this level */
        total += value;
        XtFree ( item );

        if (sliceLoc[i].slot != -1) {
          /* Draw the user supplied name */
          XSetFont ( XtDisplay (Drawing), gc, large->fid );
          numchars = strlen ( wedge );
          textWidth = XTextWidth ( large, wedge, numchars );
          while ( textWidth > (x - 20) )
            textWidth = XTextWidth ( large, wedge, --numchars);

          if (sliceLoc[i].onleft)
            textx = textLoc[sliceLoc[i].slot].left_x;
          else
            textx = textLoc[sliceLoc[i].slot].right_x;
#if 0
          /* Center text in space from edge to pointer */
          textx += (x - 5 - textWidth)/2;
#endif
          texty = textLoc[sliceLoc[i].slot].y;

          XSetForeground ( XtDisplay(Drawing), gc, bot_shadow );
          XDrawString ( XtDisplay (Drawing), Drawpixmap, gc,
                        textx +1, texty +1, wedge, numchars );
          XSetForeground ( XtDisplay(Drawing), gc, foreground );
          XDrawString ( XtDisplay (Drawing), Drawpixmap, gc,
                        textx, texty, wedge, numchars );

          /* Draw the user supplied percentage */
          XSetFont ( XtDisplay (Drawing), gc, small->fid );
          numchars = strlen ( blurb );
          textWidth = XTextWidth ( small, blurb, numchars );
          while ( textWidth > (x - 20) )
            textWidth = XTextWidth ( small, blurb, --numchars);

          XSetForeground ( XtDisplay(Drawing), gc, bot_shadow );
          XDrawString ( XtDisplay (Drawing), Drawpixmap, gc,
                        textx +1, texty + large->descent + small->ascent +1,
                        blurb, numchars );
          XSetForeground ( XtDisplay(Drawing), gc, foreground );
          XDrawString ( XtDisplay (Drawing), Drawpixmap, gc,
                        textx, texty + large->descent + small->ascent,
                        blurb, numchars );
        }
      }

      /* Tell user if everything didn't fit */
      for (i=0; i < count; i++) 
        if (sliceLoc[i].slot == -1) 
          errors |= (1 << i);

      XtFree ((char *)sliceLoc);
      XtFree ((char *)textLoc);
    }

    /* Draw in some info on the total value */
    XSetFont ( XtDisplay (Drawing), gc, small->fid );
    {
      char intro[] = "Value at '";
      char *line;
      NODE *node;
      char **levels = NULL;
      int numlevels = 0;
      int numbytes = 0;  /* How many chars in the assembled string */

      /* Assemble the line.  As we're traveling up, the names are processed
      ** backwards, so the first name printed is the last name found.  */

#ifdef STRFMON  /* Level value */
      strfmon ( blurb, 20, "' - %.2n", total );
#else
      sprintf ( blurb, "' - $%.2f", total );
#endif
      numbytes += strlen (blurb);

      /* Level names */
      XtVaGetValues ( Portpercentlist, XmNuserData, &node, NULL );
      while (node) {
	levels = (char **) XtRealloc ( (char *)levels, 
                                       (numlevels+1) * sizeof(char **) );
	levels[numlevels++] = node->name;
	numbytes += strlen(node->name) +1;
	if (node->parent)
	  node = node->parent->child;
	else
          break;
      }
      numbytes += strlen (intro) +1;

      /* Create single string and copy in all of the fields in forward order */
      line = XtMalloc ( numbytes * sizeof (char) );
      strcpy (line, intro);
      while (numlevels--) {
	strcat (line, levels[numlevels]);
	if (numlevels)
  	  strcat (line, ".");
      }
      strcat (line, blurb);

      textWidth = XTextWidth ( small, line, strlen ( line ) );
      x = (width - textWidth) / 2;
      if ( bound_ok ( x + textWidth, width, &errors, count) ||
	   bound_ok ( 0, x, &errors, count) ) {
        XSetForeground ( XtDisplay(Drawing), gc, bot_shadow );
        XDrawString ( XtDisplay (Drawing), Drawpixmap, gc,
                      x +1, height - small->descent +1, line, strlen(line) );
        XSetForeground ( XtDisplay(Drawing), gc, foreground );
        XDrawString ( XtDisplay (Drawing), Drawpixmap, gc,
                      x, height - small->descent, line, strlen(line) );
      }
      XtFree (line);
    }

    XtFree( (char *)angle1 );
    XtFree( (char *)angle2 );
    XSetForeground( XtDisplay(Drawing), gc, foreground );

    /* If anything went awry, pop-up a message */
    /* We could get very specific, since we know where it went wrong */
    /* but don't right now. */
    if ( errors && !suppressed() ) {
      write_status ("Can't display all information, increase graph size\n"
                    "or reduce font sizes. See \"help Fonts\".", WARN ); 
    }

    return (count);
}


/* Given a token string, place it in the tree */
static char *mknodes ( NODE *parent, char *percents, float tot_value, 
                     int errorchk )
{
   char *copy;
   char *element;
   char *value;

   NODE *node;

   static char errmsg[512];

   copy = strdup ( percents );

   /* do it the old fashioned way because strtok can not use more than
   ** one char as a delimter, such as "white space"
   */

   /* skip percent directive and white space */
   value = copy;
   while ( isalpha(*value) || isspace(*value) )
     value++;

   /* look for end of value, skip digits, and '.' or ',' */
   element =  value;
   while ( !isspace(*element) )
     element++;
   *element++ = '\0';

   /* look for first element */
   while ( !isalnum(*element) )
     element++;

   /* Look for first element with known delimiter */
   strtok (element, ".");

   while (element) {

     /* Check if exists */
     node = (NODE *) exists ( parent, element );
     if (!node) {
       if ( errorchk && parent->isleaf == True ) {
         /* What we have here is a user error.  This is a leaf in another
         ** file, but it isn't here.  This screws up the accumulation in this
         ** node, better tell the user.
         */
         sprintf ( errmsg, "Node %s is specified as a branch in this file\n"
                           "and a leaf node in this or a previous file.\n"
                           "Node not added.",
                   parent->name );
         return (errmsg);
       }
       node = create ( parent, element );
       if (!node) {
        sprintf ( errmsg, "Could not create new node \"%s\".", element );
        return (errmsg);
       }
     }
     parent = node;  

     element = strtok ( NULL, "." );
   }

   XtFree ( copy );

   /* at the bottom */

   if ( errorchk && node->children ) {
     /* what we have here is a user error.  This is not a leaf in another
     ** file, but it is here.  this screws up the accumulation in this node,
     ** better tell the user.  
     */
     sprintf ( errmsg, "Node %s is specified as a leaf in this file\n"
                       "and a non-leaf node in this or a previous file.\n"
                       "Node not added.",
               node->name );
     return (errmsg);
   }

   if ( !isdigit(value[0]) && *value != '.' && *value != ',' && errorchk) {
     sprintf ( errmsg, "%s\n^ expected digit or [,.], received '%c'. Total\n"
                       "value is probably wrong! Press CLEAR to reset.",
               value, value[0] );
     return (errmsg);
   }

   if (node->value == -1) 
     node->value =  tot_value * (float) atof ( value ) / 100.0;
   else
     node->value += (tot_value * (float) atof ( value ) / 100.0); 

   node->isleaf = True;

   return (NULL);
}

/* Given a token string, remove it from the tree */
static char *delnodes ( NODE *parent, char *percents, float total_value, 
                       int errorchk )
{
   char *element;
   static char *value;

   char current[80];
   NODE *node;

   static char errmsg[512];

   if ( strncasecmp( percents, "percent", 7 ) == 0 ) {
     /* skip percent directive and white space */
     value = percents;
     while ( isalpha(*value) || isspace(*value) )
       value++;

     /* look for end of value, skip digits, and '.' or ',' */
     element =  value;
     while ( !isspace(*element) )
       element++;
     *element++ = '\0';

     /* look for first element */
     while ( !isalnum(*element) )
       element++;

     /* parent is still top level */
     delnodes ( parent, element, total_value, errorchk );
 
     if ( parent->children == 0 )
       delete (parent, node);
   }

   /* midst of elements */
   else if ( (element = strchr ( percents, '.' )) ) {

     /* Get current level node */
     strncpy ( current, percents, (int) (element-percents) ); 
     current[element-percents] = '\0';

     /* Check if exists */
     node = (NODE *) exists ( parent, current );
     if (!node) {
       sprintf( errmsg, "Node %s.%s, not found in tree.",
                parent->name, element );
       return (errmsg);
     }

     element++;
     delnodes ( node, element, total_value, errorchk );

     if ( node->children == 0 )
       delete (parent, node);
   }

   /* no more elements, remove the node */
   else {

     /* Get current level node */
     strcpy ( current, percents ); 
     node = (NODE *) exists ( parent, current );
     if (!node && errorchk) {
       sprintf( errmsg, "Node %s.%s, not found in tree.",
                parent->name, element );
       return (errmsg);
     }

     if ( !isdigit(value[0]) && *value != '.' && *value != ',' && errorchk) {
       sprintf ( errmsg, 
                 "%s\n^ expected digit or [,.], received '%c'. Total\n"
                 "value is probably wrong! Press CLEAR to reset",
                 value, value[0] );
       return (errmsg);
     }

     node->value -=  total_value * (float) atof ( value ) / 100.0;

     if (node->value <= 0.009 )
       delete ( parent, node );
   }

   return (NULL);
}

/* Given a parent and a new child name create and attach child to parent */
static NODE *create ( NODE *parent, char *child )
{
  LINK *newlink;
  LINK *current;
  NODE *newnode;

  /* make a node */
  newnode = (NODE *) malloc ( sizeof (NODE) );
  if (!newnode) 
    return (newnode);
  
  newnode->value = -1.0;
  newnode->children = NULL;
  newnode->name = (char *)malloc ( strlen(child)+1 );
  if (!newnode->name) 
    return (NULL);
  strcpy ( newnode->name, child );
  newnode->isleaf = False;

  /* Make a link */
  newlink = (LINK *)malloc ( sizeof (LINK) );
  if (!newlink) 
    return (NULL);
  newlink->child = newnode;
  newlink->next  = NULL;

  /* no children */
  if ( parent->children == NULL )
     parent->children = newlink;

  /* connect to previous */
  else  {
    current = parent->children;
    while ( current->next != NULL )
      current = current->next;
    current->next = newlink;
  }

  /* Make a link to parent */
  newlink = (LINK *)malloc ( sizeof (LINK) );
  if (!newlink) 
    return (NULL);
  newlink->child = parent;
  newlink->next  = NULL;
  newnode->parent = newlink;

  return (newnode);
}

/* Given a parent and a child delete child from parent */
static void delete ( NODE *parent, NODE *node )
{
  LINK *current, *prev;
  char errmsg[132];
  
  /* no children */
  if ( parent->children == NULL )
    return;

  /* first child matches */
  if ( strcmp ( parent->children->child->name, node->name ) == 0 )  {
    current = parent->children;
    parent->children = parent->children->next;

  } else {

    /* 2nd or subsequent children */
    /* Find the link */
    current = parent->children;

    while ( current ) {
      /* Won't match 1st time, we already know */
      if ( strcmp ( current->child->name, node->name ) == 0 )
        break;
      prev    = current;
      current = current->next;
    }

    if ( current == NULL) {
      sprintf( errmsg, "Node %s.%s, not found in tree.",
               parent->name, node->name );
      write_status ( errmsg, ERR );
      return;
    }
    /* Fix the links */
    prev->next = current->next;
  }

  /* Remove it */
  XtFree ( (char *)current );
 
  /* remove the node */
  XtFree ( node->name );
  XtFree ( (char *)node->parent );
  XtFree ( (char *)node );

}

/* Given a parent node and a name, see if its in the tree */
static int exists ( NODE *parent, char *element )
{
  LINK *current = parent->children;

  while (current) {
    if ( strcmp ( current->child->name, element ) == 0 )
      return ( (int)current->child );
    current = current->next;
  }
  return (  (int)current );
}

/* Given a node, what is its percentage of the total */  
static double what_value ( NODE *node )
{
  double value = 0.0;
  LINK  *current;

  if (node->children == NULL)
    return ( node->value ); 

  else {
    current = node->children;
    while (current) {
      value += what_value ( current->child );
      current = current->next;
    }
    
    node->value = value;
    return ( value );
  }
} 

/* Return the number of slices in the current "pie" */
int getPActive ()
{
  extern Widget Portpercentlist;
  int p_active;

  XtVaGetValues ( Portpercentlist, XmNitemCount, &p_active, NULL );
  return (p_active);
}

static double  extract_percent ( char *line )
{
   char *next = line;

   /* skip percent directive and white space */
   while ( isalpha(*next) || isspace(*next) )
     next++;

   return ( atof ( next ) ); 

}

