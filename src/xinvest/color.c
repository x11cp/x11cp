/*
  Xinvest is copyright 1995 by Mark Buser.

  Permission is hereby granted to copy
  and freely distribute copies of this
  program for non-commercial purposes
  without fee, provided that this notice
  appears in all copies.
  
  All redistributions must be in their
  entirety as originally distributed.
  Feel free to modify Xinvest, but
  modified versions may not be distributed
  without prior consent of the author.
  
  This software is provided 'as-is'
  without any express or implied warranty.
  In no event will the author be held
  liable for any damages resulting from
  the use of this software.

  $Revision: 2.2 $ $Date: 1997/08/10 14:49:53 $
*/
#include <stdio.h>
#include <Xm/Xm.h>
#include "color.h"
#include "colorP.h"

static Pixel color[XtNumber(colors)];


static Colormap _cmap;
Colormap GetColormap()
{
  return _cmap;
}

Colormap NewColormap( Display *dpy, Colormap cmap)
{
   Colormap new = XCopyColormapAndFree ( dpy, cmap );
   if ( new == (Colormap)BadAlloc ||
        new == (Colormap)BadColor) {
      return (Colormap)NULL;
   }
   _cmap = new;
   return new;
}



Pixel GetColor ( int which )
{
  return ( color[which] );
} 

/* 
** Allocate all colors in color table.  If it fails, create a 
** new colormap and return it.  Return NULL if any failures.
*/
Colormap InitColor(Widget top, Colormap cmap, Pixel *appColor) 
{

  XColor actual, exact;
  Display *dpy = XtDisplay (top);
  int i;
  int how_many = 0;

  /* Just copy app level color resources */
  if (appColor)
    for (i = USER0; i <= USER7; i++)
      color[i] = appColor[i];

  /* The remainder are hardcoded color names */
  do {
    if ( !XAllocNamedColor( dpy, cmap, colors[i], &actual, &exact) ) {

      if (how_many == 1)
        return (Colormap) NULL;

      fprintf (stderr, "Warning: could not allocate color \"%s\".\n", colors[i]);
      fprintf (stderr, "Warning: trying to allocate private colormap.\n");
      if ( (cmap = NewColormap( dpy, cmap )) == (Colormap)NULL)
        return (Colormap) NULL;
      how_many++;

    } else {
      /* Successful, add to color table */
      color[i] = actual.pixel;
      i++;
      how_many = 0;
    }
  } while (i < XtNumber(colors));

  _cmap = cmap;
  return cmap;
}


int isColorVisual (Display *dpy)
{
  Visual *visual = XDefaultVisual( dpy, XDefaultScreen( dpy ));

  /* We're not supposed to do this, but its so much easier. */
  switch ( visual->class ) {

#ifndef MONO_TEST
     case DirectColor:
     case PseudoColor:
     case StaticColor:
     case TrueColor:
                       return True;
                       /* break; */
#endif

     case GrayScale:
     case StaticGray:
     default:
                       return False;

                       /* break; */
  }
}


int isDarkPixel ( Display *dpy, Pixel color )
{
  int color_intensity = 0, max_intensity = 0;
  XColor xcolor;

  xcolor.pixel = color;
  XQueryColor (dpy, GetColormap(), &xcolor);

  if ( xcolor.flags & DoGreen ) {
    color_intensity += xcolor.green;
    max_intensity += 65535;
  }
  if ( xcolor.flags & DoRed ) {
    color_intensity += xcolor.red;
    max_intensity += 65535;
  }
  if ( xcolor.flags & DoBlue ) {
    color_intensity += xcolor.blue;
    max_intensity += 65535;
  }

  if ( (float)color_intensity / (float)max_intensity < 0.67 )
    return True;
  else
    return False;
}
