/*
  Xinvest Copyright 1995-97 Mark Buser,
  All Rights Reserved.

  Permission is hereby granted to copy
  and freely distribute copies of this
  program for non-commercial purposes
  without fee, provided that this notice
  appears in all copies.
  
  All redistributions must be in their
  entirety as originally distributed.
  Feel free to modify Xinvest, but
  modified versions may not be distributed
  without prior consent of the author.
  
  This software is provided 'as-is'
  without any express or implied warranty.
  In no event will the author be held
  liable for any damages resulting from
  the use of this software.

  $Revision: 2.29 $ $Date: 1997/09/20 17:04:03 $
*/

#include <stdio.h>
#include <time.h>
#include <unistd.h>

#include <X11/Xlib.h>
#include <Xm/XmAll.h>

#ifdef EDITRES
#include <X11/Xmu/Editres.h>
#endif

#include "account.h"
#include "calendar.h"
#include "color.h"
#include "drawing.h"
#include "edit.h"
#include "file.h"
#include "graph.h"
#include "nav.h"
#include "pixmap.h"
#include "rate.h"
#include "remote.h"
#include "report.h"
#include "session.h"
#include "status.h"
#include "tooltips.h"
#include "util.h"
#include "xinvest.h"
#include "xutil.h"

/* Menu & button callbacks */
       void toolCB ();
extern void accountCB();
extern void statusCB();
extern void helpCB();


/* Globals */
static char version[] = "Xinvest 2.4";

static char *portoptmenu[] = { "Display", "Root", "Accounts", "Flatten" };

static char *calcfuncs[] =   { "Function", "Future Value (Lump)", 
                               "Future Value (Series)", "Periodic Payment", 
                               "Present Value", "Rate" };

       char *calcparams[] =  { "Present Value", "Future Value", "Rate", 
                        "Compounded", "Years", "Payment"};
static char *calccompnd[] =  { "", "Daily", "Weekly", "Monthly", "Quarterly", 
                        "Bi-annually", "Annually", "180/365", "360/365" };

static MENUITEMS file[] = {
 { "button_0",    XmVaPUSHBUTTON, fileCB },
 { "button_1",    XmVaPUSHBUTTON, fileCB },
 { "button_2",    XmVaPUSHBUTTON, fileCB },
 { "button_3",    XmVaPUSHBUTTON, fileCB },
 { "button_4",    XmVaPUSHBUTTON, fileCB },
 { "separator_0", XmVaSEPARATOR,  NULL },
 { "button_5",    XmVaPUSHBUTTON, fileCB }
};

static MENUITEMS edit[] = {
 { "button_0",    XmVaPUSHBUTTON, editCB },
 { "button_1",    XmVaPUSHBUTTON, editCB },
 { "button_2",    XmVaPUSHBUTTON, editCB }
};

static MENUITEMS tool[] = {
 { "button_0",    XmVaRADIOBUTTON, toolCB },
 { "button_1",    XmVaRADIOBUTTON, toolCB },
 { "button_2",    XmVaRADIOBUTTON, toolCB },
 { "button_3",    XmVaRADIOBUTTON, toolCB },
 { "button_4",    XmVaRADIOBUTTON, toolCB },
 { "button_5",    XmVaRADIOBUTTON, toolCB },
};

static MENUITEMS account[] = {
 { "button_0",    XmVaPUSHBUTTON, accountCB },
 { "button_1",    XmVaPUSHBUTTON, accountCB },
 { "button_2",    XmVaPUSHBUTTON, accountCB },
 { "separator_0", XmVaSEPARATOR,   NULL }
};

static MENUITEMS log[] = {
 { "button_0",    XmVaPUSHBUTTON, statusCB }
};

static MENUITEMS option[] = {
 { "button_0",    XmVaCHECKBUTTON, graph_option },
 { "button_1",    XmVaCHECKBUTTON, graph_option },
 { "button_2",    XmVaCHECKBUTTON, graph_option }
};

static MENUITEMS help[] = {
 { "button_0",    XmVaPUSHBUTTON, helpCB }
};

static BUTTONITEMS toolbar[] = {
  { "About", XmVaPUSHBUTTON, (Pixmap)NULL, (Pixmap)NULL, (Pixmap)NULL,
    (XtPointer)NULL, NULL, toolCB, NULL 
  },
  { "Edit", XmVaPUSHBUTTON, (Pixmap)NULL, (Pixmap)NULL, (Pixmap)NULL,
    (XtPointer)NULL, NULL, toolCB, NULL 
  },
  { "Gain", XmVaPUSHBUTTON, (Pixmap)NULL, (Pixmap)NULL, (Pixmap)NULL,
    (XtPointer)NULL, NULL, toolCB, NULL 
  },
  { "Graphbut",	XmVaPUSHBUTTON, (Pixmap)NULL, (Pixmap)NULL, (Pixmap)NULL,
    (XtPointer)NULL, NULL, toolCB, NULL 
  },
  { "Portfolio", XmVaPUSHBUTTON, (Pixmap)NULL, (Pixmap)NULL, (Pixmap)NULL,
    (XtPointer)NULL, NULL, toolCB, NULL 
  },
  { "Calculate", XmVaPUSHBUTTON, (Pixmap)NULL, (Pixmap)NULL, (Pixmap)NULL,
    (XtPointer)NULL, NULL, toolCB, NULL 
  },
  { "Quit", XmVaPUSHBUTTON, (Pixmap)NULL, (Pixmap)NULL, (Pixmap)NULL,
    (XtPointer)NULL, NULL, toolCB, NULL 
  },
};

static BUTTONITEMS acctbar[] = {
  { "button_0", XmVaTOGGLEBUTTON, (Pixmap)NULL, (Pixmap)NULL, (Pixmap)NULL,
    (XtPointer)NULL, NULL, reportSetAccount, NULL 
  },
  { "button_1", XmVaTOGGLEBUTTON, (Pixmap)NULL, (Pixmap)NULL, (Pixmap)NULL,
    (XtPointer)NULL, NULL, reportSetAccount, NULL 
  },
  { "button_2", XmVaTOGGLEBUTTON, (Pixmap)NULL, (Pixmap)NULL, (Pixmap)NULL,
    (XtPointer)NULL, NULL, reportSetAccount, NULL 
  }
};

static BUTTONITEMS timebar[] = {
  { "button_0", XmVaTOGGLEBUTTON, (Pixmap)NULL, (Pixmap)NULL, (Pixmap)NULL,
    (XtPointer)NULL, NULL, reportSetTime, NULL 
  },
  { "button_1", XmVaTOGGLEBUTTON, (Pixmap)NULL, (Pixmap)NULL, (Pixmap)NULL,
    (XtPointer)NULL, NULL, reportSetTime, NULL 
  },
  { "button_2", XmVaTOGGLEBUTTON, (Pixmap)NULL, (Pixmap)NULL, (Pixmap)NULL,
    (XtPointer)NULL, NULL, reportSetTime, NULL 
  },
  { "button_3", XmVaTOGGLEBUTTON, (Pixmap)NULL, (Pixmap)NULL, (Pixmap)NULL,
    (XtPointer)NULL, NULL, reportSetTime, NULL 
  },
  { "button_4", XmVaTOGGLEBUTTON, (Pixmap)NULL, (Pixmap)NULL, (Pixmap)NULL,
    (XtPointer)NULL, NULL, reportSetTime, NULL 
  },
  { "button_5", XmVaTOGGLEBUTTON, (Pixmap)NULL, (Pixmap)NULL, (Pixmap)NULL,
    (XtPointer)NULL, NULL, reportSetTime, NULL 
  },
  { "button_6", XmVaTOGGLEBUTTON, (Pixmap)NULL, (Pixmap)NULL, (Pixmap)NULL,
    (XtPointer)NULL, NULL, reportSetTime, NULL 
  },
  { "button_7", XmVaTOGGLEBUTTON, (Pixmap)NULL, (Pixmap)NULL, (Pixmap)NULL,
    (XtPointer)NULL, NULL, reportSetTime, NULL 
  }
};


XFontStruct *small, *large;

Widget Filemenu; 
Widget Editmenu; 
static Widget Toolmenu; 
Widget Optionmenu; 
Widget Accountmenu; 
Widget Transtext; 
Widget Translabel;
Widget Filecwdlabel;
Widget Listfile;
static Widget Infoframe;
static Widget Reportform; 
Widget Timeform;
Widget Valrow;
static Widget Graphform; 
Widget Graphhbar; 
Widget Graphhline; 
Widget GraphconstToggle; 
Widget Graphscale; 
Widget Graphcform;  
static Widget Portform;
Widget Portpbut;
Widget Portmbut;
Widget Portcbut;
Widget Portabut;
Widget Portpercentlist;
Widget Portoptmenu;
Widget Portvaluelabel; 
static Widget Calcform;
Widget Calclabel;
Widget Calclabels[5];
Widget Calctext[3]; 
Widget Savebut;
Widget Nextbut; 
Widget Prevbut; 
Widget Gainbut; 
Widget Graphbut;
Widget Portbut;
Widget Editbut;

/* 
** Forward Declarations 
*/

/* Portfolio callbacks */
extern void  portAddSubList();
extern void  portTraverse();

/* Calculator callbacks */
extern void  calcFuncSel();
extern void  calcTextSel();
extern void  calcCompSel();

void main (int argc, char **argv)
{

  XtAppContext app;

  /* application resources */
  AppData appdata;

  XmFontContext   fontContext;

  /* Main Window Widgets */
  Widget Toplevel;
  Widget Mainform;
   Widget Menubar;
/*   Widget Filemenu; */
/*   Widget Editmenu; */
/*   Widget Toolmenu; */
/*   Widget Accountmenu; */
/*   Widget Optionmenu; */
   Widget Workform;
    Widget HPane;    /* manages left side of Workform */
     Widget VPane;   /* manages left side of Workform */
/*    Widget Translabel; */
/*    Widget Transtext; */
      Widget Graphframe;
        Widget Graph;   
     Widget Midform; /* manages center of Workform */
       Widget Fileframe;
/*     Widget Filecwdlabel; */
         Widget Fileform;
/*        Widget Listfile; */
/*        Widget Savebut; */
          Widget Newbut;
          Widget Openbut;
/*        Widget Prevbut; */
/*        Widget Nextebut; */
       Widget Actframe;
         Widget Actform; /* The following share the same screen area */
/*        Widget Infoframe; */
/*        Widget Reportform; */
          Widget ReportInnerform;
           Widget Screenbut;
           Widget Timeframe;
/*          Widget Timeform; */
           Widget Valframe;
/*          Widget Valrow; */
           Widget Acctframe;
            Widget Acctform;
           Widget Reportbut;
/*        Widget Graphform; */
            Widget Graphpframe;
              Widget Graphprow;
            Widget Graphhframe;
              Widget Graphhrow;
            Widget Graphvframe;
              Widget Graphvform;
                Widget Graphvscroll;
/*              Widget Graphcform; */
                  Widget Graphvtext1;
                  Widget Graphvlabel; 
                  Widget Graphvtext2;
/*              Widget Graphscale; */
                Widget Graphslabel;
/*        Widget Portform; */
           Widget Portselrow;
            Widget Portframe;
             Widget Portbutform;
/*            Widget Portpbut; */
/*            Widget Portmbut; */
/*            Widget Portabut; */
/*            Widget Portcbut; */
           Widget Portscrform;
            Widget Portpercentlabel;
/*          Widget Portpercentlist; */
/*          Widget Portoptmenu; */
/*          Widget Portvaluelabel; */
/*        Widget Calcform; */
           Widget Calcoptmenu;
           Widget Calcframe;
/*           Widget Calclabel; */
           Widget Calcrow;
/*	    Widget Calclabels[5]; */
/*	    Widget Calctext[3]; */
            Widget Calccmpdmenu;
     Widget Butform;

  /* Display */
  Display *dpy;

  /* Window */
  Window win;

  /* Color resources */
  Colormap cmap;
  GC       gc;

  /* X strings */
  XmString Temp[ XtNumber (calccompnd)];

  /* array of resources */
  int n;
  Arg args[15];

  char  dir[BUFSIZ];

  int    i;  /* ubiquitous i */
  Widget w;  /* temporary widget handle */

  /* Initialize toolkit */
  XtSetLanguageProc (NULL, NULL, NULL);

#if XtSpecificationRelease > 5
  /* R6 session shell supports session management */
  Toplevel = XtVaOpenApplication( &app, "Xinvest",
                                  options, XtNumber(options), 
                                  &argc, argv, 
                                  getFallbackResource(), 
                                  sessionShellWidgetClass, NULL);
  /* These callbacks are required */
  XtAddCallback (Toplevel, XtNsaveCallback, sessSave, 0);
  XtAddCallback (Toplevel, XtNcancelCallback, sessResume, 0);
  XtAddCallback (Toplevel, XtNsaveCompleteCallback, sessResume, 0);
  XtAddCallback (Toplevel, XtNdieCallback, sessDie, 0);

#else
  /* Old style WM_SAVE_YOURSELF property method of save state notification */
  Toplevel = XtVaAppInitialize( &app, "Xinvest",
                                options, XtNumber(options), &argc, argv, 
                                getFallbackResource(), NULL);
  {
     Atom XaWmSaveYourself = XmInternAtom ( XtDisplay (Toplevel), 
		                            "WM_SAVE_YOURSELF", False);
     XmAddWMProtocols ( Toplevel, &XaWmSaveYourself, 1);
     XmAddWMProtocolCallback ( Toplevel, XaWmSaveYourself, sessSave, 0);
  }
#endif

  XmRepTypeInstallTearOffModelConverter();
  XtVaGetApplicationResources ( Toplevel, 
                                &appdata, 
                                resources, 
                                XtNumber( resources ),
                                NULL );

  /* If help, print syntax and exit */
  if (appdata.help) {
    syntax (argc, argv);
    exit(0);
  }

#ifdef EDITRES
  XtAddEventHandler( Toplevel, 0, True, _XEditResCheckMessages, NULL);
#endif

  dpy = XtDisplay( Toplevel );
  win = RootWindowOfScreen (XtScreen(Toplevel));
  cmap = DefaultColormapOfScreen (XtScreen(Toplevel));
  gc = XCreateGC ( dpy, win, 0, NULL );

  /**********************/

  /* Get a color map */
  if (appdata.install)
    cmap = NewColormap (dpy, cmap );

  { /* Application resource colors get added to color list. */
    Pixel appcolors[8];

    appcolors[0] = appdata.color0; 
    appcolors[1] = appdata.color1; 
    appcolors[2] = appdata.color2; 
    appcolors[3] = appdata.color3; 
    appcolors[4] = appdata.color4; 
    appcolors[5] = appdata.color5; 
    appcolors[6] = appdata.color6; 
    appcolors[7] = appdata.color7; 

    cmap = InitColor ( Toplevel, cmap, appcolors );
    if ( cmap == (Colormap)NULL) {
       fprintf( stderr, "Error: can't create XColor colormap, exiting.\n");
       exit(1);
    }
  }

  /* Set the colormap */
  XtVaSetValues ( Toplevel, XmNcolormap, cmap, NULL );


  /* Create the main window */
  Mainform = XtVaCreateWidget(  "Mainform", xmFormWidgetClass, Toplevel,        
                                 XmNresizable, True,
        			 NULL );

  /* Load up all of the pixmaps. Why here? Because Toplevel may have a
  ** bogus background color.  We can't load a "good" colormap till after
  ** its up.  This is the first widget with a likely allocated bg color. 
  */
  cmap = InitPixmaps( Mainform, cmap);
  if ( cmap == (Colormap)NULL) {
     fprintf( stderr, "Error: can't create Pixmap colormap, exiting.\n");
     exit(1);
  }

  /* Set the icon */
  MakeIconWindow ( Toplevel );

  /**********************/
 
  /* Create menu bar and pulldown menus */
  n = 0;
  XtSetArg(args[n], XmNtopAttachment,   XmATTACH_FORM); n++;
  XtSetArg(args[n], XmNleftAttachment,   XmATTACH_FORM); n++;
  XtSetArg(args[n], XmNrightAttachment,   XmATTACH_FORM); n++;
  Menubar =  XmCreateMenuBar ( Mainform, "menubar", args, n );

  XtVaCreateManagedWidget("button_0", xmCascadeButtonGadgetClass,Menubar, NULL);
  XtVaCreateManagedWidget("button_1", xmCascadeButtonGadgetClass,Menubar, NULL);
  XtVaCreateManagedWidget("button_2", xmCascadeButtonGadgetClass,Menubar, NULL);
  XtVaCreateManagedWidget("button_3", xmCascadeButtonGadgetClass,Menubar, NULL);
  XtVaCreateManagedWidget("button_4", xmCascadeButtonGadgetClass,Menubar, NULL);
  XtVaCreateManagedWidget("button_5", xmCascadeButtonGadgetClass,Menubar, NULL);
  XtVaCreateManagedWidget("button_6", xmCascadeButtonGadgetClass,Menubar, NULL);

  /* Set help menu */
  if ( (w = XtNameToWidget (Menubar, "button_6")) )
    XtVaSetValues( Menubar, XmNmenuHelpWidget, w, NULL);

  /* 
  ** File menu 
  */
  Filemenu = makeMenuPulldown ( Menubar, "Filemenu", 0, file, XtNumber(file) );
  /* Some don't make sense til file loaded*/
  if ( (w = XtNameToWidget (Filemenu, "button_2")) )   /* close */
     XtVaSetValues( w, XmNsensitive, False, NULL);
  if ( (w = XtNameToWidget (Filemenu, "button_3")) )   /* save */
     XtVaSetValues( w, XmNsensitive, False, NULL);

  /*
  ** Edit menu
  */
  Editmenu = makeMenuPulldown ( Menubar, "Editmenu", 1, edit, XtNumber(edit) );
  /* Some don't make sense til file loaded*/
  if ( (w = XtNameToWidget (Editmenu, "button_0")) )   /* copy */
     XtVaSetValues( w, XmNsensitive, False, NULL);
  if ( (w = XtNameToWidget (Editmenu, "button_1")) )   /* cut */
     XtVaSetValues( w, XmNsensitive, False, NULL);

  /* 
  ** Tool menu 
  */
  Toolmenu = makeMenuPulldown ( Menubar, "Toolmenu", 2, tool, XtNumber(tool) );
  /* Some don't make sense til file loaded */
  if ( (w = XtNameToWidget (Toolmenu, "button_1")) )   /* edit transaction */
     XtVaSetValues( w, XmNsensitive, False, NULL);
  if ( (w = XtNameToWidget (Toolmenu, "button_2")) )   /* total return */
     XtVaSetValues( w, XmNsensitive, False, NULL);
  if ( (w = XtNameToWidget (Toolmenu, "button_3")) )   /* plot */
     XtVaSetValues( w, XmNsensitive, False, NULL);
  if ( (w = XtNameToWidget (Toolmenu, "button_4")) )   /* portfolio */
     XtVaSetValues( w, XmNsensitive, False, NULL);

  /* 
  ** Option menu 
  */
  Optionmenu = makeMenuPulldown ( Menubar, "Optionmenu", 3, 
                                  option, XtNumber(option) );
  if ( (w = XtNameToWidget (Optionmenu, "button_0")) )   /* zoom */
     XtVaSetValues( w, XmNsensitive, False, NULL);
  if ( (w = XtNameToWidget (Optionmenu, "button_1")) )   /* show points */
     XtVaSetValues( w, XmNsensitive, False, NULL);
  if ( (w = XtNameToWidget (Optionmenu, "button_2")) )   /* show grid */
     XtVaSetValues( w, XmNsensitive, False, NULL);

  /* 
  ** Account menu 
  */
  Accountmenu = makeMenuPulldown ( Menubar, "Accountmenu", 4,
                                   account, XtNumber(account) );
  /* Some don't make sense til file loaded*/
  if ( (w = XtNameToWidget (Accountmenu, "button_0")) )   /* previous */
     XtVaSetValues( w, XmNsensitive, False, NULL);
  if ( (w = XtNameToWidget (Accountmenu, "button_1")) )   /* next */
     XtVaSetValues( w, XmNsensitive, False, NULL);
  if ( (w = XtNameToWidget (Accountmenu, "button_2")) )   /* apply changes */
     XtVaSetValues( w, XmNsensitive, False, NULL);

  /* 
  ** Log menu 
  */
  makeMenuPulldown ( Menubar, "Viewmenu", 5, log, XtNumber(log) );

  /* 
  ** Help menu 
  */
  makeMenuPulldown ( Menubar, "Helpmenu", 6, help, XtNumber(help) );

  XtManageChild (Menubar);
   
  /* Set "about" as initial selection */
  w = XtNameToWidget ( Toolmenu, "button_0" );
  XtVaSetValues ( w, XmNset, True, NULL);

  /**********************/

  /* Create work area form */
  Workform = XtVaCreateWidget ( "Workform", xmFormWidgetClass, Mainform, 
			        XmNtopAttachment,    XmATTACH_WIDGET,
                                XmNtopWidget,        Menubar,
			        XmNleftAttachment,   XmATTACH_FORM,
			        XmNrightAttachment,  XmATTACH_FORM,
                                XmNbottomAttachment, XmATTACH_FORM,
			        NULL );
  /**********************/

#if XmVERSION > 1
  /* Create pane for left and center areas */
  HPane = XtVaCreateWidget ( "HPane", xmPanedWindowWidgetClass, Workform,
                             XmNorientation,      XmHORIZONTAL,
			     XmNtopAttachment,    XmATTACH_FORM,
                             XmNbottomAttachment, XmATTACH_FORM,
			     XmNleftAttachment,   XmATTACH_FORM,
	               	     NULL ); 
  /* Create pane for text and graph */
  VPane = XtVaCreateWidget ( "VPane", xmPanedWindowWidgetClass, HPane,
                            XmNorientation,     XmVERTICAL,
	               	    NULL ); 
#else
  /* Create pane for text and graph */
  VPane = XtVaCreateWidget ( "VPane", xmPanedWindowWidgetClass, Workform,
		            XmNtopAttachment,    XmATTACH_FORM,
			    XmNleftAttachment,   XmATTACH_FORM,
		            XmNbottomAttachment, XmATTACH_FORM,
	               	    NULL ); 
#endif
  /*********************/

  Translabel = XtVaCreateManagedWidget("Translabel",
                          xmLabelWidgetClass, VPane,
                          NULL);
  {
    Dimension height, margin;
    XtVaGetValues (Translabel, XmNheight, &height,
                               XmNmarginHeight, &margin,
                   NULL);
    XtVaSetValues (Translabel, XmNpaneMinimum, height + 2*margin,
                               XmNpaneMaximum, height + 2*margin,
                   NULL); 
  }
  /*********************/

  /* Scrolled text widget holding transaction history*/
  n = 0;
  XtSetArg(args[n], XmNeditMode,  XmMULTI_LINE_EDIT); n++;
  Transtext = XmCreateScrolledText( VPane, "Transtext", args, n);
  XtAddCallback( Transtext, XmNgainPrimaryCallback,
                 (XtCallbackProc) editSelectionCB, (XtPointer) NULL);
  XtAddCallback( Transtext, XmNmotionVerifyCallback,
                 (XtCallbackProc) editSelectionCB, (XtPointer) NULL);
  XtAddCallback( Transtext, XmNlosePrimaryCallback,
                 (XtCallbackProc) editSelectionCB, (XtPointer) NULL);

  XtManageChild (Transtext);
  XtSetSensitive (Transtext, False);
  /*******************/

  /* Drawing area to graph cost */
  Graphframe = XtVaCreateManagedWidget( "Graphframe", 
					xmFrameWidgetClass, VPane,
					NULL);

  Graph = XtVaCreateWidget ( "Graph", 
                             xmDrawingAreaWidgetClass, Graphframe, 
                             NULL );

  XtAddCallback( Graph, XmNexposeCallback, 
                   (XtCallbackProc) redrawDrawing, (XtPointer) 1);

  /* Set gc for Xlib drawing */
  XtVaSetValues ( Graph, XmNuserData, gc, NULL );

  /* tell drawing routines where the drawing area is */
  setDrawingArea ( Graph );

  /* Get fonts for graph titles, etc. */

  small = large = NULL;
  if ( XmFontListInitFontContext ( &fontContext, appdata.graph_font_list ) 
       == True ) {
  
    XmFontListEntry entry;
    XmFontType      fonttype =  XmFONT_IS_FONT;
    char *tag;
    
    while ( (entry = XmFontListNextEntry ( fontContext )) != NULL ) {
      
       tag = XmFontListEntryGetTag ( entry );
       if ( strcmp( tag, "small") == 0 )
         small = (XFontStruct *) XmFontListEntryGetFont ( entry, &fonttype );
       if ( strcmp( tag, "large") == 0 )
         large = (XFontStruct *) XmFontListEntryGetFont ( entry, &fonttype );

       if ( tag != NULL )
         XtFree (tag);

    }

    XmFontListFreeFontContext ( fontContext );
  }

  if ( small == NULL || large == NULL ) {
    fprintf (stderr, "Error: unable to load drawing area fonts. \n");
    fprintf (stderr, "Xinvest will crash on a graph operation! \n");
  }
       
  XtManageChild(Graph);

  /******************/


  /* Create form to keep file action and status forms tidy */
#if XmVERSION > 1
  Midform = XtVaCreateWidget ( "Midform", xmFormWidgetClass, HPane, 
			        NULL );
#else
  Midform = XtVaCreateWidget ( "Midform", xmFormWidgetClass, Workform, 
			       	XmNtopAttachment,    XmATTACH_FORM,
			       	XmNbottomAttachment, XmATTACH_FORM,
			        NULL );
 
  /* Now we have a Midform, attach Pane to it */
  XtVaSetValues ( VPane, XmNrightAttachment,  XmATTACH_WIDGET,
                         XmNrightWidget,      Midform,
                  NULL );
#endif

  /* File area frame */
  Fileframe = XtVaCreateManagedWidget( "Fileframe", 
					xmFrameWidgetClass, Midform,
			       		XmNtopAttachment,   XmATTACH_FORM,
			       		XmNleftAttachment,  XmATTACH_FORM,
			       		XmNrightAttachment, XmATTACH_FORM,
			         	NULL );
  /* title for frame */
  Filecwdlabel = XtVaCreateManagedWidget("File Manager",
                          xmLabelGadgetClass, Fileframe,
                          XmNchildType, XmFRAME_TITLE_CHILD,
                          XmNchildVerticalAlignment, XmALIGNMENT_CENTER,
                          NULL);
  
  /* File area form */
  Fileform = XtVaCreateWidget ( "Fileform", xmFormWidgetClass, Fileframe, 
			        NULL );
  /**********************/

  /* Create new file button */
  Newbut =  XtVaCreateManagedWidget( "New",
         xmPushButtonWidgetClass, Fileform,
         XmNlabelType,            XmPIXMAP,
         XmNlabelPixmap,          GetPixmap(PNEW, NORMAL),
         XmNlabelInsensitivePixmap,   GetPixmap(PNEW, INSENS), 
         XmNsensitive,            True,
         XmNtopAttachment,        XmATTACH_FORM,       
         XmNleftAttachment,       XmATTACH_FORM,       
         NULL);
  XtAddCallback ( Newbut, XmNactivateCallback, fileCB, (XtPointer) 0);
  installToolTips (Newbut);

  /* Create open file button */
  Openbut =  XtVaCreateManagedWidget( "Open",
         xmPushButtonWidgetClass,  Fileform,
         XmNlabelType,             XmPIXMAP,
         XmNlabelPixmap,           GetPixmap(POPEN, NORMAL),
         XmNlabelInsensitivePixmap,   GetPixmap(POPEN, INSENS), 
         XmNsensitive,             True,
         XmNtop,                   XmATTACH_FORM,       
         XmNleftAttachment,        XmATTACH_WIDGET,       
         XmNleftWidget,            Newbut,       
         NULL);
  XtAddCallback ( Openbut, XmNactivateCallback, fileCB, (XtPointer) 1);
  installToolTips (Openbut);

  /* Create save file button */
  Savebut =  XtVaCreateManagedWidget( "Save",
         xmPushButtonWidgetClass, Fileform,
         XmNlabelType,            XmPIXMAP,
         XmNlabelPixmap,          GetPixmap(PSAVE, NORMAL),
         XmNlabelInsensitivePixmap,   GetPixmap(PSAVE, INSENS), 
         XmNsensitive,            False,
         XmNtopAttachment,        XmATTACH_WIDGET,       
         XmNtopWidget,            Openbut,       
         XmNleftAttachment,       XmATTACH_FORM,       
         NULL);
  /* So we can sensitize the menu, when we do the button */
  if ( (w = XtNameToWidget (Filemenu, "button_3")) )   /* save */
     XtVaSetValues( Savebut, XmNuserData, w, NULL);
  XtAddCallback ( Savebut, XmNactivateCallback, fileCB, (XtPointer) 3);
  installToolTips (Savebut);

  /* Create prev account button */
  Prevbut =  XtVaCreateManagedWidget( "Prevbut",
         xmPushButtonWidgetClass, Fileform,
         XmNlabelType,            XmPIXMAP,
         XmNlabelPixmap,          GetPixmap(PPREV, NORMAL),
         XmNlabelInsensitivePixmap,   GetPixmap(PPREV, INSENS), 
         XmNsensitive,            False,
         XmNtopAttachment,        XmATTACH_WIDGET,       
         XmNtopWidget,            Savebut,       
         XmNleftAttachment,       XmATTACH_FORM,       
         NULL);

  /* We can sensitize the menu, when we do the button */
  if ( (w = XtNameToWidget (Accountmenu, "button_0")) )   /* cycle */
     XtVaSetValues( Prevbut, XmNuserData, w, NULL);
  XtAddCallback ( Prevbut, XmNactivateCallback, accountCB, (XtPointer)0 );
  installToolTips (Prevbut);

  /* Create next account button */
  Nextbut =  XtVaCreateManagedWidget( "Nextbut",
         xmPushButtonWidgetClass, Fileform,
         XmNlabelType,            XmPIXMAP,
         XmNlabelPixmap,          GetPixmap(PNEXT, NORMAL),
         XmNlabelInsensitivePixmap,   GetPixmap(PNEXT, INSENS), 
         XmNsensitive,            False,
         XmNtopAttachment,        XmATTACH_OPPOSITE_WIDGET,       
         XmNtopWidget,            Prevbut,       
         XmNleftAttachment,       XmATTACH_WIDGET,       
         XmNleftWidget,           Prevbut,       
         NULL);

  /* We can sensitize the menu, when we do the button */
  if ( (w = XtNameToWidget (Accountmenu, "button_1")) )   /* cycle */
     XtVaSetValues( Nextbut, XmNuserData, w, NULL);
  XtAddCallback ( Nextbut, XmNactivateCallback, accountCB, (XtPointer)1 );
  installToolTips (Nextbut);

  /* Create file list window */
  n = 0;
  XtSetArg(args[n], XmNselectionPolicy,  XmBROWSE_SELECT); n++;
  XtSetArg(args[n], XmNtopAttachment,    XmATTACH_FORM); n++;
  XtSetArg(args[n], XmNleftAttachment,   XmATTACH_WIDGET); n++;
  XtSetArg(args[n], XmNleftWidget,       Openbut); n++;
  XtSetArg(args[n], XmNrightAttachment,  XmATTACH_FORM); n++;
  XtSetArg(args[n], XmNbottomAttachment, XmATTACH_FORM); n++;
  XtSetArg(args[n], XmNuserData,         NULL); n++;
  Listfile = XmCreateScrolledList(Fileform, "Listfile", args, n);
  XtAddCallback ( Listfile, XmNdefaultActionCallback, fileCB, (XtPointer) 1);
  XtManageChild(Listfile);

  if ( getcwd( dir, BUFSIZ) )
    loadfile( dir );

  /**************************/


  /* Action area form */
  Actframe = XtVaCreateManagedWidget( "Actframe", 
				      xmFrameWidgetClass, Midform,
			              XmNleftAttachment,  XmATTACH_FORM,
				      XmNrightAttachment, XmATTACH_FORM,
				      XmNbottomAttachment, XmATTACH_FORM,
                                      NULL);

  /* Now we have a Actframe, attach Fileframe to it */
  XtVaSetValues ( Fileframe, XmNbottomAttachment,  XmATTACH_WIDGET,
                             XmNbottomWidget,      Actframe,
                  NULL );

  /* title for frame */
  XtVaCreateManagedWidget("Tool Manager",
                          xmLabelGadgetClass, Actframe,
                          XmNchildType, XmFRAME_TITLE_CHILD,
                          XmNchildVerticalAlignment, XmALIGNMENT_CENTER,
                          NULL);
  
  Actform = XtVaCreateWidget ( "Actform", xmFormWidgetClass, Actframe, 
                                NULL );
  /**********************/

  /* 
  ** The info label 
  */
  Infoframe = XtVaCreateManagedWidget( "Infoframe", 
				      xmFrameWidgetClass, Actform,
			              XmNtopAttachment,   XmATTACH_SELF,
			              XmNleftAttachment,  XmATTACH_SELF,
                                      NULL);
  XtVaCreateManagedWidget( "Infolabel",
                           xmLabelWidgetClass, Infoframe,
                           XmNlabelType, XmPIXMAP,
                           XmNlabelPixmap, GetPixmap(PINFO, NORMAL),
                           XmNlabelInsensitivePixmap, GetPixmap(PINFO, NORMAL),
                           NULL);
  
  /* 
  ** Make the graph selection area for actform
  */
  Graphform = XtVaCreateWidget ( "Graphform", xmFormWidgetClass, Actform, 
                               XmNtopAttachment,  XmATTACH_SELF,
                               XmNleftAttachment, XmATTACH_SELF,
                               NULL );

  /* account or portfolio choices */
  Graphpframe = XtVaCreateManagedWidget( "Graphpframe", 
			       xmFrameWidgetClass, Graphform,
                               XmNtopAttachment,  XmATTACH_FORM,
                               XmNleftAttachment, XmATTACH_FORM,
		NULL);

  XtVaCreateManagedWidget("GraphMode",
                          xmLabelGadgetClass, Graphpframe,
                          XmNchildType, XmFRAME_TITLE_CHILD,
                          NULL);

  Graphprow = XmVaCreateSimpleRadioBox ( Graphpframe, "Graphprow", 0,
                                   graph_pselect,
                                   XmVaRADIOBUTTON, NULL, NULL, NULL, NULL,
                                   XmVaRADIOBUTTON, NULL, NULL, NULL, NULL,
                                   NULL);

  if ( (w = XtNameToWidget ( Graphprow, "button_0" )) != NULL)
    XtVaSetValues ( w,
                    XmNlabelType, XmPIXMAP,
                    XmNlabelPixmap, GetPixmap (PACCT, NORMAL),
                    NULL);
  if ( (w = XtNameToWidget ( Graphprow, "button_1" )) != NULL)
    XtVaSetValues ( w,
                    XmNlabelType, XmPIXMAP,
                    XmNlabelPixmap, GetPixmap (PACCTS, NORMAL),
                    NULL);
  XtManageChild( Graphprow );

  /* horizontal choices */
  Graphhframe = XtVaCreateManagedWidget( "Graphhframe", 
		               xmFrameWidgetClass, Graphform,
                               XmNtopAttachment,  XmATTACH_FORM,
                               XmNleftAttachment, XmATTACH_WIDGET,
                               XmNleftWidget, Graphpframe,
		NULL);

  XtVaCreateManagedWidget("GraphType",
                          xmLabelGadgetClass, Graphhframe,
                          XmNchildType, XmFRAME_TITLE_CHILD,
                          NULL);

  Graphhrow = XmVaCreateSimpleRadioBox ( Graphhframe, "Graphhrow", 0,
                                   graph_hselect,
                                   XmVaRADIOBUTTON, NULL, NULL, NULL, NULL,
                                   XmVaRADIOBUTTON, NULL, NULL, NULL, NULL,
                                   NULL);
  if ( (Graphhbar = XtNameToWidget ( Graphhrow, "button_0" )) != NULL)
    XtVaSetValues ( Graphhbar,
                    XmNlabelType, XmPIXMAP,
                    XmNlabelPixmap, GetPixmap (PBAR, NORMAL),
                    XmNlabelInsensitivePixmap, GetPixmap (PBAR, INSENS),
                    NULL);
  if ( (Graphhline = XtNameToWidget ( Graphhrow, "button_1" )) != NULL)
    XtVaSetValues ( Graphhline,
                    XmNlabelType, XmPIXMAP,
                    XmNlabelPixmap, GetPixmap (PLINE, NORMAL),
                    NULL);
  XtManageChild( Graphhrow );
  
  /* vertical choices */
  Graphvframe = XtVaCreateManagedWidget( "Graphvframe", 
			       xmFrameWidgetClass, Graphform,
                               XmNtopAttachment,  XmATTACH_WIDGET,
                               XmNtopWidget,      Graphpframe,
                               XmNleftAttachment, XmATTACH_FORM,
			       NULL);

  XtVaCreateManagedWidget("GraphVar",
                          xmLabelGadgetClass, Graphvframe,
                          XmNchildType, XmFRAME_TITLE_CHILD,
                          NULL);

  Graphvform  = XtVaCreateWidget ( "Graphvform", xmFormWidgetClass, 
                                   Graphvframe, NULL );


{
  Widget row, form, button;
  Pixel  background;
  Pixmap legendoff, legendon;

  /* Make a scrolled window with a checkbox in it holding all variables
  ** we are willing to plot. */
  Graphvscroll = XtVaCreateManagedWidget ( "Graphvscroll", 
                                 xmScrolledWindowWidgetClass, Graphvform,
                                 XmNscrollingPolicy, XmAUTOMATIC,
                                 XmNtopAttachment, XmATTACH_FORM,
                                 XmNleftAttachment, XmATTACH_FORM,
                                 NULL);
  XtAddCallback ( Graphvscroll, 
                  XmNtraverseObscuredCallback, graphTraverse,
                  NULL );

  row = XtVaCreateWidget ( "Graphvrow", xmRowColumnWidgetClass, Graphvscroll,
                                 XmNnumColumns, NUM_PLOTS,
                                 XmNorientation, XmHORIZONTAL,
                                 XmNpacking, XmPACK_COLUMN,
                                 XmNuserData, 0,
                                 NULL );

  legendoff = XCreatePixmap ( XtDisplay(Toplevel),
                              RootWindowOfScreen( XtScreen(Toplevel) ),
                              LEGEND_WIDTH, LEGEND_HEIGHT,
                              DefaultDepthOfScreen( XtScreen(Toplevel) ) );

  /* Use this color for legend background */
  XtVaGetValues ( Graph, XmNbackground, &background, NULL);

  for (i = 0; i < NUM_PLOTS; i++) {
    char name[10];

    sprintf( name, "form_%d", i);
    form  = XtVaCreateWidget ( name, 
                               xmFormWidgetClass, row, 
                               XmNnavigationType, XmNONE,
                               NULL);

    legendon = XCreatePixmap ( XtDisplay(Toplevel),
                               RootWindowOfScreen( XtScreen(Toplevel) ),
                               LEGEND_WIDTH, LEGEND_HEIGHT,
                               DefaultDepthOfScreen( XtScreen(Toplevel) ) );


    sprintf( name, "button_%d", i);
    button = XtVaCreateManagedWidget ( name,
                          xmToggleButtonWidgetClass, form,
                          XmNindicatorType,  XmN_OF_MANY,
                          XmNlabelType, XmPIXMAP,
                          XmNlabelPixmap, legendoff,
                          XmNlabelInsensitivePixmap, legendoff,
                          XmNselectPixmap, legendon,
                          XmNuserData, i,
                          XmNleftAttachment, XmATTACH_FORM,
                          XmNtopAttachment, XmATTACH_FORM,
                          XmNbottomAttachment, XmATTACH_FORM,
             NULL );
    XtAddCallback ( button, XmNvalueChangedCallback,
                    (XtCallbackProc) graph_vselect, (XtPointer) i );

    makeLegend ( button, LEGEND_INIT, background);

    /* Label describing which plot type this toggle is for */
    sprintf( name, "label_%d", i);
    XtVaCreateManagedWidget ( name,
                          xmLabelWidgetClass, form,
                          XmNtraversalOn, False,
                          XmNleftAttachment, XmATTACH_WIDGET,
                          XmNleftWidget, button,
                          XmNtopAttachment, XmATTACH_FORM,
                          XmNbottomAttachment, XmATTACH_FORM,
                          NULL);

    XtManageChild (form);
  }

  if ( (GraphconstToggle = XtNameToWidget (row, "form_7")) )   /* constant */
     XtSetSensitive ( GraphconstToggle, False);

  XtManageChild (row);

}
  /* Make a form so that the all widgets end up the same height */
  Graphcform  = XtVaCreateWidget ( "Graphcform", 
                                  xmFormWidgetClass, Graphform, 
                                  XmNnavigationType, XmNONE,
                                  XmNsensitive,       False,
                                  XmNtopAttachment,  XmATTACH_WIDGET,
                                  XmNtopWidget,      Graphvframe,
                                  XmNleftAttachment, XmATTACH_FORM,
                NULL );

  Graphvlabel = XtVaCreateManagedWidget( "Graphvlabel", 
			        xmLabelWidgetClass, Graphcform,
                                XmNbottomAttachment, XmATTACH_FORM,
                                XmNtopAttachment,    XmATTACH_FORM,
                                XmNleftAttachment,   XmATTACH_FORM,
               NULL );

  Graphvtext1 = XtVaCreateManagedWidget ("Graphvstart", xmTextFieldWidgetClass, 
                                Graphcform,
                                XmNeditable,         True,
                                XmNtopAttachment,    XmATTACH_FORM,
                                XmNbottomAttachment, XmATTACH_FORM,
                                XmNleftAttachment,   XmATTACH_WIDGET,
                                XmNleftWidget,       Graphvlabel,
                NULL );
  XtAddCallback ( Graphvtext1, XmNactivateCallback, 
                  (XtCallbackProc) const_change, (XtPointer) 0);

  Graphvlabel = XtVaCreateManagedWidget( "Graphvlabel", 
			        xmLabelWidgetClass, Graphcform,
			        XtVaTypedArg, XmNlabelString, XmRString,
			        "@", sizeof("@"),  
                                XmNbottomAttachment, XmATTACH_FORM,
                                XmNtopAttachment,    XmATTACH_FORM,
                                XmNleftAttachment,   XmATTACH_WIDGET,
                                XmNleftWidget,       Graphvtext1,
               NULL );

  Graphvtext2 = XtVaCreateManagedWidget ("Graphvrate", xmTextFieldWidgetClass, 
                                Graphcform,
                                XmNeditable,         True,
                                XmNtopAttachment,    XmATTACH_FORM,
                                XmNbottomAttachment, XmATTACH_FORM,
                                XmNleftAttachment,   XmATTACH_WIDGET,
                                XmNleftWidget,       Graphvlabel,
               NULL );
  XtAddCallback ( Graphvtext2, XmNactivateCallback, 
                    (XtCallbackProc) const_change, (XtPointer) 0);

  XtManageChild( Graphcform );

  /* Units for the scale, weeks or transactions */
  Graphslabel = XtVaCreateManagedWidget( "Graphslabel", 
	       xmLabelWidgetClass, Graphform,
               XmNtopAttachment,     XmATTACH_OPPOSITE_WIDGET,
               XmNtopWidget,         Graphvframe,
               XmNrightAttachment,   XmATTACH_FORM,
               XmNleftAttachment,    XmATTACH_WIDGET,
               XmNleftWidget,        Graphvframe,
               NULL);

  /* The horizontal control can change the labelString */
  XtVaSetValues ( Graphhrow, XmNuserData, Graphslabel, NULL);

  Graphscale = XtVaCreateManagedWidget ("Graphscale", 
               xmScaleWidgetClass,   Graphform, 
               XmNtopAttachment,     XmATTACH_WIDGET,
               XmNtopWidget,         Graphslabel,
               XmNleftAttachment,    XmATTACH_WIDGET,
               XmNleftWidget,        Graphvframe,
               XmNrightAttachment,   XmATTACH_FORM,
               XmNbottomAttachment,  XmATTACH_OPPOSITE_WIDGET,
               XmNbottomWidget,      Graphvframe,
               NULL);
  XtAddCallback ( Graphscale, XmNvalueChangedCallback, 
                    (XtCallbackProc) graph_slide, (XtPointer) 0);
  XtSetSensitive ( Graphscale, False );


  XtManageChild( Graphvform );


  /* 
  ** The total return action area
  */
  Reportform = XtVaCreateWidget ( 
                 "Reportform", xmFormWidgetClass, Actform,
                 XmNtopAttachment,  XmATTACH_SELF,
                 XmNleftAttachment, XmATTACH_SELF,
               NULL );

  ReportInnerform = XtVaCreateWidget ( 
                 "ReportInnerform", xmFormWidgetClass, Reportform,
                 XmNtopAttachment,  XmATTACH_FORM,
                 XmNleftAttachment, XmATTACH_FORM,
               NULL );

  /* Time frame */
  Timeframe = XtVaCreateManagedWidget( 
                "Timeframe", xmFrameWidgetClass, ReportInnerform,
	      	XmNtopAttachment,   XmATTACH_FORM,
	   	XmNleftAttachment,  XmATTACH_FORM,
	      NULL );
  XtVaCreateManagedWidget("TimeLabel", xmLabelGadgetClass, Timeframe,
                          XmNchildType, XmFRAME_TITLE_CHILD, NULL);
  timebar[0].sens = GetPixmap (PDAY, NORMAL);
  timebar[1].sens = GetPixmap (PWEEK, NORMAL);
  timebar[2].sens = GetPixmap (PMONTH, NORMAL);
  timebar[3].sens = GetPixmap (PQTR, NORMAL);
  timebar[4].sens = GetPixmap (PYTD, NORMAL);
  timebar[5].sens = GetPixmap (PYEAR, NORMAL);
  timebar[6].sens = GetPixmap (PFIRST, NORMAL);
  timebar[7].sens = GetPixmap (PPICK, NORMAL);
  /* Button bar made below */


  /* Report account frame */
  Acctframe = XtVaCreateManagedWidget( 
                "Acctframe", xmFrameWidgetClass, ReportInnerform,
	      	XmNtopAttachment,  XmATTACH_WIDGET,
	      	XmNtopWidget,      Timeframe,
	   	XmNleftAttachment, XmATTACH_FORM,
	      NULL );
  XtVaCreateManagedWidget("AcctLabel", xmLabelGadgetClass, Acctframe,
                          XmNchildType, XmFRAME_TITLE_CHILD, NULL);

  /* account/accounts/portfolio */
  acctbar[0].sens = GetPixmap( PACCT, NORMAL);
  acctbar[1].sens = GetPixmap( PACCTS, NORMAL);
  acctbar[2].sens = GetPixmap( PFOLD, NORMAL);

  Acctform = makeButtonbar ( Acctframe, "Acctform", XmHORIZONTAL, 
                             acctbar, XtNumber(acctbar) );
  XtManageChild ( Acctform );

  /* Report option frame */
  Valframe = XtVaCreateManagedWidget( 
                "Valframe", xmFrameWidgetClass, ReportInnerform,
	      	XmNtopAttachment,   XmATTACH_WIDGET,
	      	XmNtopWidget,       Timeframe,
	   	XmNleftAttachment,  XmATTACH_WIDGET,
	   	XmNleftWidget,      Acctframe,
	      NULL );
  XtVaCreateManagedWidget("ValLabel", xmLabelGadgetClass, Valframe,
                          XmNchildType, XmFRAME_TITLE_CHILD, NULL);
  Valrow = XmVaCreateSimpleCheckBox ( Valframe, "Valrow", reportSetValue,
             XmVaCHECKBUTTON, NULL, NULL, NULL, NULL,
             XmVaCHECKBUTTON, NULL, NULL, NULL, NULL,
             XmVaCHECKBUTTON, NULL, NULL, NULL, NULL,
            NULL);
  XtManageChild (Valrow);

  Reportbut = XtVaCreateManagedWidget(
                          "Reportbut", xmPushButtonWidgetClass, ReportInnerform,
                          XmNlabelType,   XmPIXMAP,
                          XmNlabelPixmap, GetPixmap(PCHECK,NORMAL),
                          XmNlabelInsensitivePixmap, GetPixmap(PCHECK,INSENS),
                          XmNsensitive,   False,
                          XmNleftAttachment,  XmATTACH_POSITION,
                          XmNrightAttachment, XmATTACH_POSITION,
                          XmNtopAttachment, XmATTACH_WIDGET,
                          XmNtopWidget,     Valframe,
                          NULL);
  XtAddCallback (Reportbut, XmNactivateCallback, 
                 (XtCallbackProc) reportGenerate, (XtPointer) NULL);  

  /* next/previous screen */
  Screenbut = XtVaCreateManagedWidget (
                "Screenbut", xmPushButtonWidgetClass, Reportform,
                XmNlabelType,    XmPIXMAP,
                XmNlabelPixmap,  GetPixmap(PNEXT,NORMAL),
                XmNlabelInsensitivePixmap, GetPixmap(PNEXT,INSENS),
                XmNsensitive,    False,
                XmNtopAttachment, XmATTACH_WIDGET,
                XmNtopWidget,     ReportInnerform,
                XmNrightAttachment, XmATTACH_FORM,
                NULL);
  installToolTips (Screenbut);
  XtAddCallback (Screenbut, XmNactivateCallback, 
                 (XtCallbackProc)reportCustomTime, (XtPointer)ReportInnerform);  

  /* Moved from above so we can pass button widget */
  timebar[7].userdata = (XtPointer) Screenbut;

  Timeform = makeButtonbar (Timeframe, "Timeform", XmHORIZONTAL, 
                            timebar, XtNumber(timebar));
  XtManageChild (Timeform);

  XtVaSetValues (Acctform, XmNuserData, Reportbut, NULL);
  XtVaSetValues (Timeform, XmNuserData, Reportbut, NULL);
  
  XtManageChild (ReportInnerform);

  /********************/
    

  /* Make the portfolio action area */
  Portform = XtVaCreateWidget ( "Portform", xmFormWidgetClass, Actform, 
                               XmNtopAttachment,  XmATTACH_SELF,
                               XmNleftAttachment, XmATTACH_SELF,
                               NULL );

  /* Row column for the two button areas */
  Portselrow = XtVaCreateWidget ( "Portselrow", xmRowColumnWidgetClass, 
                                  Portform,
                                  XmNtopAttachment,  XmATTACH_FORM,
                                  XmNleftAttachment, XmATTACH_FORM,
                                  XmNrightAttachment,XmATTACH_FORM,
                                  NULL );

  /* Each button area is a frame, rowcolumn and two buttons */
  /* Frame and form for single account buttons */
  Portframe = XtVaCreateManagedWidget( "Portframe", xmFrameWidgetClass,
                                       Portselrow,
                                       NULL);
 
  XtVaCreateManagedWidget("PortAccount",
                          xmLabelGadgetClass, Portframe,
                          XmNchildType, XmFRAME_TITLE_CHILD,
                          NULL);

  Portbutform = XtVaCreateWidget ( "Portbutform", xmFormWidgetClass, 
                                   Portframe,
                                   NULL );

  /* + or - for current account */
  Portpbut =  XtVaCreateManagedWidget ( "Add", 
                       xmPushButtonWidgetClass, Portbutform, 
                       XmNlabelType,            XmPIXMAP,
                       XmNlabelPixmap,          GetPixmap(PCHECK, NORMAL),
                       XmNlabelInsensitivePixmap,  GetPixmap(PCHECK, INSENS),
                       XmNsensitive,            True,
                       XmNtopAttachment,        XmATTACH_FORM,
                       XmNleftAttachment,       XmATTACH_POSITION,
                       XmNrightAttachment,      XmATTACH_POSITION,
                       NULL );
  XtAddCallback ( Portpbut, XmNactivateCallback, 
                 (XtCallbackProc) portAddSubList, (XtPointer) 0);  

  Portmbut =  XtVaCreateManagedWidget ( "Remove", 
                       xmPushButtonWidgetClass, Portbutform, 
                       XmNlabelType,            XmPIXMAP,
                       XmNlabelPixmap,          GetPixmap(PX, NORMAL),
                       XmNlabelInsensitivePixmap,  GetPixmap(PX, INSENS),
                       XmNsensitive,            False,
                       XmNtopAttachment,        XmATTACH_FORM,
                       XmNleftAttachment,       XmATTACH_POSITION,
                       XmNrightAttachment,      XmATTACH_POSITION,
                       NULL );
  XtAddCallback ( Portmbut, XmNactivateCallback, 
                 (XtCallbackProc) portAddSubList, (XtPointer) 1);  

  XtManageChild ( Portbutform );


  /* Frame and form for all accounts buttons */
  Portframe = XtVaCreateManagedWidget( "Portframe", xmFrameWidgetClass,
                                       Portselrow,
                                       NULL);
 
  XtVaCreateManagedWidget("PortAccounts",
                          xmLabelGadgetClass, Portframe,
                          XmNchildType, XmFRAME_TITLE_CHILD,
                          NULL);

  Portbutform = XtVaCreateWidget ( "Portbutform", xmFormWidgetClass, 
                                   Portframe,
                                   NULL );

  Portabut = XtVaCreateManagedWidget ( "AddAll", 
                       xmPushButtonWidgetClass, Portbutform, 
                       XmNsensitive,            True,
                       XmNlabelType,            XmPIXMAP,
                       XmNlabelPixmap,          GetPixmap(PCHECK, NORMAL),
                       XmNlabelInsensitivePixmap,  GetPixmap(PCHECK, INSENS),
                       XmNtopAttachment,        XmATTACH_FORM,
                       XmNleftAttachment,       XmATTACH_POSITION,
                       XmNrightAttachment,      XmATTACH_POSITION,
                       NULL );
  XtAddCallback ( Portabut, XmNactivateCallback, 
                 (XtCallbackProc) portAddSubList, (XtPointer) 2);  

  Portcbut =  XtVaCreateManagedWidget ( "Clear", 
                       xmPushButtonWidgetClass, Portbutform, 
                       XmNsensitive,            True,
                       XmNlabelType,            XmPIXMAP,
                       XmNlabelPixmap,          GetPixmap(PX, NORMAL),
                       XmNlabelInsensitivePixmap,  GetPixmap(PX, INSENS),
                       XmNtopAttachment,        XmATTACH_FORM,
                       XmNleftAttachment,       XmATTACH_POSITION,
                       XmNrightAttachment,      XmATTACH_POSITION,
                       NULL );
  XtAddCallback ( Portcbut, XmNactivateCallback, 
                 (XtCallbackProc) portAddSubList, (XtPointer) 3);  

  XtManageChild ( Portbutform );
  XtManageChild ( Portselrow );


  /* Now create the list areas */
  Portscrform = XtVaCreateWidget ( "Portscrform", xmFormWidgetClass, Portform, 
                                 XmNtopAttachment,    XmATTACH_WIDGET,
                                 XmNtopWidget,        Portselrow,
                                 XmNleftAttachment,   XmATTACH_FORM,
                                 XmNrightAttachment,  XmATTACH_FORM,
                                 XmNbottomAttachment, XmATTACH_FORM,
                                 NULL );

  Portpercentlabel = XtVaCreateManagedWidget( "Portpercentlabel", 
			        xmLabelWidgetClass, Portscrform,
                                XmNtopAttachment,    XmATTACH_FORM,
                                XmNleftAttachment,   XmATTACH_FORM,
                                XmNrightAttachment,  XmATTACH_FORM,
                                NULL );
  
  /* Create percentage list window */
  n = 0;
  XtSetArg(args[n], XmNselectionPolicy,  XmSINGLE_SELECT); n++;
  XtSetArg(args[n], XmNtopAttachment,    XmATTACH_WIDGET); n++;
  XtSetArg(args[n], XmNtopWidget,        Portpercentlabel); n++;
  XtSetArg(args[n], XmNleftAttachment,   XmATTACH_FORM); n++;
  XtSetArg(args[n], XmNrightAttachment,  XmATTACH_FORM); n++;
  Portpercentlist = XmCreateScrolledList ( Portscrform, "Portpercentlist", 
                                           args, n);

  XtAddCallback ( Portpercentlist, XmNdefaultActionCallback, 
                  (XtCallbackProc) portTraverse, NULL);

  { /* Set list width, because it will not be resizable later */
    XmFontList      fontlist;
    XmFontContext   context;
    XmFontListEntry entry;
    XmFontType fonttype = XmFONT_IS_FONT;
    XFontStruct *font = NULL;

    XtVaGetValues ( Portpercentlist, XmNfontList, &fontlist, NULL);

    if (fontlist) {
      if ( XmFontListInitFontContext ( &context, fontlist) ) {
        entry = XmFontListNextEntry ( context );
        if (entry)
           font = (XFontStruct *) XmFontListEntryGetFont ( entry, &fonttype );
        XmFontListFreeFontContext ( context );
      }
    }

    if (font)
      XtVaSetValues ( Portpercentlist,
                      XmNwidth, 35*font->max_bounds.width,
                      NULL);
  }

  XtManageChild ( Portpercentlist );

  /* Option menu for moving around the percentage tree */
  for (i=0; i < XtNumber(portoptmenu); i++)
    Temp[i] = XmStringCreateLocalized( portoptmenu[i] );

  Portoptmenu = XmVaCreateSimpleOptionMenu( Portscrform, "Portoptmenu", 
                     Temp[0], (KeySym)'P' , 0, (XtCallbackProc) portTraverse,
                     XmVaPUSHBUTTON, Temp[1], 'R', NULL, NULL,
                     XmVaPUSHBUTTON, Temp[2], 'F', NULL, NULL,
                     XmVaPUSHBUTTON, Temp[3], 'A', NULL, NULL,
                     XmNtopAttachment,        XmATTACH_WIDGET,
                     XmNtopWidget,            Portpercentlist,
                     XmNleftAttachment,       XmATTACH_FORM,
                     NULL);
  XtManageChild ( Portoptmenu );
  XtSetSensitive ( Portoptmenu, False );

  for (i=0; i < XtNumber(portoptmenu); i++)
    XmStringFree ( Temp[i] );


  Portvaluelabel = XtVaCreateManagedWidget( "Portvaluelabel", 
			        xmLabelWidgetClass, Portscrform,
			        XtVaTypedArg, XmNlabelString, XmRString,
			          "Total Portfolio Value: $0", 
                                  sizeof( "Total Portfolio Value: $0" ),  
                                XmNtopAttachment,    XmATTACH_WIDGET,
                                XmNtopWidget,        Portoptmenu,
                                XmNleftAttachment,   XmATTACH_FORM,
                                XmNrightAttachment,  XmATTACH_FORM,
                                XmNbottomAttachment, XmATTACH_FORM,
                                NULL );
  XtManageChild ( Portscrform );
  
  /********************/
    
  /* Make the calculator action area */
  Calcform = XtVaCreateWidget ( "Calcform", xmFormWidgetClass, Actform,
                                 XmNtopAttachment,        XmATTACH_SELF,
                                 XmNleftAttachment,       XmATTACH_SELF,
                                 NULL );

  for (i=0; i < XtNumber(calcfuncs); i++)
    Temp[i] = XmStringCreateLocalized( calcfuncs[i] );

  Calcoptmenu = XmVaCreateSimpleOptionMenu( Calcform, "Calcoptmenu", 
                     Temp[0], (KeySym)'u' , 0, (XtCallbackProc) calcFuncSel,
                     XmVaPUSHBUTTON, Temp[1], 'F', NULL, NULL,
                     XmVaPUSHBUTTON, Temp[2], 'u', NULL, NULL,
                     XmVaPUSHBUTTON, Temp[3], 'P', NULL, NULL,
                     XmVaPUSHBUTTON, Temp[4], 'e', NULL, NULL,
                     XmVaPUSHBUTTON, Temp[5], 'R', NULL, NULL,
                     XmNtopAttachment,    XmATTACH_FORM,
                     XmNleftAttachment,   XmATTACH_FORM,
                     NULL);
  XtManageChild ( Calcoptmenu );

  for (i=0; i < XtNumber(calcfuncs); i++)
    XmStringFree ( Temp[i] );

  Calcframe = XtVaCreateManagedWidget( "Calcframe", xmFrameWidgetClass,
                                Calcform,
                                XmNtopAttachment,    XmATTACH_WIDGET,
                                XmNtopWidget,        Calcoptmenu,
                                XmNleftAttachment,   XmATTACH_FORM,
                                NULL);

  Calclabel = XtVaCreateManagedWidget( "Calclabel", 
			        xmLabelWidgetClass,  Calcframe,
                                XmNlabelType,        XmPIXMAP,
                                XmNlabelPixmap,      GetPixmap(PFV, NORMAL),
                                NULL );
  

  /* Option menu for selecting the function  */
  Calcrow = XtVaCreateWidget ( "Calcrow", xmRowColumnWidgetClass, Calcform,
                               XmNnavigationType, XmNONE,
                               XmNtopAttachment,  XmATTACH_WIDGET,
                               XmNtopWidget,      Calcframe,
                               XmNleftAttachment, XmATTACH_FORM,
                               NULL );

  /* There are only ever four parameters, we'll switch labels as we need to */
  for (i=0; i < 4; i++) {
    Calclabels[i] = XtVaCreateManagedWidget ( calcparams[i],
                              xmLabelGadgetClass, Calcrow,
                              NULL );
    if ( i < 3 ) {
      /* Text widgets for number entry */
      Calctext[i] = XtVaCreateManagedWidget ( calcparams[i], 
                                xmTextFieldWidgetClass, Calcrow,
                                XmNnavigationType, XmNONE,
                                NULL );
      XtAddCallback ( Calctext[i], XmNactivateCallback, 
                      (XtCallbackProc) calcTextSel, (XtPointer) i ); 
    } else {
      /* Option menu for selecting compounding */
      for (i=0; i < XtNumber(calccompnd); i++)
        Temp[i] = XmStringCreateLocalized( calccompnd[i] );

      Calccmpdmenu = XmVaCreateSimpleOptionMenu( Calcrow, "Calccmpdmenu", 
                         Temp[0], (KeySym)0 , 2, (XtCallbackProc) calcCompSel,
                         XmVaPUSHBUTTON, Temp[1], 'D', NULL, NULL,
                         XmVaPUSHBUTTON, Temp[2], 'W', NULL, NULL,
                         XmVaPUSHBUTTON, Temp[3], 'M', NULL, NULL,
                         XmVaPUSHBUTTON, Temp[4], 'Q', NULL, NULL,
                         XmVaPUSHBUTTON, Temp[5], 'B', NULL, NULL,
                         XmVaPUSHBUTTON, Temp[6], 'A', NULL, NULL,
                         XmVaPUSHBUTTON, Temp[7], '1', NULL, NULL,
                         XmVaPUSHBUTTON, Temp[8], '3', NULL, NULL,
                         XmNbottomAttachment,     XmATTACH_OPPOSITE_WIDGET,
                         XmNbottomWidget,         Calcrow,
                         XmNleftAttachment,       XmATTACH_WIDGET,
                         XmNleftWidget,           Calcrow,
                      NULL);
      XtManageChild ( Calccmpdmenu );

      for (i=0; i < XtNumber(calccompnd); i++)
        XmStringFree ( Temp[i] );
    }
  }
  XtManageChild ( Calcrow );

  /* Get a default of the first function. Side effect is setting labels, text
  ** and result string */
  calcFuncSel ( NULL, 0, NULL ); 

  /********************/

  /* Button area form */
  /* Point to pixmaps we want */
  toolbar[0].sens = GetPixmap( PABOUT, NORMAL);
  toolbar[1].sens = GetPixmap( PEDIT, NORMAL);
  toolbar[1].insens = GetPixmap( PEDIT, INSENS);
  toolbar[2].sens = GetPixmap( PRETURN, NORMAL);
  toolbar[2].insens = GetPixmap( PRETURN, INSENS);
  toolbar[3].sens = GetPixmap( PGRAPH, NORMAL);
  toolbar[3].insens = GetPixmap( PGRAPH, INSENS);
  toolbar[4].sens = GetPixmap( PPORTFOLIO, NORMAL);
  toolbar[4].insens = GetPixmap( PPORTFOLIO, INSENS);
  toolbar[5].sens = GetPixmap( PCALC, NORMAL);
  toolbar[6].sens = GetPixmap( PSTOP, NORMAL);

  toolbar[1].userdata = (XtPointer) XtNameToWidget (Toolmenu, "button_1");
  toolbar[2].userdata = (XtPointer) XtNameToWidget (Toolmenu, "button_2");
  toolbar[3].userdata = (XtPointer) XtNameToWidget (Toolmenu, "button_3");
  toolbar[4].userdata = (XtPointer) XtNameToWidget (Toolmenu, "button_4");

  /* Make the button bar */
  Butform = makeButtonbar ( Workform, "Butform", XmVERTICAL, 
                            toolbar, XtNumber(toolbar) );

  /* Add some constraints */
  XtVaSetValues ( Butform,
                  XmNtopAttachment,    XmATTACH_FORM,
                  XmNrightAttachment,  XmATTACH_FORM,
                  XmNbottomAttachment, XmATTACH_FORM,
                  NULL); 

  /* Temporary setting of globals.  Should be replaced by macro or access 
     routine */
  Editbut = XtNameToWidget (Butform, "Edit");
  Gainbut = XtNameToWidget (Butform, "Gain");
  Graphbut = XtNameToWidget (Butform, "Graphbut");
  Portbut = XtNameToWidget (Butform, "Portfolio");

#if XmVERSION > 1
  /* Now we have a Butform, attach Midform to it */
  XtVaSetValues ( HPane, XmNrightAttachment,  XmATTACH_WIDGET,
                         XmNrightWidget,      Butform,
                  NULL );
#else
  /* Now we have a Butform, attach Midform to it */
  XtVaSetValues ( Midform, XmNrightAttachment,  XmATTACH_WIDGET,
                           XmNrightWidget,      Butform,
                  NULL );
#endif
  /**********************/


  /* Manage children and off we go */
  XtManageChild( Butform ); 
  XtManageChild( Fileform ); 

  XtVaSetValues (Reportform, XmNmappedWhenManaged, False, NULL );
  XtManageChild (Reportform );
  XtVaSetValues (Reportform, XmNresizable, False, NULL );

  XtVaSetValues ( Graphform, XmNmappedWhenManaged, False, NULL );
  XtManageChild( Graphform );

  XtVaSetValues ( Portform, XmNmappedWhenManaged, False, NULL );
  XtManageChild( Portform );

  XtVaSetValues ( Calcform, XmNmappedWhenManaged, False, NULL );
  XtManageChild( Calcform );

  XtManageChild( Actform ); 
  XtManageChild( Midform );
  XtManageChild( VPane );
#if XmVERSION > 1
  XtManageChild( HPane );
#endif
  XtManageChild( Workform );
  XtManageChild( Mainform );

  XtRealizeWidget ( Toplevel );

  /* Stop midform from resizing */
  XtVaSetValues (Midform, XmNresizable, False, NULL );

  /* Set Margins of children so they are centered in Actform */
  /* Toplevel must be realized for this to work */
  CenterWidget (Actform, Infoframe);
  CenterWidget (Actform, Reportform);
  CenterWidget (Actform, Graphform);
  CenterWidget (Actform, Portform);
  CenterWidget (Actform, Calcform);

  if (appdata.restore)
    restoreState (appdata.restore);

  /* Communication with Xquote */
  remoteInit (Toplevel);

  /* 
  ** If any options left, they must be data files beginning with -f. 
  ** There may be more than one -f option and each can have zero or more
  ** filenames [-f filename(s) -f filename(s) ...].
  */
  if (argc > 1)
    processFileOption ( argc, argv );

  XtAppMainLoop ( app );
}


/* ARGSUSED */
void toolCB ( Widget w, XtPointer client_data, XtPointer call_data)
{
  int which = (int) client_data;
  static int last = 0;

  if (which != 6) {  /* Don't ignore/unmap if it's the quit action */
    if (which == last)
      return;
    last = which;

    XtSetMappedWhenManaged (Reportform, False);
    XtSetMappedWhenManaged (Graphform, False);
    XtSetMappedWhenManaged (Portform,  False);
    XtSetMappedWhenManaged (Calcform,  False);
    XtSetMappedWhenManaged (Infoframe, False);
  }

  switch ( which ) {

  case 2:   /* Return report */
            setLastGraph(NOTHING);
            XtSetMappedWhenManaged ( Reportform, True );
            break;
  case 3:   /* Graph */
            setLastGraph(PLOT);
            XtSetMappedWhenManaged ( Graphform, True );
            break;
  case 4:   /* Portfolio */
            setLastGraph(PORTFOLIO);
            XtSetMappedWhenManaged ( Portform, True );
            break;
  case 5:   /* Financial Calculator */
            setLastGraph(FUNCCALC);
            XtSetMappedWhenManaged ( Calcform, True );
            break;
  case 6:   /* Quit */
            fileCB (w, (XtPointer)5, (XtPointer)NULL);
            break;

  case 0:   /* About */
  case 1:   /* Edit */
  default:
            setLastGraph(NOTHING);
            XtSetMappedWhenManaged ( Infoframe, True );
            break;
  } 

  /* make new menu the default. */
  {
    extern Widget Toolmenu;
    WidgetList buttons;
    int numbuttons, i;

    XtVaGetValues ( Toolmenu, 
                    XmNchildren, &buttons, 
                    XmNnumChildren, &numbuttons, 
                    NULL );

    /* Don't touch the option buttons. This works because there is a separator 
    ** in the menu before the option buttons. */
    for (i = 0; i < numbuttons; i++)
      XmToggleButtonSetState ( buttons[i], (which == i)?True:False, False);

    /* If plot is on, turn on plot options */
    graphSense( which==3 );
     
  }

  /* sigh */
  drawDrawingArea();
}
