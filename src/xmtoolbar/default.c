/*********************************************************************
 * all for the default button 
 * the new created buttons are using some callacks in this file
 *********************************************************************/

#include "toolbar.h"
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

Widget pixmap_dialog, label, popup_frame;
Pixmap button_pixmap, file_pixmap;
  
extern void new_popup_callback();
extern void app_callback();
extern void preferences_dialog();
extern void group_popup_callback();
extern void group_callback();

void pixmap_selection(Widget);

extern int childs_open;

char TOOLBAR_CONFIG_FILE[255];
int START_X;
int START_Y;

/* this function tests if the right button (button 3) is pressed */
/* if so then a popup menu is raised */
void button_event( Widget button, XtPointer client_data, XEvent *event)
{
    Widget popup = (Widget) client_data;
    XButtonPressedEvent *bevent = (XButtonPressedEvent *) event;

    if(bevent->button != 3)
 	return;

    XmMenuPosition(popup, bevent);
    XtManageChild(popup);

}

/* this function shows the description of the button when entering the
   window */
void show_help(Widget button, XtPointer client_data, XEvent
		   *event)
{
  Widget label;
  Position x, y, top_x, top_y;
  Dimension bx, by, height, width;
  int off_x, off_y;
  XmString string;

  XEnterWindowEvent *cevent = (XEnterWindowEvent *) event;
  if(cevent->type != EnterNotify)
    return;


  if(popup_frame)
    return;



  toolbar = start;
  while(toolbar->button != button)
    toolbar = toolbar->next;

  if(!toolbar)
    return; 

  /* bring the window to the top of the window stack if stay on top is
     set */
  if(stay_on_top)
    XRaiseWindow(XtDisplay(XtParent(XtParent(button))),
		 XtWindow(XtParent(XtParent(button))));

  /* if quick info is disabled or the current button is the default
     button return */
  if((!quick_info) || (toolbar == start))
    return;

  string = XmStringCreateLocalized(toolbar->description);
  /* get the position and the dimension of the button to calculate the
     position of the help window */
  
  popup_frame =  XtVaCreatePopupShell("help_popup",
				      topLevelShellWidgetClass,
				      rowcol,
				      XmNoverrideRedirect, 
				      True,
				      XmNresizePolicy,
				      XmRESIZE_ANY,
				      XmNx,
				      -500,
				      XmNy,
				      -500,
				      NULL);

  label = XtVaCreateManagedWidget("label",
				  xmLabelWidgetClass,
				  popup_frame,
				  XmNlabelString,
				  string,
				  NULL);
  XtManageChild(popup_frame);
  XtVaGetValues(popup_frame,
		XmNheight,
		&height,
		XmNwidth,
		&width,
		NULL);
  XtUnmanageChild(popup_frame);

  if(toolbar->type != SUB)
    XtVaGetValues(toplevel,
		  XmNx,
		  &top_x,
		  XmNy,
		  &top_y,
		  NULL);
  else
    XtVaGetValues(XtParent(toolbar->rowcol),
		  XmNx,
		  &top_x,
		  XmNy,
		  &top_y,
		  NULL);


  XtVaGetValues(button,
		XmNx,
		&x,
		XmNy,
		&y,
		XmNheight,
		&by,
		XmNwidth,
		&bx,
		NULL);

  if(toolbar->type != SUB)
    {
      if(orientation == XmHORIZONTAL)
	{
	  if(top_y > (screen_height/2))
	    off_y = (-1) * height;
	  else
	    off_y = by;	
	  off_x = 0;
      }
      else
      {
	if(top_x > (screen_width/2))
	  off_x = (-1)*(width);
	else
	  off_x = bx;
	off_y = 0;
      }
    }
  else
    {
      if(orientation == XmVERTICAL)
	{
	  if(top_y > (screen_height/2))
	    off_y = (-1) * height;
	  else
	    off_y = by;	
	  off_x = 0;
      }
      else
      {
	if(top_x > (screen_width/2))
	  off_x = (-1)*(width);
	else
	  off_x = bx;
	off_y = 0;
      }
    }
 
  XtVaSetValues(popup_frame,
		XmNx,
		top_x + x + off_x,
		XmNy,
		top_y + y + off_y,
		NULL);
  


  XmStringFree(string);

  XtManageChild(popup_frame);

}

void delete_help(Widget button, XtPointer client_data, XEvent
		   *event)
{
  XLeaveWindowEvent *cevent = (XLeaveWindowEvent *) event;
  if(cevent->type != LeaveNotify)
    return;


  if(!popup_frame)
    return;



  toolbar = start;
  while(toolbar->button != button)
    toolbar = toolbar->next;
  if(!toolbar)
    return;  

  if((!quick_info) || (toolbar == start))
    return;
  XtDestroyWidget(popup_frame);
  popup_frame = NULL;

}

/* add a new button to the toolbar and set its popup-menu */
Widget add_new_button(Widget parent, Widget button, int button_type)
{
    Widget new_button, popup, row_col, group;
    Pixmap pixmap;
    int pos;
    Position x_pos, y_pos;
    tool_button *help, *help_2;
    Position x, y;
    int layout;
    int counter = 1;

    pixmap = xmtGetPixmapByDepth(screen, def_pix, 0, 0, screen_depth); /* andre */

    toolbar = start;

    if(button_type != SUB)
      while(toolbar->next != NULL) 
	{
	  if(toolbar->type != SUB)
	    counter++;
	  toolbar = toolbar->next;
	}
    else
      while(toolbar->button != button)
	toolbar = toolbar->next;
	

       
    if (pixmap == XmUNSPECIFIED_PIXMAP)
	fprintf(stderr, "pixmap read failed\n" );
    else
	{
	    help = malloc(sizeof(tool_button));
	    if(!help)
		{
		    fprintf(stderr,"not enough memory - go and buy some ;-)\n");
		    exit(1);
		}

	    if(button_type == SUB)
	      {
		help_2 = toolbar->next;
		toolbar->next = help;
		toolbar = help;
		toolbar->next = help_2;
	      }
	    else
	      {
		toolbar->next = help;
		toolbar = help;
		toolbar->next = NULL;
	      }
	    strcpy(toolbar->pixmap_path, def_pix);
	    toolbar->app_path[0] = '\0';
	    toolbar->parameters[0] = '\0';
	    
	    expand = 0;

	    if(button_type != SUB)
	      button_num++;
	    
	    pos = XmLAST_POSITION;

	    if((position == TOPRIGHT) && (orientation == XmHORIZONTAL))
		pos = 0;
	    if(position == BOTTOMRIGHT)
		pos = 0;
	    if((position == BOTTOMLEFT) && (orientation == XmVERTICAL))
		pos = 0;
	    if((position == CUSTOM) && (orientation == XmHORIZONTAL)
	       && (START_X > (screen_width/2)))
	       pos = 0;

	    new_button = XtVaCreateManagedWidget("Button",
						 xmPushButtonWidgetClass,
						 parent,
						 XmNlabelType, 
						 XmPIXMAP, 
						 XmNlabelPixmap,
						 pixmap, 
						 XmNpositionIndex,
						 pos,
						 NULL);
	    toolbar->button = new_button;
	    if(button_type != GROUP)
	      toolbar->rowcol = parent;
	    if((button_type == NORMAL) || (button_type == SUB))
	      {
		popup = XmVaCreateSimplePopupMenu(new_button, "test", new_popup_callback,
						  XmVaTITLE,
						  XmStringCreateLocalized("Button Menu"),
						  XmVaDOUBLE_SEPARATOR,
						  XmVaPUSHBUTTON,
						  XmStringCreateLocalized("Pixmap"), 
						  'P',
						  NULL, NULL,
						  XmVaPUSHBUTTON,
						  XmStringCreateLocalized("Application"), 
						  'A',
						  NULL, NULL,
						  XmVaPUSHBUTTON,
						  XmStringCreateLocalized("App Parameters"), 
						  'r',
						  NULL, NULL,
						  XmVaPUSHBUTTON,
						  XmStringCreateLocalized("Enter Description"), 
						  'E',
						  NULL, NULL,
						  XmVaSEPARATOR,
						  XmVaPUSHBUTTON,
						  XmStringCreateLocalized("Delete Button"),
						  'D',
						  NULL, NULL,
						  NULL);
		strcpy(toolbar->description, "No description");
	      }
	    else
	      {
		popup = XmVaCreateSimplePopupMenu(new_button, "test", group_popup_callback,
						  XmVaTITLE,
						  XmStringCreateLocalized("Group Menu"),
						  XmVaDOUBLE_SEPARATOR,
						  XmVaPUSHBUTTON,
						  XmStringCreateLocalized("Pixmap"), 
						  'P',
						  NULL, NULL,
						  XmVaPUSHBUTTON,
						  XmStringCreateLocalized("Add Button"), 
						  'A',
						  NULL, NULL,
						  XmVaPUSHBUTTON,
						  XmStringCreateLocalized("Enter Description"), 
						  'E',
						  NULL, NULL,
						  XmVaSEPARATOR,
						  XmVaPUSHBUTTON,
						  XmStringCreateLocalized("Delete Group"),
						  'D',
						  NULL, NULL,
						  NULL);
		XtVaGetValues(toplevel,
			      XmNx,
			      &x,
			      XmNy,
			      &y,
			      NULL);
		
		if(orientation == XmHORIZONTAL)
		  {
		    x += counter * button_x;
		    y += button_y;
		    layout = XmVERTICAL;
		  }
		else
		  {
		    x += button_x;
		    y += counter * button_y; 
		    layout = XmHORIZONTAL;
		  }
		
		group = XtVaCreatePopupShell("Popup",
					     topLevelShellWidgetClass,
					     rowcol,
					     XmNoverrideRedirect, 
					     True,
					     XmNx,
					     x,
					     XmNy,
					     y,
					     XmNallowShellResize,
					     True,
					     XmNresizePolicy,
					     XmRESIZE_ANY,
					     NULL);
  
		row_col = XtVaCreateManagedWidget("Rowcol",
						  xmRowColumnWidgetClass,
						  group,
						  XmNorientation,
						  layout,
						  XmNpacking,
						  XmPACK_COLUMN,
						  XmNentryAlignment,
						  XmALIGNMENT_CENTER,
						  XmNrowColumnType, 
						  XmMENU_BAR, 
						  XmNshadowThickness,
						  0,
						  XmNmarginHeight,
						  0,
						  XmNmarginWidth,
						  0,
						  NULL);

		toolbar->rowcol = row_col;
		strcpy(toolbar->description, "Group Button");
	      }

	    XtAddEventHandler(new_button, ButtonPressMask, False, (XtEventHandler)button_event, popup);    
	    XtManageChild(new_button);
	    
	    if(button_type != SUB)
	      { 
		XtVaGetValues(new_button, 
			      XmNheight,
			      &button_y,
			      XmNwidth,
			      &button_x,
			      NULL);
	      }


	    XtVaGetValues(toplevel, 
			  XmNx, 
			  &x_pos, 
			  XmNy, 
			  &y_pos, 
			  XmNwidth,
			  &window_width,
			  XmNheight,
			  &window_height,
			  NULL);
	    
	    
	    if(((y_pos+window_height) > screen_height)&&(orientation==XmVERTICAL)) 
	      y_pos = screen_height-window_height; 

	    if(((x_pos+window_width) > screen_width)&&(orientation==XmHORIZONTAL)) 
	      x_pos = screen_width-window_width;   
	    
	    
	      
	      
	    if((position == CUSTOM) && (orientation == XmHORIZONTAL)
	       && (START_X > (screen_width/2)))
	      XtVaSetValues(toplevel,  
			    XmNx,
			    START_X - window_width + button_x,
			    XmNy,
			    y_pos,
			    NULL );
	    else
	      XtVaSetValues(toplevel,  
			    XmNx,
			    x_pos,
			    XmNy,
			    y_pos,
			    NULL );

	    if(XtIsManaged(XtParent(toolbar->rowcol)))
	       {
		 Dimension b_height, b_width;
		 Position sub_x, sub_y;

		 XtVaGetValues(new_button,
			       XmNwidth,
			       &b_width,
			       XmNheight,
			       &b_height,
			       NULL);

		 XtVaGetValues(XtParent(toolbar->rowcol),
			       XmNx,
			       &sub_x,
			       XmNy,
			       &sub_y,
			       NULL);

		 if(((position == TOPRIGHT) && (orientation == XmVERTICAL)) ||
		    ((position == BOTTOMRIGHT) && (orientation == XmVERTICAL)))
		   XtVaSetValues(XtParent(toolbar->rowcol),
				 XmNx,
				 sub_x - b_width,
				 XmNy,
				 sub_y,
				 NULL);
		 if(((position == BOTTOMLEFT) && (orientation == XmHORIZONTAL)) ||
		    ((position == BOTTOMRIGHT) && (orientation ==
						   XmHORIZONTAL)))
		   XtVaSetValues(XtParent(toolbar->rowcol),
				 XmNx,
				 sub_x,
				 XmNy,
				 sub_y - b_height,
				 NULL);

	       }

	    if(button_type != GROUP)
	      XtAddCallback(new_button, XmNactivateCallback,
			    app_callback, NULL);
	    else
	      XtAddCallback(new_button, XmNactivateCallback,
			    group_callback, NULL);
	    
	    XtAddEventHandler(new_button, EnterWindowMask, False,
			     (XtEventHandler) show_help, NULL);

	    XtAddEventHandler(new_button, LeaveWindowMask, False,
			     (XtEventHandler) delete_help, NULL);

	    toolbar->type = button_type;
	    
	    return new_button;
	}
    return NULL;
}

void select_file(Widget file, XtPointer client_data, XtPointer
		      call_data)
{
    char *string;

    XmListCallbackStruct *cbs = (XmListCallbackStruct *) call_data;
    XmStringGetLtoR(cbs->item, XmFONTLIST_DEFAULT_TAG,
		    &string);

    XmDestroyPixmap(screen, file_pixmap);
    file_pixmap = xmtGetPixmapByDepth(screen, string, 0, 0, screen_depth); /* andre */
    if (file_pixmap == XmUNSPECIFIED_PIXMAP)
	file_pixmap = xmtGetPixmapByDepth(screen, def_pix, 0, 0, screen_depth); /* andre */
    XtVaSetValues(label, XmNlabelPixmap, file_pixmap, NULL );

    XmUpdateDisplay(label);
    XtFree(string);
}

void file_ok(Widget file, XtPointer client_data, XtPointer
		      call_data)
{
    char *filename;
    Widget button;
    Position x_pos, y_pos;

    XmFileSelectionBoxCallbackStruct *cbs =
	(XmFileSelectionBoxCallbackStruct *) call_data;

    childs_open--;

    if(!XmStringGetLtoR(cbs->value, XmFONTLIST_DEFAULT_TAG, &filename))
	return;

    button = XtParent(XtParent(file));
    
    XtVaGetValues(button, 
		  XmNlabelPixmap,
		  &button_pixmap,
		  NULL);

    XmDestroyPixmap(screen, button_pixmap);
    button_pixmap = xmtGetPixmapByDepth(screen, filename, 0, 0, screen_depth); /* andre */

    if(button_pixmap == XmUNSPECIFIED_PIXMAP)
      button_pixmap = xmtGetPixmapByDepth(screen, def_pix, 0, 0, screen_depth); /* andre */
    if(button_pixmap == XmUNSPECIFIED_PIXMAP)
      return;

    toolbar = start;
    while(toolbar != NULL)
      {
	if((toolbar->type == GROUP) &&
	   (XtIsManaged(XtParent(toolbar->rowcol))))
	  XtUnmanageChild(XtParent(toolbar->rowcol));
	toolbar = toolbar->next;
      }

    toolbar = start;
    while(toolbar->button != button)
	toolbar=toolbar->next;

    if(toolbar)
      strcpy(toolbar->pixmap_path, filename);
	
    XtVaSetValues(button,  
		  XmNlabelType, 
		  XmPIXMAP,
		  XmNlabelPixmap,  
		  button_pixmap,
		  NULL);
    
    if(toolbar->type != SUB)
      {
	XtVaGetValues(toplevel, 
		      XmNx, 
		      &x_pos, 
		      XmNy, 
		      &y_pos,
		      XmNwidth,
		      &window_width,
		      XmNheight,
		      &window_height,
		      NULL);
	
	if((y_pos+window_height) > screen_height) 
	  y_pos = screen_height-window_height; 
	if((x_pos+window_width) > screen_width) 
	  x_pos = screen_width-window_width;


	XtVaSetValues(toplevel,
		      XmNx,
		      x_pos,
		      XmNy,
		      y_pos,
		      NULL );
	
	XtVaGetValues(button,
		      XmNwidth,
		      &button_x,
		      XmNheight,
		      &button_y,
		      NULL);
      }

    XtDestroyWidget(file);
    
    XtFree(filename);
    pixmap_dialog = NULL;
}

void file_cancel(Widget file, XtPointer client_data, XtPointer
		      call_data)
{
  childs_open--;
  
  XtDestroyWidget(file);
  pixmap_dialog = NULL;
}


void pixmap_selection(Widget button)
{

    Widget dir = NULL;
    Widget frame, parent;
    Pixmap file_pixmap;
    char path[MAXPATH];
    int counter;

    if(pixmap_dialog)
      return;

    childs_open++;

    toolbar = start;
    while(toolbar->button != button)
	toolbar = toolbar->next;
    
    strcpy(path, toolbar->pixmap_path);
    counter = strlen(path);

    if(toolbar->pixmap_path[0] == '\0')
      parent = toplevel;
    else
      parent = button;

    while((path[counter--] != '/') && (counter > 0));
    path[++counter] = '\0';



    pixmap_dialog = XmCreateFileSelectionDialog( parent,
						 "Pixmaps",
						 NULL,
						 0);


    XtVaSetValues(pixmap_dialog,
		  XmNdialogStyle,
		  XmDIALOG_PRIMARY_APPLICATION_MODAL,
		  XmNpattern,
		  XmStringCreateLocalized("*.xpm"),
		  XmNdirectory,
		  XmStringCreateLocalized(path),
		  XmNdialogTitle,
		  XmStringCreateLocalized("Pixmap Selection"),
		  XmNresizePolicy,
		  XmRESIZE_ANY,
		  NULL);
    
    file_pixmap = xmtGetPixmapByDepth(screen, def_pix, 0, 0, screen_depth); /* andre */
    if (file_pixmap == XmUNSPECIFIED_PIXMAP)
      fprintf(stderr, "file: pixmap read failed\n" );
    frame = XtVaCreateManagedWidget( "Frame",
				     xmFrameWidgetClass,
				     pixmap_dialog,
				     NULL);
    
    label = XtVaCreateManagedWidget("Label_Pixmap",
				    xmLabelWidgetClass,
				    frame,
				    XmNlabelType,
				    XmPIXMAP,
				    XmNlabelPixmap,
				    file_pixmap,
				    NULL);
    
	    
    dir = XtNameToWidget(pixmap_dialog, "*ItemsList");
    
    if(dir == NULL) 
      fprintf(stderr, "cannot find ItemList\n" ); 
    
    XtAddCallback(pixmap_dialog, XmNcancelCallback, 
		  file_cancel, NULL); 
    XtAddCallback(pixmap_dialog, XmNokCallback, file_ok, NULL); 
    XtAddCallback(dir, XmNbrowseSelectionCallback, select_file, NULL); 

    XtManageChild(pixmap_dialog);
}

void save_config(Widget parent)
{
    tool_button *button;
    int config;
    char filename[MAXPATH];
    Widget message_box;
    int counter = 0;

    strcpy(filename, getenv("HOME"));
    strcat(filename, "/.toolbar");

    if((config = open(filename, O_WRONLY | O_CREAT | O_TRUNC, S_IRUSR | S_IWUSR)) < 0)
	{

	    message_box = XmCreateInformationDialog(parent,
						    "Message_Box",
						    NULL,
						    0);
	    XtVaSetValues(message_box, 
			  XmNmessageString,
			  XmStringCreateLocalized("Sorry, I am not able to serve you master" ),
			  XmNdialogTitle,
			  XmStringCreateLocalized("OOps!"),
			  NULL);

	    XtUnmanageChild(XmMessageBoxGetChild(message_box,
						 XmDIALOG_CANCEL_BUTTON));
	    XtUnmanageChild(XmMessageBoxGetChild(message_box, XmDIALOG_HELP_BUTTON));
	    XtManageChild(message_box);	
	}
    else
      {
	counter = 0;
	button = start;
	while(button != NULL)
	  {
	    counter++;
	    button = button->next;
	  }
	write(config,(char *) &counter, sizeof(int));
	button = start;
	while((counter > 0) && (button != NULL))
	  {
	    if(write(config, button, sizeof(tool_button)) !=
	       sizeof(tool_button))
	      {
		
		message_box = XmCreateInformationDialog(parent,
							"Message_Box",
							NULL,
							0);
		XtVaSetValues(message_box, 
			      XmNmessageString,
			      XmStringCreateLocalized("Write failed"),
			      XmNdialogTitle,
			      XmStringCreateLocalized("OOps!"),
			      NULL);
		
		XtUnmanageChild(XmMessageBoxGetChild(message_box,
						     XmDIALOG_CANCEL_BUTTON));
		XtUnmanageChild(XmMessageBoxGetChild(message_box, XmDIALOG_HELP_BUTTON));
		XtManageChild(message_box);   	
	      }
	    button = button->next;
	    counter--;
	  }
	if(write(config,(char *) &orientation, sizeof(int)) !=
	   sizeof(int))
	  fprintf(stderr, "write failed\n");
	write(config,(char *) &position, sizeof(int));
	write(config,(char *) &save_on_exit, sizeof(int));
	write(config,(char *) &stay_on_top, sizeof(int));
	write(config,(char *) &quick_info, sizeof(int));
        write(config,(char *) &START_X, sizeof(int));
        write(config,(char *) &START_Y, sizeof(int));
	write(config,(char *) &auto_hide, sizeof(int));
	close(config);
      }
    
}


void export_it(Widget parent, XtPointer client_data, XtPointer
		     call_data)
{
  char *filename;
  tool_button *help;
  FILE *fd;
  Widget message_box;

  XmFileSelectionBoxCallbackStruct *cbs =
    (XmFileSelectionBoxCallbackStruct *) call_data;

  childs_open--;

  if(!XmStringGetLtoR(cbs->value, XmFONTLIST_DEFAULT_TAG, &filename))
    return;

  if((fd = fopen(filename, "w+")) == NULL)
    {
      message_box = XmCreateInformationDialog(parent,
					      "Message_Box",
					      NULL,
					      0);
      XtVaSetValues(message_box, 
		    XmNmessageString,
		    XmStringCreateLocalized("Sorry, I am not able to serve you master" ),
		    XmNdialogTitle,
		    XmStringCreateLocalized("OOps!"),
		    NULL);
      
      XtUnmanageChild(XmMessageBoxGetChild(message_box,
					   XmDIALOG_CANCEL_BUTTON));
      XtUnmanageChild(XmMessageBoxGetChild(message_box, XmDIALOG_HELP_BUTTON));
      XtManageChild(message_box);	
    }
  else
    {
      help = start;
      while(help != NULL)
	{
	  fprintf(fd, "%s\n", help->description);
	  fprintf(fd, "%s\n", help->pixmap_path);
	  fprintf(fd, "%s\n", help->app_path);
	  fprintf(fd, "%s\n", help->parameters);
	  fprintf(fd, "---------------------------------\n");
	  help = help->next;
	}
      
      fclose(fd);
    }

  XtDestroyWidget(parent);
  XtFree(filename);

}

void export_cancel(Widget parent, XtPointer client_data, XtPointer
		     call_data)
{
  childs_open--;
  XtDestroyWidget(parent);
}

/* export writes all data of the structure into a given file */
void export(Widget parent)
{
  Widget file;
  char path[MAXPATH];

  childs_open++;

  file = XmCreateFileSelectionDialog(parent,
				     "P",
				     NULL,
				     0);

  strcpy(path, getenv("HOME"));

  XtVaSetValues(file,
		XmNdialogStyle,
		XmDIALOG_PRIMARY_APPLICATION_MODAL,
		XmNpattern,
		XmStringCreateLocalized("*"),
		XmNdirectory,
		XmStringCreateLocalized(path),
		XmNdialogTitle,
		XmStringCreateLocalized("Export"),
		XmNresizePolicy,
		XmRESIZE_ANY,
		NULL);
  
  XtAddCallback(file, XmNcancelCallback, export_cancel, NULL);
  XtAddCallback(file, XmNokCallback, export_it, NULL);
  XtManageChild(file);

}    
/* this function manages the popup at the default button */
void popup_callback(Widget button, XtPointer client_data, XtPointer
		     call_data)
{
    int selected = (int) client_data;
    Widget default_button, message_box;
    XmString message;

    default_button = XtParent(XtParent(XtParent(button)));

    switch(selected)
	{
	case 0:
	  /* add a new button to the toolbar. */
	  /* maybe this could be done more elegant */
	  add_new_button(XtParent(XtParent(XtParent(XtParent(button)))), default_button, NORMAL); 
	  break;
	case 1:
	  /* add a group */
	  add_new_button(XtParent(XtParent(XtParent(XtParent(button)))), default_button, GROUP);
	  break;
	case 2:
	  /* select the pixmap for the default button */
	  pixmap_selection(default_button); 
	  break;
	case 3:
	  /* save the current configuration to ~/.toolbar */
	  save_config(default_button);
	  break;
	case 4:
	  /* change the layout of the application and update the */
	  /* label on the button */
	  preferences_dialog();
	  break;
	case 5:
	  export(default_button);
	  break;
	case 6:
	  /* open the information dialog */
	  message_box = XmCreateInformationDialog(default_button,
						  "Message_Box",
						  NULL,
						  0);

	  message = XmStringLtoRCreate("Toolbar 1.2 by Thomas Zwettler\nSend bugreports or comments to\nspiff@calvin.priv.at", XmSTRING_DEFAULT_CHARSET);
	  XtVaSetValues(message_box,  
			XmNdialogStyle,
			XmDIALOG_PRIMARY_APPLICATION_MODAL,
			XmNmessageString,
			message,
			XmNdialogTitle,
			XmStringCreateLocalized("Information"),
			XmNx,
			screen_width/2,
			XmNy,
			screen_height/2,
			XmNmessageAlignment,
			XmALIGNMENT_CENTER,
			NULL);

	  XtUnmanageChild(XmMessageBoxGetChild(message_box,
					       XmDIALOG_CANCEL_BUTTON));
	  XtUnmanageChild(XmMessageBoxGetChild(message_box,
					       XmDIALOG_HELP_BUTTON));
	  XmStringFree(message);
	  XtManageChild(message_box);
	  break;
	case 7:
	  /* no help at the moment */
	  message_box = XmCreateInformationDialog(default_button,
						  "Message_Box",
						  NULL,
						  0);

	  message = XmStringLtoRCreate("Sorry, no help available at the moment", 
				       XmSTRING_DEFAULT_CHARSET);

	  XtVaSetValues(message_box, 
			XmNdialogStyle,
			XmDIALOG_PRIMARY_APPLICATION_MODAL,
			XmNmessageString,
			message,
			XmNdialogTitle,
			XmStringCreateLocalized("Help"),
			XmNx,
			screen_width/2,
			XmNy,
			screen_height/2,
			NULL);

	  XtUnmanageChild(XmMessageBoxGetChild(message_box,
					       XmDIALOG_CANCEL_BUTTON));
	  XtUnmanageChild(XmMessageBoxGetChild(message_box,
					       XmDIALOG_HELP_BUTTON));
	  XmStringFree(message);
	  XtManageChild(message_box);
	  break;
	case 8:
	  /* exit the program */
	  
	  if(save_on_exit)
	    save_config(default_button);
	  
	  fprintf(stdout, "Goodbye!\n");
	  exit(0);
	}
    
}






