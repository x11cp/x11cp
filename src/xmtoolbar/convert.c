#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#include <signal.h>
#include <Xm/Xm.h>

typedef struct new {
  Widget button;
  Widget rowcol;
  int type;
  char pixmap_path[255];
  int width;
  int height;
  char app_path[255];
  char parameters[255];
  char description[255];
  struct new *next;
}new;

typedef struct old {
  Widget button;
  Widget rowcol;
  int type;
  char pixmap_path[255];
  int width;
  int height;
  char app_path[255];
  char parameters[255];
  struct old *next;
}old;

void convert(char *old_file, char *new_file)
{
    struct stat file;
    old old;
    new new;
    int old_fd, new_fd;
    int ok = 0, counter = 0;
    int orientation, stay_on_top, position, save_on_exit;
    int x, y, quick_info;

    if(stat(old_file, &file) && (errno==ENOENT))
      {
	fprintf(stderr, "convert: can't stat old config file\n");
	exit(1);
      }

    if((old_fd = open(old_file, O_RDONLY)) < 0)
	{
	  fprintf(stderr, "convert: error opening old config file\n");
	  exit(1);
	}

    if((new_fd = open(new_file, O_CREAT | O_WRONLY | O_RDONLY, S_IRUSR
		      | S_IWUSR | S_IRGRP | S_IROTH)) < 0)
	{
	  fprintf(stderr, "convert: error opening new config file\n");
	  exit(1);
	}

    if(read(old_fd, (char *)&counter, sizeof(int)) != sizeof(int))
      {
	fprintf(stderr, "convert: read counter failed\n");
	exit(1);
      }

    if(write(new_fd, (char *) &counter, sizeof(int)) != sizeof(int))
      {
	fprintf(stderr, "convert: write counter failed\n");
	exit(1);
      }     

    while(counter > 0)
      {
	if(read(old_fd, (char *)&old, sizeof(old)) !=
	   sizeof(old))
	  fprintf(stderr,"convert: read old structure failed\n");
	new.button = old.button;
	new.rowcol = old.rowcol;
	new.type = old.type;
	strcpy(new.pixmap_path, old.pixmap_path);
	new.width = old.width;
	new.height = old.height;
	strcpy(new.parameters, old.parameters);
	strcpy(new.app_path, old.app_path);
	strcpy(new.description, "No description");
	new.next = NULL;
	if(write(new_fd, &new, sizeof(new)) != sizeof(new))
	  fprintf(stderr, "convert: write struct failed\n");
	counter--;
      }
    
    if(read(old_fd, (char *)&orientation, sizeof(int)) != sizeof(int))
      fprintf(stderr, "read orientation failed\n");
    if(read(old_fd, (char *)&position, sizeof(int)) != sizeof(int))
      fprintf(stderr, "read position failed\n");
    if(read(old_fd, (char *)&save_on_exit, sizeof(int)) != sizeof(int))
      fprintf(stderr, "read save failed\n");
    if(read(old_fd, (char *)&stay_on_top, sizeof(int)) != sizeof(int))
      fprintf(stderr, "read stay failed\n");
    if(read(old_fd, (char *)&quick_info, sizeof(int)) != sizeof(int))
      fprintf(stderr, "read quick info failed\n");


    if(write(new_fd,(char *) &orientation, sizeof(int)) != sizeof(int))
      fprintf(stderr, "write failed\n");

    write(new_fd,(char *) &position, sizeof(int));
    write(new_fd,(char *) &save_on_exit, sizeof(int));
    write(new_fd,(char *) &stay_on_top, sizeof(int));
    write(new_fd,(char *) &quick_info, sizeof(int));
    write(new_fd,(char *) &x, sizeof(int));
    write(new_fd,(char *) &y, sizeof(int));

    close(old_fd);
    close(new_fd);
}

int main(int argc, char *argv[])
{

  if(argc != 3)
    {
      fprintf(stderr, "convert <old_config> <new_config>\n");
      exit(1);
    }
  convert(argv[1], argv[2]);
  return 0;
}

