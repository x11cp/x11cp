/*****************************************************************************
 * XedwForm is a modified version of the Athena Widget Form.
 * Added capabilities :
 *  1) Children may chose not to be resized in any particular plane.
 *  2) Children may chose to extend their widths to the edge of the application.
 *  3) Children may have linked widths and heights.
 * The default is that these capabilities are turned off.
 * See XedwForm.h for resource list.
 *
 * Edward Groenendaal, 20th Feb 1991.
 *
 * Changes to the original source :
 *      1) Changed all references of Form to XedwForm and form to xedwForm.
 *      2) Added resources
 *      3) Rewrote Resize(), Layout() and LayoutChild().
 *****************************************************************************/

#include <X11/IntrinsicP.h>
#include <X11/StringDefs.h>
#include <X11/Xmu/Converters.h>
#include <X11/Xmu/CharSet.h>
#include <X11/Xaw/XawInit.h>
#include "XedwFormP.h"

/* Private Definitions */

static int default_value = -99999;

#define Offset(field) XtOffset(XedwFormWidget, xedwForm.field)
static XtResource resources[] = {
    {XtNdefaultDistance, XtCThickness, XtRInt, sizeof(int),
        Offset(default_spacing), XtRImmediate, (caddr_t)4}
};
#undef Offset

static XtEdgeType defEdge = XtRubber;

#define Offset(field) XtOffset(XedwFormConstraints, xedwForm.field)
static XtResource xedwFormConstraintResources[] = {
  {XtNtop, XtCEdge, XtREdgeType, sizeof(XtEdgeType),
     Offset(top), XtREdgeType, (XtPointer)&defEdge},
  {XtNbottom, XtCEdge, XtREdgeType, sizeof(XtEdgeType),
     Offset(bottom), XtREdgeType, (XtPointer)&defEdge},
  {XtNleft, XtCEdge, XtREdgeType, sizeof(XtEdgeType),
     Offset(left), XtREdgeType, (XtPointer)&defEdge},
  {XtNright, XtCEdge, XtREdgeType, sizeof(XtEdgeType),
     Offset(right), XtREdgeType, (XtPointer)&defEdge},
  {XtNhorizDistance, XtCThickness, XtRInt, sizeof(int),
     Offset(dx), XtRInt, (XtPointer) &default_value},
  {XtNfromHoriz, XtCWidget, XtRWidget, sizeof(Widget),
     Offset(horiz_base), XtRWidget, (XtPointer)NULL},
  {XtNvertDistance, XtCThickness, XtRInt, sizeof(int),
     Offset(dy), XtRInt, (XtPointer) &default_value},
  {XtNfromVert, XtCWidget, XtRWidget, sizeof(Widget),
     Offset(vert_base), XtRWidget, (XtPointer)NULL},
  {XtNrubberWidth, XtCBoolean, XtRBoolean, sizeof(Boolean),    /* new */
     Offset(rubber_width), XtRImmediate, (XtPointer) True},
  {XtNrubberHeight, XtCBoolean, XtRBoolean, sizeof(Boolean),   /* new */
     Offset(rubber_height), XtRImmediate, (XtPointer) True},
  {XtNfullWidth, XtCBoolean, XtRBoolean, sizeof(Boolean),      /* new */
     Offset(full_width), XtRImmediate, (XtPointer) False},
  {XtNfullHeight, XtCBoolean, XtRBoolean, sizeof(Boolean),     /* new */
     Offset(full_height), XtRImmediate, (XtPointer) False},
  {XtNwidthLinked, XtCWidget, XtRWidget, sizeof(Widget),
     Offset(width_link), XtRWidget, (XtPointer)NULL},         /* new */
  {XtNheightLinked, XtCWidget, XtRWidget, sizeof(Widget),
     Offset(height_link), XtRWidget, (XtPointer)NULL},        /* new */
  {XtNresizable, XtCBoolean, XtRBoolean, sizeof(Boolean),
     Offset(allow_resize), XtRImmediate, (XtPointer) FALSE},
};
#undef Offset


#if NeedFunctionPrototypes
static void ClassInitialize(void);
static void ClassPartInitialize(WidgetClass);
static void Initialize(Widget, Widget, ArgList, Cardinal *);
static void Resize(Widget);
static void ConstraintInitialize(Widget, Widget, ArgList, Cardinal *);
static Boolean SetValues(Widget, Widget, Widget, ArgList, Cardinal *);
static Boolean ConstraintSetValues(Widget, Widget, Widget, ArgList, Cardinal *);
static XtGeometryResult GeometryManager(Widget, XtWidgetGeometry *, XtWidgetGeometry *);
static XtGeometryResult PreferredGeometry(Widget, XtWidgetGeometry *, XtWidgetGeometry *);
static void ChangeManaged(Widget);
static Boolean Layout(XedwFormWidget, Dimension, Dimension);
static void LayoutChild(Widget);
void XedwFormDoLayout(Widget, Boolean);
#else
static void ClassInitialize(), ClassPartInitialize(), Initialize(), Resize();
static void ConstraintInitialize();
static Boolean SetValues(), ConstraintSetValues();
static XtGeometryResult GeometryManager(), PreferredGeometry();
static void ChangeManaged();
static Boolean Layout();
static void LayoutChild();
void XedwFormDoLayout();
#endif

XedwFormClassRec xedwFormClassRec = {
  { /* core_class fields */
    /* superclass         */    (WidgetClass) &constraintClassRec,
    /* class_name         */    "XedwForm",
    /* widget_size        */    sizeof(XedwFormRec),
    /* class_initialize   */    ClassInitialize,
    /* class_part_init    */    ClassPartInitialize,
    /* class_inited       */    FALSE,
    /* initialize         */    Initialize,
    /* initialize_hook    */    NULL,
    /* realize            */    XtInheritRealize,
    /* actions            */    NULL,
    /* num_actions        */    0,
    /* resources          */    resources,
    /* num_resources      */    XtNumber(resources),
    /* xrm_class          */    NULLQUARK,
    /* compress_motion    */    TRUE,
    /* compress_exposure  */    TRUE,
    /* compress_enterleave*/    TRUE,
    /* visible_interest   */    FALSE,
    /* destroy            */    NULL,
    /* resize             */    Resize,                      /* new */
    /* expose             */    XtInheritExpose,
    /* set_values         */    SetValues,
    /* set_values_hook    */    NULL,
    /* set_values_almost  */    XtInheritSetValuesAlmost,
    /* get_values_hook    */    NULL,
    /* accept_focus       */    NULL,
    /* version            */    XtVersion,
    /* callback_private   */    NULL,
    /* tm_table           */    NULL,
    /* query_geometry     */    PreferredGeometry,
    /* display_accelerator*/    XtInheritDisplayAccelerator,
    /* extension          */    NULL
  },
  { /* composite_class fields */
    /* geometry_manager   */   GeometryManager,
    /* change_managed     */   ChangeManaged,
    /* insert_child       */   XtInheritInsertChild,
    /* delete_child       */   XtInheritDeleteChild,
    /* extension          */   NULL
  },
  { /* constraint_class fields */
    /* subresourses       */   xedwFormConstraintResources,
    /* subresource_count  */   XtNumber(xedwFormConstraintResources),
    /* constraint_size    */   sizeof(XedwFormConstraintsRec),
    /* initialize         */   ConstraintInitialize,
    /* destroy            */   NULL,
    /* set_values         */   ConstraintSetValues,
    /* extension          */   NULL
  },
  { /* xedwForm_class fields */
    /* layout             */   Layout
  }
};

WidgetClass xedwFormWidgetClass = (WidgetClass)&xedwFormClassRec;

/****************************************************************
 *
 * Private Procedures
 *
 ****************************************************************/


static XrmQuark XtQChainLeft, XtQChainRight, XtQChainTop,
  XtQChainBottom, XtQRubber;

#define done(address, type) \
{ toVal->size = sizeof(type); \
  toVal->addr = (caddr_t) address; \
  return; \
}

#if NeedFunctionPrototypes
static void _CvtStringToEdgeType(XrmValuePtr args, Cardinal *num_args, XrmValuePtr fromVal, XrmValuePtr toVal)
#else
static void _CvtStringToEdgeType(args, num_args, fromVal, toVal)
XrmValuePtr args;
Cardinal *num_args;
XrmValuePtr fromVal, toVal;
#endif
{
  static XtEdgeType edgeType;
  XrmQuark q;
  char lowerName[1000];

  XmuCopyISOLatin1Lowered (lowerName, (char*)fromVal->addr);
  q = XrmStringToQuark(lowerName);
  if (q == XtQChainLeft) {
    edgeType = XtChainLeft;
    done(&edgeType, XtEdgeType);
  }
  if (q == XtQChainRight) {
    edgeType = XtChainRight;
    done(&edgeType, XtEdgeType);
  }
  if (q == XtQChainTop) {
    edgeType = XtChainTop;
    done(&edgeType, XtEdgeType);
  }
  if (q == XtQChainBottom) {
    edgeType = XtChainBottom;
    done(&edgeType, XtEdgeType);
  }
  if (q == XtQRubber) {
    edgeType = XtRubber;
    done(&edgeType, XtEdgeType);
  }
  XtStringConversionWarning(fromVal->addr, "edgeType");
  toVal->addr = NULL;
  toVal->size = 0;
}

#if NeedFunctionPrototypes
static void ClassInitialize(void)
#else
static void ClassInitialize()
#endif
{
  static XtConvertArgRec parentCvtArgs[] = {
    {XtBaseOffset, (caddr_t)XtOffset(Widget, core.parent), sizeof(Widget)}
  };
  XawInitializeWidgetSet();
  XtQChainLeft   = XrmStringToQuark("chainleft");
  XtQChainRight  = XrmStringToQuark("chainright");
  XtQChainTop    = XrmStringToQuark("chaintop");
  XtQChainBottom = XrmStringToQuark("chainbottom");
  XtQRubber      = XrmStringToQuark("rubber");
  
  XtAddConverter( XtRString, XtREdgeType, _CvtStringToEdgeType, NULL, 0 );
  XtAddConverter( XtRString, XtRWidget, XmuCvtStringToWidget,
		 parentCvtArgs, XtNumber(parentCvtArgs) );
}

#if NeedFunctionPrototypes
static void ClassPartInitialize(WidgetClass class)
#else
static void ClassPartInitialize(class)
WidgetClass class;
#endif
{
  register XedwFormWidgetClass c = (XedwFormWidgetClass)class;

  if (((Boolean (*)())c->xedwForm_class.layout) ==  XtInheritLayout)
    c->xedwForm_class.layout = Layout;
}


#if NeedFunctionPrototypes
static void Initialize(Widget request, Widget new, ArgList args, Cardinal *num_args)
#else
static void Initialize(request, new)
Widget request, new;
ArgList args;
Cardinal *num_args;
#endif
{
  XedwFormWidget fw = (XedwFormWidget)new;

  fw->xedwForm.old_width = fw->core.width;
  fw->xedwForm.old_height = fw->core.height;
  fw->xedwForm.no_refigure = False;
  fw->xedwForm.needs_relayout = False;
  fw->xedwForm.resize_in_layout = True;
}


#if NeedFunctionPrototypes
static void RefigureLocations(XedwFormWidget w)
#else
static void RefigureLocations(w)
XedwFormWidget w;
#endif
{
  if (w->xedwForm.no_refigure) {
    w->xedwForm.needs_relayout = True;
  } else {
    (*((XedwFormWidgetClass)w->core.widget_class)->xedwForm_class.layout)
      ( w, w->core.width, w->core.height );
    w->xedwForm.needs_relayout = False;
  }
}

#if NeedFunctionPrototypes
static Boolean Layout(XedwFormWidget fw, Dimension width, Dimension height)
#else
static Boolean Layout(fw, width, height)
XedwFormWidget fw;
Dimension width, height;
#endif
{
  int num_children = fw->composite.num_children;
  WidgetList children = fw->composite.children;
  Widget *childP;
  Position maxx, maxy;
  Boolean ret_val;
  
  for (childP = children; childP - children < num_children; childP++) {
    XedwFormConstraints xedwForm =
      (XedwFormConstraints)(*childP)->core.constraints;
    xedwForm->xedwForm.layout_state = LayoutPending;
  }

  maxx = maxy = 1;
  for (childP = children; childP - children < num_children; childP++) {
    if (XtIsManaged(*childP)) {
      Position x, y;
      LayoutChild(*childP);
      x = (*childP)->core.x + (*childP)->core.width
	+ ((*childP)->core.border_width << 1);
      y = (*childP)->core.y + (*childP)->core.height
	+ ((*childP)->core.border_width << 1);
      if (maxx < x) maxx = x;  /* Track width  */
      if (maxy < y) maxy = y;  /* Track Height */
    }
  }

  /* I want to be this big */
  fw->xedwForm.preferred_width = (maxx += fw->xedwForm.default_spacing);
  fw->xedwForm.preferred_height = (maxy += fw->xedwForm.default_spacing);

  /* Go back and check to see if any widgets want to be full height or
   * full width, if so, do it, dont recalculate sizes, assume the programmer
   * didn't make any silly mistakes like putting another widget to the right
   * of a full width one. EG.
   * Should really do this in one pass. 
   */

  for (childP = children; childP - children < num_children; childP++) {
    XedwFormConstraints form = (XedwFormConstraints)(*childP)->core.constraints;
    Position newwidth  = (Position)(*childP)->core.width;
    Position newheight = (Position)(*childP)->core.height;
    if (form->xedwForm.full_width == True)
      form->xedwForm.virtual_width = newwidth = 
	(maxx - ((form->xedwForm.dx) + 
		 ((*childP)->core.border_width * 2) + 
		 ((*childP)->core.x)));
    if (form->xedwForm.full_height == True)
      form->xedwForm.virtual_height = newheight = 
	(maxy - ((form->xedwForm.dy) + 
		 ((*childP)->core.border_width * 2) +
		 ((*childP)->core.y)));
    /* Resize the widget if it has been changed. XtResizeWidget does the checking */
    XtResizeWidget(*childP, newwidth, newheight, (*childP)->core.border_width);
  }
  
  if (fw->xedwForm.resize_in_layout &&
      (maxx != (Position)fw->core.width || maxy != (Position)fw->core.height))
  {
    XtGeometryResult result;
    result = XtMakeResizeRequest((Widget)fw, (Dimension)maxx, (Dimension)maxy,
				 (Dimension*)&maxx, (Dimension*)&maxy );
    if (result == XtGeometryAlmost)
      result = XtMakeResizeRequest((Widget)fw, (Dimension)maxx, (Dimension)maxy,
				   NULL, NULL );
    fw->xedwForm.old_width  = fw->core.width;
    fw->xedwForm.old_height = fw->core.height;
    ret_val = (result == XtGeometryYes);
  } else ret_val = False;
  
  return ret_val;
}


#if NeedFunctionPrototypes
static void LayoutChild(Widget w)
#else
static void LayoutChild(w)
Widget w;
#endif
{
  XedwFormConstraints xedwForm = (XedwFormConstraints)w->core.constraints;
  Position x, y;
  Dimension width, height;
  Widget ref;

  switch (xedwForm->xedwForm.layout_state) {
    
  case LayoutPending:
    xedwForm->xedwForm.layout_state = LayoutInProgress;
    break;

  case LayoutDone:
    return;

  case LayoutInProgress:
    {
      String subs[2];
      Cardinal num_subs = 2;
      subs[0] = w->core.name;
      subs[1] = w->core.parent->core.name;
      XtAppWarningMsg(XtWidgetToApplicationContext(w),
		      "constraintLoop","XedwFormLayout","XawToolkitError",
		      "constraint loop detected while laying out child '%s' in XedwFormWidget '%s'",
		      subs, &num_subs);
      return;
    }
  }
  x = xedwForm->xedwForm.dx;
  y = xedwForm->xedwForm.dy;
  if ((ref = xedwForm->xedwForm.width_link) != (Widget)NULL) {
    LayoutChild(ref);
    width = xedwForm->xedwForm.virtual_width = ref->core.width;
    XtResizeWidget(w, width, w->core.height, w->core.border_width);
  }
  if ((ref = xedwForm->xedwForm.height_link) != (Widget)NULL) {
    LayoutChild(ref);
    height = xedwForm->xedwForm.virtual_height = ref->core.height;
    XtResizeWidget(w, w->core.width, height, w->core.border_width);
  }
  if ((ref = xedwForm->xedwForm.horiz_base) != (Widget)NULL) {
    LayoutChild(ref);
    x += ref->core.x + ref->core.width + (ref->core.border_width << 1);
  }
  if ((ref = xedwForm->xedwForm.vert_base) != (Widget)NULL) {
    LayoutChild(ref);
    y += ref->core.y + ref->core.height + (ref->core.border_width << 1);
  }
  XtMoveWidget( w, x, y );
  xedwForm->xedwForm.layout_state = LayoutDone;
}


#if NeedFunctionPrototypes
static Position TransformCoord(Position loc, Dimension old, Dimension new, XawEdgeType type)
#else
static Position TransformCoord(loc, old, new, type)
Position loc;
Dimension old,new;
XtEdgeType type;
#endif
{
  if (type == XtRubber) {
    if ( ((int) old) > 0)
      loc = (loc * new) / old;
  }
  else if (type == XtChainBottom || type == XtChainRight)
    loc += (Position)new - (Position)old;

  /* I don't see any problem with returning values less than zero. */

  return (loc);
}


#if NeedFunctionPrototypes
static void Resize(Widget w)
#else
static void Resize(w)
Widget w;
#endif
{
  XedwFormWidget fw = (XedwFormWidget)w;
  WidgetList children = fw->composite.children;
  int num_children = fw->composite.num_children;
  Widget *childP;
  Position x, y;
  Dimension width, height;

  for (childP = children; childP - children < num_children; childP++) {
    XedwFormConstraints xedwForm = (XedwFormConstraints)(*childP)->core.constraints;
    if (!XtIsManaged(*childP)) continue;
    x = TransformCoord( (*childP)->core.x, fw->xedwForm.old_width,
		       fw->core.width, xedwForm->xedwForm.left );
    y = TransformCoord( (*childP)->core.y, fw->xedwForm.old_height,
		       fw->core.height, xedwForm->xedwForm.top );
    
    if (xedwForm->xedwForm.rubber_width)
      xedwForm->xedwForm.virtual_width =
	TransformCoord((Position)((*childP)->core.x
				  + xedwForm->xedwForm.virtual_width
				  + 2 * (*childP)->core.border_width),
		       fw->xedwForm.old_width, fw->core.width,
		       xedwForm->xedwForm.right )
	  - (x + 2 * (*childP)->core.border_width);

    if (xedwForm->xedwForm.rubber_height)
      xedwForm->xedwForm.virtual_height =
	TransformCoord((Position)((*childP)->core.y
				  + xedwForm->xedwForm.virtual_height
				  + 2 * (*childP)->core.border_width),
		       fw->xedwForm.old_height, fw->core.height,
		       xedwForm->xedwForm.bottom )
	  - ( y + 2 * (*childP)->core.border_width);
    
    width = (Dimension)
      (xedwForm->xedwForm.virtual_width < 1) ? 1 : xedwForm->xedwForm.virtual_width;
    height = (Dimension)
      (xedwForm->xedwForm.virtual_height < 1) ? 1 : xedwForm->xedwForm.virtual_height;
    
    XtConfigureWidget( *childP, x, y, (Dimension)width, (Dimension)height,
		      (*childP)->core.border_width );
  }

  fw->xedwForm.old_width = fw->core.width;
  fw->xedwForm.old_height = fw->core.height;
}


#if NeedFunctionPrototypes
static XtGeometryResult GeometryManager(Widget w, XtWidgetGeometry *request, XtWidgetGeometry *reply)
#else
static XtGeometryResult GeometryManager(w, request, reply)
Widget w;
XtWidgetGeometry *request, *reply;
#endif
{
  XedwFormConstraints xedwForm = (XedwFormConstraints)w->core.constraints;
  XtWidgetGeometry allowed;

  if ((request->request_mode & ~(XtCWQueryOnly | CWWidth | CWHeight)) ||
      !xedwForm->xedwForm.allow_resize)
    return XtGeometryNo;

  if (request->request_mode & CWWidth)
    allowed.width = request->width;
  else
    allowed.width = w->core.width;
  
  if (request->request_mode & CWHeight)
    allowed.height = request->height;
  else
    allowed.height = w->core.height;

  if (allowed.width == w->core.width && allowed.height == w->core.height)
    return XtGeometryNo;

  if (!(request->request_mode & XtCWQueryOnly)) {
    /* reset virtual width and height. */
    xedwForm->xedwForm.virtual_width = w->core.width = allowed.width;
    xedwForm->xedwForm.virtual_height = w->core.height = allowed.height;
    RefigureLocations( (XedwFormWidget)w->core.parent );
  }
  return XtGeometryYes;
}



#if NeedFunctionPrototypes
static Boolean SetValues(Widget current, Widget request, Widget new, ArgList args, Cardinal *num_args)
#else
static Boolean SetValues(current, request, new)
Widget current, request, new;
ArgList args;
Cardinal *num_args;
#endif
{
  return( FALSE );
}


#if NeedFunctionPrototypes
static void ConstraintInitialize(Widget request, Widget new, ArgList args, Cardinal *num_args)
#else
static void ConstraintInitialize(request, new)
Widget request, new;
ArgList args;
Cardinal *num_args;
#endif
{
  XedwFormConstraints xedwForm = (XedwFormConstraints)new->core.constraints;
  XedwFormWidget fw = (XedwFormWidget)new->core.parent;

  xedwForm->xedwForm.virtual_width = (int) new->core.width;
  xedwForm->xedwForm.virtual_height = (int) new->core.height;

  if (xedwForm->xedwForm.dx == default_value)
    xedwForm->xedwForm.dx = fw->xedwForm.default_spacing;

  if (xedwForm->xedwForm.dy == default_value)
    xedwForm->xedwForm.dy = fw->xedwForm.default_spacing;
}

#if NeedFunctionPrototypes
static Boolean ConstraintSetValues(Widget current, Widget request, Widget new, ArgList args, Cardinal *num_args)
#else
static Boolean ConstraintSetValues(current, request, new)
Widget current, request, new;
ArgList args;
Cardinal *num_args;
#endif
{
  return( FALSE );
}

#if NeedFunctionPrototypes
static void ChangeManaged(Widget w)
#else
static void ChangeManaged(w)
Widget w;
#endif
{
  XedwFormWidget fw = (XedwFormWidget)w;
  XedwFormConstraints xedwForm;
  WidgetList children, childP;
  int num_children = fw->composite.num_children;
  Widget child;

  /*
   * Reset virtual width and height for all children.
   */

  for (children = childP = fw->composite.children ;
       childP - children < num_children; childP++) {
    child = *childP;
    if (XtIsManaged(child)) {
      xedwForm = (XedwFormConstraints)child->core.constraints;
      if ( child->core.width != 1)
        xedwForm->xedwForm.virtual_width = (int) child->core.width;
      if ( child->core.height != 1)
        xedwForm->xedwForm.virtual_height = (int) child->core.height;
    }
  }
  RefigureLocations( (XedwFormWidget)w );
}


#if NeedFunctionPrototypes
static XtGeometryResult PreferredGeometry(Widget widget, XtWidgetGeometry *request, XtWidgetGeometry *reply)
#else
static XtGeometryResult PreferredGeometry(widget, request, reply)
Widget widget;
XtWidgetGeometry *request, *reply;
#endif
{
  XedwFormWidget w = (XedwFormWidget)widget;

  reply->width = w->xedwForm.preferred_width;
  reply->height = w->xedwForm.preferred_height;
  reply->request_mode = CWWidth | CWHeight;
  if (((request->request_mode & (CWWidth | CWHeight)) ==
      (reply->request_mode & (CWWidth | CWHeight)))
      && (request->width == reply->width)
      && (request->height == reply->height))
    return XtGeometryYes;
  else if ((reply->width == w->core.width) && (reply->height == w->core.height))
    return XtGeometryNo;
  else
    return XtGeometryAlmost;
}


/**********************************************************************
 *
 * Public routines
 *
 **********************************************************************/

/*
 * Set or reset figuring (ignored if not realized)
 */

#if NeedFunctionPrototypes
void XedwFormDoLayout(Widget w, Boolean doit)
#else
void XedwFormDoLayout(w, doit)
Widget w;
Boolean doit;
#endif
{
  register XedwFormWidget fw = (XedwFormWidget)w;

  fw->xedwForm.no_refigure = !doit;

  if ( XtIsRealized(w) && fw->xedwForm.needs_relayout )
    RefigureLocations( fw );
}
