/*
 * Author:  Chris D. Peterson, MIT X Consortium
 * Modified: Edward Groenendaal.
 */


/* 
 * XedwListP.h - Private definitions for XedwList widget
 * 
 * This is the XedwList widget, it is useful to display a xedwList, without the
 * overhead of having a widget for each item in the xedwList.  It allows 
 * the user to select an item in a xedwList and notifies the application through
 * a callback function.
 *
 *	Created: 	8/13/88
 *	By:		Chris D. Peterson
 *                      MIT - Project Athena
 */

#ifndef _XedwListP_h
#define _XedwListP_h

/***********************************************************************
 *
 * XedwList Widget Private Data
 *
 ***********************************************************************/

#include <X11/Xaw/SimpleP.h>
#include "XedwList.h"

#define NO_HIGHLIGHT            XDTM_LIST_NONE
#define OUT_OF_RANGE            -1
#define OKAY                     0
#define XedwOff			 0
#define XedwOn			 1

/* New fields for the XedwList widget class record */

typedef struct {int foo;} XedwListClassPart;

/* Full class record declaration */
typedef struct _XedwListClassRec {
    CoreClassPart      	core_class;
    SimpleClassPart    	simple_class;
    XedwListClassPart	xedwList_class;
} XedwListClassRec;

extern XedwListClassRec xedwListClassRec;

typedef struct _LinkedList {
  int index;
  struct _LinkedList *next;
} LinkedList;

/* New fields for the XedwList widget record */
typedef struct {
  /* resources */
  Pixel         foreground;
  Dimension     internal_width,
                internal_height,
                column_space,
                row_space,
                icon_width,
                icon_height;
  int           default_cols;
  Boolean       force_cols,
                paste,
                vertical_cols,
                show_icons,	/* Show icons with list */
  		multiple;       /* Allow multiple selections */
  int           longest;
  int           nitems;		/* number of items in the xedwList. */
  XFontStruct	*font;
  XedwList  **xedwList;
  XtCallbackList  callback;

  /* private state */

  LinkedList  *is_highlighted;	/* set to the items currently highlighted. */
  int         highlight,	/* set to the item that should be highlighted.*/
              col_width,	/* width of each column. */
              row_height,	/* height of each row. */
              nrows,		/* number of rows in the xedwList. */
              ncols;		/* number of columns in the xedwList. */
  GC	      normgc,		/* a couple o' GC's. */
              revgc,
              graygc;		/* used when inactive. */
  Pixmap      default_icon;
  Pixmap      default_mask;

} XedwListPart;


/****************************************************************
 *
 * Full instance record declaration
 *
 ****************************************************************/

typedef struct _XedwListRec {
  CorePart	core;
  SimplePart	simple;
  XedwListPart	xedwList;
} XedwListRec;

#endif /* _XedwListP_h */



