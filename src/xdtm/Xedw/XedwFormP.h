/* XedwForm widget private definitions. This Form is identical to the XawForm
 * Widget except for a few added capabilities.
 */

#ifndef _XedwFormP_h
#define _XedwFormP_h

#include "XedwForm.h"
#include <X11/Xaw/FormP.h>
#include <X11/ConstrainP.h>

typedef struct {
    Boolean (*layout)(
#if NeedFunctionPrototypes
XedwFormWidget, Dimension, Dimension
#endif
		      );
} XedwFormClassPart;

typedef struct _XedwFormClassRec {
    CoreClassPart       core_class;
    CompositeClassPart  composite_class;
    ConstraintClassPart constraint_class;
    XedwFormClassPart   xedwForm_class;
} XedwFormClassRec;

extern XedwFormClassRec xedwFormClassRec;

typedef struct _XedwFormPart {
    /* resources */
    int         default_spacing;    /* default distance between children */

    /* private state */
    Dimension   old_width, old_height; /* last known dimensions      */
    int         no_refigure;        /* no re-layout while > 0        */
    Boolean     needs_relayout;     /* next time no_refigure == 0    */
    Boolean     resize_in_layout;   /* should layout() do geom request?  */
    Dimension   preferred_width, preferred_height; /* cached from layout */
} XedwFormPart;

typedef struct _XedwFormRec {
    CorePart            core;
    CompositePart       composite;
    ConstraintPart      constraint;
    XedwFormPart        xedwForm;
} XedwFormRec;

typedef struct _XedwFormConstraintsPart {
/*
 * Constraint Resources.
 */
    XtEdgeType  top, bottom,    /* where to drag edge on resize     */
                left, right;
    int         dx;             /* desired horiz offset         */
    int         dy;             /* desired vertical offset      */
    Widget      horiz_base;     /* measure dx from here if non-null */
    Widget      vert_base;      /* measure dy from here if non-null */
    Widget      width_link;     /* get width from this widget if non-null */
    Widget      height_link;    /* get height from this widget if non-null */
    Boolean     allow_resize;   /* True if child may request resize */
    Boolean     full_width;     /* True if child wants to be full width */
    Boolean     full_height;    /* True if child wants to be full height */
    Boolean     rubber_width;   /* True if the child may resize width */
    Boolean     rubber_height;  /* True if the child may resize height */

/*
 * Private contstraint resources.
 */

    int         virtual_width, virtual_height;

/*
 * What the size of this child would be if we did not impose the
 * constraint the width and height must be greater than zero (0).
 */

    LayoutState layout_state;   /* temporary layout state       */
} XedwFormConstraintsPart;

typedef struct _XedwFormConstraintsRec {
    XedwFormConstraintsPart xedwForm;
} XedwFormConstraintsRec, *XedwFormConstraints;

#endif /* _XawXedwFormP_h */
