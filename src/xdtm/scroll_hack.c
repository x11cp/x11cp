/*****************************************************************************
 ** File          : scroll_hack.c                                           **
 ** Purpose       : Reset the scrollbar in a viewport widget to the top     **
 ** Author        : Edward Groenendaal                                      **
 ** Date          : April 1991                                              **
 ** Documentation : Xdtm Design Folder                                      **
 ** Related Files :                                                         **
 *****************************************************************************/

#include "xdtm.h"
#include <X11/IntrinsicP.h>
#include <X11/StringDefs.h>
#include <X11/Xaw/XawInit.h>
#include <X11/Xaw/Viewport.h>
#include <X11/Xaw/ViewportP.h>
#include <X11/Xaw/Scrollbar.h>
#include <X11/Xaw/ScrollbarP.h>

/*****************************************************************************
 *                               setscroll                                   *
 *****************************************************************************/
public void setscroll(w, pos)
Widget w;
float pos;
{
  /* This procedure takes a viewpoet widget, find's it's vertical scrollbar
   * sets the value of this to the position supplied, then calls the 
   * callbacks on that scrollbar to inform the viewport of the change.
   *
   * This is a *bit* of a hack, but I couldn't think of another way around
   * it without sub-classing the viewport widget.
   */

  ViewportWidget  vw = (ViewportWidget) w;
  ScrollbarWidget sw = (ScrollbarWidget) vw->viewport.vert_bar;

  if (!sw) return;
  
  if (sizeof(float) > sizeof(XtArgVal)) {
    XtVaSetValues((Widget)sw, XtNtopOfThumb, &pos, NULL);
  } else {
    XtArgVal *l_pos = (XtArgVal *) &pos;
    XtVaSetValues((Widget)sw, XtNtopOfThumb, *l_pos, NULL);
  }
  XtCallCallbacks((Widget)sw, XtNthumbProc, &sw->scrollbar.top);
  XtCallCallbacks((Widget)sw, XtNjumpProc, &sw->scrollbar.top);
}
