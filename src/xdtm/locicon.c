/*****************************************************************************
 ** File          : quit.c                                                  **
 ** Purpose       : Initialise and Realise quit dialogs                     **
 ** Author        : Ramon Santiago, Lionel Mallet                           **
 ** Date          : August 1993                                             **
 ** Documentation : Xdtm Design Folder                                      **
 ** Related Files :                                                         **
 ** Changes       :                                                         **
 ****************************************************************************/

#include <stdio.h>
#include "xdtm.h"
#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Intrinsic.h>
#include <X11/Xmu/Drawing.h>

#ifdef XPM
#include "xpm.h"
#endif

#if NeedFunctionPrototypes
           int  XdtmLocateIconFile(Screen *, char *, Pixmap *, Pixmap *, int *, int *, int *, int *);
    static void XbmErrorMessage(Display *, char *, int ) ;
    static void XpmErrorMessage(Display *, char *, char *, int) ;
#else
           int  XdtmLocateIconFile();
    static void XbmErrorMessage() ;
    static void XpmErrorMessage() ;
#endif

/* ****************************************************************************
 * version that reads pixmap data as well as bitmap data
 * ****************************************************************************
 */
#if NeedFunctionPrototypes
int XdtmLocateIconFile(Screen *screen,
		       char *name,
		       Pixmap *iconPixmap,
		       Pixmap *iconMask,
		       int *widthp,
		       int *heightp,
		       int *xhotp,
		       int *yhotp )
#else
int XdtmLocateIconFile(screen, name, iconPixmap, iconMask,
		       widthp, heightp, xhotp, yhotp )
     Screen *screen;
     char *name;
     Pixmap *iconPixmap, *iconMask; /* RETURN */
     int *widthp, *heightp, *xhotp, *yhotp;  /* RETURN */
#endif
{
    Display *dpy = DisplayOfScreen(screen);
    Window rootWindowOfScreen = RootWindowOfScreen(screen);
    unsigned int depthOfScreen = DefaultDepth(dpy, DefaultScreen(dpy));
    Bool tryToRead = True;
    unsigned int width = 0;
    unsigned int height = 0;
    int xhot;
    int yhot;
    Pixmap pixmap = None, mask = None;
    
#ifdef XPM
    /* Try to read in an X Pixmap */
    
    if (tryToRead == TRUE)
    {
	XpmAttributes pixmapAttrs ;
	int pixmapReadStatus = XpmSuccess ;
	
	pixmapAttrs.visual       = DefaultVisual(dpy, DefaultScreen(dpy));
	pixmapAttrs.colormap     = DefaultColormap(dpy, DefaultScreen(dpy));
	pixmapAttrs.depth        = depthOfScreen;
	pixmapAttrs.colorsymbols = (XpmColorSymbol *)NULL;
	pixmapAttrs.numsymbols   = 0;
	
	pixmapAttrs.valuemask    = XpmVisual | XpmColormap |
	  XpmDepth | XpmReturnPixels | XpmReturnInfos;
	
	pixmapReadStatus =
	  XpmReadFileToPixmap(
			      dpy,
			      rootWindowOfScreen,
			      name,
			      &pixmap,
			      &mask,
			      &pixmapAttrs ) ;
	
	if (pixmapReadStatus == XpmSuccess)
	{
	    tryToRead = FALSE;
	    width = pixmapAttrs.width;
	    height = pixmapAttrs.height;
	}	    
#ifdef DEBUG
	else 
	{
	    XpmErrorMessage(
			    dpy,
			    "PseudoColor",
			    name, pixmapReadStatus ) ;
	}
#endif
    }
#endif
    /* Try to read in an X Bitmap */
    
    if (tryToRead == TRUE)
    {
	unsigned int bitmapWidth = 0 ;
	unsigned int bitmapHeight = 0 ;
	unsigned char *bitmapData = (unsigned char *) NULL;
	int bitmapReadStatus = BitmapSuccess ;

	bitmapReadStatus =
	  XmuReadBitmapDataFromFile(
				    name,
				    &bitmapWidth,
				    &bitmapHeight,
				    &bitmapData,
				    &xhot,
				    &yhot ) ;
	
	if (bitmapReadStatus == BitmapSuccess)
	{
	    tryToRead = FALSE ;
	    
	    pixmap =
	      XCreatePixmapFromBitmapData(
					  dpy,
					  rootWindowOfScreen,
					  bitmapData,
					  bitmapWidth,
					  bitmapHeight,
					  BlackPixelOfScreen(screen),
					  WhitePixelOfScreen(screen),
					  depthOfScreen) ;
	    
	}
#ifdef DEBUG
	else
	{
	    XbmErrorMessage( dpy, name, bitmapReadStatus ) ;
	}
#endif
    }
    
    if (pixmap != None)
    {
	*iconPixmap = pixmap;
	*iconMask = mask;
	
	if (widthp)
	{
	    *widthp = (int) width ;
	}
	if (heightp)
	{
	    *heightp = (int) height ;
	}
	if (xhotp)
	{
	    *xhotp = xhot ;
	}
	if (yhotp)
	{
	    *yhotp = yhot ;
	}
	return 0;
    }
    
    return 1 ;
}

/* ***************************************************************************
 *
 * ****************************************************************************
 */



/* ***************************************************************************
 *
 * ****************************************************************************
 */
#if NeedFunctionPrototypes
static void XbmErrorMessage(Display *dpy, char *fn, int bitmapReadStatus )
#else
static void XbmErrorMessage( dpy, fn, bitmapReadStatus )
     Display *dpy ;
     char *fn ;
     int bitmapReadStatus ;
#endif
{
    XtAppContext appContext = XtDisplayToApplicationContext( dpy ) ;  
    char warningMsg[256] ;
    
    switch( bitmapReadStatus )
    {
    case BitmapOpenFailed :
      (void) sprintf( warningMsg,
		     "BitmapOpenFailed \"%s\" could not be opened.", fn ) ;
      XtAppWarning( appContext, warningMsg ) ;
      break ;
      
  case BitmapFileInvalid :
    (void) sprintf( warningMsg,
		   "BitmapFileInvalid \"%s\" bad bitmap file format.", fn ) ;
      XtAppWarning( appContext, warningMsg ) ;
      break ;
      
  case BitmapNoMemory :
    (void) sprintf( warningMsg,
		   "BitmapNoMemory \"%s\" insufficient memory.", fn ) ;
      XtAppWarning( appContext, warningMsg ) ;
      break ;
  }
}

/* ***************************************************************************
 *
 * ****************************************************************************
 */
#ifdef XPM
#if NeedFunctionPrototypes
static void XpmErrorMessage(Display *dpy, char *visualName, char *fn, int pixmapReadStatus )
#else
static void XpmErrorMessage( dpy, visualName, fn, pixmapReadStatus )
     Display *dpy ;
     char *visualName ;
     char *fn ;
     int pixmapReadStatus ;
#endif
{
    XtAppContext appContext = XtDisplayToApplicationContext( dpy ) ;  
    char warningMsg[256] ;
    
    switch( pixmapReadStatus )
    {
    case XpmColorError :
	(void) sprintf( warningMsg,
		       "XpmColorError[%s] \"%s\".", visualName, fn ) ;
	XtAppWarning( appContext, warningMsg ) ;
	break ;
    
    case XpmOpenFailed :
      (void) sprintf( warningMsg,
		     "XpmOpenFailed[%s] \"%s\" could not be opened.",
		     visualName, fn ) ;
	XtAppWarning( appContext, warningMsg ) ;
	break ;
	
    case XpmFileInvalid :
      (void) sprintf( warningMsg,
		     "XpmFileInvalid[%s] \"%s\" bad pixmap file format.",
		     visualName, fn ) ;
	XtAppWarning( appContext, warningMsg ) ;
	break ;
	
    case XpmNoMemory :
      (void) sprintf( warningMsg,
		     "XpmNoMemory[%s] \"%s\" insufficient memory.",
		     visualName, fn ) ;
	XtAppWarning( appContext, warningMsg ) ;
	break ;
	
    case XpmColorFailed :
      (void) sprintf( warningMsg,
		     "XpmColorFailed[%s] \"%s\" not enough colors.",
		     visualName, fn ) ;
	XtAppWarning( appContext, warningMsg ) ;
	break ;
    }
}

#endif

