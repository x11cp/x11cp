/*****************************************************************************
 ** File          : map.c                                                   **
 ** Purpose       : Initialise and Realise map dialog                       **
 ** Author        : Edward Groenendaal                                      **
 ** Date          : April 1991                                              **
 ** Documentation : Xdtm Design Folder                                      **
 ** Related Files :                                                         **
 ** Changes       : 30.3.92 eddyg - Corrected Help Callback                 **
 **                 18-04-92, Edward Groenendaal                            **
 **                 Added #if NeedFunctionPrototypes stuff                  **
 **                 June 20, 1992, Ramon Santiago                           **
 **                 Changed all XtCreate calls to XtVaCreate calls.         **
 **                 Changed all caddr_t to XtPointer.                       **
 **                 Removed some lint.                                      **
 **                 Minor variable renaming and reordering.                 **
 ****************************************************************************/

#include "xdtm.h"
#include "parse.h"
#include "Ext/appl.h"
#include <stdio.h>
#include <unistd.h> /* for R_OK */
#include <X11/Shell.h>
#include <X11/Xaw/Label.h>
#include <X11/Xaw/Command.h>
#include <X11/Xaw/AsciiText.h>
#include "Xedw/XedwForm.h"
#include "Xedw/XedwList.h"

typedef enum { ok, ok_term, cancel } MapButtons;
    
/* external and forward functions definitions */
#if NeedFunctionPrototypes
  extern String build_arguments(String, SelOptions);
  extern void displayfile(String, String, String, XtGrabKind);
  extern int execute(String, String, String, Boolean, AppProgram *);
  private void highlight_by_re(String);
  private void mapHelp(Widget, Boolean, XtPointer);
  private void mapQueryResult(Widget, MapButtons, XtPointer);
  extern void realize_dialog(Widget, Widget, XtGrabKind, XtEventHandler, XtPointer);
  extern void selection_made(Widget, XtPointer, XtPointer);
  private void selectQueryResult(Widget, MapButtons, XtPointer);
  extern void setCursor(Cursor);
#else
  extern String build_arguments();
  extern void displayfile();
  extern int execute();
  private void highlight_by_re();
  private void mapHelp();
  private void mapQueryResult();
  extern void realize_dialog();
  extern void selection_made();
  private void selectQueryResult();
  extern void setCursor();
#endif

/* Widgets */

private Widget mapDialog;        /* For mapping prog over files */
private Widget mapform;
private Widget maplabel;
private Widget mappromptlabel;
private Widget maptext;
private Widget mapOK;
private Widget mapOKTerm;
private Widget mapCancel;
private Widget mapHELP;

private AppProgram *map_node;


/*****************************************************************************
 *                                init_map                                   *
 *****************************************************************************/
public void init_map(top)
Widget top;
{
  /* Initialise the map and select dialog Widgets, they actually shar the
   * same ones.
   */

  XtTranslations mapTranslations;

  /* Translations for the text widget */
  static char defaultTranslations[] = 
    "Ctrl<Key>A:        beginning-of-line() \n\
     Ctrl<Key>E:        end-of-line() \n\
     <Key>Escape:       beginning-of-line() kill-to-end-of-line() \n\
     <Key>Right: 	forward-character() \n\
     <Key>Left:         backward-character() \n\
     <Key>Delete:       delete-previous-character() \n\
     <Key>BackSpace:    delete-previous-character() \n\
     <Key>:             insert-char() \n\
     <FocusIn>:         focus-in() \n\
     <FocusOut>:        focus-out() \n\
     <BtnDown>:         select-start()";

  mapDialog =
    XtVaCreatePopupShell(
        "mapDialog",
	transientShellWidgetClass,
	top,
	    NULL ) ;

  mapform =
    XtVaCreateManagedWidget(
        "mapform",
	xedwFormWidgetClass,
	mapDialog,
	    NULL ) ;

  maplabel =
    XtVaCreateManagedWidget(
        "maplabel",
	labelWidgetClass,
	mapform,
	    XtNborderWidth,            0,
	    XtNfullWidth,           True,
	    XtNjustify,  XtJustifyCenter,
	    NULL ) ;

  mappromptlabel =
    XtVaCreateManagedWidget(
        "mappromptlabel",
	labelWidgetClass,
	mapform,
	    XtNborderWidth,            0,
	    XtNfromVert,        maplabel,
	    NULL ) ;

  maptext =
    XtVaCreateManagedWidget(
        "maptext",
	asciiTextWidgetClass,
	mapform,
	    XtNfromVert,        maplabel,
	    XtNborderWidth,            1,
	    XtNfromHoriz, mappromptlabel,
	    XtNfullWidth,           True,
	    XtNeditType,     XawtextEdit,
	    NULL ) ;
  mapOKTerm =
    XtVaCreateManagedWidget(
        "mapOKTerm",
	commandWidgetClass,
	mapform,
	    XtNjustify, XtJustifyCenter,
	    XtNfromVert,        maptext,
	    XtNlabel,      "OK in Term",
	    NULL ) ;

  mapOK =
    XtVaCreateManagedWidget(
        "mapOK",
	commandWidgetClass,
	mapform,
	    XtNjustify, XtJustifyCenter,
	    XtNfromVert,        maptext,
	    XtNfromHoriz,     mapOKTerm,
	    XtNlabel,              "OK",
	    NULL ) ;

  mapCancel =
    XtVaCreateManagedWidget(
        "mapCancel",
	commandWidgetClass,
	mapform,
	    XtNjustify, XtJustifyCenter,
	    XtNfromVert,        maptext,
	    XtNfromHoriz,         mapOK,
	    XtNlabel,          "Cancel",
	    NULL ) ;

  mapHELP =
    XtVaCreateManagedWidget(
        "mapHelp",
	commandWidgetClass,
	mapform,
	    XtNjustify, XtJustifyCenter,
	    XtNfromVert,        maptext,
	    XtNfromHoriz,     mapCancel,
	    XtNlabel,            "Help",
	    NULL ) ;

  XtUninstallTranslations(maptext);
  mapTranslations = XtParseTranslationTable(defaultTranslations);
  XtOverrideTranslations(maptext, mapTranslations);

  if (map_node == NULL) 
  { /* Initialize map_node for map in Term window */
      map_node = XtNew(AppProgram);
      
      map_node->string = NULL;
      map_node->options = O_SEL;
      map_node->icon = (Pixmap)1; /* to avoid a bug in decrement_counter
				     which uses the icon field to know
				     if node is dummy or not */
      map_node->termopts = TERM;
      map_node->count = 0;
  }
}

/*****************************************************************************
 *                              WM_destroy_map_dialog                        *
 ****************************************************************************/
#if NeedFunctionPrototypes
private void WM_destroy_map_dialog(Widget w,
				   XtPointer client_data,
				   XEvent *event,
				   Boolean *dispatch)
#else
private void WM_destroy_map_dialog(w, client_data, event, dispatch)
     Widget w;
     XtPointer client_data;
     XEvent *event;
     Boolean *dispatch;
#endif
{
    extern Atom protocols[2]; /* defined in dialogs.c */
    
    if (event->xclient.message_type == protocols[1] &&
	event->xclient.data.l[0] == protocols[0])
    /* the widget got a kill signal */
    {
	/* call callbacks on Cancel button */
	XtCallCallbacks(mapCancel, XtNcallback, NULL);
    }
}

/*****************************************************************************
 *                                 map_dialog                                *
 ****************************************************************************/
public void map_dialog(map)
Boolean map;
{
  /* if map is true then put map dialog up, otherwise use the 
   * select dialog.
   */

  XtVaSetValues(maptext, XtNstring, "", NULL);

  if (map == True)
    {
      XtManageChild(mapOKTerm);
      XtVaSetValues(maplabel, XtNlabel, " Map Program over Selected Files ",
		    NULL);
      XtVaSetValues(mappromptlabel, XtNlabel, "Program:", NULL);

      XtAddCallback(mapCancel, XtNcallback, (XtCallbackProc)mapQueryResult,
		    (XtPointer)cancel);
      XtAddCallback(mapOK,     XtNcallback, (XtCallbackProc)mapQueryResult,
		    (XtPointer)ok);
      XtAddCallback(mapOKTerm, XtNcallback, (XtCallbackProc)mapQueryResult,
		    (XtPointer)ok_term);
      XtAddCallback(mapHELP,   XtNcallback, (XtCallbackProc)mapHelp,
		    (XtPointer)False);
    }
  else
    {
      XtUnmanageChild(mapOKTerm);
      XtVaSetValues(maplabel, XtNlabel, "Select Files by Regular Expression",
		    NULL);
      XtVaSetValues(mappromptlabel, XtNlabel, "RegExp: ", NULL);

      XtAddCallback(mapCancel, XtNcallback, (XtCallbackProc)selectQueryResult,
		    (XtPointer)cancel);
      XtAddCallback(mapOK,     XtNcallback, (XtCallbackProc)selectQueryResult,
		    (XtPointer)ok);
      XtAddCallback(mapHELP,   XtNcallback, (XtCallbackProc)mapHelp,
		    (XtPointer)True);
    }

  realize_dialog(mapDialog, NULL, XtGrabNonexclusive,
		 (XtEventHandler)WM_destroy_map_dialog, NULL);
}

/*****************************************************************************
 *                         destroy_map_dialog                                *
 *****************************************************************************/
private void destroy_map_dialog()
{
  /* Popdown the map dialog, remove the callbacks on the buttons so that
   * they can be reset next time map_dialog is called, in case the select
   * dialog is required.
   */

  XtPopdown(mapDialog);

  XtRemoveAllCallbacks(mapCancel, XtNcallback);
  XtRemoveAllCallbacks(mapOK,     XtNcallback);
  XtRemoveAllCallbacks(mapOKTerm, XtNcallback);
  XtRemoveAllCallbacks(mapHELP,   XtNcallback);
}


/****************************************************************************
 *                              mapHelp                                     *
 ****************************************************************************/
/*ARGSUSED*/
#if NeedFunctionPrototypes
private void mapHelp(Widget w, Boolean IsRegExp, XtPointer  call_data)
#else
private void mapHelp(w, IsRegExp,call_data)
Widget w;
Boolean IsRegExp;
XtPointer call_data;
#endif
{
  if (access(help_file, R_OK) == 0) {
    if (IsRegExp) 
      displayfile(help_file, "12. Regular Expressions",
		  "xdtm regular expression syntax", XtGrabNonexclusive);
    else 
      displayfile(help_file, "7. Mapping a program over selected files",
		  "xdtm map help", XtGrabNonexclusive);
    } else
      alert_dialog("Help file not found at", help_file, "Cancel");      
}


/*****************************************************************************
 *                             mapQueryResult                                *
 *****************************************************************************/
/*ARGSUSED*/
private void mapQueryResult(w, status, call_data)
Widget w;
MapButtons status;
XtPointer call_data;
{
  /* Either map a program over the selected files if OK was pressed,
   * otherwise do nothing.
   */
  extern Cursor busy;
  String mapprogram, program, filename;

  destroy_map_dialog();

  if (status != cancel) {
    setCursor(busy);

    /* get program name from text widget */

    XtVaGetValues(maptext, XtNstring, &mapprogram, NULL);

    program = XtNewString(mapprogram);

    /* extract filename from program */
    filename = XtNewString(program);
    filename = strtok(filename, " ");

    if (filename)
      /* Get list of files */
      if ((program = build_arguments(program, M_SEL)) == NULL) 
	fprintf(stderr, "Programmer Error: map was selected without files\n");
      else 
	if (status == ok_term) {
	    if (map_node->string != NULL) XtFree(map_node->string);
	    map_node->string = XtNewString(program);
	    execute(NULL, filename, program, False, map_node);
	} else
	  execute(NULL, filename, program, False, NULL);

    setCursor((Cursor)NULL);
  }
}

/*****************************************************************************
 *                             selectQueryResult                             *
 *****************************************************************************/
/*ARGSUSED*/
private void selectQueryResult(w, status, call_data)
Widget w;
MapButtons status;
XtPointer call_data;
{
  /* If Ok was pressed then select files using the regular expression
   * contained within the text widget.
   */
  extern Cursor busy;
  String re;

  destroy_map_dialog();

  if (status == ok) {
    /* Get regular expression */
    setCursor(busy);
    XtVaGetValues(maptext, XtNstring, &re, NULL);
    
    highlight_by_re(re);

    setCursor((Cursor)NULL);
  }
}

/*****************************************************************************
 *                            highlight_by_re                                *
 *****************************************************************************/
private void highlight_by_re(re)
String re;
{
  extern XdtmList **icon_list;
  extern Cardinal icon_list_size;
  extern Widget directoryManager;

  regexp *buffer;
  Cardinal i;

  buffer = regcomp(re);

  /* traverse icon_list calling XedwListHighlight on every filename
   * matched.
   */
  for (i = 0; i < icon_list_size; i++) {
    if (regexec(buffer, icon_list[i]->string) != 0) 
      XedwListHighlight(directoryManager, i);
  }

  selection_made(directoryManager, 0, 0);

  XtFree((char *)buffer);

}
