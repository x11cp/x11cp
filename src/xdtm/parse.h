/*****************************************************************************
 ** File          : parse.h                                                 **
 ** Purpose       :                                                         **
 ** Author        : Edward Groenendaal                                      **
 ** Date          : 18th Feb 1991                                           **
 ** Documentation : Xdtm Design Folder                                      **
 ** Related Files : parse.c, lexical.h, parser.y                            **
 *****************************************************************************/

#ifndef _parse_h
#define _parse_h

#include "regexp/regexp.h"

/* Added IGNORE_SEL from Jon's code - bit like A_SEL but what the hell */
typedef enum {IGNORE_SEL, M_SEL, O_SEL, N_SEL, A_SEL} SelOptions;

typedef struct {
  String     string;
  Pixmap     icon;
  Pixmap     mask;
  String     program;
  int        count;   /* Added by Jon */
  SelOptions options;
  int        termopts;
} AppProgram;

typedef struct {
  String       name;
  Cardinal     number;
  AppProgram **list;
} AppSelection;

typedef struct _iconOps {
    Widget      menu;
    AppProgram *cmd;
} iconOps;

typedef struct _iconSelection {
  String                 name;
  Cardinal               number;
  AppProgram           **applist;
  struct _iconSelection *next;
} iconSelection;

/* Directory Manager Structures */

typedef struct _iconPrefs {
  regexp   *expbuf;
  Boolean   checkpath;
  XtPointer extra;
  Pixmap icon;
  Pixmap mask;
  Boolean cmd_is_first;
  iconOps *user_data;
  iconSelection *select;
  struct _iconPrefs *next;
} iconPrefs;

typedef struct _typePrefs {
  iconPrefs *iconprefs;
  struct _typePrefs *dir;
  struct _typePrefs *file;
  struct _typePrefs *block;	
  struct _typePrefs *character;	
  struct _typePrefs *fifo;	
  struct _typePrefs *slink;	
  struct _typePrefs *socket;	
  struct _typePrefs *exe;
  struct _typePrefs *read;
  struct _typePrefs *write;
} typePrefs;

/* Application Manager Structure */

#define APPPSIZE 10 /* Initial size of an AppProgram Array */
#define APPPINC  5  /* Increment by which to increase AppProgram Array on overflow */

typedef struct pidlist {
  int             pid;
  AppProgram     *node;
  struct pidlist *next;
} ProcessList;  

extern char *preferences_filename;
#endif /* _parse_h */
