/*****************************************************************************
 ** File          : menus.h                                                 **
 ** Purpose       :                                                         **
 ** Author        : Edward Groenendaal                                      **
 ** Date          : 18th Feb 1990                                           **
 ** Documentation : Xedw Design Folder                                      **
 ** Related Files : menus.c                                                 **
 ** Changes       : 28-11-91, Edward Groenendaal                            **
 **                 Added Listprocs for Jon, bringing this file to V2.0     **
 *****************************************************************************/


#ifndef _EG_menus_h
#define _EG_menus_h

#include <X11/Intrinsic.h>

#define LINE    "line"

#define PERMS   (1 << 1)
#define NLINKS  (1 << 2)
#define OWNER   (1 << 3)
#define GROUP   (1 << 4)
#define SIZE    (1 << 5)
#define MODTM   (1 << 6)
#define ACCTM   (1 << 7)

typedef enum {flagged, noflag} Flags;

typedef enum {
  About,
  Help,
  New,
  Duplicate,
  Rename,
  Getinfo,
  Listprocs,
  Copy,
  Move,
  Trash,
  Reload,
  Quit,
  Dirfirst,
#ifndef TRUE_SYSV
  FollowSymLinks,
#endif
  Usedotspec,
  Usedotdotspec,
  Silentsel,
  Icons,
  Short,
  Long,
  Options,
  Map,
  Select
} MenuValue;

typedef struct {
  String paneName;
  String paneLabel;
  MenuValue paneNumber;
  Flags set;
  int options;
} MenuContents;

typedef struct {
  Widget w;
  MenuValue mode;
  /* long listing options */
  Cardinal length;
  char options;
} Icon_mode;

extern Widget menuBar;
extern Icon_mode current_mode;
extern Pixmap tick;
extern Pixmap emptytick;

#endif /* _EG_menus_h */
