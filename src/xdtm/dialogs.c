/*****************************************************************************
 ** File          : dialogs.c                                               **
 ** Purpose       : Initialise and Realise dialogs                          **
 ** Author        : Edward Groenendaal                                      **
 ** Date          : 18th Feb 1991                                           **
 ** Documentation : Xdtm Design Folder                                      **
 ** Related Files : quit.c buttons.c listoption.c map.c select.c display.c  **
 **                 newfile.c doubleclick.c				    **
 ** Changes       : 18-04-92, Edward Groenendaal                            **
 **                 Added #if NeedFunctionPrototypes stuff                  **
 **                 June 20, 1992, Ramon Santiago                           **
 **                 Changed all XtCreate calls to XtVaCreate calls.         **
 **                 Changed all caddr_t to XtPointer.                       **
 **                 Removed some lint.                                      **
 *****************************************************************************/

/* dialog box's provided: Quit        - Quit (yes/no)
 *			  Button      - Do action to these files (yes/no)
 *                        Longl       - Select options on Long Listing
 *                        Map         - Map program x over selected files OR
 *                        Select      - Select files using RE x.
 * 			  Display     - Display a file.
 *           		  Newfile     - Ask for filename, create empty file. 
 *           		  DoubleClick - Display/Change permissions/execute/etc.
 *
 */

#include "xdtm.h"

/* external and forward functions definitions */
#if NeedFunctionPrototypes
  extern void init_quit(Widget);
  extern void init_button(Widget);
  extern void init_listoption(Widget);
  extern void init_doubleclick(Widget);
  extern void init_map(Widget);
  extern void init_display(Widget);
  extern void init_newfile(Widget);
#else
  extern void init_quit();
  extern void init_button();
  extern void init_listoption();
  extern void init_doubleclick();
  extern void init_map();
  extern void init_display();
  extern void init_newfile();
#endif

/*****************************************************************************
 *                         createDialogWidgets                               *
 *****************************************************************************/
public void createDialogWidgets(top)
Widget top;
{
  /* Call the initialise procedures of all the xdtm dialogs */

  init_quit(top);
  init_button(top);
  init_listoption(top);
  init_doubleclick(top);
  init_map(top);
  init_display(top);
  init_newfile(top);
 				 
}

/*****************************************************************************
 *                  defaultHandler, unsetKillFromWM                          *
 *****************************************************************************/
public Atom protocols[2];

#if NeedFunctionPrototypes
private void defaultHandler(Widget w, XtPointer client_data,
			    XEvent *event, Boolean *dispatch)
#else
private void defaultHandler(w, client_data, event, dispatch)
     Widget w;
     XtPointer client_data;
     XEvent *event;
     Boolean *dispatch;
#endif
{
    if (event->xclient.message_type == protocols[1] &&
	event->xclient.data.l[0] == protocols[0])
    /* the widget got a kill signal */
    {
	/* do nothing, just ignore and avoid crashing */
    }
}

#if NeedFunctionPrototypes
public void unsetKillFromWM(Widget w,
		     XtEventHandler callback,
		     XtPointer client_data)
#else
public void unsetKillFromWM(w, callback, client_data)
     Widget w;
     XtEventHandler callback;
     XtPointer client_data;
#endif
{
    static char once = 0;
    
    if (!once) {
	/* define atoms only once */
	protocols[0] = XInternAtom(XtDisplay(w), "WM_DELETE_WINDOW", True);
	protocols[1] = XInternAtom(XtDisplay(w), "WM_PROTOCOLS", True);
	once = 1;
    }
    
    /* set WM property to receive a window deletion and avoid getting killed */
    XSetWMProtocols(XtDisplay(w), XtWindow(w), protocols, 1);

    if (callback == (XtEventHandler)NULL)
      callback = (XtEventHandler)defaultHandler;

    /* add handler triggered on WM's client message */
    XtAddEventHandler(w, ClientMessage, True, callback, client_data);
}

/*****************************************************************************
 *                               realize_dialog                              *
 *****************************************************************************/
public void realize_dialog(popup, parent, grab_kind, handler, client_data)
Widget popup, parent;
XtGrabKind grab_kind;
XtEventHandler handler;
XtPointer client_data;
{
  /* Given a popup widget this procedure, modified from one in the X Toolkit
   * Intrinsics Programmers Manual, O'Reilly & Associates, pops it up with
   * the center of the popup over the center of the application.
   */

  extern Widget topLevel;
  Position x,y;
  Dimension appWidth, appHeight, qWidth, qHeight;
  
  /* You dont know the dimensions of a widget for certain until it is
   * realized, therefore realize the dialog.
   * NOTE: XtRealizeWidget returns without an error if widget is already
   *       realized, hence no check via XtIsRealized.
   */

  if (parent == (Widget)NULL)
    parent = topLevel;

  XtRealizeWidget(popup);

  /* Get dimensions of application */

  XtVaGetValues(
      parent,
          XtNwidth,  &appWidth,
	  XtNheight, &appHeight,
	  NULL );

  /* Get dimensions of quit popup */

  XtVaGetValues(
      popup,
          XtNwidth,  &qWidth,
	  XtNheight, &qHeight,
	  NULL ) ;

  /* Translate application coordinates to screen coordinates */

  XtTranslateCoords(parent,
		    (Position) ((appWidth/2)-(qWidth/2)),
		    (Position) ((appHeight/2)-(qHeight/2)),
		    &x, &y);
  if (x < 0)
    x = 0;
  else {
      int scr_width = WidthOfScreen(XtScreen(topLevel));
      if (x + qWidth > scr_width)
	x = scr_width - qWidth;
  }
  if (y < 0)
    y = 0;
  else {
      int scr_height = HeightOfScreen(XtScreen(topLevel));
      if (y + qHeight > scr_height)
	y = scr_height - qHeight;
  }
  
  /* move popup shell to that position */
  XtVaSetValues(
      popup,
          XtNx, x,
	  XtNy, y,
	  NULL ) ;

  XtPopup(popup, grab_kind);
  unsetKillFromWM(popup, handler, client_data);
}
