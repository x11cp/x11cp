/*****************************************************************************
 ** File          : main.c                                                  **
 ** Purpose       : Initialise and Realise xdtm                             **
 ** Author        : Edward Groenendaal                                      **
 ** Date          : 18th Feb 1991                                           **
 ** Documentation : Xdtm Design Folder                                      **
 ** Related Files : All other xdtm files                                    **
 ** Changes       : 28-11-91, Edward Groenendaal                            **
 **                 Merged 1.8 and 2.0b to make this file                   **
 **                 June 20, 1992, Ramon Santiago                           **
 **                 Changed all XtCreate calls to XtVaCreate calls.         **
 **                 Changed all caddr_t to XtPointer.                       **
 **                 Removed some lint.                                      **
 *****************************************************************************/

#include "xdtm.h"
#include "menus.h"
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

/* local headers */

#include <X11/Shell.h>
#include "Xedw/XedwForm.h"

public String help_file;
public String perm_help_file;
public Widget topLevel;
public String home;
public AppData app_data;

static char xdtminfo[]="X Desktop Manager (C) 1991 Edward Groenendaal";

static XtResource resources[] = {
  {   
    XtNviewWidth, 
    XtCViewWidth,
    XtRInt,
    sizeof(int),
    XtOffset(AppDataPtr, view_width),
    XtRImmediate,
    (XtPointer) 85,
  },
  { 
    XtNviewHeight,
    XtCViewHeight,
    XtRInt,
    sizeof(int),
    XtOffset(AppDataPtr, view_height),
    XtRImmediate,
    (XtPointer) 25,
  },
  {   
    XtNtermWidth, 
    XtCTermWidth,
    XtRInt,
    sizeof(int),
    XtOffset(AppDataPtr, term_width),
    XtRImmediate,
    (caddr_t) 85,
  },
  { 
    XtNtermHeight,
    XtCTermHeight,
    XtRInt,
    sizeof(int),
    XtOffset(AppDataPtr, term_height),
    XtRImmediate,
    (caddr_t) 10,
  },
  {
    XtNmode,
    XtCMode,
    XtRString,
    sizeof(String),
    XtOffset(AppDataPtr, mode),
    XtRImmediate,
    (XtPointer) "icons",
  },
  {
    XtNoptions,
    XtCOptions,
    XtRInt,
    sizeof(int),
    XtOffset(AppDataPtr, options),
    XtRImmediate,
    (XtPointer) 32,
  },
  {
    XtNdelay,
    XtCDelay,
    XtRInt,
    sizeof(int),
    XtOffset(AppDataPtr, delay),
    XtRImmediate,
    (XtPointer) 0,
  },
  {
    XtNdirOnExit,
    XtCDirOnExit,
    XtRBoolean,
    sizeof(Boolean),
    XtOffset(AppDataPtr, dironexit),
    XtRImmediate,
    (XtPointer) False,
  },
  {
    XtNdirFirst,
    XtCDirFirst,
    XtRBoolean,
    sizeof(Boolean),
    XtOffset(AppDataPtr, dirfirst),
    XtRImmediate,
    (XtPointer) True,
  },
  {
    XtNscrollOnExit,
    XtCScrollOnExit,
    XtRBoolean,
    sizeof(Boolean),
    XtOffset(AppDataPtr, scrollonexit),
    XtRImmediate,
    (caddr_t) False,
  },
  {
    XtNconfirmAction,
    XtCConfirmAction,
    XtRBoolean,
    sizeof(Boolean),
    XtOffset(AppDataPtr, confirmaction),
    XtRImmediate,
    (caddr_t) True,
  },
  {
    XtNbellOnExit,
    XtCBellOnExit,
    XtRBoolean,
    sizeof(Boolean),
    XtOffset(AppDataPtr, bellonexit),
    XtRImmediate,
    (caddr_t) True,
  },
  {
    XtNbellOnWarn,
    XtCBellOnWarn,
    XtRBoolean,
    sizeof(Boolean),
    XtOffset(AppDataPtr, bellonwarn),
    XtRImmediate,
    (caddr_t) True,
  },
  {
    XtNuseDotSpec,
    XtCUseDotSpec,
    XtRBoolean,
    sizeof(Boolean),
    XtOffset(AppDataPtr, usedotspec),
    XtRImmediate,
    (caddr_t) False,
  },
  {
    XtNuseDotDotSpec,
    XtCUseDotDotSpec,
    XtRBoolean,
    sizeof(Boolean),
    XtOffset(AppDataPtr, usedotdotspec),
    XtRImmediate,
    (caddr_t) False,
  },
  {
    XtNfollowSymLinks,
    XtCFollowSymLinks,
    XtRBoolean,
    sizeof(Boolean),
    XtOffset(AppDataPtr, followsymlinks),
    XtRImmediate,
    (caddr_t) True,
  },
  {
    XtNsilentSelection,
    XtCSilentSelection,
    XtRBoolean,
    sizeof(Boolean),
    XtOffset(AppDataPtr, silentsel),
    XtRImmediate,
    (caddr_t) False,
  },
  {
    XtNsystemLibDir,
    XtCSystemLibDir,
    XtRString,
    sizeof(String),
    XtOffset(AppDataPtr, systemlibdir),
    XtRImmediate,
    (XtPointer) NULL,
  },
  {
    XtNconfigFile,
    XtCConfigFile,
    XtRString,
    sizeof(String),
    XtOffset(AppDataPtr, cffile),
    XtRImmediate,
    (XtPointer) NULL,
  },
  {
    XtNviewFont,
    XtCFont,
    XtRFontStruct,
    sizeof(XFontStruct*),
    XtOffset(AppDataPtr, view_font),
    XtRString,
    (XtPointer) "6x10",
  },
  {
    XtNtermFont,
    XtCFont,
    XtRFontStruct,
    sizeof(XFontStruct*),
    XtOffset(AppDataPtr, term_font),
    XtRString,
    (caddr_t) "6x10",
  },
  {
    XtNdmFont,
    XtCFont,
    XtRFontStruct,
    sizeof(XFontStruct*),
    XtOffset(AppDataPtr, dm_font),
    XtRString,
    (XtPointer) "*-courier-bold-r-*-120-*",
  },
};

/* external and forward functions definitions */
#if NeedFunctionPrototypes
  extern void createMenuWidgets(Widget);          /* menus.c */
  extern void createAppManagerWidgets(Widget);    /* appman.c */
  extern void createFileManagerWidgets(Widget);   /* fileman.c */
  extern void createDialogWidgets(Widget);        /* dialogs.c */
  extern void destroy_quit_dialog();
  extern void initAppManager(Widget);               /* appman.c */
  extern void initFileManager(Widget);              /* fileman.c */
  private void initialiseXdtm(void);
  extern void parsePreferences(Widget);           /* parse.c */
  extern void unsetKillFromWM(Widget, XtEventHandler, XtPointer); /* dialogs.c */
#else
  extern void createMenuWidgets();          /* menus.c */
  extern void createAppManagerWidgets();    /* appman.c */
  extern void createFileManagerWidgets();   /* fileman.c */
  extern void createDialogWidgets();        /* dialogs.c */
  extern void destroy_quit_dialog();
  extern void initAppManager();               /* appman.c */
  extern void initFileManager();              /* fileman.c */
  private void initialiseXdtm();
  extern void parsePreferences();           /* parse.c */
  extern void unsetKillFromWM();            /* dialogs.c */
#endif

/*****************************************************************************
 *                                WMQuit                                     *
 *****************************************************************************/
#if NeedFunctionPrototypes
private void WMQuit(Widget w, XtPointer client_data,
		    XEvent *event, Boolean *dispatch)
#else
private void WMQuit(w, client_data, event, dispatch)
     Widget w;
     XtPointer client_data;
     XEvent *event;
     Boolean *dispatch;
#endif
{
    extern Atom protocols[2]; /* defined in dialogs.c */
    Widget fileMenu = XtNameToWidget(w, "*fileMenu");
    Widget quitButton = XtNameToWidget(fileMenu, "*quit");

    if (event->xclient.message_type == protocols[1] &&
	event->xclient.data.l[0] == protocols[0])
    /* the widget got a kill signal */
    {
	XtCallCallbacks(quitButton, XtNcallback, NULL);
    }
}

      
/*****************************************************************************
 *                                main                                       *
 *****************************************************************************/
public void main(argc, argv)
int argc;
char *argv[];
{
  /* Parse the command line arguments, initialise the toplevel form, call
   * other initialising procedures, enter the X Main Loop.
   */

  extern Icon_mode current_mode;

  Widget topForm;
  XtAppContext defaultContext ;

  /* Command Line Arguments */

  static XrmOptionDescRec options[] = {
    {"-dmfont",    "*directoryManager.font", XrmoptionSepArg, NULL},
    {"-cf", 	   ".configFile",            XrmoptionSepArg, NULL},
    {"-delay",     ".delay",                 XrmoptionSepArg, NULL}
  };

  /* Initialise Program */

  if ((home = (String) getenv("HOME")) == NULL) {
    fprintf(stderr, "Warning: can't get environment variable HOME\n");
    home = XtNewString("/");
  } else 
    home = XtNewString(home);

  /* Initialise the user interface */

  topLevel =
    XtAppInitialize(
	&defaultContext,
	"Xdtm",
	options,
	XtNumber(options),
	&argc,
	argv,
	(String *) NULL,
	(ArgList) NULL,
	(Cardinal) 0 ) ;

#ifdef DEBUG
  XSynchronize(XtDisplay(topLevel), True);
#endif

  /* Check left over command line arguments */
  if (argc > 1) {
    /* incorrect command line arguments */
    int i;
    static int errs = False;
    for (i = 1; i < argc; i++) {
      if (!errs++)
	fprintf(stderr, "%s: command line option unknown:\n", argv[0]);
      fprintf(stderr, "option: %s\n\n", argv[i]);
    }
    fprintf(stderr, "%s understands all standard Xt command-line options.\n",
	    argv[0]);
    fprintf(stderr, "Additional options are as follows:\n");
    fprintf(stderr, "Option              Valid Range\n");
    fprintf(stderr, "-dmfont             Any font, should be fixed width\n");
    fprintf(stderr, "-cf                 config filename\n");
    fprintf(stderr, "-delay              delay after copying, for NFS\n");
    exit(2);
  }
    
  /* get application resources */
  XtGetApplicationResources(topLevel,
			    &app_data,
			    resources,
			    XtNumber(resources),
			    NULL, 0);

  /* check values of application resources */
  if (app_data.view_width < 1 || app_data.view_height < 1) {
    fprintf(stderr, "%s: error in resource settings:\nview window must be greater than 1x1 characters\n",
	    argv[0]);
    exit(2);
  }
  
  /* check values of application resources */
  if (app_data.term_width < 1 || app_data.term_height < 1) {
    fprintf(stderr, "%s: error in resource settings:\n%s",
	    argv[0], "term window must be greater than 1x1 characters\n");
    exit(2);
  }
  
  if (strcmp(app_data.mode, "icons") == 0) 
    current_mode.mode = Icons;
  else if (strcmp(app_data.mode, "short") == 0)
    current_mode.mode = Short;
  else if (strcmp(app_data.mode,  "long") == 0)
    current_mode.mode = Long;
  else {
    fprintf(stderr, "%s: error in resource settings:\n%s%s%s%s",
	    argv[0],
	    "mode must be one of either:\n",
	    "'icons'     Show icons\n",
	    "'short'     Just display file names\n",
	    "'long'      Display file names with additional data.\n");
    exit(2);
  }
  
  if (app_data.delay < 0 || app_data.delay > 5) {
    fprintf(stderr, "%s: error in resource settings:\ndelay must be between 0 and 5\n", argv[0]);
    exit(2);
  }

  /* initialize xdtm lib dir */
  if (app_data.systemlibdir == NULL) 
    app_data.systemlibdir = XtNewString(SYSTEM_LIB_DIR);


  /* initialize help files */
  help_file = (String)XtMalloc((strlen(app_data.systemlibdir) +
				strlen(SYSTEM_HELP) + 2) * sizeof(char));
  perm_help_file = (String)XtMalloc((strlen(app_data.systemlibdir) +
				     strlen(SYSTEM_HELP_PERM) + 2) *
				    sizeof(char));
  sprintf(help_file, "%s/%s", app_data.systemlibdir, SYSTEM_HELP);
  sprintf(perm_help_file, "%s/%s", app_data.systemlibdir,
	  SYSTEM_HELP_PERM);

  topForm =
    XtVaCreateManagedWidget(
        "topForm",
	xedwFormWidgetClass,
	topLevel,
	    XtNbottom, XtChainBottom,
	    XtNtop,       XtChainTop,
	    XtNleft,     XtChainLeft,
	    XtNright,   XtChainRight,
	    XtNhorizDistance,      5,
	    XtNvertDistance,       5,
	    NULL ) ;

  /* Create Widgets in rest of program.
   * These must be called in this order, top left -> bottom right 
   */
  createMenuWidgets        (topForm);
  createAppManagerWidgets  (topForm);
  createFileManagerWidgets (topForm);
  createDialogWidgets      (topForm); 

  XtVaSetValues(
      topLevel,
	  XtNminWidth,  400,
	  XtNminHeight, 200,
	  NULL ) ;

  tzset(); /* Make sure we have the time zone info so that the date 
	    * functions work, probably don't *need* this.. */

  /* Realize the widgets, (display program) then loop waiting for events */

  XtRealizeWidget(topLevel);
  unsetKillFromWM(topLevel, (XtEventHandler)WMQuit, NULL);

  initialiseXdtm();

  XtAppMainLoop(defaultContext);

}

/*****************************************************************************
 *                              quitQueryResult                              *
 *****************************************************************************/
/*ARGSUSED*/
public void quitQueryResult(w, quit, call_data)
Widget w;
Boolean quit;
XtPointer call_data;
{
  /* Action procedure called when a button is pressed in a quit dialog,
   *
   * - Takes a widget, quit - whether to quit, call_data - ignored.
   */

  /* Quit selected */
  if (quit == True)
    exit(0);

  /* Cancel selected */
  destroy_quit_dialog();
}

/*****************************************************************************
 *                              initialiseXdtm                               *
 *****************************************************************************/
private void initialiseXdtm()
{
  /* initialise the program */

  parsePreferences(topLevel);
  initAppManager(topLevel);
  initFileManager(topLevel);
}
