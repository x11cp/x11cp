/*****************************************************************************
 ** File          : doubleclick.c                                           **
 ** Purpose       : Initialise and Realise double click dialog options      **
 ** Author        : Jonathan Abbey ARL:UT                                   **
 ** Date          : Aug 1991                                                **
 ** Documentation : Xdtm Design Folder                                      **
 ** Related Files :                                                         **
 ** Changes       : 18-04-92, Edward Groenendaal                            **
 **                 Added #if NeedFunctionPrototypes stuff                  **
 **                 25-05-92, Edward Groenendaal                            **
 **                 Added support for multiple instances                    **
 **                 June 20, 1992, Ramon Santiago                           **
 **                 Changed all XtCreate calls to XtVaCreate calls.         **
 **                 Changed all caddr_t to XtPointer.                       **
 **                 Removed some lint.                                      **
 **                 Modified some variable names, and moved language        **
 **                 specific strings to app-defaults file.                  **
 ****************************************************************************/

/*
#define DEBUG 1
*/

#include "xdtm.h"
#include "menus.h"
#include "parse.h"		  /* AppProgram, etc. */
#include "Ext/appl.h"
#include <X11/Shell.h>
#include <X11/Xaw/Label.h>
#include <X11/Xaw/Command.h>
#include <X11/Xaw/AsciiText.h>
#include "Xedw/XedwForm.h"
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/time.h>
#include <grp.h>
#include <sys/param.h>
#include <pwd.h>
#include <unistd.h> /* for R_OK */
#ifdef alliant
#include <a.out.h>            /* Alliant FX series magic number file */
#endif

#ifndef NGROUPS
#define NGROUPS 255 /* Number of groups */
#endif

/* current_mode controls the icon display in the display manager.
   We need to change it so that we can get an icon in our
   doubleclick dialog box even if the display manager is in
   non-icon mode. */

extern Icon_mode current_mode;

/* The Filestatus structure is used to keep track of
   elements of filestat that the user is capable of
   changing through the doubleclick dialog box. */

typedef struct filestatus {
  u_short mode;
  char filename[80];
  char owner[16];
  char group[16];
} Filestatus;

/* applist is a linked list of nodes that keep track of information for
   process started from the doubleclick requester. */

typedef struct appnode {
  AppProgram node;
  struct appnode *next;
} AppNode;


/*  #define's for undo_func() */

#define RESET 0
#define UNDO  1

/* #define's for update_buttons() */

#define INIT 	0
#define UNDO    1
#define UPDATE  2
#define TEMP    3

/* Translation table for text widgets */

static char textWidgetTranslations [] = "#override\n\
    <Key>Return : no-op(RingBell)\n\
    <Key>Up : no-op()\n\
    <Key>Down : no-op()\n\
    <Key>space : no-op(RingBell)\n\
    <Btn1Motion> : no-op()\n\
    <Btn2Down> : no-op()\n\
    <Btn3Down> : no-op()\n\
    <Btn3Motion> : no-op()\n\
    <Btn3Up> : no-op()\n\
    <Btn1Up> : no-op()";  /* If we can't have a mac, then we can't have
			     any sort of drag selection at all.  It just
			     confuses our users. */

XtTranslations textWidgettranslations;

XawTextSelectType no_extended_select [] = {XawselectPosition,XawselectNull};

/* Define permission bits */

#define IREAD  0400
#define IWRITE 0200
#define IEXEC  0100
#define GREAD  0040
#define GWRITE 0020
#define GEXEC  0010
#define AREAD  0004
#define AWRITE 0002
#define AEXEC  0001

/* Button indices */

#define FIRSTBUTTON 0
#define LASTBUTTON  11

typedef enum {
  rown = 0, rgrp, roth, wown, wgrp, woth, xown, xgrp, xoth,
  uid_but, gid_but, stky_but
} Buttons;

struct buttontype {
  u_short bitmask;
  Widget  widget;
  int     state;
};

/* SetButton (button_enum) -- sets the bitmap imagery for the widget
   corresponding to the enum index b, according to the bit in filestat.st_mode
   defined in button[]. */

#define SetButton(b) \
{ \
  XtVaSetValues( \
      current->button[b].widget, \
          XtNbitmap, (current->filestat.st_mode & current->button[b].bitmask) ? tick : emptytick, \
	  NULL ) ; \
  current->button[b].state = (current->filestat.st_mode & current->button[b].bitmask); \
}


typedef struct fileinfo {
  /* Used for callbacks */
  int mode;

  /* Data about instance */
  struct stat filestat;
  AppNode *applist;
  Filestatus laststat;
  Filestatus oldstat;
  char *filedate;          /* 26 */
  char *filesize;          /* 10 */
  XdtmList *xdtmlist_element;
  char *edit_filename;     /* 255 */
  char *edit_owner;        /* 255 */
  char *edit_group;        /* 255 */
  char *dc_fullname;       /* 255 */
  char *dc_filename;       /* 255 */
  char *dc_path;           /* 255 */
  char *eb_input;          /* 255 */
  char *eb_output;         /* 255 */
  char *eb_error;          /* 255 */
  int file_editable;       /* FALSE */
  int file_edited;         /* FALSE */
  
  /* Widgets */
  Widget fileInfoDialog;
  Widget form;
  Widget form1;
  Widget form1a;
  Widget form1b;
  Widget form1c;
  Widget form2;
  Widget form2a;
  Widget form2a1;
  Widget form2a2;
  Widget form2a3;
  Widget form2a4;
  Widget form2a5;
  Widget form2a6;
  Widget form2b;
  Widget form3;
  
  
  /* form 1a widgets */

  Widget form1a_label_icon;
  Widget form1a_text_filename;
  Widget form1a_text_filename_src;

  /* form 1b widgets */

  Widget form1b_label_date;
  Widget form1b_label_size;
  Widget form1b_label_datelabel;
  Widget form1b_label_sizelabel;
  
  /* form 1c widgets */
  
  Widget form1c_label_owner;
  Widget form1c_text_owner;
  Widget form1c_text_owner_src;
  Widget form1c_label_group;
  Widget form1c_text_group;
  Widget form1c_text_group_src;
  
  /* form2a widgets */
  
  Widget form2a_label_permissions;
  
  Widget form2a1_label_blank;
  Widget form2a1_label_owner;
  Widget form2a1_label_group;
  Widget form2a1_label_other;
  
  Widget form2a2_label_read;
  Widget form2a2_button_rown;
  Widget form2a2_button_rgrp;
  Widget form2a2_button_roth;
  
  Widget form2a3_label_write;
  Widget form2a3_button_wown;
  Widget form2a3_button_wgrp;
  Widget form2a3_button_woth;
  
  Widget form2a4_label_exec;
  Widget form2a4_button_xown;
  Widget form2a4_button_xgrp;
  Widget form2a4_button_xoth;
  
  Widget form2a5_label_blank;
  Widget form2a5_label_setuid;
  Widget form2a5_label_setgid;
  Widget form2a5_label_sticky;
  
  Widget form2a6_label_blank;
  Widget form2a6_button_setuid;
  Widget form2a6_button_setgid;
  Widget form2a6_button_sticky;
  
  /* form 2b widgets */
  
  Widget form2b_command_update;
  Widget form2b_command_undo;
  Widget form2b_command_reset;
  
  /* form3 command widgets */
  
  Widget form3_command_done;
  Widget form3_command_view;
  Widget form3_command_edit;
  Widget form3_command_execwin;
  Widget form3_command_execback;
  Widget form3_command_help;
  
  /* Execute in background popup */
  
  Widget eb_popup;
  Widget ebform;
  Widget ebform1;
  Widget ebform2;
  Widget eblabeltitle;
  Widget eblabelsubtitle;
  Widget eblabelinput;
  Widget eblabeloutput;
  Widget eblabelerror;
  Widget ebtextinput;
  Widget ebtextoutput;
  Widget ebtexterror;
  Widget ebcommandcancel;
  Widget ebcommandok;
  
  /* Verify Quit Popup */
  
  Widget verify_popup;
  Widget verify_form;
  Widget verify_label;
  Widget verify_yes;
  Widget verify_no;
  Widget verify_cancel;

  struct buttontype button[LASTBUTTON+1];
} Fileinfo;

typedef struct {
  Fileinfo *fileinfo;
  int mode;
  Buttons select;
} InfoButtons;

typedef struct fileinfoelement {
  Fileinfo *fileinfo;
  struct fileinfoelement *next;
} Fileinfoelement;

Fileinfoelement *Fileinfolist;

Widget toplevel;

/* external and forward functions definitions */
#if NeedFunctionPrototypes
  void cancel_func(Widget, XtPointer, XtPointer);
  private AppProgram *compute_appnode(String, Fileinfo*);
  void dcbutton_toggled(Widget, InfoButtons *, XtPointer);
  void destroy_doubleclick_dialog(Widget, Fileinfo *, XtPointer);
  extern Boolean directoryManagerNewDirectory(String);
  extern void displayfile(String, String, String, XtGrabKind);
  extern void editfile(String);
  void execback_popdown(Widget, InfoButtons *, XtPointer);
  void execback_realize(Widget, Fileinfo *, XtPointer);
  public int execute (String, String, String, Boolean, AppProgram *);
  void execwin_func(Widget, Fileinfo *, XtPointer);
  void free_info(Widget, InfoButtons *, XtPointer);
  extern Boolean getIconType(String, String, XdtmList *);
  int group_number(char *);
  void help_func(Widget, XtPointer, XtPointer);
  public void ioerr_dialog(int);
  private void load_text(Widget, char *);
  public void popup_verify_app(AppProgram *);
  public void realize_dialog(Widget, Widget, XtGrabKind, XtEventHandler, XtPointer);
  public void run_app(AppProgram *, char *, char *, char *);
  extern void setCursor(Cursor);
  private void set_icon(Fileinfo*);
  private void set_size_date(Fileinfo*);
  void text_callback(Widget, Fileinfo *, XtPointer);
  void verify_callback(Widget, Fileinfo *, XtPointer);
  void undo_func(Widget, InfoButtons*, XtPointer);
  private void update_buttons(int, Fileinfo*);
  void update_func(Widget, Fileinfo *, XtPointer);
  int user_is_member(gid_t);
  void view_func(Widget, Fileinfo *, XtPointer);
#else
  void cancel_func();
  private AppProgram *compute_appnode();
  void dcbutton_toggled();
  void destroy_doubleclick_dialog();
  extern Boolean directoryManagerNewDirectory();
  extern void displayfile();
  extern void editfile();
  void execback_popdown();
  void execback_realize();
  public int execute ();
  void execwin_func();
  void free_info();
  extern Boolean getIconType();
  int group_number();
  void help_func();
  public void ioerr_dialog();
  private void load_text();
  public void popup_verify_app();
  public void realize_dialog();
  public void run_app();
  extern void setCursor();
  private void set_icon();
  private void set_size_date();
  void text_callback();
  void verify_callback();
  void undo_func();
  private void update_buttons();
  void update_func();
  int user_is_member();
  void view_func();
#endif

/*****************************************************************************
 *                          init_doubleclick_dialog                          *
 *****************************************************************************/
public void init_doubleclick(top)
Widget top;
{
  /* Not much to do here now.. just make sure we've got the top Widget ready
   * for later;
   */
  toplevel = top;
  Fileinfolist = NULL;
}

/*****************************************************************************
 *                               create_doubleclick                          *
 *****************************************************************************/
public Fileinfo *create_doubleclick()
{
  Fileinfo *current;
  InfoButtons *infobutton;

  static String permLabel = "Permissions";
  static String readLabel = "Read";
  static String writeLabel = "Write";
  static String execLabel = "Execute";
  static String ownerLabel = "Owner";
  static String groupLabel = "Group";
  static String otherLabel = "Other";

  /* OK lets create a new Fileinfo */
  if (Fileinfolist == NULL) {
    /* List is empty */
    Fileinfolist = (Fileinfoelement*) XtMalloc(sizeof(Fileinfoelement));
    current = Fileinfolist->fileinfo = (Fileinfo*) XtMalloc(sizeof(Fileinfo));
    Fileinfolist->next = NULL;
  } else {
    /* Find end of list */
    Fileinfoelement *p = Fileinfolist;
    while (p->next != NULL)
      p = p->next;
    p->next = (Fileinfoelement*) XtMalloc(sizeof(Fileinfoelement));
    current = p->next->fileinfo = (Fileinfo*) XtMalloc(sizeof(Fileinfo));
    p->next->next = NULL;
  }
  
  /* Initialise Fileinfo structure */
  current->filedate = (char*) XtMalloc(sizeof(char)*26);
  current->filesize = (char*) XtMalloc(sizeof(char)*10);
  current->edit_filename = (char*) XtMalloc(sizeof(char)*255);
  current->edit_owner = (char*) XtMalloc(sizeof(char)*255);
  current->edit_group = (char*) XtMalloc(sizeof(char)*255);
  current->dc_fullname = (char*) XtMalloc(sizeof(char)*255);
  current->dc_filename = (char*) XtMalloc(sizeof(char)*255);
  current->dc_path = (char*) XtMalloc(sizeof(char)*255);
  current->eb_input = (char*) XtMalloc(sizeof(char)*255);
  current->eb_output = (char*) XtMalloc(sizeof(char)*255);
  current->eb_error = (char*) XtMalloc(sizeof(char)*255);
  current->file_editable = FALSE;
  current->file_edited = FALSE;
  current->xdtmlist_element = (XdtmList*) XtMalloc(sizeof(XdtmList));

  /* Purify complains about uninitialized memory read, so initialize... */
  current->filedate[0] = 0;
  current->filesize[0] = 0;
  current->edit_filename[0] = 0; 
  current->edit_owner[0] = 0;
  current->edit_group[0] = 0;
  current->dc_fullname[0] = 0;
  current->xdtmlist_element->string = NULL;
  current->xdtmlist_element->icon = DUMMY;
  current->xdtmlist_element->user_data = NULL;
  
  textWidgettranslations = XtParseTranslationTable(textWidgetTranslations);

  /* Create main doubleclick pop-up shell widget */

  current->fileInfoDialog =
    XtVaCreatePopupShell(
        "fileInfoDialog",
	transientShellWidgetClass,
	toplevel,
	    NULL ) ;

  /* Create main form widget for popup */

  current->form =
    XtVaCreateManagedWidget(
        "form",
	xedwFormWidgetClass,
	current->fileInfoDialog,
	    NULL ) ;

  /* Form 1 */

  current->form1 =
    XtVaCreateManagedWidget(
        "form1",
	xedwFormWidgetClass,
	current->form,
	    XtNborderWidth, 1,
	    XtNfullWidth, True,
	    XtNresizable, True,
	    NULL ) ;

  /* Form 1a */

  current->form1a =
    XtVaCreateManagedWidget(
        "form1a",
	xedwFormWidgetClass,
	current->form1,
	    XtNfullHeight, True,
	    XtNfullWidth, False,
	    XtNborderWidth, 0,
	    XtNresizable, True,
	    NULL ) ;

  /** Widgets for Form 1a **/

  current->form1a_label_icon =
    XtVaCreateManagedWidget(
        "form1a_label_icon",
	labelWidgetClass,
	current->form1a,
	    XtNborderWidth, 0,
	    NULL ) ;

  current->form1a_text_filename =
    XtVaCreateManagedWidget(
        "form1a_text_filename",
	asciiTextWidgetClass,
	current->form1a,
	    XtNborderWidth, 1,
	    XtNfromVert, current->form1a_label_icon,
	    XtNstring, current->edit_filename,
	    XtNeditType, XawtextEdit,
	    XtNselectTypes, no_extended_select,
	    XtNlength, 255,
	    XtNuseStringInPlace, True,
	    NULL ) ;

  XtVaGetValues(
      current->form1a_text_filename,
          XtNtextSource, &(current->form1a_text_filename_src),
	  NULL ) ;

  XtAddCallback(current->form1a_text_filename_src, XtNcallback,
		(XtCallbackProc)text_callback, (XtPointer)current);
  XtOverrideTranslations(current->form1a_text_filename,
			 textWidgettranslations);

  /* Form 1b */

  current->form1b =
    XtVaCreateManagedWidget(
        "form1b",
	xedwFormWidgetClass,
	current->form1,
	    XtNfullHeight, True,
	    XtNborderWidth, 0,
	    XtNfromHoriz, current->form1a,
	    XtNresizable, True,
	    NULL ) ;

  /** Widgets for Form 1b **/

  current->form1b_label_sizelabel =
    XtVaCreateManagedWidget(
        "form1b_label_sizelabel",
	labelWidgetClass,
	current->form1b,
	    XtNlabel, "File Size: ",
	    XtNborderWidth, 0,
	    NULL ) ;

  current->form1b_label_size =
    XtVaCreateManagedWidget(
        "form1b_label_size",
	labelWidgetClass,
	current->form1b,
	    XtNlabel, current->filesize,
	    XtNborderWidth, 0,
	    XtNfromHoriz, current->form1b_label_sizelabel,
	    XtNjustify, XtJustifyLeft,
	    XtNinternalWidth, 0,
	    XtNresizable, True,
	    NULL ) ;

  current->form1b_label_datelabel =
    XtVaCreateManagedWidget(
        "form1b_label_datelabel",
	labelWidgetClass,
	current->form1b,
	    XtNlabel, "File Date: ",
	    XtNborderWidth, 0,
	    XtNfromVert, current->form1b_label_sizelabel,
	    NULL ) ;

  current->form1b_label_date =
    XtVaCreateManagedWidget(
        "form1b_label_date",
	labelWidgetClass,
	current->form1b,
	    XtNlabel, current->filedate,
	    XtNborderWidth, 0,
	    XtNfromVert, current->form1b_label_sizelabel,
	    XtNfromHoriz, current->form1b_label_datelabel,
	    XtNjustify, XtJustifyLeft,
	    XtNinternalWidth, 0,
	    NULL ) ;

  /* Form 1c */

  current->form1c =
    XtVaCreateManagedWidget(
        "form1b",
	xedwFormWidgetClass,
	current->form1,
	    XtNfullHeight, True,
	    XtNborderWidth, 0,
	    XtNfromHoriz, current->form1b,
	    XtNresizable, True,
	    NULL ) ;

  /** Widgets for Form 1c **/

  current->form1c_label_owner =
    XtVaCreateManagedWidget(
        "form1c_label_owner",
	labelWidgetClass,
	current->form1c,
	    XtNborderWidth, 0,
	    XtNlabel, "Owner:",
	    XtNjustify, XtJustifyRight,
	    NULL ) ;

  current->form1c_text_owner =
    XtVaCreateManagedWidget(
        "form1c_text_owner",
	asciiTextWidgetClass,
	current->form1c,
	    XtNborderWidth, 1,
	    XtNfromHoriz, current->form1c_label_owner,
	    XtNstring, current->edit_owner,
	    XtNeditType, XawtextEdit,
	    XtNlength, 255,
	    XtNuseStringInPlace, True,
	    XtNselectTypes, no_extended_select,
	    NULL ) ;

  XtVaGetValues(
      current->form1c_text_owner,
          XtNtextSource, &(current->form1c_text_owner_src),
	  NULL ) ;

  XtAddCallback(current->form1c_text_owner_src, XtNcallback,
		(XtCallbackProc)text_callback, (XtPointer)current);
  XtOverrideTranslations(current->form1c_text_owner, textWidgettranslations);

  current->form1c_label_group =
    XtVaCreateManagedWidget(
        "form1c_label_group",
	labelWidgetClass,
	current->form1c,
	    XtNborderWidth, 0,
	    XtNfromVert, current->form1c_label_owner,
	    XtNlabel, "Group:",
	    XtNjustify, XtJustifyRight,
	    NULL ) ;

  current->form1c_text_group =
    XtVaCreateManagedWidget(
        "form1c_text_group",
	asciiTextWidgetClass,
        current->form1c,
	    XtNborderWidth, 1,
	    XtNfromHoriz, current->form1c_label_group,
	    XtNfromVert, current->form1c_text_owner,
	    XtNselectTypes, no_extended_select,
	    XtNstring, current->edit_group,
	    XtNeditType, XawtextEdit,
	    XtNlength, 255,
	    XtNuseStringInPlace, True,
	    NULL ) ;

  XtVaGetValues(
      current->form1c_text_group,
          XtNtextSource, &(current->form1c_text_group_src),
	  NULL ) ;

  XtAddCallback(current->form1c_text_group_src, XtNcallback,
		(XtCallbackProc)text_callback, (XtPointer)current);
  XtOverrideTranslations(current->form1c_text_group, textWidgettranslations);

  /* Form 2 */

  current->form2 =
    XtVaCreateManagedWidget(
        "form2",
	xedwFormWidgetClass,
	current->form,
	    XtNfullWidth, True,
	    XtNfromVert, current->form1,
	    XtNborderWidth, 0,
	    NULL ) ;

  /** Widgets for Form 2 **/

  current->form2a =
    XtVaCreateManagedWidget(
        "form2a",
	xedwFormWidgetClass,
	current->form2,
	    XtNfullHeight, True,
	    XtNborderWidth, 1,
	    NULL ) ;

  /** Widgets for Form 2a */

  current->form2a_label_permissions =
    XtVaCreateManagedWidget(
        "form2a_label_permissions",
	labelWidgetClass,
	current->form2a,
	    XtNlabel, permLabel,
	    XtNborderWidth, 0,
	    XtNfullWidth, True,
	    NULL );

  /* Form 2a1 */

  current->form2a1 =
    XtVaCreateManagedWidget(
        "form2a1",
	xedwFormWidgetClass,
	current->form2a,
	    XtNborderWidth, 0,
	    XtNfromVert, current->form2a_label_permissions,
	    NULL ) ;

  /** Widgets for Form 2a1 **/

  current->form2a1_label_blank =
    XtVaCreateManagedWidget(
        "form2a1_label_blank",
	labelWidgetClass,
	current->form2a1,
	    XtNlabel, " ", /* Empty label to hold space in form */
	    XtNborderWidth, 0,
	    NULL ) ;

  current->form2a1_label_owner =
    XtVaCreateManagedWidget(
        "form2a1_label_owner",
	labelWidgetClass,
	current->form2a1,
	    XtNlabel, ownerLabel,
	    XtNfromVert, current->form2a1_label_blank,
	    XtNborderWidth, 0,
	    NULL ) ;

  current->form2a1_label_group =
    XtVaCreateManagedWidget(
        "form2a1_label_owner",
	labelWidgetClass,
	current->form2a1,
	    XtNlabel, groupLabel,
	    XtNfromVert, current->form2a1_label_owner,
	    XtNborderWidth, 0,
	    NULL ) ;

  current->form2a1_label_other =
    XtVaCreateManagedWidget(
        "form2a1_label_other",
	labelWidgetClass,
	current->form2a1,
	    XtNlabel, otherLabel,
	    XtNborderWidth, 0,
	    XtNfromVert, current->form2a1_label_group,
	    NULL ) ;

  /* Form 2a2 */

  current->form2a2 =
    XtVaCreateManagedWidget(
        "form2a2",
	xedwFormWidgetClass,
	current->form2a,
            XtNfromHoriz, current->form2a1,
	    XtNfromVert, current->form2a_label_permissions,
	    XtNborderWidth, 0,
	    NULL ) ;

  /** Widgets for Form 2a2 **/

  current->form2a2_label_read =
    XtVaCreateManagedWidget(
        "form2a2_label_read",
	labelWidgetClass,
	current->form2a2,
	    XtNlabel, readLabel,
	    XtNborderWidth, 0,
	    XtNjustify, XtJustifyLeft,
	    XtNinternalWidth, 0,
	    NULL ) ;

  current->form2a2_button_rown =
    XtVaCreateManagedWidget(
        "form2a2_button_rown",
        commandWidgetClass,
	current->form2a2,
	    XtNfromVert, current->form2a2_label_read,
	    NULL ) ;

  infobutton = (InfoButtons*) XtMalloc(sizeof(InfoButtons));
  infobutton->fileinfo = current;
  infobutton->select = rown;
  XtAddCallback(current->form2a2_button_rown, XtNcallback,
		(XtCallbackProc)dcbutton_toggled, (XtPointer)infobutton);
  XtAddCallback(current->form2a2_button_rown, XtNdestroyCallback,
		(XtCallbackProc)free_info, (XtPointer)infobutton);

  current->form2a2_button_rgrp =
    XtVaCreateManagedWidget(
        "form2a2_button_rgrp",
	commandWidgetClass,
	current->form2a2,
	    XtNfromVert, current->form2a2_button_rown,
	    NULL ) ;

  infobutton = (InfoButtons*) XtMalloc(sizeof(InfoButtons));
  infobutton->fileinfo = current;
  infobutton->select = rgrp;
  XtAddCallback(current->form2a2_button_rgrp, XtNcallback,
		(XtCallbackProc)dcbutton_toggled, (XtPointer)infobutton);
  XtAddCallback(current->form2a2_button_rgrp, XtNdestroyCallback,
		(XtCallbackProc)free_info, (XtPointer)infobutton);

  current->form2a2_button_roth =
    XtVaCreateManagedWidget(
        "form2a2_button_roth",
	commandWidgetClass,
	current->form2a2,
	    XtNfromVert, current->form2a2_button_rgrp,
	    NULL ) ;

  infobutton = (InfoButtons*) XtMalloc(sizeof(InfoButtons));
  infobutton->fileinfo = current;
  infobutton->select = roth;
  XtAddCallback(current->form2a2_button_roth, XtNcallback,
		(XtCallbackProc)dcbutton_toggled, (XtPointer)infobutton);
  XtAddCallback(current->form2a2_button_roth, XtNdestroyCallback,
		(XtCallbackProc)free_info, (XtPointer)infobutton);

  /* Form 2a3 */

  current->form2a3 =
    XtVaCreateManagedWidget(
        "form2a3",
	xedwFormWidgetClass,
	current->form2a,
	    XtNfromHoriz, current->form2a2,
	    XtNfromVert, current->form2a_label_permissions,
	    XtNborderWidth, 0,
	    NULL ) ;

  /** Widgets for Form 2a3 **/

  current->form2a3_label_write =
    XtVaCreateManagedWidget(
        "form2a3_label_write",
	labelWidgetClass,
	current->form2a3,
	    XtNlabel, writeLabel,
	    XtNjustify, XtJustifyLeft,
	    XtNborderWidth, 0,
	    XtNinternalWidth, 0,
	    NULL ) ;

  current->form2a3_button_wown =
    XtVaCreateManagedWidget(
        "form2a3_button_wown",
	commandWidgetClass,
	current->form2a3,
	    XtNfromVert, current->form2a3_label_write,
	    NULL ) ;

  infobutton = (InfoButtons*) XtMalloc(sizeof(InfoButtons));
  infobutton->fileinfo = current;
  infobutton->select = wown;
  XtAddCallback(current->form2a3_button_wown, XtNcallback,
		(XtCallbackProc)dcbutton_toggled, (XtPointer)infobutton);
  XtAddCallback(current->form2a3_button_wown, XtNdestroyCallback,
		(XtCallbackProc)free_info, (XtPointer)infobutton);

  current->form2a3_button_wgrp =
    XtVaCreateManagedWidget(
        "form2a3_button_wgrp",
	commandWidgetClass,
	current->form2a3,
	    XtNfromVert, current->form2a3_button_wown,
	    NULL ) ;

  infobutton = (InfoButtons*) XtMalloc(sizeof(InfoButtons));
  infobutton->fileinfo = current;
  infobutton->select = wgrp;
  XtAddCallback(current->form2a3_button_wgrp, XtNcallback,
		(XtCallbackProc)dcbutton_toggled, (XtPointer)infobutton);
  XtAddCallback(current->form2a3_button_wgrp, XtNdestroyCallback,
		(XtCallbackProc)free_info, (XtPointer)infobutton);

  current->form2a3_button_woth =
    XtVaCreateManagedWidget(
        "form2a3_button_woth",
	commandWidgetClass,
	current->form2a3,
	    XtNfromVert, current->form2a3_button_wgrp,
	    NULL ) ;

  infobutton = (InfoButtons*) XtMalloc(sizeof(InfoButtons));
  infobutton->fileinfo = current;
  infobutton->select = woth;
  XtAddCallback(current->form2a3_button_woth, XtNcallback,
		(XtCallbackProc)dcbutton_toggled, (XtPointer)infobutton);
  XtAddCallback(current->form2a3_button_woth, XtNdestroyCallback,
		(XtCallbackProc)free_info, (XtPointer)infobutton);

  /* Form 2a4 */

  current->form2a4 =
    XtVaCreateManagedWidget(
        "form2a4",
	xedwFormWidgetClass,
	current->form2a,
	    XtNfromHoriz, current->form2a3,
	    XtNfromVert, current->form2a_label_permissions,
	    XtNborderWidth, 0,
	    NULL ) ;

  /** Widgets for Form 2a4 **/

  current->form2a4_label_exec =
    XtVaCreateManagedWidget(
        "form2a4_label_exec",
	labelWidgetClass,
	current->form2a4,
	    XtNlabel, execLabel,
	    XtNborderWidth, 0,
	    XtNjustify, XtJustifyLeft,
	    XtNinternalWidth, 0,
	    NULL ) ;

  current->form2a4_button_xown =
    XtVaCreateManagedWidget(
        "form2a4_button_xown",
	commandWidgetClass,
	current->form2a4,
	    XtNfromVert, current->form2a4_label_exec,
	    NULL ) ;

  infobutton = (InfoButtons*) XtMalloc(sizeof(InfoButtons));
  infobutton->fileinfo = current;
  infobutton->select = xown;
  XtAddCallback(current->form2a4_button_xown, XtNcallback,
		(XtCallbackProc)dcbutton_toggled, (XtPointer)infobutton);
  XtAddCallback(current->form2a4_button_xown, XtNdestroyCallback,
		(XtCallbackProc)free_info, (XtPointer)infobutton);

  current->form2a4_button_xgrp =
    XtVaCreateManagedWidget(
        "form2a4_button_xgrp",
	commandWidgetClass,
	current->form2a4,
            XtNfromVert, current->form2a4_button_xown,
	    NULL ) ;

  infobutton = (InfoButtons*) XtMalloc(sizeof(InfoButtons));
  infobutton->fileinfo = current;
  infobutton->select = xgrp;
  XtAddCallback(current->form2a4_button_xgrp, XtNcallback,
		(XtCallbackProc)dcbutton_toggled, (XtPointer)infobutton);
  XtAddCallback(current->form2a4_button_xgrp, XtNdestroyCallback,
		(XtCallbackProc)free_info, (XtPointer)infobutton);

  current->form2a4_button_xoth =
    XtVaCreateManagedWidget(
        "form2a4_button_xoth",
	commandWidgetClass,
	current->form2a4,
	    XtNfromVert, current->form2a4_button_xgrp,
	    NULL ) ;

  infobutton = (InfoButtons*) XtMalloc(sizeof(InfoButtons));
  infobutton->fileinfo = current;
  infobutton->select = xoth;
  XtAddCallback(current->form2a4_button_xoth, XtNcallback,
		(XtCallbackProc)dcbutton_toggled, (XtPointer)infobutton);
  XtAddCallback(current->form2a4_button_xoth, XtNdestroyCallback,
		(XtCallbackProc)free_info, (XtPointer)infobutton);

  /* Form 2a5 */

  current->form2a5 =
    XtVaCreateManagedWidget(
        "form2a5",
	xedwFormWidgetClass,
	current->form2a,
	    XtNfromHoriz, current->form2a4,
	    XtNfromVert, current->form2a_label_permissions,
	    XtNborderWidth, 0,
	    NULL ) ;

  /** Widgets for Form 2a5 **/

  current->form2a5_label_blank =
    XtVaCreateManagedWidget(
        "form2a5_label_blank",
	labelWidgetClass,
	current->form2a5,
	    XtNlabel," ", /* Empty label to hold space in form */
	    XtNborderWidth, 0,
	    NULL ) ;

  current->form2a5_label_setuid =
    XtVaCreateManagedWidget(
        "form2a5_label_setuid",
	labelWidgetClass,
	current->form2a5,
	    XtNlabel, "SetUID",
	    XtNfromVert, current->form2a5_label_blank,
	    XtNborderWidth, 0,
	    NULL ) ;

  current->form2a5_label_setgid =
    XtVaCreateManagedWidget(
        "form2a5_label_setgid",
	labelWidgetClass,
	current->form2a5,
	    XtNlabel, "SetGID",
	    XtNfromVert, current->form2a5_label_setuid,
	    XtNborderWidth, 0,
	    NULL );

  current->form2a5_label_sticky =
    XtVaCreateManagedWidget(
        "form2a5_label_sticky",
	labelWidgetClass,
	current->form2a5,
	    XtNlabel, "Sticky",
	    XtNfromVert, current->form2a5_label_setgid,
	    XtNborderWidth, 0,
	    NULL );

  /* Form 2a6 */

  current->form2a6 =
    XtVaCreateManagedWidget(
        "form2a6",
        xedwFormWidgetClass,
	current->form2a,
            XtNfromHoriz, current->form2a5,
	    XtNfromVert, current->form2a_label_permissions,
	    XtNborderWidth, 0,
	    NULL ) ;

  /** Widgets for Form 2a6 **/

  current->form2a6_label_blank =
    XtVaCreateManagedWidget(
        "form2a6_label_blank",
	labelWidgetClass,
	current->form2a6,
	    XtNlabel," ", /* Empty label to hold space in form */
	    XtNborderWidth, 0,
	    NULL ) ;

  current->form2a6_button_setuid =
    XtVaCreateManagedWidget(
        "form2a6_button_setuid",
	commandWidgetClass,
	current->form2a6,
	    XtNfromVert, current->form2a6_label_blank,
	    NULL ) ;

  infobutton = (InfoButtons*) XtMalloc(sizeof(InfoButtons));
  infobutton->fileinfo = current;
  infobutton->select = uid_but;
  XtAddCallback(current->form2a6_button_setuid, XtNcallback,
		(XtCallbackProc)dcbutton_toggled, (XtPointer)infobutton);
  XtAddCallback(current->form2a6_button_setuid, XtNdestroyCallback,
		(XtCallbackProc)free_info, (XtPointer)infobutton);

  current->form2a6_button_setgid =
    XtVaCreateManagedWidget(
        "form2a6_button_setgid",
	commandWidgetClass,
	current->form2a6,
	    XtNfromVert, current->form2a6_button_setuid,
	    NULL ) ;

  infobutton = (InfoButtons*) XtMalloc(sizeof(InfoButtons));
  infobutton->fileinfo = current;
  infobutton->select = gid_but;
  XtAddCallback(current->form2a6_button_setgid, XtNcallback,
		(XtCallbackProc)dcbutton_toggled, (XtPointer)infobutton);
  XtAddCallback(current->form2a6_button_setgid, XtNdestroyCallback,
		(XtCallbackProc)free_info, (XtPointer)infobutton);

  current->form2a6_button_sticky =
    XtVaCreateManagedWidget(
        "form2a6_button_sticky",
	commandWidgetClass,
	current->form2a6,
	    XtNfromVert, current->form2a6_button_setgid,
	    NULL ) ;

  infobutton = (InfoButtons*) XtMalloc(sizeof(InfoButtons));
  infobutton->fileinfo = current;
  infobutton->select = stky_but;
  XtAddCallback(current->form2a6_button_sticky, XtNcallback,
		(XtCallbackProc)dcbutton_toggled, (XtPointer)infobutton);
  XtAddCallback(current->form2a6_button_sticky, XtNdestroyCallback,
		(XtCallbackProc)free_info, (XtPointer)infobutton);

  /* Form 2b */

  current->form2b =
    XtVaCreateManagedWidget(
        "form2b",
	xedwFormWidgetClass,
	current->form2,
	    XtNfullHeight, True,
	    XtNfromHoriz, current->form2a,
	    XtNborderWidth, 0,
	    XtNheightLinked, current->form2a6,
	    NULL ) ;

  /** Widgets for Form 2b **/

  current->form2b_command_update =
    XtVaCreateManagedWidget(
        "form2b_command_update",
	commandWidgetClass,
	current->form2b,
	    XtNlabel, "Update",
	    XtNfullWidth, True,
	    NULL ) ;

  XtAddCallback(current->form2b_command_update, XtNcallback,
		(XtCallbackProc)update_func, (XtPointer)current);

  current->form2b_command_undo =
    XtVaCreateManagedWidget(
        "form2b_command_undo",
	commandWidgetClass,
	current->form2b,
	    XtNlabel, "Undo",
	    XtNfromVert, current->form2b_command_update,
	    XtNfullWidth, True,
	    NULL ) ;

  infobutton = (InfoButtons*) XtMalloc(sizeof(InfoButtons));
  infobutton->fileinfo = current;
  infobutton->mode = UNDO;
  XtAddCallback(current->form2b_command_undo, XtNcallback,
		(XtCallbackProc)undo_func, (XtPointer)infobutton);
  XtAddCallback(current->form2b_command_undo, XtNdestroyCallback,
		(XtCallbackProc)free_info, (XtPointer)infobutton);

  current->form2b_command_reset =
    XtVaCreateManagedWidget(
        "form2b_command_reset",
	commandWidgetClass,
	current->form2b,
	    XtNlabel, "Reset",
	    XtNfromVert, current->form2b_command_undo,
	    XtNfullWidth, True,
	    NULL ) ;

  infobutton = (InfoButtons*) XtMalloc(sizeof(InfoButtons));
  infobutton->fileinfo = current;
  infobutton->mode = RESET;
  XtAddCallback(current->form2b_command_reset, XtNcallback,
		(XtCallbackProc)undo_func, (XtPointer)infobutton);
  XtAddCallback(current->form2b_command_reset, XtNdestroyCallback,
		(XtCallbackProc)free_info, (XtPointer)infobutton);

  /* Form 3 */

  current->form3 =
    XtVaCreateManagedWidget(
        "form3",
        xedwFormWidgetClass,
	current->form,
	    XtNfullWidth, True,
	    XtNborderWidth, 1,
	    XtNfromVert, current->form2,
	    NULL ) ;

  /** Widgets for Form 3 **/

  current->form3_command_done =
    XtVaCreateManagedWidget(
        "form3_command_done",
	commandWidgetClass,
	current->form3,
	    XtNlabel, "DONE",
	    NULL ) ;

  XtAddCallback(current->form3_command_done, XtNcallback,
		(XtCallbackProc)destroy_doubleclick_dialog,
		(XtPointer)current);

  current->form3_command_view =
    XtVaCreateManagedWidget(
        "form3_command_view",
	commandWidgetClass,
	current->form3,
	    XtNlabel, " View ",
	    XtNfromHoriz, current->form3_command_done,
	    NULL ) ;

  XtAddCallback(current->form3_command_view, XtNcallback,
		(XtCallbackProc)view_func, (XtPointer)current);

  current->form3_command_edit =
    XtVaCreateManagedWidget(
        "form3_command_edit",
	commandWidgetClass,
	current->form3,
	    XtNlabel, " Edit ",
	    XtNfromHoriz, current->form3_command_view,
	    NULL ) ;

  XtAddCallback(current->form3_command_edit, XtNcallback,
		(XtCallbackProc)view_func, (XtPointer)current);

  current->form3_command_execwin =
    XtVaCreateManagedWidget(
        "form3_command_execwin",
	commandWidgetClass,
	current->form3,
	    XtNlabel, "Execute in Window",
	    XtNfromHoriz, current->form3_command_edit,
	    NULL ) ;

  XtAddCallback(current->form3_command_execwin, XtNcallback,
		(XtCallbackProc)execwin_func, (XtPointer)current);

  current->form3_command_execback =
    XtVaCreateManagedWidget(
        "form3_command_execback",
	commandWidgetClass,
	current->form3,
	    XtNlabel, "Execute in Background",
	    XtNfromHoriz, current->form3_command_execwin,
	    NULL ) ;

  XtAddCallback(current->form3_command_execback, XtNcallback,
		(XtCallbackProc)execback_realize, (XtPointer)current);

  current->form3_command_help =
    XtVaCreateManagedWidget(
        "form3_command_help",
	commandWidgetClass,
	current->form3,
	    XtNlabel, "Help",
	    XtNfromHoriz, current->form3_command_execback,
	    NULL ) ;

  XtAddCallback(current->form3_command_help, XtNcallback,
		(XtCallbackProc)help_func, (XtPointer)current);

  /* Execute in background popup initialization */

  current->eb_popup =
    XtVaCreatePopupShell(
        "eb_popup",
	transientShellWidgetClass,
	current->fileInfoDialog,
	    NULL ) ;

  current->ebform =
    XtVaCreateManagedWidget(
        "ebform",
	xedwFormWidgetClass,
	current->eb_popup,
	    XtNborderWidth, 1,
	    NULL ) ;

  current->eblabeltitle =
    XtVaCreateManagedWidget(
        "eblabeltitle",
	labelWidgetClass,
	current->ebform,
	    XtNlabel, "Execute in Background",
	    XtNjustify, XtJustifyCenter,
	    XtNborderWidth, 1,
	    XtNfullWidth, True,
	    NULL ) ;

  current->eblabelsubtitle =
    XtVaCreateManagedWidget(
        "eblabelsubtitle",
	labelWidgetClass,
	current->ebform,
	    XtNlabel, "Set I/O Redirection",
	    XtNfromVert, current->eblabeltitle,
	    XtNjustify, XtJustifyCenter,
	    XtNborderWidth, 0,
	    XtNfullWidth, True,
	    NULL ) ;

  current->ebform1 =
    XtVaCreateManagedWidget(
        "ebform1",
	xedwFormWidgetClass,
	current->ebform,
	    XtNborderWidth, 0,
	    XtNfromVert, current->eblabelsubtitle,
	    NULL ) ;

  current->ebform2 =
    XtVaCreateManagedWidget(
        "ebform2",
	xedwFormWidgetClass,
	current->ebform,
	    XtNborderWidth, 0,
	    XtNfromHoriz, current->ebform1,
	    XtNfromVert, current->eblabelsubtitle,
	    XtNheightLinked, current->ebform1,
	    NULL ) ;

  current->eblabelinput =
    XtVaCreateManagedWidget(
        "eblabelinput",
	labelWidgetClass,
	current->ebform1,
	    XtNlabel, "Input:",
	    XtNborderWidth, 0,
	    XtNjustify, XtJustifyRight,
	    XtNfullWidth, True,
	    NULL ) ;

  strcpy(current->eb_input, "/dev/null");
  strcpy(current->eb_output, current->eb_input);
  strcpy(current->eb_error, current->eb_input);

  current->ebtextinput =
    XtVaCreateManagedWidget(
        "ebtextinput",
	asciiTextWidgetClass,
	current->ebform2,
            XtNstring, current->eb_input,
	    XtNeditType, XawtextEdit,
	    XtNlength, 255,
	    XtNuseStringInPlace, True,
	    NULL ) ;

  XtOverrideTranslations(current->ebtextinput, textWidgettranslations);

  current->eblabeloutput =
    XtVaCreateManagedWidget(
        "eblabeloutput",
	labelWidgetClass,
	current->ebform1,
	    XtNlabel, "Output:",
	    XtNfromVert, current->eblabelinput,
	    XtNfullWidth, True,
	    XtNjustify, XtJustifyRight,
	    XtNborderWidth, 0,
	    NULL ) ;

  current->ebtextoutput =
    XtVaCreateManagedWidget(
        "ebtextoutput",
	asciiTextWidgetClass,
	current->ebform2,
	    XtNstring, current->eb_output,
	    XtNeditType, XawtextEdit,
	    XtNfromVert, current->ebtextinput,
	    XtNlength, 255,
	    XtNuseStringInPlace, True,
	    NULL ) ;

  XtOverrideTranslations(current->ebtextoutput, textWidgettranslations);

  current->eblabelerror =
    XtVaCreateManagedWidget(
        "eblabelerror",
	labelWidgetClass,
	current->ebform1,
	    XtNlabel, "Error:",
	    XtNfromVert, current->eblabeloutput,
	    XtNfullWidth, True,
	    XtNjustify, XtJustifyRight,
	    XtNborderWidth, 0,
	    NULL ) ;

  current->ebtexterror =
    XtVaCreateManagedWidget(
        "ebtexterror",
	asciiTextWidgetClass,
	current->ebform2,
	    XtNfromVert, current->ebtextoutput,
	    XtNstring, current->eb_error,
	    XtNeditType, XawtextEdit,
	    XtNlength, 255,
	    XtNuseStringInPlace, True,
	    NULL ) ;

  XtOverrideTranslations(current->ebtexterror, textWidgettranslations);

  current->ebcommandok =
    XtVaCreateManagedWidget(
        "ebcommandok",
	commandWidgetClass,
	current->ebform,
            XtNfromVert, current->ebform2,
	    XtNlabel, "OK",
	    NULL ) ;

  infobutton = (InfoButtons*) XtMalloc(sizeof(InfoButtons));
  infobutton->fileinfo = current;
  infobutton->select = 1;
  XtAddCallback(current->ebcommandok, XtNcallback,
		(XtCallbackProc)execback_popdown, (XtPointer)infobutton);
  XtAddCallback(current->ebcommandok, XtNdestroyCallback,
		(XtCallbackProc)free_info, (XtPointer)infobutton);

  current->ebcommandcancel =
    XtVaCreateManagedWidget(
        "ebcommandcancel",
	commandWidgetClass,
	current->ebform,
	    XtNfromVert, current->ebform1,
	    XtNfromHoriz, current->ebcommandok,
	    XtNlabel, "Cancel",
	    NULL ) ;

  infobutton = (InfoButtons*) XtMalloc(sizeof(InfoButtons));
  infobutton->fileinfo = current;
  infobutton->select = 0;
  XtAddCallback(current->ebcommandcancel, XtNcallback,
		(XtCallbackProc)execback_popdown, (XtPointer)infobutton);
  XtAddCallback(current->ebcommandcancel, XtNdestroyCallback,
		(XtCallbackProc)free_info, (XtPointer)infobutton);
  
  /* Verify Quit PopUp */

  current->verify_popup =
    XtVaCreatePopupShell(
        "Verify Done",
	transientShellWidgetClass,
        current->fileInfoDialog,
	    NULL ) ;

  current->verify_form =
    XtVaCreateManagedWidget(
        "verify_form",
	xedwFormWidgetClass,
	current->verify_popup,
	    XtNborderWidth, 1,
	    NULL ) ;

  current->verify_label =
    XtVaCreateManagedWidget(
        "verify_label",
	labelWidgetClass,
	current->verify_form,
	    XtNlabel, "Save changes?",
	    XtNborderWidth, 0,
	    NULL ) ;

  current->verify_yes =
    XtVaCreateManagedWidget(
        "verify_yes",
	commandWidgetClass,
	current->verify_form,
	    XtNlabel, "Yes",
	    XtNborderWidth, 1,
	    XtNfromVert, current->verify_label,
	    NULL ) ;

  XtAddCallback(current->verify_yes, XtNcallback,
		(XtCallbackProc)verify_callback, (XtPointer)current);

  current->verify_no =
    XtVaCreateManagedWidget(
        "verify_no",
	commandWidgetClass,
	current->verify_form,
	    XtNlabel, "No",
	    XtNborderWidth, 1,
	    XtNfromVert, current->verify_label,
	    XtNfromHoriz, current->verify_yes,
	    NULL ) ;

  XtAddCallback(current->verify_no, XtNcallback,
		(XtCallbackProc)verify_callback, (XtPointer)current);

  current->verify_cancel =
    XtVaCreateManagedWidget(
        "verify_cancel",
	commandWidgetClass,
	current->verify_form,
	    XtNlabel, "Cancel",
	    XtNborderWidth, 1,
	    XtNfromVert, current->verify_label,
	    XtNfromHoriz, current->verify_no,
	    NULL ) ;

  XtAddCallback(current->verify_cancel, XtNcallback,
		(XtCallbackProc)verify_callback, (XtPointer)current);

  current->button[rown].bitmask = IREAD;
  current->button[rown].widget = current->form2a2_button_rown;
  current->button[rown].state = FALSE;
  current->button[rgrp].bitmask = GREAD;
  current->button[rgrp].widget = current->form2a2_button_rgrp;
  current->button[rgrp].state = FALSE;
  current->button[roth].bitmask = AREAD;
  current->button[roth].widget = current->form2a2_button_roth;
  current->button[roth].state = FALSE;
  current->button[wown].bitmask = IWRITE;
  current->button[wown].widget = current->form2a3_button_wown;
  current->button[wown].state = FALSE;
  current->button[wgrp].bitmask = GWRITE;
  current->button[wgrp].widget = current->form2a3_button_wgrp;
  current->button[wgrp].state = FALSE;
  current->button[woth].bitmask = AWRITE;
  current->button[woth].widget = current->form2a3_button_woth;
  current->button[woth].state = FALSE;
  current->button[xown].bitmask = IEXEC;
  current->button[xown].widget = current->form2a4_button_xown;
  current->button[xown].state = FALSE;
  current->button[xgrp].bitmask = GEXEC;
  current->button[xgrp].widget = current->form2a4_button_xgrp;
  current->button[xgrp].state = FALSE;
  current->button[xoth].bitmask = AEXEC;
  current->button[xoth].widget = current->form2a4_button_xoth;
  current->button[xoth].state = FALSE;
  current->button[uid_but].bitmask = S_ISUID;
  current->button[uid_but].widget = current->form2a6_button_setuid;
  current->button[uid_but].state = FALSE;
  current->button[gid_but].bitmask = S_ISGID;
  current->button[gid_but].widget = current->form2a6_button_setgid;
  current->button[gid_but].state = FALSE;
  current->button[stky_but].bitmask = S_ISVTX;
  current->button[stky_but].widget = current->form2a6_button_sticky;
  current->button[stky_but].state = FALSE;

  /* Initialize applist */

  current->applist = NULL;

  return(current);
}

/*****************************************************************************
 *                         WM_destroy_doubleclick_dialog                     *
 *****************************************************************************/
#if NeedFunctionPrototypes
private void WM_destroy_doubleclick_dialog(Widget w,
					   Fileinfo *current,
					   XEvent *event,
					   Boolean *dispatch)
#else
private void WM_destroy_doubleclick_dialog(w, current, event, dispatch)
     Widget w;
     Fileinfo *current;
     XEvent *event;
     Boolean *dispatch;
#endif
{
    extern Atom protocols[2]; /* defined in dialogs.c */
    
    if ((event->xclient.message_type == protocols[1]) &&
	(event->xclient.data.l[0] == protocols[0]))
    /* the widget got a kill signal */
    {
	/* Directly call callback of Done button */
	destroy_doubleclick_dialog(current->form3_command_done, current, NULL);
    }
}

/*****************************************************************************
 *                            doubleclick_dialog                             *
 *****************************************************************************/
public void doubleclick_dialog(filename, path)
String filename, path;
{
  Fileinfo *current;

  current = create_doubleclick();
  
  strcpy(current->dc_filename, filename);
  strcpy(current->dc_path, path);
  if (strcmp(current->dc_path, "/") != 0)
    strcat(current->dc_path, "/");

  strcpy(current->dc_fullname,current->dc_path);
  strcat(current->dc_fullname,filename);

  strcpy(current->edit_filename, filename);

  /* Load filename widget */

  XtVaSetValues(
      current->form1a_text_filename,
          XtNstring, current->edit_filename,
	  NULL ) ;

  /* Stat our file for permission and owner/group information */

  if (stat(current->dc_fullname, &(current->filestat)) == -1) {
    fprintf(stderr, "File status unreadable!\n");
  } else {
      struct passwd *pw;
      struct group *grp;
      

    /* Save the initial stat information for undo */

    current->oldstat.mode = current->laststat.mode = current->filestat.st_mode;

    /* Load the name of the owner of the file into the owner text widget */
    if ((pw = (struct passwd*)getpwuid(current->filestat.st_uid)) != NULL)
      strcpy(current->edit_owner, pw->pw_name);
    else sprintf(current->edit_owner, "%d",
		 (short)current->filestat.st_uid);


    XtVaSetValues(
        current->form1c_text_owner,
	    XtNstring, current->edit_owner,
	    NULL ) ;

    /* Load the name of the group of the file into the group text widget */
    if ((grp = (struct group*)getgrgid(current->filestat.st_gid)) != NULL)
      strcpy(current->edit_group, grp->gr_name);
    else sprintf(current->edit_group, "%d",
		 (short)current->filestat.st_gid);


    XtVaSetValues(
        current->form1c_text_group,
	    XtNstring, current->edit_group,
	    NULL ) ;

    /* Save filename, owner and group into last_* and old_* buffers for
       posterity. */

    strcpy(current->oldstat.filename, current->edit_filename);
    strcpy(current->oldstat.owner, current->edit_owner);
    strcpy(current->oldstat.group, current->edit_group);

    strcpy(current->laststat.filename, current->edit_filename);
    strcpy(current->laststat.owner, current->edit_owner);
    strcpy(current->laststat.group, current->edit_group);

    /* Load the proper icon for the icon_label */

    set_icon(current);

    /* Load the file modification date and size */

    set_size_date(current);

    /* Set the buttons and command widgets */

    update_buttons(INIT, current);

    /* Put the dialog on screen */

    XtRealizeWidget(current->fileInfoDialog);

    XawTextSetInsertionPoint(current->form1a_text_filename,
			     strlen(current->edit_filename));
    XawTextSetInsertionPoint(current->form1c_text_owner,
			     strlen(current->edit_owner));
    XawTextSetInsertionPoint(current->form1c_text_group,
			     strlen(current->edit_group));

    realize_dialog(current->fileInfoDialog, NULL, XtGrabNone,
		   (XtEventHandler)WM_destroy_doubleclick_dialog, current);

  }  /* end of if (stat(dc_fullname, &filestat) == -1) */
}

/*****************************************************************************
 *                            update_buttons                                 *
 *****************************************************************************/
private void update_buttons(mode, current)
int mode;
Fileinfo *current;
{
  static uid_t uid, euid;     /* user and effective user id's for program */
  Cardinal i;
  struct stat dirstat;

#define CHANGED(b) (current->button[b].state != (current->button[b].bitmask & current->filestat.st_mode))
#define USEREXEC  (current->filestat.st_mode & IEXEC)
#define USERREAD  (current->filestat.st_mode & IREAD)
#define USERWRITE (current->filestat.st_mode & IWRITE)
#define GROUPEXEC  (current->filestat.st_mode & GEXEC)
#define GROUPREAD  (current->filestat.st_mode & GREAD)
#define GROUPWRITE (current->filestat.st_mode & GWRITE)
#define OTHEREXEC    (current->filestat.st_mode & AEXEC)
#define OTHERREAD    (current->filestat.st_mode & AREAD)
#define OTHERWRITE   (current->filestat.st_mode & AWRITE)
#define ANYEXEC    (current->filestat.st_mode & (IEXEC | GEXEC | AEXEC))
#define REGULAR    ((current->filestat.st_mode & S_IFMT) == S_IFREG)
#define SYMLINK    ((current->filestat.st_mode & S_IFMT) == S_IFLNK)
#define DIRECTORY  ((current->filestat.st_mode & S_IFMT) == S_IFDIR)

#define D_USERWRITE  (dirstat.st_mode & IWRITE)
#define D_GROUPWRITE (dirstat.st_mode & GWRITE)
#define D_OTHERWRITE (dirstat.st_mode & AWRITE)
#define D_STICKY     (dirstat.st_mode & S_ISVTX)

  /* Force all buttons to be initialized. */

  if (mode == INIT) for (i= FIRSTBUTTON; i <= LASTBUTTON; i++)
                        current->button[i].state = -1;

  if (mode == INIT || mode == UNDO) {
    if CHANGED(rown)     SetButton(rown);
    if CHANGED(rgrp)     SetButton(rgrp);
    if CHANGED(roth)     SetButton(roth);
    if CHANGED(wown)     SetButton(wown);
    if CHANGED(wgrp)     SetButton(wgrp);
    if CHANGED(woth)     SetButton(woth);
    if CHANGED(xown)     SetButton(xown);
    if CHANGED(xgrp)     SetButton(xgrp);
    if CHANGED(xoth)     SetButton(xoth);
    if CHANGED(uid_but)  SetButton(uid_but);
    if CHANGED(gid_but)  SetButton(gid_but);
    if CHANGED(stky_but) SetButton(stky_but);
  }

  if (mode == INIT) {

    if (stat(current->dc_path, &dirstat) == -1) {
      fprintf(stderr, "Can't stat %s.\n", current->dc_path);
    }

    /* Set up the default button sensitivity */

    uid = getuid();
    euid = geteuid();

    XtVaSetValues(current->form2b_command_reset, XtNsensitive, False, NULL);
    XtVaSetValues(current->form2b_command_undo, XtNsensitive, False, NULL);
    XtVaSetValues(current->form2b_command_update, XtNsensitive, False, NULL);

    /* Assume we can modify permissions until proven otherwise.
       Note that we only do this at INIT, as we are assuming
       that the user can't change his any of his identities
       during his usage of this dialog box.  So the section
       that handles root user access won't set any of these to
       true, since it assumes that they couldn't have been
       set to false, since the user must have always been
       root. */

    XtVaSetValues(current->form2a2_button_rown, XtNsensitive, True, NULL);
    XtVaSetValues(current->form2a2_button_rgrp, XtNsensitive, True, NULL);
    XtVaSetValues(current->form2a2_button_roth, XtNsensitive, True, NULL);
    XtVaSetValues(current->form2a3_button_wown, XtNsensitive, True, NULL);
    XtVaSetValues(current->form2a3_button_wgrp, XtNsensitive, True, NULL);
    XtVaSetValues(current->form2a3_button_woth, XtNsensitive, True, NULL);
    XtVaSetValues(current->form2a4_button_xown, XtNsensitive, True, NULL);
    XtVaSetValues(current->form2a4_button_xgrp, XtNsensitive, True, NULL);
    XtVaSetValues(current->form2a4_button_xoth, XtNsensitive, True, NULL);
    XtVaSetValues(current->form2a6_button_setuid, XtNsensitive, True, NULL);
    XtVaSetValues(current->form2a6_button_setgid, XtNsensitive, True, NULL);
    XtVaSetValues(current->form2a6_button_sticky, XtNsensitive, True, NULL);
    XtVaSetValues(current->form1a_text_filename, XtNsensitive, True, NULL);
    XtVaSetValues(current->form1c_text_owner, XtNsensitive, True, NULL);
    XtVaSetValues(current->form1c_text_group, XtNsensitive, True, NULL);
    XawTextDisplayCaret(current->form1a_text_filename, True);
    XawTextDisplayCaret(current->form1c_text_owner, True);
    XawTextDisplayCaret(current->form1c_text_group, True);
  }

  if (!uid || !euid)    /* We're root */
    {
      if (ANYEXEC && (REGULAR || SYMLINK))
	{
	  if (mode == UPDATE || mode == UNDO || mode == INIT)
	    {
	      XtVaSetValues(current->form3_command_execwin,
			    XtNsensitive, True, NULL);
	      XtVaSetValues(current->form3_command_execback,
			    XtNsensitive, True, NULL);
	    }
	  XtVaSetValues(current->form2a6_button_sticky,
			XtNsensitive, True, NULL);
	}
      else
	{
	  if (mode == UPDATE || mode == UNDO || mode == INIT)
	    {
	      XtVaSetValues(current->form3_command_execwin,
			    XtNsensitive, False, NULL);
	      XtVaSetValues(current->form3_command_execback,
			    XtNsensitive, False, NULL);
	    }
	  if (!DIRECTORY)
	    {
	      XtVaSetValues(current->form2a6_button_sticky,
			    XtNsensitive, False, NULL);
	      current->filestat.st_mode &= ~S_ISVTX;
	      SetButton(stky_but);
	    }
	}

      /* The setgid and setuid bits may only be on if the corresponding execute
	 bit is on. We check those bits here, even though they don't affect the
	 root user's ability to do anything else. */

      if (USEREXEC && (REGULAR || SYMLINK))
	{
	  XtSetSensitive(current->form2a6_button_setuid, True);
	}
      else
	{
	  current->filestat.st_mode &= ~S_ISUID;
	  SetButton(uid_but);
	  XtSetSensitive(current->form2a6_button_setuid, False);
	}

      if (GROUPEXEC && (REGULAR || SYMLINK))
	{
	  XtSetSensitive(current->form2a6_button_setgid, True);
	}
      else
	{
	  current->filestat.st_mode &= ~S_ISGID;
	  SetButton(gid_but);
	  XtSetSensitive(current->form2a6_button_setgid, False);
	}

      if (DIRECTORY)
	{
	  XtSetSensitive(current->form3_command_view, False);
	  XtSetSensitive(current->form3_command_edit, False);
	  XtSetSensitive(current->form2a6_button_sticky, True);
	}
      else
	{
	  XtSetSensitive(current->form3_command_view, True);
	  XtSetSensitive(current->form3_command_edit, True);
	}

    /* If we can view the file, (the file is not a directory)
       We can edit the file regardless of permission bits */

      current->file_editable = TRUE;
    }
  else    /* Oops, we're not root */
    {
      if (mode == INIT)
	{
	  /* If we don't have write permission in this directory,
	     we can't rename the file. */

	  if ((uid == dirstat.st_uid) || (euid == dirstat.st_uid))
	    {
	      if (!D_USERWRITE)
		{
		  XtSetSensitive(current->form1a_text_filename, False);
		  XawTextDisplayCaret(current->form1a_text_filename, False);
		}
	    }
	  else if (user_is_member(dirstat.st_gid))
	    {
	      if (!D_GROUPWRITE || D_STICKY)
		{
		  XtSetSensitive(current->form1a_text_filename, False);
		  XawTextDisplayCaret(current->form1a_text_filename, False);
		}
	    }
	  else if (!D_OTHERWRITE || D_STICKY)
	    {
	      XtSetSensitive(current->form1a_text_filename, False);
	      XawTextDisplayCaret(current->form1a_text_filename, False);
	    }

	  /* Non-root, can't chown */

	  XtVaSetValues(
	      current->form1c_text_owner,
	          XtNsensitive, False,
		  XtNeditType, XawtextRead,
		  NULL ) ;
	  XawTextDisplayCaret(current->form1c_text_owner, False);

	  if (!DIRECTORY)
	    {
	      current->filestat.st_mode &= ~S_ISVTX;  /* Turn off sticky */
	      SetButton(stky_but);
	      XtSetSensitive(current->form2a6_button_sticky, False);
	    }
	}

      if ((uid == current->filestat.st_uid) ||
	  (euid == current->filestat.st_uid))
	{

      /* We are the owner of the file.  Let's see how our permissions
	 affect our possible commands, and turn off ones that do
	 not apply with the current permissions. */

      if (USEREXEC && (REGULAR || SYMLINK))
	{
	  if (mode == UPDATE || mode == UNDO || mode == INIT)
	    {
	      XtVaSetValues(current->form3_command_execwin,
			    XtNsensitive, True, NULL);
	      XtVaSetValues(current->form3_command_execback,
			    XtNsensitive, True, NULL);
	    }
	  XtVaSetValues(current->form2a6_button_setuid,
			XtNsensitive, True, NULL);
	}
      else
	{
	  if (mode == UPDATE || mode == UNDO || mode == INIT)
	    {
	      XtVaSetValues(current->form3_command_execback,
			    XtNsensitive, False, NULL);
	      XtVaSetValues(current->form3_command_execwin,
			    XtNsensitive, False, NULL);
	    }
	  current->filestat.st_mode &= ~S_ISUID; /* Turn off Setuid */
	  SetButton(uid_but);
	  XtVaSetValues(current->form2a6_button_setuid,
			XtNsensitive, False, NULL);
	}
      if (GROUPEXEC && (REGULAR || SYMLINK))
	{
	  XtSetSensitive(current->form2a6_button_setgid, True);
	}
      else /* Group exec permission is off */
	{
	  current->filestat.st_mode &= ~S_ISGID; /* Turn off Setgid */
	  SetButton(gid_but);
	  XtSetSensitive(current->form2a6_button_setgid, False);
	}

      if (mode == UPDATE || mode == UNDO || mode == INIT)
	{
	  if (USERREAD && (REGULAR || SYMLINK))
	    {
	      XtSetSensitive(current->form3_command_view, True);
	    }
	  else
	    {
	      XtSetSensitive(current->form3_command_view, False);
	      XtSetSensitive(current->form3_command_edit, False);
	    }
	  if (!USERWRITE && (REGULAR || SYMLINK))
	    {
	      XtSetSensitive(current->form3_command_edit, False);
	      current->file_editable = FALSE;
	    }
	  else if (USERREAD && (REGULAR || SYMLINK))
	    {
	      XtSetSensitive(current->form3_command_edit, True);
	      current->file_editable = TRUE;
	    }
	  else
	    XtSetSensitive(current->form3_command_edit, False);
	}

      /* End of owner permission handling section */

    } else /* not owner or root */ {

      /* We're not root, and we don't own the file,
	 so we can't change the permissions. */

      if (mode == INIT)
	{
	/* Again, we do this only at INIT time, and we
	   assume throughout this function that these values
	   will remain unchanged for the duration of this
	   dialog box. */

	XtVaSetValues(current->form2a2_button_rown, XtNsensitive, False, NULL);
	XtVaSetValues(current->form2a2_button_rgrp, XtNsensitive, False, NULL);
	XtVaSetValues(current->form2a2_button_roth, XtNsensitive, False, NULL);
	XtVaSetValues(current->form2a3_button_wown, XtNsensitive, False, NULL);
	XtVaSetValues(current->form2a3_button_wgrp, XtNsensitive, False, NULL);
	XtVaSetValues(current->form2a3_button_woth, XtNsensitive, False, NULL);
	XtVaSetValues(current->form2a4_button_xown, XtNsensitive, False, NULL);
	XtVaSetValues(current->form2a4_button_xgrp, XtNsensitive, False, NULL);
	XtVaSetValues(current->form2a4_button_xoth, XtNsensitive, False, NULL);
	XtVaSetValues(current->form2a6_button_setuid, XtNsensitive, False,
		      NULL);
	XtVaSetValues(current->form2a6_button_setgid, XtNsensitive, False,
		      NULL);
	XtVaSetValues(current->form2a6_button_sticky, XtNsensitive, False,
		      NULL);
	XtVaSetValues(current->form1c_text_group, XtNsensitive, False, NULL);
	XawTextDisplayCaret(current->form1c_text_group, False);
      }

      /* Since we're not root and don't own the file,
	 we don't have to worry about making changes to
	 the setgid and setuid buttons in response to your
	 run of the mill permission button clicks.  So,
	 all _group_ and _other_ permission handling will
	 be done only if we are in UPDATE, UNDO, or INIT mode. */

      if (mode == UPDATE || mode == UNDO || mode == INIT)
	{
	  if (user_is_member(current->filestat.st_gid))
	    {
	      /* We're in the file's group */

	      if (GROUPEXEC && (REGULAR || SYMLINK))
		{
		  XtSetSensitive(current->form3_command_execwin, True);
		  XtSetSensitive(current->form3_command_execback, True);
		}
	      else
		{
		  XtSetSensitive(current->form3_command_execwin, False);
		  XtSetSensitive(current->form3_command_execback, False);
		}
	      if (GROUPREAD && (REGULAR || SYMLINK))
		{
		  XtSetSensitive(current->form3_command_view, True);
		}
	      else
		{
		  XtSetSensitive(current->form3_command_view, False);
		  XtSetSensitive(current->form3_command_edit, False);
		}
	      if (!GROUPWRITE)
		{
		  XtSetSensitive(current->form3_command_edit, False);
		  current->file_editable = FALSE;
		}
	      else if (GROUPREAD && (REGULAR || SYMLINK))
		{
		  XtSetSensitive(current->form3_command_edit, True);
		  current->file_editable = TRUE;
		}

	      /* End of group permission handling */
	    }
	  else
	    {
	      /* We're not owner or in group */

	      if (OTHEREXEC && (REGULAR || SYMLINK))
		{
		  XtSetSensitive(current->form3_command_execwin, True);
		  XtSetSensitive(current->form3_command_execback, True);
		}
	      else
		{
		  XtSetSensitive(current->form3_command_execwin, False);
		  XtSetSensitive(current->form3_command_execback, False);
		}
	      if (OTHERREAD && (REGULAR || SYMLINK))
		{
		  XtSetSensitive(current->form3_command_view, True);
		}
	      else
		{
		  XtSetSensitive(current->form3_command_view, False);
		  XtSetSensitive(current->form3_command_edit, False);
		}

	      if (!OTHERWRITE)
		{
		  XtSetSensitive(current->form3_command_edit, False);
		  current->file_editable = FALSE;
		}
	      else if (OTHERREAD && (REGULAR || SYMLINK))
		{
		  XtSetSensitive(current->form3_command_edit, True);
		  current->file_editable = TRUE;
		}

	      /* End of other permissions handling */
	    }

	  /* End of the UPDATE, UNDO, INIT test that surrounds
	     all group and other permission handling */
	}
      /* End of non-root handling */
    }
    /* End of main body.. starting with if (!uid || !euid) */
  }

  /* We don't want to allow folks to rename '.' or '..' */

  if (!strcmp(".", current->dc_filename) ||
      !strcmp("..", current->dc_filename))
    {
      XawTextDisplayCaret(current->form1a_text_filename, False);
      XtSetSensitive(current->form1a_text_filename, False);
    }
}

/*****************************************************************************
 *                                verify_callback                            *
 *****************************************************************************/
/*ARGSUSED*/
void verify_callback(w, current, call_data)
Widget w;
Fileinfo *current;
XtPointer call_data;
{
  extern Cursor left_ptr;

  if (w == current->verify_cancel)
    XtPopdown(current->verify_popup);
  else
    {
      if (w == current->verify_yes)
	update_func(w, (XtPointer)current, 0);

      XtPopdown(current->verify_popup);
      XtPopdown(current->fileInfoDialog);

      if ((current->laststat.mode != current->oldstat.mode) ||
	  (strcmp(current->laststat.filename, current->oldstat.filename)) ||
	  (strcmp(current->laststat.owner, current->oldstat.owner)) ||
	  (strcmp(current->laststat.group, current->oldstat.group)) ||
	  (current->file_edited && (current_mode.mode != Icons)))
      {
	  char path[255];
	  int len = strlen(current->dc_path);
	  
	  strcpy(path, current->dc_path);
	  
	  if ((strcmp(path, "/") != 0) && (path[len-1] == '/'))
	    path[len-1] = 0;
	  directoryManagerNewDirectory(path);
      }

      /* We can finally let the main interface go back to the non-busy cursor. */

      setCursor(left_ptr);
    }
}

/*****************************************************************************
 *                            destroy_doubleclick_dialog                     *
 *****************************************************************************/
/*ARGSUSED*/
void destroy_doubleclick_dialog(w, current, call_data)
Widget w;
Fileinfo *current;
XtPointer call_data;
{
  extern Cursor left_ptr;

  /* Popdown the doubleclick dialog */

  if ((current->filestat.st_mode != current->laststat.mode) ||
      (strcmp(current->edit_filename, current->laststat.filename)) ||
      (strcmp(current->edit_group, current->laststat.group)) ||
      (strcmp(current->edit_owner, current->laststat.owner))) {
    realize_dialog(current->verify_popup, NULL, XtGrabNonexclusive,
		   NULL, NULL);
  } else {
    Fileinfoelement *c = Fileinfolist;
    Fileinfoelement *p;

    XtPopdown(current->fileInfoDialog);

    if ((current->laststat.mode != current->oldstat.mode) ||
	(strcmp(current->laststat.filename, current->oldstat.filename)) ||
	(strcmp(current->laststat.owner, current->oldstat.owner)) ||
	(strcmp(current->laststat.group, current->oldstat.group))
	)
      {
	  char path[255];
	  int len = strlen(current->dc_path);
	  
	  strcpy(path, current->dc_path);
	  
	  if ((strcmp(path, "/") != 0) && (path[len-1] == '/'))
	    path[len-1] = 0;
	  directoryManagerNewDirectory(path);
      }

    setCursor(left_ptr);

    /* Destroy all the widgets.. I could do this via just destroying the fileInfoDialog,
     * but I prefer to do it in a few seperate stages.
     */
    XtDestroyWidget(current->eb_popup);
    XtDestroyWidget(current->verify_popup);
    XtDestroyWidget(current->form1);
    XtDestroyWidget(current->form2);
    XtDestroyWidget(current->form3);
    XtDestroyWidget(current->fileInfoDialog);

    /* delete all the memory associated with this instance of a double-click
     * then delete the Widgets them selves.
     */
    XtFree(current->filedate);
    XtFree(current->filesize);
    XtFree(current->edit_filename);
    XtFree(current->edit_owner);
    XtFree(current->edit_group);
    XtFree(current->dc_fullname);
    XtFree(current->dc_filename);
    XtFree(current->dc_path);
    XtFree(current->eb_input);
    XtFree(current->eb_output);
    XtFree(current->eb_error);
    XtFree((char *)current->xdtmlist_element);

    /* Now we must delete this instance from our Fileinfolist, then free it */
    p = c;
    while (c != NULL && c->fileinfo != current) {
      /* This node is valid AND This node is NOT the current one. 
       * Let's try the next node. 
       */
      p = c;        /* A pointer to the predecessor */
      c = c->next;
    }
    if (c == NULL) {
      /* We did NOT find the node in the list.. Mmmm VERY strange, in fact 
       * stange enough for me to print a cryptic error message.
       */
      fprintf(stderr, "xdtm: Warning: permissions dialog not found in list when free'd\n");
    } else {
      /* OK we must have found it, let's take it out of the list. */
      if (Fileinfolist == c) {
	/* ah.. first element in the list.. OK.. if there are any more nodes
	 * in the list then set the Fileinfolist to the second one.. if not
	 * delete the whole bloody list.
	 */
	if (c->next == NULL) {
	  /* We are the only element in the list.. trash it!! */
	  XtFree((char *)Fileinfolist);
	  Fileinfolist = NULL;
	} else {
	  /* There are more.. */
	  Fileinfolist = c->next;
	  XtFree((char *)c);
	}
      } else {
	/* We are in the middle of end of the list */
	p->next = c->next;
	XtFree((char *)c);
      }
    }
    /* We ALWAYS want to free the current node anyway */
    XtFree((char *)current);
  }
}

/*****************************************************************************
 *                                  free_info                                *
 *****************************************************************************/
/*ARGSUSED*/
void free_info(w, infobutton, call_data)
Widget w;
InfoButtons *infobutton;
XtPointer call_data;
{
  XtFree((char *)infobutton);
}

/*****************************************************************************
 *                             dcbutton_toggled                              *
 *****************************************************************************/
/*ARGSUSED*/
void dcbutton_toggled(w, infobutton, call_data)
Widget w;
InfoButtons *infobutton;
XtPointer call_data;
{
  Buttons select;
  Fileinfo *current;

  select = infobutton->select;
  current = infobutton->fileinfo;
  if (current->filestat.st_mode & current->button[select].bitmask) {
    current->filestat.st_mode &= ~current->button[select].bitmask;
  } else {
    current->filestat.st_mode |= current->button[select].bitmask;
  }
  SetButton(select);

  /* If we've toggled the buttons around such that we are in
     the state we were in when we last wrote the permissions
     out, dim the update button to let the user know we
     are paying attention. */

  if ((current->filestat.st_mode == current->laststat.mode) &&
      (!strcmp(current->edit_filename, current->laststat.filename)) &&
      (!strcmp(current->edit_owner, current->laststat.owner)) &&
      (!strcmp(current->edit_group, current->laststat.group))) {
    XtVaSetValues(current->form2b_command_update, XtNsensitive, False, NULL);
    XtVaSetValues(current->form2b_command_undo, XtNsensitive, False, NULL);
  } else {
    XtVaSetValues(current->form2b_command_update, XtNsensitive, True, NULL);
    XtVaSetValues(current->form2b_command_undo, XtNsensitive, True, NULL);
  }

  if ((current->filestat.st_mode == current->oldstat.mode) &&
      (!strcmp(current->edit_filename, current->oldstat.filename)) &&
      (!strcmp(current->edit_owner, current->oldstat.owner)) &&
      (!strcmp(current->edit_group, current->oldstat.group))) {
    XtVaSetValues(current->form2b_command_reset, XtNsensitive, False, NULL);
  } else {
    XtVaSetValues(current->form2b_command_reset, XtNsensitive, True, NULL);
  }

  update_buttons(TEMP, current);
}

/*****************************************************************************
 *                             text_callback                                 *
 *****************************************************************************/
/*ARGSUSED*/
void text_callback(w, current, call_data)
Widget w;
Fileinfo *current;
XtPointer call_data;
{

  XtSetSensitive(current->form2b_command_update, True);
  XtSetSensitive(current->form2b_command_undo, True);
  XtSetSensitive(current->form2b_command_reset, True);

  if (w == current->form1c_text_group_src)
    {
      /* Turn off group set id if group name is changed. */

      current->filestat.st_mode &= ~S_ISGID;
      SetButton(gid_but);
    }
  else if (w == current->form1c_text_owner_src)
    {
      /* Turn off user set id if group name is changed */

      current->filestat.st_mode &= ~S_ISUID;
      SetButton(uid_but);
    }
  else if (w != current->form1a_text_filename_src)
    {
      fprintf(stderr, "Mystery text call-back!\n");
    }
}

/*****************************************************************************
 *                             cancel_func                                   *
 *****************************************************************************/
/*ARGSUSED*/
void cancel_func(w, client_data, call_data)
Widget w;
XtPointer client_data;
XtPointer call_data;
{
}

/*****************************************************************************
 *                             view_func                                     *
 *****************************************************************************/
/*ARGSUSED*/
void view_func(w, current, call_data)
Widget w;
Fileinfo *current;
XtPointer call_data;
{
  int localdescrip;
#ifdef alliant
  int status;
  short magic_number;
#endif

  if (current->file_editable && (w == current->form3_command_edit)) {
    if ((localdescrip = open(current->dc_fullname, O_RDWR, 0644)) == -1)
      ioerr_dialog(errno);
    else {

      /* Each processor has different magic numbers, therefore I won't
       * bother with trying to catch the editing of a binary file other
       * than for the alliant which I suppose must be Jon's machine.
       */

#ifdef alliant
      status = read(localdescrip, &magic_number, sizeof(short));
      if (status == -1) ioerr_dialog(errno);
      else if (magic_number == AVMAGIC) {
	alert_dialog("Can't edit a binary executable!", NULL, "Cancel");
	close(localdescrip);
      } else if (magic_number == ACMAGIC) {
	alert_dialog("Can't edit a binary object file!", NULL, "Cancel");
	close(localdescrip);
      } else if ((magic_number == OMAGIC) ||
		 (magic_number == NMAGIC) ||
		 (magic_number == ZMAGIC)) {
	alert_dialog("Can't edit binary load file!", NULL, "Cancel");
	close(localdescrip);
      } else {

#endif
	close(localdescrip);
	editfile(current->dc_fullname);
	current->file_edited = 1;
#ifdef alliant
      }
#endif
    }
  } else /* We are in non file_editable mode */ {
    if ((localdescrip = open(current->dc_fullname, O_RDONLY, 0644)) == -1)
      ioerr_dialog(errno);
    else /* We can open the file. */ {


#ifdef alliant
      status = read(localdescrip, &magic_number, sizeof(short));

      if (status == -1) ioerr_dialog(errno);
      else if (magic_number == AVMAGIC) {
	alert_dialog("Can't view a binary executable!", NULL, "Cancel");
	close(localdescrip);
      } else if (magic_number == ACMAGIC) {
	alert_dialog("Can't view a binary object file!", NULL, "Cancel");
	close(localdescrip);
      } else if ((magic_number == OMAGIC) ||
		 (magic_number == NMAGIC) ||
		 (magic_number == ZMAGIC)) {
	alert_dialog("Can't view binary load file!", NULL, "Cancel");
	close(localdescrip);
      } else {

#endif
	close(localdescrip);
	displayfile(current->dc_fullname, NULL, NULL, XtGrabNone);
	/* Close the double_click popup, otherwise no movement can occur in
	 * the file being viewed.
	 */
	/* destroy_doubleclick_dialog(NULL, NULL, NULL); */
#ifdef alliant
      }
#endif
    }
  }
}

/*****************************************************************************
 *                             ioerr_dialog                                  *
 *****************************************************************************/
public void ioerr_dialog(errnum)
int errnum;
{
  /* Yo Jon I like it.. Eddy. */

  switch( errnum )
    {
      case ENOTDIR :
	/* FALL THROUGH */

      case EINVAL :
	/* FALL THROUGH */

      case ENAMETOOLONG :
	alert_dialog("Invalid filename error", NULL, NULL);
      break ;

      case ENOENT :
	alert_dialog("Component of path or", "file does not exist", NULL);
      break ;

      case EACCES :
	alert_dialog("Permission denied", NULL, NULL);
      break;

#ifndef TRUE_SYSV
      case ELOOP :
	alert_dialog("Symbolic link loop in path", NULL, NULL);
      break ;
#endif

      case EISDIR :
	alert_dialog("Tried to write to directory file", NULL, NULL);
      break;

      case EROFS:
	alert_dialog("Can't write to file;", " File is on a read-only file system", NULL);
      break;

      case EMFILE:
	alert_dialog("Too many open file descriptors", NULL, NULL);
      break;

      case ENFILE:
	alert_dialog("System file table is full", NULL, NULL);
      break;

      case ENXIO:
	alert_dialog("Device driver missing", NULL, NULL);
      break;

      case ENOSPC:
	alert_dialog("No space to create file", NULL, NULL);
      break;

#ifdef EDQUOT
      case EDQUOT:
	alert_dialog("Quota over limit", NULL, NULL);
      break;
#endif

      case EIO:
	alert_dialog("I/O error while allocating inode", NULL, NULL);
      break;

      case ETXTBSY:
	alert_dialog("File is currently executing", NULL, NULL);
      break;

      case EFAULT:
	alert_dialog("Path points outside address space", NULL, NULL);
      break;

      case EEXIST:
	alert_dialog("File already exists", NULL, NULL);
      break;

#ifndef TRUE_SYSV
      case EOPNOTSUPP :
	alert_dialog("Can't open a socket", NULL, NULL);
      break;
#endif
    }
}
/*****************************************************************************
 *                             execwin_func                                  *
 *****************************************************************************/
/*ARGSUSED*/
void execwin_func(w, current, call_data)
Widget w;
Fileinfo *current;
XtPointer call_data;
{
  extern Cursor busy;

  AppProgram *node;

  XtVaSetValues( current->fileInfoDialog, XtNcursor, busy, NULL ) ;

#ifdef DEBUG
  fprintf(stderr, "eb_input = %s\neb_output = %s\neb_error = %s\n",
	  current->eb_input, current->eb_output, current->eb_error);
#endif

  /* compute_appnode loads node->string and node->program with
     dc_fullname. */

  node = compute_appnode(current->dc_fullname, current);

  /* say we want a pseudo-terminal */
  node->termopts = TERM;
  
#ifdef DEBUG
  fprintf(stderr, "execwin_func : node->program = %s\n",node->program);
  fprintf(stderr, "execwin_func : running...\n");
#endif

  /* Is a version is already running */
  if (node->count) 
    popup_verify_app(node);
  else 
    run_app(node, current->eb_input, current->eb_output, current->eb_error);

#ifdef DEBUG
  fprintf(stderr, "execwin_func : whew..\n");
#endif

  XtVaSetValues(
      current->fileInfoDialog,
          XtNcursor, NULL,
	  NULL ) ;
}

/*****************************************************************************
 *                          WM_execback_popdown                              *
 *****************************************************************************/
#if NeedFunctionPrototypes
private void WM_execback_popdown(Widget w,
				 Fileinfo *current,
				 XEvent *event,
				 Boolean *dispatch)
#else
private void WM_execback_popdown(w, current, event, dispatch)
     Widget w;
     Fileinfo *current;
     XEvent *event;
     Boolean *dispatch;
#endif
{
    extern Atom protocols[2]; /* defined in dialogs.c */
    
    if (event->xclient.message_type == protocols[1] &&
	event->xclient.data.l[0] == protocols[0])
    /* the widget got a kill signal */
    {
	/* call callbacks on Cancel button */
	XtCallCallbacks(current->ebcommandcancel, XtNcallback, NULL);
    }
}

/*****************************************************************************
 *                             execback_realize                              *
 *****************************************************************************/
/*ARGSUSED*/
void execback_realize(w, current, call_data)
Widget w;
Fileinfo *current;
XtPointer call_data;
{

  XawTextSetInsertionPoint(current->ebtextinput, strlen(current->eb_input));
  XawTextSetInsertionPoint(current->ebtextoutput, strlen(current->eb_output));
  XawTextSetInsertionPoint(current->ebtexterror, strlen(current->eb_error));

  realize_dialog(current->eb_popup, NULL, XtGrabNonexclusive,
		 (XtEventHandler)WM_execback_popdown, current);
}

/*****************************************************************************
 *                             execback_popdown                              *
 *****************************************************************************/
/*ARGSUSED*/
void execback_popdown(w, infobutton, call_data)
Widget w;
InfoButtons *infobutton;
XtPointer call_data;
{
  AppProgram *node;
  Fileinfo *current;

  current = infobutton->fileinfo;

  XtPopdown (current->eb_popup);

  /* fprintf(stderr, "eb_input = %s\neb_output = %s\neb_error = %s\n", eb_input,
     eb_output, eb_error); */

  if (infobutton->select == 1) {
    extern Cursor busy;

    XtVaSetValues( current->fileInfoDialog, XtNcursor, busy, NULL ) ;

    node = compute_appnode(current->dc_fullname, current);
    if (node->count) 
      popup_verify_app(node);
    else 
      run_app(node, current->eb_input, current->eb_output, current->eb_error);

    XtVaSetValues( current->fileInfoDialog, XtNcursor, NULL, NULL ) ;
  }
}

/*****************************************************************************
 *                             compute_appnode                               *
 *  	      	      	      	      	      	      	      	      	     *
 * compute_appnode is used to find or generate an AppProgram node for	     *
 * <programname>.  The AppProgram node is used in the application manager,   *
 * which keeps a list of currently running processes.  The AppProgram node   *
 * holds four pieces of information relevant to our purposes here. 	     *
 *									     *
 * node->string 							     *
 *   contains the name of the program we are executing. In this case, this   *
 *   includes the complete path to the file as well as the filename itself.  *
 *   e.g., /usr/bin/X11/xcolors.  This field is used to test for the identity*
 *   of the program being run.  Routines which check to see if the user	     *
 *   already has a copy of the program running check this field.	     *
 *   This field is a label only, and need not contain the actual command     *
 *   used to invoke the program.					     *
 *									     *
 *   The application manager knows that we provide a fully specified path in *
 *   our identity field.  If it sees node->options == IGNORE_SEL, it will    *
 *   take that as a sign that the node is ours, and will only display the    *
 *   last component of node->string in process lists.                        *
 *									     *
 * node->program							     *
 *   contains the actual command used to invoke the program. 		     *
 *		      	      	      	      	      	      	      	     *
 * node->options							     *
 *   if IGNORE_SEL, application manager will not try to parse arguments for  *
 *   us, and will only display the last component of node->string in process *
 *   lists.                                                                  *
 *									     *
 * node->count                                                               *
 *   maintains a count of the number of invokations of node->string running. *
 * 									     *
 * 									     *
 * 									     *
 * Note: We never delete nodes.  The assumption is that the user won't run   *
 * five unique bajillion programs.  Things get more complex if he does.      *
 *									     *
 *****************************************************************************/
private AppProgram *compute_appnode(programname, current)
String programname;
Fileinfo *current;
{
  AppNode *ptr, *newptr;
  char *newstring ;

  ptr = current->applist;

#ifdef DEBUG
  fprintf(stderr, "compute_appnode : starting %s\n", programname);
  fprintf(stderr,"compute_appnode : traversing dirman process list\n");
#endif

  /* Let's see if we've already allocated a node to handle this
     program. */

  while (ptr && strcmp(ptr->node.string, programname)) {
    
#ifdef DEBUG
    fprintf(stderr, "compute_appnode : node.string = %s\n", ptr->node.string);
    fprintf(stderr, "compute_appnode : node.count = %d\n", ptr->node.count);
#endif

    ptr = ptr->next;
  }

#ifdef DEBUG
  fprintf(stderr,"compute_appnode : done traversing dirman process list\n");
#endif

  if (ptr) {

    /* We don't want to change node.string, as that is
       our tag, but we do want to make sure our invocation
       is updated. */

#ifdef DEBUG
    fprintf(stderr, "compute_appnode : found existing node.\n\
compute_appnode : node->program = %s\n",ptr->node.program);
#endif

    if (ptr->node.program) XtFree(ptr->node.program);

    newstring = (char *) XtMalloc(strlen(programname));
    if (!newstring) {
      fprintf(stderr, "compute_appnode : newstring XtMalloc\n");
      exit(-1);
    }
    ptr->node.program = strcpy(newstring, programname);

#ifdef DEBUG
    fprintf(stderr, "\ncompute_appnode : programname = %s\n", programname);
    fprintf(stderr, "compute_appnode : now node->program = %s\n", ptr->node.program);
#endif 

    return &(ptr->node);
  }

  /* Otherwise allocate node, insert at the head of the list */

#ifdef DEBUG
  fprintf(stderr, "compute_appnode : allocating new node\n");
#endif

  newptr = (AppNode *) XtMalloc(sizeof(AppNode));

#ifdef DEBUG
  fprintf(stderr, "compute_appnode : newly minted node, node.program = %d\n",newptr->node.program);
#endif

  if (!newptr) {
    fprintf(stderr, "compute_appnode: Error in XtMalloc!\n");
    exit(-1);
  }

  /* New node */

  newstring = (char *) XtMalloc(strlen(programname));
  newptr->node.string = strcpy(newstring, programname);
  newptr->node.program = newptr->node.string;
  newptr->node.count = 0;
  newptr->node.icon = (Pixmap)NULL;
  newptr->node.options = IGNORE_SEL;

  /* We'll insert it at the head of the list.. older progs
     will take longer to search for. */

  newptr->next = current->applist;
  current->applist = newptr;

  return &(newptr->node);
}

/*****************************************************************************
 *                             update_func                                   *
 *****************************************************************************/
/*ARGSUSED*/
void update_func(w, current, call_data)
Widget w;
Fileinfo *current;
XtPointer call_data;
{
  int group;
  struct passwd *pass;
  struct stat dummy_stat;

  if (strcmp(current->edit_group, current->laststat.group))
    if ((group = group_number(current->edit_group)) == -1) {
      alert_dialog("Error", "Invalid group name.", NULL);
      strcpy(current->edit_group, current->laststat.group);
      load_text(current->form1c_text_group, current->edit_group);
    } else if (chown(current->dc_fullname, -1, group) == -1) {
      if (errno == EPERM)
	alert_dialog("Not a member of group requested", NULL, NULL);
      else alert_dialog("Error in chown(group)", NULL, NULL);
      strcpy(current->edit_group, current->laststat.group);
      load_text(current->form1c_text_group, current->edit_group);
    } else strcpy(current->laststat.group, current->edit_group);

  if (strcmp(current->edit_filename, current->laststat.filename)) {
    char temp_name[1024], temp_name2[1024];
    strcpy(temp_name, current->dc_path);
    strcpy(temp_name2, current->dc_path);
    if (!stat(strcat(temp_name, current->edit_filename), &dummy_stat)) {
      char tempstring[80];
      ioerr_dialog(errno);
      sprintf(tempstring, "File %s already exists!",current->edit_filename);
      alert_dialog(tempstring, NULL, NULL);
      strcpy(current->edit_filename, current->laststat.filename);
      load_text(current->form1a_text_filename, current->edit_filename);
    } else if (rename(strcat(temp_name2, current->laststat.filename),
		      temp_name) == -1) {
      alert_dialog("Error in rename", NULL, NULL);
      strcpy(current->edit_filename, current->laststat.filename);
      load_text(current->form1a_text_filename, current->edit_filename);
    } else /* Success! */ {
      strcpy(current->laststat.filename, current->edit_filename);
      strcpy(current->dc_filename, current->edit_filename);
      strcpy(current->dc_fullname, current->dc_path);
      strcat(current->dc_fullname, current->dc_filename);
    }
  }

  if (strcmp(current->edit_owner, current->laststat.owner))
    if (!(pass=(getpwnam(current->edit_owner))) &&
	!(pass=(getpwuid((uid_t)atoi(current->edit_owner))))) {
      alert_dialog("No such user", current->edit_owner, NULL);
      strcpy(current->edit_owner, current->laststat.owner);
      load_text(current->form1c_text_owner, current->edit_owner);
      return;
    } else if (chown(current->dc_fullname, pass->pw_uid, -1) == -1) {
      alert_dialog("Error in chown(user)", NULL, NULL);
      strcpy(current->edit_owner, current->laststat.owner);
      load_text(current->form1c_text_owner, current->edit_owner);
      return;
    } else strcpy(current->laststat.owner, current->edit_owner);

  /* Mark the update button as inoperative */

  XtVaSetValues(
      current->form2b_command_update,
          XtNsensitive, False,
	  NULL ) ;

  update_buttons(UPDATE, current);

  chmod(current->dc_fullname, current->filestat.st_mode);

  /* Read the file status so we'll have the updated file
     modification time, etc. */

  if (stat(current->dc_fullname, &current->filestat) == -1) {
    fprintf(stderr, "doubleclick:update_func:File status unreadable!\n");
  }

  set_icon(current);

  /* If our update takes us back to our initial mode, clear
     both undo buttons, else just clear our our undo button. */


  XtSetSensitive(current->form2b_command_reset,
   (
    ((current->laststat.mode = current->filestat.st_mode) == current->oldstat.mode) &&
    !strcmp(current->edit_filename, current->oldstat.filename) &&
    !strcmp(current->edit_group, current->oldstat.group) &&
    !strcmp(current->edit_owner, current->oldstat.owner)
   ) ? False : True);

  XtSetSensitive(current->form2b_command_undo, False);

  set_size_date(current);
}

/*****************************************************************************
 *                             undo_func                                     *
 *****************************************************************************/
/*ARGSUSED*/
void undo_func(w, infobutton, call_data)
Widget w;
InfoButtons *infobutton;
XtPointer call_data;
{
  int group;
  struct passwd *pass;
  Fileinfo *current;
  int mode;

  mode = infobutton->mode;
  current = infobutton->fileinfo;

  switch(mode) {
  case RESET:
    current->laststat.mode = current->filestat.st_mode = current->oldstat.mode;

    if (strcmp(current->laststat.group, current->oldstat.group)) {
      if ((group = group_number(current->oldstat.group)) == -1) {
	alert_dialog("Error", "Impossibly Invalid group name.", NULL);
	strcpy(current->edit_group, current->laststat.group);
	load_text(current->form1c_text_group, current->edit_group);
      } else if (chown(current->dc_fullname, -1, group) == -1) {
	alert_dialog("Error in resetting group (chown)", NULL, NULL);
	strcpy(current->edit_group, current->laststat.group);
	load_text(current->form1c_text_group, current->edit_group);
      } else /* Success! */ {
	strcpy(current->edit_group, current->oldstat.group);
	strcpy(current->laststat.group, current->edit_group);
	load_text(current->form1c_text_group, current->edit_group);
      }
    } else if (strcmp(current->edit_group, current->oldstat.group)) {
      strcpy(current->edit_group, current->oldstat.group);
      load_text(current->form1c_text_group, current->edit_group);
    }

    if (strcmp(current->laststat.filename, current->oldstat.filename))
      {
	char temp_name[1024], temp_name2[1024];
	strcpy(temp_name, current->dc_path);
	strcpy(temp_name2, current->dc_path);
	if (rename(strcat(temp_name, current->laststat.filename),
		   strcat(temp_name2, current->oldstat.filename)) == -1) {
	  alert_dialog("Error in resetting filename", NULL, NULL);
	  strcpy(current->edit_filename, current->laststat.filename);
	  load_text(current->form1a_text_filename, current->edit_filename);
	} else {
	  strcpy(current->edit_filename, current->oldstat.filename);
	  strcpy(current->laststat.filename, current->edit_filename);
	  strcpy(current->dc_filename, current->edit_filename);
	  strcpy(current->dc_fullname, current->dc_path);
	  strcat(current->dc_fullname, current->dc_filename);
	  load_text(current->form1a_text_filename, current->edit_filename);
	}
      } else if (strcmp(current->edit_filename, current->oldstat.filename)) {
	strcpy(current->edit_filename, current->oldstat.filename);
	strcpy(current->dc_filename, current->edit_filename);
	strcpy(current->dc_fullname, current->dc_path);
	strcat(current->dc_fullname, current->dc_filename);
	load_text(current->form1a_text_filename, current->edit_filename);
     }

    if (strcmp(current->laststat.owner, current->oldstat.owner)) {
      if (!(pass=(getpwnam(current->oldstat.owner)))) {
	alert_dialog("Original user no longer exists!", NULL, NULL);
	strcpy(current->edit_owner, current->laststat.owner);
	load_text(current->form1c_text_owner, current->edit_owner);
      } else {
	if (chown(current->dc_fullname, pass->pw_uid, -1) == -1) {
	  alert_dialog("Error in resetting owner (chown)", NULL, NULL);
	  strcpy(current->edit_owner, current->laststat.owner);
	  load_text(current->form1c_text_owner, current->edit_owner);
	} else /* Success! */ {
	  strcpy(current->edit_owner, current->oldstat.owner);
	  strcpy(current->laststat.owner, current->edit_owner);
	  load_text(current->form1c_text_owner, current->edit_owner);
	}
      }
    } else if (strcmp(current->edit_owner, current->oldstat.owner)) {
      strcpy(current->edit_owner, current->oldstat.owner);
      load_text(current->form1c_text_owner, current->edit_owner);
    }

    XtSetSensitive(current->form2b_command_reset, False);
    XtSetSensitive(current->form2b_command_undo, False);
    break;

  case UNDO:
    current->filestat.st_mode = current->laststat.mode;

    if (strcmp(current->edit_group, current->laststat.group)) {
      strcpy(current->edit_group, current->laststat.group);
      load_text(current->form1c_text_group, current->edit_group);
    }

    if (strcmp(current->edit_filename, current->laststat.filename)) {
      strcpy(current->edit_filename, current->laststat.filename);
      load_text(current->form1a_text_filename, current->edit_filename);
    }

    if (strcmp(current->edit_owner, current->laststat.owner)) {
      strcpy(current->edit_owner, current->laststat.owner);
      load_text(current->form1c_text_owner, current->edit_owner);
    }

    XtSetSensitive(current->form2b_command_undo, False);
    XtSetSensitive(current->form2b_command_reset,
      ((current->filestat.st_mode == current->oldstat.mode) &&
       !strcmp(current->edit_filename, current->oldstat.filename) &&
       !strcmp(current->edit_group, current->oldstat.group) &&
       !strcmp(current->edit_owner, current->oldstat.owner)) ? False : True);
  }

  /* Change the buttons back to their original state. */

  update_buttons(UNDO, current);

  XtSetSensitive(current->form2b_command_update, False);

  /* Restore the permissions on disk to their original state. */

  chmod(current->dc_fullname, current->filestat.st_mode);

  set_icon(current);

  set_size_date(current);

}

/*****************************************************************************
 *                             help_func                                     *
 *****************************************************************************/
/*ARGSUSED*/
void help_func(w, client_data, call_data)
Widget w;
XtPointer client_data;
XtPointer call_data;
{
    if (access(perm_help_file, R_OK) == 0)
      displayfile(perm_help_file, NULL, "xdtm permissions help", XtGrabNone);
    else
      alert_dialog("Help file not found at", perm_help_file, "Cancel");
}

/*****************************************************************************
 *                             group_number                                  *
 *****************************************************************************/
int group_number(groupname)
char *groupname;
{
  struct group *group;

  setgrent ();

  if (!(group=getgrnam(groupname)) && !(group=getgrgid(atoi(groupname))))
    return (-1);
  else
    return (group->gr_gid);
}

/*****************************************************************************
 *                             user_is_member                                *
 *****************************************************************************/
int user_is_member( gid )
gid_t gid ;
{
#ifdef SYSV
  gid_t gidset[NGROUPS];
#else
  int gidset[NGROUPS];
#endif
  int ngroups, i;

  /* Get the list of groups that the user is in. */

#ifdef DEBUG
 fprintf(stderr, "Entering user_is_member()\n");
 fprintf(stderr, "gid = %d\n", gid);
 fflush(stderr);
 fprintf(stderr, "NGROUPS = %d\n", NGROUPS);
#endif

  ngroups = getgroups(NGROUPS, gidset);

#ifdef DEBUG
 fprintf(stderr, "Done getgroups\n");
#endif

  /* If we were able to successfully get the groups,
     check to see if the user is a member of group
     number gid. */

  if (ngroups == -1) {
    fprintf(stderr, "xdtm: Error in reading groups in doubleclick.c:user_is_member \n");
  } else {
#ifdef DEBUG
    fprintf(stderr, "ngroups = %d\n",ngroups);
    fflush(stderr);
#endif
    for(i=0; i<ngroups; i++) {
#ifdef DEBUG
      fprintf(stderr, "looping.. \n");
      fprintf(stderr, "gidset[%d] = %d\n", i, gidset[i]);
      fflush(stderr);
#endif
      if (gidset[i] == gid) {
#ifdef DEBUG
	fprintf(stderr, "return(1)\n");
	fflush(stderr);
#endif
	return 1;
      }
    }
  }
#ifdef DEBUG
  fprintf(stderr, "returning 0\n");
  fflush(stderr);
#endif
  return 0;
}

/*****************************************************************************
 *                             set_size_date                                 *
 *****************************************************************************/
private void set_size_date(current)
Fileinfo *current;
{
  strcpy(current->filedate,ctime(&current->filestat.st_mtime));
  sprintf(current->filesize, "%9ld",current->filestat.st_size);

  XtVaSetValues(
      current->form1b_label_date,
          XtNlabel, current->filedate,
	  NULL ) ;

  XtVaSetValues(
      current->form1b_label_size,
	  XtNlabel, current->filesize,
	  NULL ) ;
}

/*****************************************************************************
 *                             set_icon                                      *
 *****************************************************************************/
private void set_icon(current)
Fileinfo *current;
{
  Icon_mode save_mode;

  save_mode.mode = current_mode.mode;

  current_mode.mode = Icons;

  /* getIconType will overwrite xdtmlist_element->string, free it before */
  if (current->xdtmlist_element->string)
    XtFree(current->xdtmlist_element->string);
  
  if (!getIconType(current->dc_filename, current->dc_path,
		   current->xdtmlist_element)) {
    fprintf(stderr, "doubleclick:File icon unreadable!\n");
    current->xdtmlist_element->icon = emptytick;
  }

  XtVaSetValues(
      current->form1a_label_icon,
          XtNbitmap, current->xdtmlist_element->icon,
	  NULL ) ;

  current_mode.mode = save_mode.mode;
}

/*****************************************************************************
 *                             load_text                                     *
 *****************************************************************************/
private void load_text(w, string)
Widget w;
char *string;
{
  XtVaSetValues(
      w,
          XtNstring, string,
	  NULL ) ;

  XawTextSetInsertionPoint(w, strlen(string));
}
