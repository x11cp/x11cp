/*****************************************************************************
 ** File          : buttons.c                                               **
 ** Purpose       : Initialise and Realise button dialogs                   **
 ** Author        : Edward Groenendaal                                      **
 ** Date          : April 1991                                              **
 ** Documentation : Xdtm Design Folder                                      **
 ** Related Files :                                                         **
 ** Changes       : 28-11-91, Edward Groenendaal                            **
 **                 Merged 1.8 and 2.0b to make this file                   **
 **                 18-04-92, Edward Groenendaal                            **
 **                 Added the #if NeedFunctionPrototypes stuff              **
 **                 June 20, 1992, Ramon Santiago                           **
 **                 Changed all XtCreate calls to XtVaCreate calls.         **
 **                 Changed all caddr_t to XtPointer.                       **
 **                 Removed some lint.                                      **
 *****************************************************************************/

#include "xdtm.h"
#include "menus.h"

#include <stdlib.h>

#include <X11/Shell.h>
#include <X11/Xaw/Label.h>
#include <X11/Xaw/Command.h>
#include <X11/Xaw/Viewport.h>
#include "Xedw/XedwForm.h"
#include "Xedw/XedwList.h"

/* external and forward functions definitions */
#if NeedFunctionPrototypes
private int buttoniconcmp(const void *, const void *);
extern void copyQueryResult(Widget, Boolean, XtPointer);
extern String getfilename(String);
extern void moveQueryResult(Widget, Boolean, XtPointer);
extern void realize_dialog(Widget, Widget, XtGrabKind, XtEventHandler, XtPointer);
extern void setscroll(Widget, double);
extern void trashQueryResult(Widget, Boolean, XtPointer);
#else
private int buttoniconcmp();
extern void copyQueryResult();
extern String getfilename();
extern void moveQueryResult();
extern void realize_dialog();
extern void setscroll();
extern void trashQueryResult();
#endif

/* Widgets */

private Widget buttonpopup;   /* For confirmation of move/copy/trash buttons */
private Widget buttonform;
private Widget buttonlabel1;
private Widget buttonlabel2;
private Widget buttonyes;
private Widget buttonno;
private Widget fileview;
private Widget filelist;

/*****************************************************************************
 *                             init_button                                   *
 *****************************************************************************/
public void init_button(top)
Widget top;
{
  /* initialise the widget which display's a list of filenames with a message
   * and a couple of buttons.
   */

  Dimension height, space;
  XFontStruct *font;

  buttonpopup =
    XtVaCreatePopupShell(
        "Trash/Move/Copy",
	transientShellWidgetClass,
	top,
	    NULL ) ;

  buttonform =
    XtVaCreateManagedWidget(
        "buttonform",
	xedwFormWidgetClass,
	buttonpopup,
	    NULL ) ;

  buttonlabel1 =
    XtVaCreateManagedWidget(
        "buttonlabel1",
	labelWidgetClass,
	buttonform,
	    XtNjustify, XtJustifyCenter,
	    XtNfullWidth,          True,
	    XtNborderWidth,           0,
	    NULL ) ;

  buttonlabel2 =
    XtVaCreateManagedWidget(
        "buttonlabel2",
	labelWidgetClass,
	buttonform,
	    XtNjustify, XtJustifyCenter,
	    XtNfullWidth,          True,
	    XtNborderWidth,           0,
	    XtNfromVert,   buttonlabel1,
	    NULL ) ;

  fileview =
    XtVaCreateManagedWidget(
        "fileview",
	viewportWidgetClass,
	buttonform,
            XtNfromVert, buttonlabel2,
	    XtNfullWidth,        True,
	    XtNforceBars,        True,
	    XtNallowVert,        True,
	    NULL ) ;

  filelist =
    XtVaCreateManagedWidget(
        "filelist",
	xedwListWidgetClass,
	fileview,
	    XtNdefaultColumns,  1,
	    XtNforceColumns, True,
	    XtNrowSpacing,      4,
	    XtNtranslations, XtParseTranslationTable(""),
	    NULL ) ;

  /* Get font height from filelist, then set fileview to be 5 times that
   * size.
   */

  XtVaGetValues(
      filelist,
          XtNfont, &font,
	  XtNrowSpacing, &space,
	  NULL ) ;

  height = (font->max_bounds.ascent +
           font->max_bounds.descent +
	   space) * 5;

  XtVaSetValues(
      fileview,
          XtNheight, height,
	  NULL ) ;

  buttonyes =
    XtVaCreateManagedWidget(
        "buttonyes",
	commandWidgetClass,
	buttonform,
	    XtNfromVert,       fileview,
	    XtNjustify, XtJustifyCenter,
	    NULL ) ;

  buttonno =
    XtVaCreateManagedWidget(
        "buttonyes",
	commandWidgetClass,
	buttonform,
	    XtNfromVert,       fileview,
	    XtNfromHoriz,     buttonyes,
	    XtNjustify, XtJustifyCenter,
	    NULL ) ;

}

/*****************************************************************************
 *                              WM_destroy_button_dialog                     *
 ****************************************************************************/
#if NeedFunctionPrototypes
private void WM_destroy_button_dialog(Widget w,
				      XtPointer client_data,
				      XEvent *event,
				      Boolean *dispatch)
#else
private void WM_destroy_button_dialog(w, client_data, event, dispatch)
     Widget w;
     XtPointer client_data;
     XEvent *event;
     Boolean *dispatch;
#endif
{
    extern Atom protocols[2]; /* defined in dialogs.c */
    
    if (event->xclient.message_type == protocols[1] &&
	event->xclient.data.l[0] == protocols[0])
    /* the widget got a kill signal */
    {
	/* call callbacks on Cancel button */
	XtCallCallbacks(buttonno, XtNcallback, NULL);
    }
}

/*****************************************************************************
 *                                button_dialog                              *
 *****************************************************************************/
public void button_dialog(type, list)
Cardinal type;
XedwListReturnStruct *list;
{
  /* Set the resources of the dialog to the type of dialog required, 
   * the type is either Trash, Copy or Move then popup the dialog.
   *
   * - Takes the type of dialog, and the list of files to display.
   */

  XedwListReturnStruct *tmp;
  XedwList **buttonlist;
  Cardinal i, n;
  private String CancelButtonLabel = "Cancel";

  /* Count items in linked list */
  n = 0;
  tmp = list;
  while (tmp != NULL) {
    tmp = tmp->next;
    n++;
  }

  /* Allocate an array of XedwList* of that size */
  buttonlist = (XedwList**) XtMalloc (sizeof(XedwList*) * (n+1));

  /* Put Strings from linked list into array, using NULL icons */
  for (i = 0; i < n; i++) {
    buttonlist[i] = XtNew(XedwList);
    buttonlist[i]->string = list->string;
    list = list->next;
  }
  buttonlist[i] = NULL;

  /* Sort the list */
  qsort((char*)buttonlist, n, sizeof(buttonlist[0]), buttoniconcmp);

  /* Reset view to top */

  setscroll(fileview, 0.0);

  switch (type) {
  case Trash:
    {
      private String TrashTitle        = "Delete File(s)";
      private String TrashLabel1       = "Delete these files";
      private String TrashLabel2       = "from current directory?";
      private String TrashButtonLabel  = "Delete";

      /* dialog title */

      XtVaSetValues(
	  buttonpopup,
	      XtNtitle, TrashTitle,
	      NULL ) ;

      /* label */

      XtVaSetValues(
          buttonlabel1,
	      XtNlabel, TrashLabel1,
	      NULL ) ;
    
      /* label2 */

      XtVaSetValues(
          buttonlabel2,
	      XtNlabel, TrashLabel2,
	      NULL ) ;
      
      /* file list */

      XtVaSetValues(
          filelist,
	      XtNlongest,           0,
	      XtNnumberStrings,     0,
	      XtNxedwList, buttonlist,
	      NULL ) ;

      /* button1 */
      XtVaSetValues(
          buttonyes,
	      XtNlabel, TrashButtonLabel,
	      NULL ) ;
      /* button2 */
      XtVaSetValues(
          buttonno,
	      XtNlabel, CancelButtonLabel,
	      NULL ) ;

      XtAddCallback(buttonno, XtNcallback, (XtCallbackProc)trashQueryResult,
		    (XtPointer)False);
      XtAddCallback(buttonyes, XtNcallback, (XtCallbackProc)trashQueryResult,
		    (XtPointer)True);
      realize_dialog(buttonpopup, NULL, XtGrabNonexclusive,
		     (XtEventHandler)WM_destroy_button_dialog, NULL);
      break;
    }
  case Copy:
    {
      private String CopyTitle        = "Copy File(s)";
      private String CopyLabel1       = "Copy these files";
      private String CopyLabel2       = "to current directory?";
      private String CopyButtonLabel  = "Copy";

      /* dialog title */

      XtVaSetValues(
	  buttonpopup,
	      XtNtitle, CopyTitle,
	      NULL ) ;

      /* label */

      XtVaSetValues(
          buttonlabel1,
	      XtNlabel, CopyLabel1,
	      NULL ) ;
    
      /* label2 */

      XtVaSetValues( buttonlabel2, XtNlabel, CopyLabel2, NULL ) ;
      
      /* file list */

      XtVaSetValues(
          filelist,
	      XtNlongest,           0,
	      XtNnumberStrings,     0,
	      XtNxedwList, buttonlist,
	      NULL ) ;

      /* button1 */
      XtVaSetValues(
          buttonyes,
	  XtNlabel, CopyButtonLabel,
	  NULL ) ;
      
      /* button2 */
      XtVaSetValues(
	  buttonno,
	  XtNlabel, CancelButtonLabel,
	  NULL ) ;

      XtAddCallback(buttonno, XtNcallback,  (XtCallbackProc)copyQueryResult,
		    (XtPointer)False);
      XtAddCallback(buttonyes, XtNcallback, (XtCallbackProc)copyQueryResult,
		    (XtPointer)True);
      realize_dialog(buttonpopup, NULL, XtGrabNonexclusive,
		     (XtEventHandler)WM_destroy_button_dialog, NULL);
      break;
    }
  case Move:
    {
      private String MoveTitle        = "Move File(s)";
      private String MoveLabel1       = "Move these files";
      private String MoveLabel2       = "to current directory?";
      private String MoveButtonLabel  = "Move";

      /* dialog title */

      XtVaSetValues(
	  buttonpopup,
	      XtNtitle, MoveTitle,
	      NULL ) ;

      /* label */

      XtVaSetValues(
          buttonlabel1,
	      XtNlabel, MoveLabel1,
	      NULL ) ;

      /* label2 */

      XtVaSetValues(
          buttonlabel2,
	      XtNlabel, MoveLabel2,
	      NULL ) ;
      
      /* file list */

      XtVaSetValues(
          filelist,
	      XtNlongest,           0,
	      XtNnumberStrings,     0,
	      XtNxedwList, buttonlist,
	      NULL ) ;

      /* button1 */
      XtVaSetValues(
          buttonyes,
	      XtNlabel, MoveButtonLabel,
	      NULL ) ;

      /* button2 */
      XtVaSetValues(
          buttonno,
	      XtNlabel, CancelButtonLabel,
	      NULL ) ;

      XtAddCallback(buttonno, XtNcallback,  (XtCallbackProc)moveQueryResult,
		    (XtPointer)False);
      XtAddCallback(buttonyes, XtNcallback, (XtCallbackProc)moveQueryResult,
		    (XtPointer)True);
      realize_dialog(buttonpopup, NULL, XtGrabNonexclusive,
		     (XtEventHandler)WM_destroy_button_dialog, NULL);
      break;
    }

  default:
    fprintf(stderr, "Unrecognised button dialog request\n");
    break;
  }

}

/*****************************************************************************
 *                             destroy_button_dialog                         *
 *****************************************************************************/
public void destroy_button_dialog()
{
  /* Popdown and destroy the callback lists of the dialog */
  XedwList **list;
  int i = 0;
    
  XtPopdown(buttonpopup);

  /* Free the allocated filelist */
  XtVaGetValues(filelist, XtNxedwList, &list, NULL);
  if (list) {
      while (list[i]) XtFree((char *)list[i++]);
      XtFree((char *)list);
  }
  
  XtRemoveAllCallbacks(buttonyes, XtNcallback);
  XtRemoveAllCallbacks(buttonno, XtNcallback);

}

/*****************************************************************************
 *                               buttoniconcmp                               *
 *****************************************************************************/
#if NeedFunctionPrototypes
private int buttoniconcmp(const void *ip1, const void *ip2)
#else
private int buttoniconcmp(ip1, ip2)
void *ip1;
void *ip2;
#endif
{
  /* compare the strings of 2 XedwList's */

  return (strcmp((*(XedwList **)ip1)->string, 
		 (*(XedwList **)ip2)->string));
}
