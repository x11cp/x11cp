/*

      coordinate data structures

*/


#ifndef PI
#define PI 3.14159265359
#endif


/* radius of earth in km */
#define EARTH_RADIUS 6378.14486779

/* length of one degree on a meridian (ca. 60 NM) */
#define ONE_DEGREE 111.321137


/* LOCATION is used to specify geographic locations by degrees.
   Resolution is 1/100th of a second.
   Positive values mean North/East, negative values South/West.
   So: 1 second is     100
       1 minute is   6,000
       1 degree is 360,000
                  -------- x 360
	       129,600,600 is earth's circumference, so we can
               easily use 32 bit long vars to represent these values.   */

typedef struct {
  long latitude;
  long longitude;
} LOCATION;


/* supported projections */

#define PROJECT_LAMBERT      1
#define PROJECT_MERCATOR     2

extern char map_projection;


/* calculate the distance between two locations,
   output in NM */

double distance (LOCATION a, LOCATION b);



/* calculate the mean true track of the shortest connection between
   two locations. output in degrees, 0=North, 90=East etc. */

double truetrack (LOCATION a, LOCATION b);




/* convert either a longitude of a latitude into a value that
   is suitable for the LOCATION struct.
   input: 99.99'99"X    (character string)
          where 9 is any digit and X is one of N, S, E, W   */

long deg2num (char *string);



/* convert degrees into radians */

double rad(double deg);



/* convert radians into degrees */

double deg(double rad);



/* convert internal repr. into radians */

double internal2rad(long internal);


/* convert internal to human readable string */

char *internal2string (LOCATION pos);


/* convert radians into internal rep. */

long rad2internal (double rad);



/* convert map position (in cm) to human readable coordinate pair */

char *cm2string (double mapx, double mapy);



/* convert position x,y (in cm) to internal format */

LOCATION cm2internal (double mapx, double mapy);


/* convert internal format to map position in cm */

void internal2cm (LOCATION position, double *mapx, double *mapy);



/*
  drawing to the map, it may be necessary to find out what offset has to added
  to a true track at a given position to properly draw that angle on the map
  (shifts by zeromeridian etc.):
*/

int truenorthoffset (LOCATION pos);











